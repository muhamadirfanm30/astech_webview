<?php

use App\Http\Controllers\B2BSchedulesController;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use Illuminate\Support\Facades\Response;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ListOutletController;
use App\Http\Controllers\CreateOrderController;
use App\Http\Controllers\PeviewDocumentController;
use App\Http\Controllers\Api\OrderUserB2bController;
use App\Http\Controllers\Api\UserB2bLoginController;
use App\Model\Master\TechnicianSchedule;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('login')->name('login');
// });

Route::get('storage/file-order/{filename}', function ($filename) {
    $path      = storage_path('app/public/posts_image/' . $filename);
    if (!File::exists($path)) {
        abort(404);
    }
    $file     = File::get($path);
    $type     = File::mimeType($path);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
});

Route::get('storage/img-user/{filename}', function ($filename) {
    $path      = storage_path('app/public/user_img/' . $filename);
    if (!File::exists($path)) {
        abort(404);
    }
    $file     = File::get($path);
    $type     = File::mimeType($path);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
});

Route::get('/', [DashboardController::class, 'loginForm'])->name('login');
Route::post('/login', [UserB2bLoginController::class, 'login'])->name('action.login');

Route::post('/login-user-b2b', [AuthController::class, 'loginB2b'])->name('action.login.b2b');
Route::post('/logout-user-b2b', [AuthController::class, 'logoutB2b'])->name('logout');

Route::middleware('auth')->group(function () {

    Route::prefix('b2b')->group(function () {
        Route::get('dashboard', [DashboardController::class, 'index'])->name('home');
        Route::get('success-response', [DashboardController::class, 'successView']);
        Route::get('success-checkinout', [DashboardController::class, 'successCheckinout']);
        Route::get('cart-view', [DashboardController::class, 'cartView']);
        Route::get('error-response', [DashboardController::class, 'errorView']);

        Route::group(['prefix' => 'request-order'], function () {
            Route::get('show', [CreateOrderController::class,  'index'])->name('list.show');
            Route::get('cart', [CreateOrderController::class,  'cart'])->name('list.cart');
            Route::get('create', [CreateOrderController::class,  'create'])->name('order.create');
            Route::get('order-detail/{id}', [CreateOrderController::class,  'detail'])->name('order.detail');
            Route::get('select2-outlet', [CreateOrderController::class,  'select2Outlet'])->name('list.outlet');
            Route::get('select2-unit/{id}', [CreateOrderController::class,  'select2Unit'])->name('list.unit');
            Route::post('add-to-cart', [CreateOrderController::class,  'addToCart'])->name('add.cart');
            Route::post('checkout', [CreateOrderController::class,  'checkout'])->name('checkout');
            Route::post('checkout-data-cart', [CreateOrderController::class,  'checkoutDataCart'])->name('checkout.data.cart');
            Route::Delete('delete-data-cart/{id}', [CreateOrderController::class,  'deleteDataCart'])->name('delete.data.cart');
            Route::post('delete-all-data-cart', [CreateOrderController::class,  'deleteAllData'])->name('delete.all.data.cart');
        });

        Route::group(['prefix' => 'profile'], function () {
            Route::get('show', [ProfileController::class,  'index'])->name('profile.show');
            Route::get('update/{id}', [ProfileController::class,  'update'])->name('profile.update');
            Route::post('edit/{id}', [ProfileController::class,  'edit'])->name('profile.edit');
        });

        Route::group(['prefix' => 'preview-document'], function () {
            Route::get('quotation/{id}', [PeviewDocumentController::class,  'quot'])->name('quot.show');
            Route::get('invoice/{id}', [PeviewDocumentController::class,  'invoice'])->name('invoice.show');
        });

        Route::group(['prefix' => 'list-outlet'], function () {
            Route::get('show', [ListOutletController::class,  'index'])->name('quot.show');
            Route::get('detail/{id}', [ListOutletController::class,  'detail'])->name('quot.detail');
        });
        Route::group(['prefix' => 'schedules'], function () {
            Route::get('index', [B2BSchedulesController::class,  'index'])->name('teknisi-schedule.listAll');
            Route::get('checkin', [B2BSchedulesController::class,  'checkin'])->name('teknisi-schedule.checkin');
            Route::get('checkout', [B2BSchedulesController::class,  'checkout'])->name('teknisi-schedule.checkout');
            Route::get('checkin/{id}', [B2BSchedulesController::class,  'checkinTeknisi'])->name('teknisi-schedule.checkin.id');
            Route::get('checkout/{id}', [B2BSchedulesController::class,  'checkoutTeknisi'])->name('teknisi-schedule.checkout.id');
            Route::post('checkin', [B2BSchedulesController::class,  'checkinTeknisiPost'])->name('teknisi-schedule.checkin.post');
            Route::post('checkout', [B2BSchedulesController::class,  'checkoutTeknisiPost'])->name('teknisi-schedule.checkout.post');
        });

    });

});
