<!doctype html>
<html lang="en">


@include('template_login.header')

<body class="bg-white">
    <!-- loader -->
    <!-- <div id="loader">
        <div class="spinner-border text-primary" role="status"></div>
    </div> -->
    <!-- * loader -->
    <!-- App Capsule -->
    <form action="{{ route('action.login.b2b') }}" method="POST" >
        @csrf
        <div id="appCapsule" class="pt-0">
            <div class="login-form mt-5">
                <div class="section">
                    <img src="{{ asset('/assets/img/sample/photo/logo-c69f1-3314_108.png') }}" alt="image" class="form-image">
                </div><br>
                <div class="section mt-1">
                    <h4>Fill the form to log in</h4>
                </div>
                    
                
                <div class="section mt-1 mb-5">
                    <div class="form-group boxed">
                        <div class="input-wrapper">
                            <input type="text" class="form-control" name="email" id="email" placeholder="Email address">
                            @error('email')
                                <div div  id="error-email" style="color:red;text-align: left;text-size-adjust: 1000px;font-size: 11px;">{{ $message }}</div>
                            @enderror
                            <!-- <div  id="error-email" style="color:red;text-align: left;text-size-adjust: 1000px;font-size: 11px;"></div> -->
                        </div>
                        
                    </div>
                    
                    <div class="form-group boxed">
                        <div class="input-wrapper">
                            <input type="password" class="form-control" name="password" id="password" placeholder="Password">
                            @error('password')
                                <div div  id="error-password" style="color:red;text-align: left;text-size-adjust: 1000px;font-size: 11px;">{{ $message }}</div>
                            @enderror
                            <!-- <div  id="error-password" style="color:red;text-align: left;text-size-adjust: 1000px;font-size: 11px;"></div> -->
                        </div>
                    </div>
                    <div class="form-group boxed">
                        <!-- <input type="submit" value="Login"> -->
                        <button type="submit" class="btn btn-primary btn-block btn-lg" onclick="Helper.loadingStart();">Log in</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <div class="modal fade dialogbox" id="DialogIconedDanger" data-backdrop="static" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-icon text-danger">
                    <ion-icon name="close-circle"></ion-icon>
                </div>
                <div class="modal-header">
                    <h5 class="modal-title">Error</h5>
                </div>
                <div class="modal-body">
                    Username Password Not Match
                </div>
                <div class="modal-footer">
                    <div class="btn-inline">
                        <a href="#" class="btn" data-dismiss="modal">CLOSE</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- DialogIconedSuccess -->
    <div class="modal fade dialogbox" id="DialogIconedSuccess" data-backdrop="static" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-icon text-success">
                    <ion-icon name="checkmark-circle"></ion-icon>
                </div>
                <div class="modal-header">
                    <h5 class="modal-title">Success</h5>
                </div>
                <div class="modal-body">
                    Welcome
                </div>
                <div class="modal-footer">
                    <div class="btn-inline">
                        <a href="#" class="btn" data-dismiss="modal">CLOSE</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

@include('b2b.template_ui._script')
@section('script')
@if(session()->has('error'))
    <!-- <div class="alert alert-success"> -->
        <!-- {{ session()->get('error') }} -->
    <!-- </div> -->
    <script>
        // Helper.loadingStop();
        $(document).ready(function(){
            $('#DialogIconedDanger').modal('show');
        });

        function validasiEmail() {
            var rs = document.forms["formInput"]["email"].value;
            var atps=rs.indexOf("@");
            var dots=rs.lastIndexOf(".");
            if (atps<1 || dots<atps+2 || dots+2>=rs.length) {
                alert("Alamat email tidak valid.");
                return false;
            } else {
                alert("Alamat email valid.");
            }
        }
    </script>
@endif
    <!-- <script>
        $('#btn_login').submit(function(e){
            Helper.loadingStart();
            var bodyFormData = new FormData();

            $('#email').each(function(){ 
                bodyFormData.append('email', $(this).val()); 
            });
             
            $('#password').each(function(){ 
                bodyFormData.append('password', $(this).val() ); 
            });

            Axios.post('/login', bodyFormData, {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                })
                .then(function(response) {
                    document.cookie = "laravel_token=" + response.data.data.token;
                    console.log(response.data.data.token)
                    location.href = Helper.redirectUrl('/b2b/dashboard');
                    // Helper.loadingStop();
                    
                })
                .catch(function(error) {
                    console.log(error.response.status)
                    if (error.response.status == 422) {
                        Helper.loadingStop();
                        messages = error.response.data.data;
                        // clean msg
                        $('.error-validation-mgs').remove();
                        _.each(messages, function(pesan, field) {
                            $('#error-'+ field).text(pesan[0])
                        })

                    }else{
                        Helper.loadingStop();
                        $('#DialogIconedDanger').modal('show');
                    }
                    
                });

            e.preventDefault();
        })
    </script> -->
@section('script')
</body>

</html>