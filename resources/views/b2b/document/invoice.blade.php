@extends('b2b.master')
@section('content')
    <!-- App Header -->
    <div class="appHeader">
        <div class="left">
            <a href="{{ url('/b2b/request-order/order-detail/'. $showInv->id_transaction) }}" class="headerButton" onclick="Helper.loadingStart()">
                <ion-icon name="chevron-back-outline"></ion-icon>
            </a>
        </div>
        <div class="pageTitle">Invoice</div>
        <div class="right">
            <!-- <a href="#" class="headerButton">
                <ion-icon name="print-outline"></ion-icon>
            </a>
            <a href="#" class="headerButton">
                <ion-icon name="download-outline"></ion-icon>
            </a> -->
        </div>
    </div>
    <div class="section full mt-3">
        <div class="invoice">
            <div class="invoiceBackgroundLogo">
                
                <img src="{{ asset('/assets/img/sample/photo/logo-c69f1-3314_108.png') }}" alt="background-logo">
            </div>
            <div class="invoice-page-header">
                <div class="invoice-logo">
                    <img src="{{ asset('/assets/img/sample/photo/logo-c69f1-3314_108.png') }}" alt="logo">
                </div>
                <div class="invoice-id">{{str_replace('QUOT', 'INV', $showInv->quotation_code) }}</div>
                <!-- <div class="invoice-id"><small>{{ date('M-d-Y', strtotime($showInv->created_at)) }}</small></div> -->
            </div>
            <div class="invoice-person mt-4">
                <div class="invoice-to">
                    <h4>ASTech</h4>
                    <p>ASTech@email.com</p>
                    <p>Daan Mogot Rd No.Km. 11, RT.5/RW.4,<br>Kedaung Kali Angke, Cengkareng, <br> West Jakarta City, Jakarta 11710</p>
                </div>
                <div class="invoice-from">
                    <h4>{{$showInv->company}}</h4>
                    <p>{{$showInv->phone}}</p>
                    <p>{{$showInv->address}}</p>
                </div>
            </div>
            <div class="invoice-detail mt-4">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <td>Nama Produk</td>
                                <td>Qty</td>
                                <td>Price</td>
                                <td>SUBTOTAL</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($showInv->get_part as $item)
                                <tr>
                                    {{-- <td>{{ $item->qty }}</td> --}}
                                    <td>{{ $item->part_description }}</td>
                                    <td>Rp. {{ number_format($item->selling_price) }}</td>
                                    <td>{{ $item->qty }}</td>
                                    <?php $subtotal = $item->qty * $item->selling_price ?>
                                    <td>Rp. {{ number_format($subtotal) }}</td>
                                </tr>
                                @php
                                    if(!empty($showInv->nama_jasa) && !empty($showInv->harga_jasa)){
                                        $harga_jasa = $showInv->harga_jasa;
                                    }else{
                                        $harga_jasa = 0;
                                    }

                                    if(!empty($showInv->get_labor)){
                                        $service = $showInv->get_labor->service_price;
                                    }else{
                                        $service = 0;
                                    }
                                    $arr = 0;
                                    foreach ($showInv->get_part as $value) {
                                        $arr += $value->selling_price * $value->qty;
                                    }
                                    
                                    $totals = $arr + $service + $harga_jasa;
                                @endphp
                            @endforeach
                            @if (!empty($showInv->get_labor))
                                <tr>
                                    <td>{{!empty($showInv->get_labor) ? $showInv->get_labor->name : '-'}}</td>
                                    <td>Rp.{{!empty($showInv->get_labor) ? number_format($showInv->get_labor->service_price) : '0'}}</td>
                                    <td>-</td>
                                    <td>Rp.{{!empty($showInv->get_labor) ? number_format($showInv->get_labor->service_price) : '0'}}</td>
                                </tr>
                            @endif
                            @if (!empty($showInv->nama_jasa) && !empty($showInv->harga_jasa))
                                <tr>
                                    <td>{{!empty($showInv->nama_jasa) ? $showInv->nama_jasa : '-'}}</td>
                                    <td>Rp.{{!empty($showInv->harga_jasa) ? number_format($showInv->harga_jasa) : '0'}}</td>
                                    <td>-</td>
                                    <td>Rp.{{!empty($showInv->harga_jasa) ? number_format($showInv->harga_jasa) : '0'}}</td>
                                </tr>
                            @endif
                        </tbody>
                        <tbody>
                            <tr>
                                <td colspan="2" class="text-right">Sub Total :</td>
                                <td colspan="2" class="text-right">Rp. {{number_format($totals)}}</td>
                            </tr>
                            <tr>
                                <td colspan="2" class="text-right">TAX :</td>
                                @php
                                if($totals >= 5000000){
                                        $materai = 10000;
                                    }else{
                                        $materai = 0;
                                    }
                                    $percentage = 10;
                                    $totalHarga = $totals;

                                    $new_tax = ($percentage / 100) * $totalHarga;
                                    
                                @endphp
                                <td colspan="2" class="text-right">Rp. 10% (Rp.{{number_format($new_tax)}})</td>
                            </tr>
                            @if ($totals >= 5000000)
                                <tr>
                                    <td colspan="2" class="text-right">Materai :</td>
                                    <td colspan="2" class="text-right">Rp. 10.000</td>
                                </tr>
                            @endif
                            <tr>
                                @php
                                    $tax = $new_tax;
                                    $hargasemua = $totals;
                                    $hasilAkhir = $tax + $hargasemua + $materai;
                                @endphp
                                <td colspan="2" class="text-right">Total :</td>
                                <td colspan="2" class="text-right"> Rp. {{ number_format($hasilAkhir) }} </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <!-- <div class="invoice-signature mt-4">
                <div class="signature-block">
                    Signature Here
                </div>
            </div>

            <div class="invoice-total mt-4">
                <ul class="listview transparent simple-listview">
                    <li>Subtotal <span class="hightext">$800.00</span></li>
                    <li>Tax (10%)<span class="hightext">$80.00</span></li>
                    <li>Total<span class="totaltext text-primary">$880.00</span></li>
                </ul>
            </div> -->

            <div class="invoice-bottom">
                This invoice is for preview purposes only.
            </div>
        </div>
    </div>
@endsection
@section('script')

@endsection