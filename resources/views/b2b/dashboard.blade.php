@extends('b2b.master')
@section('content')
<div class="container" style="margin-bottom: 15px;">
    <div class="appHeader">
        <div class="left">
            <img src="{{ asset('/assets/img/icon/login.png') }}" alt="image" class="form-image" width="25px">
            <a href="{{ url('/b2b/profile/show') }}">&nbsp; HI, {{ Auth::user()->name }}</a>
        </div>
        <div class="right">
            <form action="{{ route('logout') }}" method="post">
                @csrf
                <button style="border:none; background-color:white" class="headerButton"><ion-icon name="exit-outline"></ion-icon></button>
            </form>
            <!-- <bu href="#" class="headerButton">
                <ion-icon name="exit-outline"></ion-icon>
            </a> -->
        </div>
    </div>
</div>
    <div class="section full mt-2">
        <div class="">
            <div class="container">
                @if($showListDataOrder > 0)
                    <div class="alert alert-danger mb-1" role="alert" style="background-color: #ff6b6b; border: #ff6b6b">
                        <img src="{{ asset('warning.png') }}" alt="" width="20px">&nbsp;
                        You have <strong>{{ $showListDataOrder }}</strong> Waiting Payment
                    </div>
                @else
                    <div class="alert alert-success mb-1" role="alert">
                        <img src="{{ asset('tick.png') }}" alt="" width="20px">&nbsp;
                        You have 0 Waiting Payment
                    </div>
                @endif
                <div class="row">
                    <div class="col-4" style="margin-bottom: 20px;">
                        <div class="card">
                            <a href="{{ url('b2b/request-order/cart') }}" class="btn btn-lg btn-block" style="height: 85px; box-shadow: 5px 10px #888888; " onclick="Helper.loadingStart()">
                                <div style="margin-top:12px">
                                    <img src="{{ asset('drafts.png') }}" alt="" width="50px">
                                    <!-- <ion-icon class="iconedbox text-center" name="calendar-outline" style="margin-left: 12px; color:#f6e58d"></ion-icon> -->
                                    <bold><h5 style="color:black; font-style:bold">Draft</h5></bold>
                                </div>
                            </a>
                        </div>
                    </div><br>
                    <div class="col-4" style="margin-bottom: 20px;">
                        <div class="card">
                            <a href="{{ url('b2b/request-order/show') }}" class="btn btn-lg btn-block" onclick="Helper.loadingStart()" style="height: 85px; box-shadow: 5px 10px #888888;" onclick="Helper.loadingStart()">
                                <div style="margin-top:12px">
                                    <img src="{{ asset('cargo.png') }}" alt="" width="50px">
                                    <!-- <ion-icon class="iconedbox text-center" name="list-outline" style="color: #686de0;"></ion-icon> -->
                                    <bold><h5 style="color:black; font-style:bold">Order</h5></bold>
                                </div>
                            </a>
                        </div>
                    </div><br>
                    <div class="col-4" style="margin-bottom: 20px;">
                        <div class="card">
                            <a href="{{ url('b2b/list-outlet/show') }}" class="btn btn-lg btn-block" style="height: 85px; box-shadow: 5px 10px #888888;" onclick="Helper.loadingStart()">
                                <div style="margin-top:12px">
                                    <img src="{{ asset('store.png') }}" alt="" width="50px">
                                    <!-- <ion-icon class="iconedbox text-center" name="cash-outline" style="margin-left: 12px; color: #eb4d4b;"></ion-icon> -->
                                    <bold><h5 style="color:black; font-style:bold">Outlet</h5></bold>
                                </div>
                            </a>
                        </div>
                    </div><br>
                    <div class="col-4" style="margin-bottom: 20px;">
                        <div class="card">
                            <a href="{{ url('b2b/profile/show') }}" class="btn btn-lg btn-block" style="height: 85px; box-shadow: 5px 10px #888888;" onclick="Helper.loadingStart()">
                                <div style="margin-top:12px">
                                    <!-- <ion-icon class="iconedbox" name="calendar-outline" style="margin-left: 11px; color: #6ab04c"></ion-icon> -->
                                    <img src="{{ asset('man.png') }}" alt="" width="50px">
                                    <bold><h5 style="color:black; font-style:bold">Profile</h5></bold>
                                </div>

                            </a>
                        </div>
                    </div><br>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="fab-button text bottom-center dropdown">
    <a href="{{ url('b2b/request-order/create') }}"  class="fab create-order"  style="font-size: 13px;">
        <ion-icon name="add-outline"></ion-icon>Add Request
    </a>
</div>
@endsection
@section('script')
    <script>
        $(document).ready(function(){
            Helper.loadingStop();
        })
    </script>
@endsection
