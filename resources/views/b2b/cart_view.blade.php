@extends('b2b.master')
@section('content')
    <!-- App Header -->
    <div class="appHeader no-border transparent position-absolute">
        <div class="left">
            <a href="{{ url('b2b/request-order/create') }}" class="headerButton">
                <ion-icon name="chevron-back-outline"></ion-icon>
            </a>
        </div>
        <div class="pageTitle">Success</div>
        <div class="right"></div>
    </div>
    <!-- * App Header -->

    <!-- App Capsule -->
    <div id="appCapsule">

        <div class="error-page">
            <div class="icon-box text-success">
                <img src="{{ asset('notepad.png') }}" alt="" width="90px">
                <!-- <ion-icon name="cart-outline"></ion-icon> -->
            </div>
            <h1 class="title">Success</h1>
            <div class="text mb-5">
                Data Has Been Saved To the draft. <a href="{{ url('b2b/request-order/cart') }}">View Draft</a>
            </div>

            <div class="fixed-footer">
                <div class="row">
                    <div class="col-12">
                        <a href="{{ url('b2b/request-order/create') }}" class="btn btn-secondary btn-lg btn-block">Request Order</a>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- * App Capsule -->
@endsection