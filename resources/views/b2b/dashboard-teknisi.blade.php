@extends('b2b.master')
@section('content')
<div class="container" style="margin-bottom: 15px;">
    <div class="appHeader">
        <div class="left">
            <img src="{{ asset('/assets/img/icon/login.png') }}" alt="image" class="form-image" width="25px">
            <a href="{{ url('/b2b/profile/show') }}">&nbsp; HI, {{ Auth::user()->name }}</a>
        </div>
        <div class="right">
            <form action="{{ route('logout') }}" method="post">
                @csrf
                <button style="border:none; background-color:white" class="headerButton"><ion-icon name="exit-outline"></ion-icon></button>
            </form>
            <!-- <bu href="#" class="headerButton">
                <ion-icon name="exit-outline"></ion-icon>
            </a> -->
        </div>
    </div>
</div>
    <div class="section full mt-2">
        <div class="container">
            <div class="d-flex justify-content-center" style="margin-top:10px;">
                <div class="row">
                    <div class="col-4" style="margin-bottom: 20px;">
                        <div class="text-right" style="height:25px;">
                        </div>
                        <div class="card">
                            <a href="{{ url('b2b/schedules/index') }}" class="btn btn-lg btn-block" style="height: 85px; box-shadow: 5px 10px #888888;" onclick="Helper.loadingStart()">
                                <div style="margin-top:12px">
                                    <!-- <ion-icon class="iconedbox" name="calendar-outline" style="margin-left: 11px; color: #6ab04c"></ion-icon> -->
                                    <img src="{{ asset('timetable.png') }}" alt="" width="50px">
                                    <bold><h5 style="color:black; font-style:bold">All Schedules</h5></bold>
                                </div>

                            </a>
                        </div>
                    </div><br>
                    <div class="col-4" style="margin-bottom: 20px;">
                        <div class="text-right" style="height:25px;">
                            <span class="badge badge-danger">{{ $countCheckin != 0 ? $countCheckin:'' }}</span>
                        </div>
                        <div class="card">
                            <a href="{{ url('b2b/schedules/checkin') }}" class="btn btn-lg btn-block" style="height: 85px; box-shadow: 5px 10px #888888;" onclick="Helper.loadingStart()">
                                <div style="margin-top:12px">
                                    <!-- <ion-icon class="iconedbox" name="calendar-outline" style="margin-left: 11px; color: #6ab04c"></ion-icon> -->
                                    <img src="{{ asset('checkin.png') }}" alt="" width="50px">
                                    <bold><h5 style="color:black; font-style:bold">Check In</h5></bold>
                                </div>

                            </a>

                        </div>
                    </div><br>
                    <div class="col-4" style="margin-bottom: 20px;">
                        <div class="text-right" style="height:25px;">
                            <span class="badge badge-danger">{{ $countCheckout != 0 ? $countCheckout:'' }}</span>
                        </div>
                        <div class="card">
                            <a href="{{ url('b2b/schedules/checkout') }}" class="btn btn-lg btn-block" style="height: 85px; box-shadow: 5px 10px #888888;" onclick="Helper.loadingStart()">
                                <div style="margin-top:12px">
                                    <!-- <ion-icon class="iconedbox" name="calendar-outline" style="margin-left: 11px; color: #6ab04c"></ion-icon> -->
                                    <img src="{{ asset('checkout.png') }}" alt="" width="50px">
                                    <bold><h5 style="color:black; font-style:bold">Check Out</h5></bold>
                                </div>

                            </a>
                        </div>
                    </div><br>

                </div>
            </div>

        </div>
    </div>
</div>

@endsection
@section('script')

    <script>
        $(document).ready(function(){
            Helper.loadingStop();
        })
    </script>
@endsection
