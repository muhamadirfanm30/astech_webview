@extends('b2b.master')
@section('content')
    <!-- App Header -->
    <!-- loader -->
    <div id="loader" hidden>
        <div class="spinner-border text-primary" role="status"></div>
    </div>
    <div class="appHeader">
        <div class="left">
            <a href="{{ url('b2b/dashboard') }}" class="headerButton" onclick="Helper.loadingStart()">
                <ion-icon name="chevron-back-outline"></ion-icon>
            </a>
        </div>
        <div class="pageTitle">List CheckIn</div>
        <div class="right">
            <!-- <a href="#" class="headerButton">
                <ion-icon name="search-outline"></ion-icon>
            </a> -->
        </div>
    </div>
    <!-- * App Header -->

    <!-- App Capsule -->
    <!-- <div class="section full mt-1 mb-2"> -->
        <div class="">
            <ul class="listview link-listview mb-2">
                @foreach($showData as $data)
                    <li class="multi-level">
                        <a href="#" class="item">{{ $data->schedule_date }} / {{ \Carbon\Carbon::parse($data->start_work)->format('H:i') }} - {{  \Carbon\Carbon::parse($data->end_work)->format('H:i') }}</a>
                        <ul class="listview link-listview">
                                <li>
                                    <a href="#" class="item">
                                        <small class="badge badge-primary badge-sm"> #{{ $data->get_order->request_code }}</small>
                                    </a>
                                    @if($data->checkin == null && $data->checkout == null)
                                        <a href="{{ url('b2b/schedules/checkin/'.$data->id) }}" class="item" onclick="Helper.loadingStart()">
                                            <small class="badge badge-danger badge-sm"> Belum CheckIn </small>
                                        </a>
                                    @endif
                                </li>
                        </ul>
                    </li>
                @endforeach
            </ul>
        </div>
@endsection
@section('script')
<script>
    $(document).ready(function(){
        Helper.loadingStop();
    })
</script>

@endsection
