@extends('b2b.master')
@section('content')
<style>
    .time{
        place-content: center;
        width: 100%;
        margin-top: 15%;
        margin-left:auto ;
        margin-right: auto;

    }
    .myClock{
        width: 100%;
        max-width: 75px;
        display: flex;
        justify-content: center;
        align-items: center;
        height: 75px;
        margin: 0px 10px;
        border-radius: 20px;
        box-shadow: rgba(0, 0, 0, 0.25) 0px 0.0625em 0.0625em, rgba(48, 47, 47, 0.25) 0px 0.125em 0.5em, rgba(255, 255, 255, 0.1) 0px 0px 0px 1px inset;
    }
    .dateAndDay{
        justify-content: space-around;
        width: 100%;
        max-width: 210px;
        margin: 40px auto;
        background-color: #39404e;
        border-radius: 10px;
    }
    .fsize {
        font-size:20px;
    }
    .txtwhite {
        color: white;
    }
</style>
<style>
    div.browse-wrap {
        top:0;
        left:0;
        /* margin:20px; */
        cursor:pointer;
        overflow:hidden;
        padding:10px 30px;
        text-align:center;
        position:relative;
        background-color:#f6f7f8;
    }
    input.upload {
        right:0;
        margin:0;
        bottom:0;
        padding:0;
        opacity:0;
        height:300px;
        outline:none;
        cursor:inherit;
        position:absolute;
        font-size:1000px !important;
    }
    span.upload-image {
        margin:5px;
        display:block;
    }
    span.upload-vidio {
        margin:5px;
        display:block;
    }
</style>
    <!-- App Header -->
    <!-- <div class="spinner-border text-primary" role="status"></div> -->
    <div class="appHeader">
        <div class="left">
            <a href="{{ url('b2b/schedules/index') }}" class="headerButton">
                <ion-icon name="chevron-back-outline"></ion-icon>
            </a>
        </div>
        <div class="pageTitle">Confirm CheckIn</div>
        <div class="right">
            <a href="{{ url('b2b/schedules/checkin') }}" class="headerButton">
            </a>
        </div>
    </div><br>
    <form method="POST" enctype="multipart/form-data" id="check" action="javascript:void(0)" >
        <!-- @csrf -->
            <input type="hidden" name="id" id="id" value="{{ $id }}">
            <div class="col-md-12" style="margin-top: 10px;" id="outlet-content">
                <div class="time d-flex text-white display-1 fw-normal">
                    <!-- Hours -->
                    <div class="myClock hour" style="background-color: #F95658;text-align: center;">
                        <span class="h fsize"></span>
                    </div>
                    <!-- Minutes -->
                    <div class="myClock minutes " style="background-color: #2DCB74; text-align: center;">
                        <span class="m fsize"></span>
                    </div>
                    <!-- Seconds -->
                    <div class="myClock seconds" style="background-color: #F6BC58;text-align: center;">
                        <span class="s fsize"></span>
                    </div>
                </div>
                <!-- Date Day and Meridiem -->
                <div class=" container dateAndDay text-muted fs-4 d-flex">
                    <span class="Day txtwhite"></span>
                    <span class="Date txtwhite"></span>
                    <span class="meridiem txtwhite"></span>
                </div>
            </div>
            <div class="col-md-12" style="margin-top: 10px;">
            <div class="card">
                <div class="card-body">
                    <h6 class="text-center"  style="font-weight: bold; font-size:10px">Detail Order</h6>
                    @php
                        if(!empty($detailOrder->get_order)){
                            if(!empty($detailOrder->get_order->get_b2b_order_detail)){
                                $merek = $detailOrder->get_order->get_b2b_order_detail->merek;
                                $remark = $detailOrder->get_order->get_b2b_order_detail->remark;
                                $unit_ke = $detailOrder->get_order->get_b2b_order_detail->unit_ke;
                                $no_quotation = $detailOrder->get_order->get_b2b_order_detail->no_quotation;
                                $no_invoice = $detailOrder->get_order->get_b2b_order_detail->no_invoice;
                            }else{
                                $remark = '-';
                                $merek = '-';
                                $unit_ke = '-';
                                $no_quotation = '-';
                                $no_invoice = '-';
                            }
                        }
                    @endphp
                    <div class="row">
                        <div class="col-6">
                            <label style="font-weight: bold; font-size:10px">Merek </label>
                        </div>
                        <div class="col-6">
                            <h5 class="text-right">{{ $merek }}</h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3">
                            <label style="font-weight: bold; font-size:10px">Remark </label>
                        </div>
                        <div class="col-9">
                            <h5 class="text-right">{{ $remark }}</h5>
                        </div>
                    </div>
                    <div class="row">
                        @php
                            if(!empty($detailOrder)){
                                if($detailOrder->get_order->get_b2b_order_detail->status_quotation == 'Requested'){
                                    $status = 'Requested';
                                    $warna = 'badge badge-primary';
                                }elseif($detailOrder->get_order->get_b2b_order_detail->status_quotation == 'Process'){
                                    $status = 'Process';
                                    $warna = 'badge badge-success';
                                }elseif($detailOrder->get_order->get_b2b_order_detail->status_quotation == 'Scheduling'){
                                    $status = 'Scheduling';
                                    $warna = 'badge badge-danger';
                                }elseif($detailOrder->get_order->get_b2b_order_detail->status_quotation == 'Finished'){
                                    $status = 'Finished';
                                    $warna = 'badge badge-info';
                                }elseif($detailOrder->get_order->get_b2b_order_detail->status_quotation == 'Waiting Payment'){
                                    $status = 'Waiting Payment';
                                    $warna = 'badge badge-success';
                                }else{
                                    $status = '-';
                                    $warna = 'badge badge-secondary';
                                }
                            }else{
                                $status = 'Belum Ada Teknisi';
                                $warna = 'badge badge-danger';
                            }
                        @endphp
                        <div class="col-6">
                            <label style="font-weight: bold; font-size:10px">Status </label>
                        </div>
                        <div class="col-6">
                            <h5 class="text-right"><span class="{{ $warna }}">{{ $status }}</span></h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <label style="font-weight: bold; font-size:10px">Type Service </label>
                        </div>
                        <div class="col-6">
                            <h5 class="text-right">Maintenance</h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <label style="font-weight: bold; font-size:10px">Unit Ke </label>
                        </div>
                        <div class="col-6">
                            <h5 class="text-right">{{ $unit_ke }}</h5>
                        </div>
                    </div>
                    <br>
                    @php
                       if(!empty($detailOrder->get_order)){
                            if(!empty($detailOrder->get_order->get_outlet)){
                                $outlet = $detailOrder->get_order->get_outlet->type . ' ' . $detailOrder->get_order->get_outlet->name;
                                $unit = $detailOrder->get_order->get_outlet->jumlah_unit;
                                $alamat = $detailOrder->get_order->get_outlet->alamat;
                                $nama_pic = $detailOrder->get_order->get_outlet->nama_pic;
                                $no_pic = $detailOrder->get_order->get_outlet->nomor_pic;
                            }else{
                                $outlet = '-';
                                $unit = '-';
                                $alamat = '-';
                                $nama_pic = '-';
                                $no_pic = '-';
                            }
                        }else{
                            $outlet = '-';
                            $unit = '-';
                            $alamat = '-';
                            $nama_pic = '-';
                            $no_pic = '-';
                        }
                    @endphp
                    <h6 class="card-title">{{ $outlet }}</h6>
                    <div class="row">
                        <div class="col-6">
                            <label style="font-weight: bold; font-size:10px">Jumlah Unit </label>
                        </div>
                        <div class="col-6">
                            <h5 class="text-right">{{ $unit }}</h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <label style="font-weight: bold; font-size:10px">Nama PIC </label>
                        </div>
                        <div class="col-6">
                            <h5 class="text-right">{{ $nama_pic }}</h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <label style="font-weight: bold; font-size:10px">No PIC </label>
                        </div>
                        <div class="col-6">
                            <h5 class="text-right">{{ $no_pic }}</h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3">
                            <label style="font-weight: bold; font-size:10px">Alamat </label>
                        </div>
                        <div class="col-9">
                            <h5 class="text-right"><p>{{ $alamat }}</h5>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="browse-wrap">
                                <div class="title">
                                    <ion-icon name="image-outline"></ion-icon>
                                    Upload video / Image
                                </div>
                                <input type="file" name="images" class="upload upload-attachment upload-files" id-upload-image="" title="Choose a file to upload">
                            </div>
                            <div class="upload" style="margin-top: 5px;"></div>
                            <div  id="error-images" style="color:red;text-align: left;text-size-adjust: 1000px;font-size: 11px;"></div><br>
                        </div>
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-sm btn-info waves-effect btn-block">CHECKIN</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class="modal fade dialogbox" id="DialogIconedDanger" data-backdrop="static" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-icon text-danger">
                    <ion-icon name="close-circle"></ion-icon>
                </div>
                <div class="modal-header">
                    <h5 class="modal-title">Error</h5>
                </div>
                <div class="modal-body">
                    Terjadi Kesalahan
                </div>
                <div class="modal-footer">
                    <div class="btn-inline">
                        <a href="#" class="btn" data-dismiss="modal">CLOSE</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>


<!-- <script src=""></script> -->
    <script>
        const hours =  document.querySelector('.h');
        const minutes =  document.querySelector('.m');
        const seconds =  document.querySelector('.s');
        const checkMeridiem = document.querySelector('.meridiem')
        const day = document.querySelector('.Day')
        const date = document.querySelector('.Date')


        const tick = () =>{
            const current = new Date();


            let ss =current.getSeconds();
            let mm = current.getMinutes()
            let hh = current.getHours();
            let meridiem = 'AM';
            let currentDay = current.getDay();


            //Converting the 24 hours formate into 12 hour formate
            if(hh === 00){
                hh = 12
                meridiem = 'AM';
            }
            else if( hh === 12 ){
                meridiem = 'PM';
            }
            else if( hh > 12){
                hh = hh - 12
                meridiem = 'PM';
            }



            hours.textContent = `${hh<10? `0${hh}`:hh}`;
            minutes.textContent =`${mm<10? `0${mm}`:mm}`;
            seconds.textContent =`${ss<10? `0${ss}`:ss}`
            checkMeridiem.textContent = meridiem;
            date.textContent = current.toLocaleDateString();
            day.textContent = getDayName(currentDay);
            // day.textContent =

        }

        const getDayName = (value) =>{
            const DayNames = [
                'Sunday',
                'Monday',
                'Tuesday',
                'Wednesday',
                'Thursday',
                'Friday',
                'Saturday'
            ]
            return DayNames[value];
        }
        setInterval(tick,1000)

        $('.upload-attachment').change(function(){
            $(this).closest('.browse-wrap').next('.upload').append('<div class="alert alert-info mb-2 alert-dismissible fade show" role="alert" sty><small>Image Has Been Selected</small><button type="button" class="close" data-dismiss="alert" aria-label="Close"><ion-icon name="close-outline"></ion-icon></button></div>');
        });

        $(document).ready(function (e) {
            $.ajaxSetup({
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('#check').submit(function(e) {
                Helper.loadingStart();
                e.preventDefault();
                var fd = new FormData(this);
                var files = $('.upload-files')[0].files[0];
                if(files){
                    fd.append('attachment_checkin', files);
                }
                $.ajax({
                    type:'POST',
                    url: Helper.url('/b2b/schedules/checkin'),
                    data: fd,
                    cache:false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        Helper.loadingStop();
                        // window.location.href = Helper.redirectUrl('/b2b/schedules/index');
                        window.location.href = Helper.redirectUrl('/b2b/success-checkinout');
                    },
                    error: function(error){
                        console.log(error.responseJSON)
                        if (error.responseJSON.status_code == 422) {
                            Helper.loadingStop();
                            messages = error.responseJSON.data;
                            // clean msg
                            console.log(messages)
                            $('.error-validation-mgs').remove();
                            _.each(messages, function(pesan, field) {
                                $('#error-'+ field).text(pesan[0])
                            })

                        }else{
                            Helper.loadingStop();
                            $('#DialogIconedDanger').modal('show');
                        }
                    }
                });
            });
        });
    </script>
@endsection
