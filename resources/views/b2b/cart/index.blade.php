@extends('b2b.master')
@section('content')
    <!-- App Header -->
    <div class="appHeader bg-primary text-light">
        <div class="left">
            <a href="javascript:;" class="headerButton goBack">
                <ion-icon name="chevron-back-outline"></ion-icon>
            </a>
        </div>
        <div class="pageTitle">Draft ({{ $hitungCart }})</div>
        <div class="right">
            @if($hitungCart > 0)
                <button type="button" class="btn btn-outline-secondary btn-sm btn-delete-all" data-id="{{ Auth::user()->id }}" style="border:none; color:white" >
                    <ion-icon name="trash-outline"></ion-icon>
                </button>
            @endif
        </div>
    </div>

    @if($hitungCart > 0)
        <div class="modal fade dialogbox" id="DialogClear" data-backdrop="static" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Empty Cart</h5>
                    </div>
                    <div class="modal-body">
                        All items will be deleted.
                    </div>
                    <div class="modal-footer">
                        <div class="btn-inline">
                            <a href="#" class="btn btn-text-secondary" data-dismiss="modal">CLOSE</a>
                            <a href="#" class="btn btn-text-primary" data-dismiss="modal">DELETE</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <form id="checkout_cart">
            <div class="section mt-2">
                @foreach($getCart as $cart)
                    @php 
                        if(!empty($cart)){
                            $gambar = $cart->image;
                            if(!empty($cart->tmp_unit)){
                                $merek = $cart->tmp_unit->merek;
                                $unit = $cart->tmp_unit->unit_ke;
                            }else{
                                $merek = '-';
                                $unit = '-';
                            }
                            if(!empty($cart->tmp_outlet)){
                                $outlet = $cart->tmp_outlet->type . ' ' .  $cart->tmp_outlet->name;
                            }else{
                                $outlet = '-';
                            }
                        }else{
                            $merek = '-';
                            $unit = '-';
                            $outlet = '-';
                            $gambar = '-';
                        }
                    @endphp
                    <div class="card cart-item mb-2">
                        <div class="card-body">
                            <div class="in">
                                <img src="{{ url('storage/file-order/' . $gambar) }}" alt="product" class="imaged">
                                <div class="text">
                                    <h4 class="title">{{ $merek }} &nbsp; <small>({{ $cart->service_type }})</small></h4>
                                    <p class="detail">Unit Ke : {{ $unit }}</p>
                                    <strong class="price">{{ $outlet }}</strong>
                                </div>
                            </div>
                            <div class="cart-item-footer">
                                <div class="custom-control custom-checkbox mb-{{ $cart->id }}">
                                    <input type="checkbox" class="custom-control-input id_checkout_item" value="{{ $cart->id }}" name="id_checkout_item[]" id="customCheckb{{ $cart->id }}">
                                    <label class="custom-control-label" for="customCheckb{{ $cart->id }}">Pilih</label>
                                </div>
                                <!-- <button type="button"  class="btn btn-outline-secondary btn-sm btn-show-modal">Delete</button> -->
                                <button type="button" class="btn btn-outline-secondary btn-sm btn-delete" data-id="{{ $cart->id }}">
                                    Delete
                                </button>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade dialogbox" id="DialogBasic" data-backdrop="static" tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Dialog title</h5>
                                </div>
                                <div class="modal-body">
                                    This is a dialog message
                                </div>
                                <div class="modal-footer">
                                    <div class="btn-inline">
                                        <a href="#" class="btn btn-text-secondary" data-dismiss="modal">CLOSE</a>
                                        <a href="#" class="btn btn-text-primary" data-dismiss="modal">Delete</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

            <div class="form-button-group">
                <button class="btn btn-primary btn-block btn-sm">Save Data</button>
            </div>
        </form>
    @else 
        <div class="error-page">
            <!-- <div class="icon-box text-success">
                <ion-icon name="cart-outline"></ion-icon>
            </div> -->
            <div class="mb-2">
                <img src="{{ asset('vector3.png') }}" alt="alt" class="imaged square w200">
            </div>
            <h1 class="title">Empty</h1>
            <div class="text mb-5">
                Data Not found.
            </div>

            <div class="fixed-footer">
                <div class="row">
                    <div class="col-12">
                        <a href="{{ url('b2b/request-order/create') }}" class="btn btn-secondary btn-lg btn-block">Request Order</a>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="modal fade dialogbox" id="DialogIconedDanger" data-backdrop="static" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-icon text-danger">
                    <ion-icon name="close-circle"></ion-icon>
                </div>
                <div class="modal-header">
                    <h5 class="modal-title">Error</h5>
                </div>
                <div class="modal-body">
                    <div id="msg"></div>
                </div>
                <div class="modal-footer">
                    <div class="btn-inline">
                        <a href="#" class="btn" data-dismiss="modal">CLOSE</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade dialogbox" id="DialogIconedSuccess" data-backdrop="static" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-icon text-success">
                    <ion-icon name="checkmark-circle"></ion-icon>
                </div>
                <div class="modal-header">
                    <h5 class="modal-title">Success</h5>
                </div>
                <div class="modal-body">
                    <div class="msg_success"></div>
                </div>
                <div class="modal-footer">
                    <div class="btn-inline">
                        <a href="#" class="btn" data-dismiss="modal">CLOSE</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- * App Capsule -->
@endsection
@section('script')
<script>

    $(".btn-delete").click(function() {
        var id = $(this).attr('data-id');
        Helper.confirmDelete(function() {
            Helper.loadingStart();
            Axios.delete('/b2b/request-order/delete-data-cart/' + id)
                .then(function(response) {
                    //send notif
                    // Helper.successNotif(response.data.msg);
                    $('#DialogIconedSuccess').modal('show');
                    $('.msg_success').append(response.data.msg);
                    location.reload();
                    Helper.loadingStop();
                })
                .catch(function(error) {
                    Helper.loadingStop();
                    console.log(error.response.data.msg)
                    $('#msg').append(error.response.data.msg);
                    $('#DialogIconedDanger').modal('show');
                });
        })
    });

    $(".btn-delete-all").click(function() {
        Helper.confirmDelete(function() {
            Helper.loadingStart();
            Axios.post('/b2b/request-order/delete-all-data-cart')
                .then(function(response) {
                    //send notif
                    // Helper.successNotif(response.data.msg);
                    $('#DialogIconedSuccess').modal('show');
                    $('.msg_success').append(response.data.msg);
                    location.reload();
                    Helper.loadingStop();
                })
                .catch(function(error) {
                    Helper.loadingStop();
                    console.log(error.response.data.msg)
                    $('#msg').append(error.response.data.msg);
                    $('#DialogIconedDanger').modal('show');
                });
        })
    });


    $('#checkout_cart').submit(function(e){
        Helper.loadingStart();
        var data = new FormData($('#media-form')[0]);
        var bodyFormData = new FormData();

        $('.id_checkout_item').each(function(){ 
            if($(this).prop("checked")){
                bodyFormData.append('id_checkout_item[]', $(this).val() ); 
            }
        });

        Axios.post('/b2b/request-order/checkout-data-cart', bodyFormData, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            })
            .then(function(response) {
                console.log(response.data.id)
                Helper.loadingStop();
                window.location.href = Helper.url('/b2b/success-response');
                // // location.reload();
                // Helper.successNotif('Success Updated');
            })
            .catch(function(error) {
                if (error.response.status == 402) {
                    Helper.loadingStop();
                    console.log(error.response.data.msg)
                    $('#msg').append(error.response.data.msg);
                    $('#DialogIconedDanger').modal('show');
                    
                }
            });

        e.preventDefault();
    })
</script>
<script>
    $(document).ready(function(){
        Helper.loadingStop();
    })
</script>
@endsection