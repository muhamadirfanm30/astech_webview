@extends('b2b.master')
@section('content')
    <!-- App Header -->
    <!-- <div class="spinner-border text-primary" role="status"></div> -->
    <div class="appHeader">
        <div class="left">
            <a href="{{ url('b2b/request-order/show') }}" class="headerButton">
                <ion-icon name="chevron-back-outline"></ion-icon>
            </a>
        </div>
        <div class="pageTitle">List Order</div>
        <div class="right">
            <a href="{{ url('b2b/request-order/cart') }}" class="headerButton">
                <img src="{{ asset('drafts.png') }}" alt="" width="30px">
                <span class="badge badge-danger">{{ $hitungCart }}</span>
            </a>
        </div>
    </div><br>

    <!-- <form method="post" action="{{ url('b2b/request-order/add-to-cart') }}" enctype="multipart/form-data"> -->
    <form method="POST" enctype="multipart/form-data" id="laravel-ajax-file-upload" action="javascript:void(0)" >
        <!-- @csrf -->
            <div class="col-md-12" style="margin-top: 10px;" id="outlet-content"> </div>

            <div class="col-md-12" style="margin-top: 10px;">
            <div class="card">
                <div class="card-body">
                    <style>
                        div.browse-wrap {
                            top:0;
                            left:0;
                            /* margin:20px; */
                            cursor:pointer;
                            overflow:hidden;
                            padding:10px 30px;
                            text-align:center;
                            position:relative;
                            background-color:#f6f7f8;
                        }
                        input.upload {
                            right:0;
                            margin:0;
                            bottom:0;
                            padding:0;
                            opacity:0;
                            height:300px;
                            outline:none;
                            cursor:inherit;
                            position:absolute;
                            font-size:1000px !important;
                        }
                        span.upload-image {
                            margin:5px;
                            display:block;
                        }
                        span.upload-vidio {
                            margin:5px;
                            display:block;
                        }
                    </style>

                    <input type="hidden" id="get_id_uniq" value="">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Outlet</label>
                                    <select class="form-control outlet-select selects" id="outlet_id" name="outlet_id" style="width:100%" onclick="Helper.loadingStart();" data-uniq="">
                                        <option value="">Select Outlet</option>
                                        @foreach($showOutlet->business_to_business_outlet_transactions as $outlet)
                                            <option value="{{ $outlet->id }}">{{ $outlet->name }}</option>
                                        @endforeach
                                    </select>
                                    <div  id="error-outlet_id" style="color:red;text-align: left;text-size-adjust: 1000px;font-size: 11px;"></div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Unit</label>
                                    <select class="form-control unit-select units" id="unit_id" name="unit_id" style="width:100%">

                                    </select>
                                    <div  id="error-unit_id" style="color:red;text-align: left;text-size-adjust: 1000px;font-size: 11px;"></div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Service</label>
                                    <select class="form-control outlet-service" name="service_type" id="service_type">
                                        <option value="Repair">Repair</option>
                                        <option value="Maintenance">Maintenance</option>
                                        <option value="Checking">Checking</option>
                                    </select>
                                    <div  id="error-service_type" style="color:red;text-align: left;text-size-adjust: 1000px;font-size: 11px;"></div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Remark</label>
                                    <textarea class="form-control outlet-remark" name="remark" cols="30" rows="4"></textarea>
                                </div>
                                <div  id="error-remark" style="color:red;text-align: left;text-size-adjust: 1000px;font-size: 11px;"></div>
                            </div>
                            <div class="col-md-12">
                                <div class="browse-wrap">
                                    <div class="title">
                                        <ion-icon name="image-outline"></ion-icon>
                                        Upload video / Image
                                    </div>
                                    <input type="file" name="images" class="upload upload-attachment upload-files" id-upload-image="" title="Choose a file to upload">
                                </div>
                                <div class="upload" style="margin-top: 5px;"></div>
                                <div  id="error-images" style="color:red;text-align: left;text-size-adjust: 1000px;font-size: 11px;"></div>

                            </div>

                        </div>
                    </div><hr>
                    <div class="row">
                        <div class="col-6">
                            <button type="submit" class="btn btn-sm btn-info waves-effect btn-block btn_cart"><ion-icon name="cart-outline"></ion-icon> Save Request</button>
                        </div>
                        <div class="col-6">
                            <button type="button"  class="btn btn-sm btn-success waves-effect btn-block btn_checkout"><ion-icon name="save-outline"></ion-icon> Request Now</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class="modal fade dialogbox" id="DialogIconedDanger" data-backdrop="static" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-icon text-danger">
                    <ion-icon name="close-circle"></ion-icon>
                </div>
                <div class="modal-header">
                    <h5 class="modal-title">Error</h5>
                </div>
                <div class="modal-body">
                    Terjadi Kesalahan
                </div>
                <div class="modal-footer">
                    <div class="btn-inline">
                        <a href="#" class="btn" data-dismiss="modal">CLOSE</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<!-- select2 -->
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<!-- <script src=""></script> -->
    <script>
        $('.upload-attachment').change(function(){
            $(this).closest('.browse-wrap').next('.upload').append('<div class="alert alert-info mb-2 alert-dismissible fade show" role="alert" sty><small>Image Has Been Selected</small><button type="button" class="close" data-dismiss="alert" aria-label="Close"><ion-icon name="close-outline"></ion-icon></button></div>');
        });

        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('.selects').select2();
            $('.units').select2();
        });

        show();

        function show(){
            loadOutletSelec2();
        }

        $(document).on('change', '.outlet-select', function() {

            $('.unit-select').html('<option value="">Select Unit</option>');
            outlet_id = $(this).find(':selected').val();
            Helper.loadingStop;
            loadUnitSelect2(outlet_id);
        })

        function loadOutletSelec2() {
            globalCRUD
                .select2Static(".outlet-select", '/b2b/request-order/select2-outlet', function(item) {
                    console.log(item)
                    return {
                        id: item.id,
                        text: item.created_at,
                        data: [{name:ssk}],
                    }

                })
        }

        function loadUnitSelect2( outlet_id) {
            globalCRUD
                .select2Static(".unit-select", '/b2b/request-order/select2-unit/' + outlet_id, function(item) {
                    type_name = '';
                    type_desc = '';
                    if(item.get_type){
                        type_name = item.get_type.name;
                        type_desc = item.get_type.desc == null ? '' : item.get_type.desc;
                    }
                    return {
                        id: item.id,
                        text: item.unit_ke + ' - (' + item.merek  + ' ' + type_name + ' ' + type_desc + ')',
                        data: item,
                    }
                })
        }

        $(".btn_checkout").click(function() {
            Helper.loadingStart();
            var fd = new FormData();
            console.log()
            var files = $('.upload-files')[0].files[0];
            if(files){
                fd.append('images', files);
            }
            // fd.append('id', $('input[name="id"]').val());
            fd.append('outlet_id', $('select[name="outlet_id"]').val());
            fd.append('unit_id', $('select[name="unit_id"]').val());
            fd.append('service_type', $('select[name="service_type"]').val());
            fd.append('remark', $('textarea[name="remark"]').val());
            $.ajax({
                url:Helper.url('/b2b/request-order/checkout'),
                type: 'post',
                contentType: 'multipart/form-data',
                data: fd,
                contentType: false,
                processData: false,
                success: function(response) {
                    Helper.loadingStop();
                    window.location.href = Helper.redirectUrl('/b2b/success-response');
                },
                error: function(xhr, status, error) {
                    console.log(xhr.responseJSON)
                    console.log(status)
                    console.log(error)
                    if (xhr.responseJSON.status_code == 422) {
                        Helper.loadingStop();
                        messages = xhr.responseJSON.data;
                        // clean msg
                        console.log(messages)
                        $('.error-validation-mgs').remove();
                        _.each(messages, function(pesan, field) {
                            $('#error-'+ field).text(pesan[0])
                        })

                    }else{
                        Helper.loadingStop();
                        $('#DialogIconedDanger').modal('show');
                    }
                },
            });
        });
    </script>

    <script>
        $(document).ready(function (e) {
            $.ajaxSetup({
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('#laravel-ajax-file-upload').submit(function(e) {
                Helper.loadingStart();
                e.preventDefault();
                var formData = new FormData(this);

                $.ajax({
                    type:'POST',
                    url: Helper.url('/b2b/request-order/add-to-cart'),
                    data: formData,
                    cache:false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        Helper.loadingStop();
                        window.location.href = Helper.redirectUrl('/b2b/cart-view');
                    },
                    error: function(error){
                        console.log(error.responseJSON)
                        if (error.responseJSON.status_code == 422) {
                            Helper.loadingStop();
                            messages = error.responseJSON.data;
                            // clean msg
                            console.log(messages)
                            $('.error-validation-mgs').remove();
                            _.each(messages, function(pesan, field) {
                                $('#error-'+ field).text(pesan[0])
                            })

                        }else{
                            Helper.loadingStop();
                            $('#DialogIconedDanger').modal('show');
                        }
                    }
                });
            });
        });
    </script>
@endsection
