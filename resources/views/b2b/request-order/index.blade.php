@extends('b2b.master')
@section('content')
    <!-- App Header -->
    <!-- loader -->
    <div id="loader" hidden>
        <div class="spinner-border text-primary" role="status"></div>
    </div>
    <div class="appHeader">
        <div class="left">
            <a href="{{ url('b2b/dashboard') }}" class="headerButton" onclick="Helper.loadingStart()">
                <ion-icon name="chevron-back-outline"></ion-icon>
            </a>
        </div>
        <div class="pageTitle">List Order</div>
        <div class="right">
            <!-- <a href="#" class="headerButton">
                <ion-icon name="search-outline"></ion-icon>
            </a> -->
        </div>
    </div>
    <!-- * App Header -->

    <!-- App Capsule -->
    <!-- <div class="section full mt-1 mb-2"> -->
        <div class="">
            <ul class="listview link-listview mb-2">
                @foreach($showListDataOrder as $listOrder)
                    <li class="multi-level">
                        <a href="#" class="item">{{ $listOrder->request_code }}</a>
                        <ul class="listview link-listview">
                            @foreach($listOrder->get_history_order as $historyOrder)
                                @php
                                    if(!empty($historyOrder)){
                                        if(!empty($historyOrder->get_b2b_detail)){
                                            if(!empty($historyOrder->get_b2b_detail->business_to_business_outlet_transaction)){
                                                $outlet = $historyOrder->get_b2b_detail->business_to_business_outlet_transaction->type . ' ' . $historyOrder->get_b2b_detail->business_to_business_outlet_transaction->name;
                                                $pt = $historyOrder->get_b2b_detail->business_to_business_outlet_transaction->business_to_business_transaction->company->name;
                                            }else{
                                                $outlet = '3';
                                                $pt = '3';
                                            }
                                        }else{
                                            $outlet = '2';
                                            $pt = '2';
                                        }
                                    }else{
                                        $outlet = '1';
                                        $pt = '1';
                                    }


                                    if(!empty($historyOrder)){
                                        if(!empty($historyOrder->get_schedule)){
                                            if($historyOrder->get_schedule->status == 'Tiba Ditempat'){
                                                $status = 'Tiba Ditempat';
                                                $warna = 'badge badge-primary';
                                            }elseif($historyOrder->get_schedule->status == 'Selesai'){
                                                $status = 'Selesai';
                                                $warna = 'badge badge-success';
                                            }elseif($historyOrder->get_schedule->status == 'Penjadwalan Ulang'){
                                                $status = 'Penjadwalan Ulang';
                                                $warna = 'badge badge-danger';
                                            }elseif($historyOrder->get_schedule->status == null){
                                                $status = 'Menunggu';
                                                $warna = 'badge badge-warning';
                                            }else{
                                                $status = '-';
                                                $warna = 'badge badge-secondary';
                                            }
                                        }else{
                                            $status = 'Belum Ada Teknisi';
                                            $warna = 'badge badge-danger';
                                        }
                                    }
                                @endphp
                                <li>
                                    <a href="{{ url('b2b/request-order/order-detail/'.$historyOrder->id) }}" class="item" onclick="Helper.loadingStart()">
                                        <p style="color: #0984e3;">{{ $outlet }} <small>({{ $pt }})</small></p>
                                        <small class="{{ $warna }} badge-sm">{{ $status }} </small>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                @endforeach
            </ul>
        </div>
    <!-- </div> -->
    <div class="fab-button animate bottom-right dropdown">
        <a href="{{ url('b2b/request-order/create') }}" class="fab create-order">
            <ion-icon name="add-outline"></ion-icon>
        </a>
    </div>
    <!-- href="{{ url('b2b/request-order/create') }}" -->
@endsection
@section('script')
<script>
    $(document).ready(function(){
        Helper.loadingStop();
    })
</script>
<script>
    $(document).on('click', '.create-order', function() {
        // console.log('klik')
        Helper.loadingStart();
        location.href = Helper.url('/b2b/request-order/create');
        
    })
</script>
@endsection