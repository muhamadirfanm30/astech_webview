@extends('b2b.master')
@section('content')
    <!-- App Header -->
    <!-- loader -->
    <div id="loader" hidden>
        <div class="spinner-border text-primary" role="status"></div>
    </div>
    <div class="appHeader">
        <div class="left">
            <a href="{{ url('b2b/request-order/show') }}" class="headerButton" onclick="Helper.loadingStart()">
                <ion-icon name="chevron-back-outline"></ion-icon>
            </a>
        </div>
        <div class="pageTitle">Order Detail</div>
        <div class="right">
            <!-- <a href="#" class="headerButton">
                <ion-icon name="search-outline"></ion-icon>
            </a> -->
        </div>
    </div>
    <!-- * App Header -->
        <div class="section mt-2">
            <div class="card">
                <div class="card-body">
                    @php 
                        if(!empty($detailOrder->get_b2b_detail)){
                            if(!empty($detailOrder->get_b2b_detail->business_to_business_outlet_transaction)){
                                $outlet = $detailOrder->get_b2b_detail->business_to_business_outlet_transaction->type . ' ' . $detailOrder->get_b2b_detail->business_to_business_outlet_transaction->name;
                                $unit = $detailOrder->get_b2b_detail->business_to_business_outlet_transaction->jumlah_unit;
                                $alamat = $detailOrder->get_b2b_detail->business_to_business_outlet_transaction->alamat;
                                $nama_pic = $detailOrder->get_b2b_detail->business_to_business_outlet_transaction->nama_pic;
                                $no_pic = $detailOrder->get_b2b_detail->business_to_business_outlet_transaction->nomor_pic;
                            }else{
                                $outlet = '-';
                                $unit = '-';
                                $alamat = '-';
                                $nama_pic = '-';
                                $no_pic = '-';
                            }
                        }else{
                            $outlet = '-';
                            $unit = '-';
                            $alamat = '-';
                            $nama_pic = '-';
                            $no_pic = '-';
                        }
                    @endphp
                    <h6 class="card-title">{{ $outlet }}</h6>
                    <div class="row">
                        <div class="col-6">
                            <label style="font-weight: bold; font-size:15px">Jumlah Unit </label>
                        </div>
                        <div class="col-6">
                            <h5 class="text-right">{{ $unit }}</h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <label style="font-weight: bold; font-size:15px">Nama PIC </label>
                        </div>
                        <div class="col-6">
                            <h5 class="text-right">{{ $nama_pic }}</h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <label style="font-weight: bold; font-size:15px">No PIC </label>
                        </div>
                        <div class="col-6">
                            <h5 class="text-right">{{ $no_pic }}</h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3">
                            <label style="font-weight: bold; font-size:15px">Alamat </label>
                        </div>
                        <div class="col-9">
                            <h5 class="text-right"><p>{{ $alamat }}</h5>
                        </div>
                    </div>
                </div>
            </div><br>
            <div class="card">
                <div class="card-body">
                    <h6 class="text-center"  style="font-weight: bold; font-size:15px">Detail Order</h6>
                    @php 
                        if(!empty($detailOrder->get_b2b_detail)){
                            $merek = $detailOrder->get_b2b_detail->merek;
                            $remark = $detailOrder->remark;
                            $unit_ke = $detailOrder->get_b2b_detail->unit_ke;
                            $no_quotation = $detailOrder->get_b2b_detail->no_quotation;
                            $no_invoice = $detailOrder->get_b2b_detail->no_invoice;
                        }else{
                            $remark = '-';
                            $merek = '-';
                            $unit_ke = '-';
                            $no_quotation = '-';
                            $no_invoice = '-';
                        }
                    @endphp
                    <div class="row">
                        <div class="col-6">
                            <label style="font-weight: bold; font-size:15px">Merek </label>
                        </div>
                        <div class="col-6">
                            <h5 class="text-right">{{ $merek }}</h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3">
                            <label style="font-weight: bold; font-size:15px">Remark </label>
                        </div>
                        <div class="col-9">
                            <h5 class="text-right">{{$detailOrder->remark }}</h5>
                        </div>
                    </div>
                    <div class="row">
                        @php 
                            if(!empty($detailOrder)){
                                if($detailOrder->get_b2b_detail->status_quotation == 'Requested'){
                                    $status = 'Requested';
                                    $warna = 'badge badge-primary';
                                }elseif($detailOrder->get_b2b_detail->status_quotation == 'Process'){
                                    $status = 'Process';
                                    $warna = 'badge badge-success';
                                }elseif($detailOrder->get_b2b_detail->status_quotation == 'Scheduling'){
                                    $status = 'Scheduling';
                                    $warna = 'badge badge-danger';
                                }elseif($detailOrder->get_b2b_detail->status_quotation == 'Finished'){
                                    $status = 'Finished';
                                    $warna = 'badge badge-info';
                                }elseif($detailOrder->get_b2b_detail->status_quotation == 'Waiting Payment'){
                                    $status = 'Waiting Payment';
                                    $warna = 'badge badge-success';
                                }else{
                                    $status = '-';
                                    $warna = 'badge badge-secondary';
                                }
                            }else{
                                $status = 'Belum Ada Teknisi';
                                $warna = 'badge badge-danger';
                            }
                        @endphp
                        <div class="col-6">
                            <label style="font-weight: bold; font-size:15px">Status </label>
                        </div>
                        <div class="col-6">
                            <h5 class="text-right"><span class="{{ $warna }}">{{ $status }}</span></h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <label style="font-weight: bold; font-size:15px">Type Service </label>
                        </div>
                        <div class="col-6">
                            <h5 class="text-right">Maintenance</h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <label style="font-weight: bold; font-size:15px">Unit Ke </label>
                        </div>
                        <div class="col-6">
                            <h5 class="text-right">{{ $unit_ke }}</h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <label style="font-weight: bold; font-size:15px">No Quotation </label>
                        </div>
                        <div class="col-6">
                            <h5 class="text-right"><a href="{{ url('/b2b/preview-document/quotation/'.$detailOrder->id) }}">{{ $no_quotation }}</a></h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <label style="font-weight: bold; font-size:15px">No Invoice </label>
                        </div>
                        <div class="col-6">
                            @if($detailOrder->get_b2b_detail->status_quotation == 'Waiting Payment')
                                <h5 class="text-right"><a href="{{ url('/b2b/preview-document/invoice/'.$detailOrder->id) }}">{{ $no_invoice }}</a></h5>
                            @else 
                                <h5 class="text-right">-</h5>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <label style="font-weight: bold; font-size:15px">Tanggal Pengerjaan </label>
                        </div>
                        <div class="col-6">
                            <h5 class="text-right">{{ !empty($detailOrder->get_b2b_detail->tanggal_pengerjaan) ?  date('M D Y', strtotime($detailOrder->get_b2b_detail->tanggal_pengerjaan))  : '-' }}</h5>
                        </div>
                    </div>
                </div>
            </div><br>
            <div class="card">
                <div class="card-body">
                    <h6 class="text-center"  style="font-weight: bold; font-size:15px">List Teknisi</h6>
                    @foreach($getTeknisi as $teknisi)
                        @php 
                            if(!empty($teknisi)){
                                if($teknisi->status == 'Tiba Ditempat'){
                                    $status = 'Tiba Ditempat';
                                    $warna = 'badge badge-primary';
                                }elseif($teknisi->status == 'Selesai'){
                                    $status = 'Selesai';
                                    $warna = 'badge badge-success';
                                }elseif($teknisi->status == 'Penjadwalan Ulang'){
                                    $status = 'Penjadwalan Ulang';
                                    $warna = 'badge badge-danger';
                                }elseif($teknisi->status == null){
                                    $status = 'Menunggu';
                                    $warna = 'badge badge-danger';
                                }else{
                                    $status = '-';
                                    $warna = 'badge badge-secondary';
                                }
                            }else{
                                $status = 'Belum Ada Teknisi';
                                $warna = 'badge badge-danger';
                            }
                        @endphp
                        <div class="accordion" id="accordionExample{{ $teknisi->id }}">
                            <div class="item">
                                <div class="accordion-header">
                                    <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#accordion{{ $teknisi->id }}">
                                        {{ $teknisi->get_teknisi->name }} 
                                    </button>
                                </div>
                                <div id="accordion{{ $teknisi->id }}" class="accordion-body collapse" data-parent="#accordionExample{{ $teknisi->id }}">
                                    <div class="accordion-content">
                                        <div class="row">
                                            <div class="col-6">
                                                <label style="font-weight: bold; font-size:15px">Status </label>
                                            </div>
                                            <div class="col-6">
                                                <h5 class="text-right"><span class="{{ $warna }}">{{ $status }}</span></h5>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-6">
                                                <label style="font-weight: bold; font-size:15px">Jadwal </label>
                                            </div>
                                            <div class="col-6">
                                                <h5 class="text-right">{{ !empty($teknisi->schedule_date) ?  date('m D Y', strtotime($teknisi->schedule_date))  : '-' }}</h5>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-6">
                                                <label style="font-weight: bold; font-size:15px">Checkin </label>
                                            </div>
                                            <div class="col-6">
                                                <h5 class="text-right">{{ !empty($teknisi->checkin) ?  date('M d Y H:i', strtotime($teknisi->checkin))  : '-' }}</h5>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-6">
                                                <label style="font-weight: bold; font-size:15px">Checkout </label>
                                            </div>
                                            <div class="col-6">
                                                <h5 class="text-right">{{ !empty($teknisi->checkout) ?  date('M d Y H:i', strtotime($teknisi->checkout))  : '-' }}</h5>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-3">
                                                <label style="font-weight: bold; font-size:15px">Remark </label>
                                            </div>
                                            <div class="col-9">
                                                <h5 class="text-right">{{ $teknisi->remark }}</h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    <!-- href="{{ url('b2b/request-order/create') }}" -->
@endsection
@section('script')
<script>
    $(document).ready(function(){
        Helper.loadingStop();
    });
</script>
<script>
    $(document).on('click', '.create-order', function() {
        // console.log('klik')
        Helper.loadingStart();
        location.href = Helper.url('/b2b/request-order/create');
        
    })
</script>
@endsection