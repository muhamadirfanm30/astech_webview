@extends('b2b.master')
@section('content')
    <!-- App Header -->
    <div class="appHeader no-border transparent position-absolute">
        <div class="left">
            <a href="{{ url('b2b/schedules/index') }}"></ion-icon>
            </a>
        </div>
        <div class="pageTitle">Berhasil</div>
        <div class="right"></div>
    </div>
    <!-- * App Header -->

    <!-- App Capsule -->
    <div id="appCapsule">

        <div class="error-page">
            <div class="icon-box text-success">
                <ion-icon name="checkmark-circle"></ion-icon>
            </div>
            <h1 class="title">Berhasil Checkin / Checkout</h1>
            <div class="text mb-5">
                Selamat proses sudah selesai !
            </div>

            <div class="fixed-footer">
                <div class="row">
                    <div class="col-12">
                        <a href="{{ url('b2b/schedules/index') }}" class="btn btn-secondary btn-lg btn-block">Kembali</a>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- * App Capsule -->
@endsection
