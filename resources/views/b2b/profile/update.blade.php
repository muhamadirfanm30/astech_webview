@extends('b2b.master')
@section('content')
    <div class="appHeader bg-primary text-light">
        <div class="left">
            <a href="javascript:;" class="headerButton goBack">
                <ion-icon name="chevron-back-outline"></ion-icon>
            </a>
        </div>
        <div class="pageTitle">Profile</div>
        <!-- <div class="right">
            <a href="javascript:;" class="headerButton">
                <ion-icon name="mail-outline"></ion-icon>
            </a>
            <a href="javascript:;" class="headerButton">
                <ion-icon name="person-add-outline"></ion-icon>
            </a>
        </div> -->
    </div>
    <br>
    <form method="POST" enctype="multipart/form-data" id="laravel-ajax-file-upload" action="javascript:void(0)" >
        <div class="section mt-1 mb-2">
            <div class="profile-info">
                <div id="success_message"></div>
                <div class=" bio">

                    <div class="row">
                        <div class="col-md-12" style="margin-bottom: 15px;">
                            <div class="browse-wrap">
                                <div class="title" style="color: white;">
                                    <ion-icon name="image-outline"></ion-icon>
                                    Upload Image 
                                </div>
                                <input type="file" name="image_teknisi" class="upload upload-attachment upload-files" id-upload-image="" title="Choose a file to upload">
                            </div>
                            <div class="upload" style="margin-top: 5px;"></div>
                            @if(!empty($user->image_teknisi))
                                <a href="" data-toggle="modal" data-target="#ModalBasic">View Image</a>
                            @else 
                                <span>No Image</span>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12" style="margin-bottom: 15px;">
                            <label for="">Name</label>
                            <input type="text" class="form-control" placeholder="name" name="name" value="{{ $user->name }}" require>
                        </div>
                    </div>
                    <div>
                    <div class="row">
                        <div class="col-md-12" style="margin-bottom: 15px;">
                            <label for="">Email</label>
                            <input type="text" class="form-control" placeholder="Email" name="email" value="{{ $user->email }}" require>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12" style="margin-bottom: 15px;">
                        <label for="">Phone</label>
                            <input type="text" class="form-control" placeholder="Phone" name="phone" value="{{ $user->phone }}" require>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12" style="margin-bottom: 15px;">
                        <label for="">Address</label>
                            <textarea name="address" class="form-control" id="" cols="30" rows="5" require>{{ $user->address }}</textarea>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary btn-block btn-sm">Update Profile</button>
                        </div>
                    </div>
                    <hr>
                </div>
                <style>
                    div.browse-wrap {
                        top:0;
                        left:0;
                        /* margin:20px; */
                        cursor:pointer;
                        overflow:hidden;
                        padding:10px 30px;
                        text-align:center;
                        position:relative;
                        background-color:#7f8fa6;
                    }
                    input.upload {
                        right:0;
                        margin:0;
                        bottom:0;
                        padding:0;
                        opacity:0;
                        height:300px;
                        outline:none;
                        cursor:inherit;
                        position:absolute;
                        font-size:1000px !important;
                    }
                    span.upload-image {
                        margin:5px;
                        display:block;
                    }
                    span.upload-vidio {
                        margin:5px;
                        display:block;
                    }
                </style>
            </div>
        </div>
        <input type="hidden" name="user_id" id="user_id" value="{{ Auth::user()->id }}">
    </form>
    <div class="modal fade modalbox" id="ModalBasic" data-backdrop="static" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">previous picture </h5>
                    <a href="javascript:;" data-dismiss="modal">Close</a>
                </div>
                <div class="modal-body">
                    <img src="{{ url('storage/img-user/'. Auth::user()->image_teknisi) }}" alt="" width="100%">
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
<script>
    $('.upload-attachment').change(function(){
        $(this).closest('.browse-wrap').next('.upload').append('<div class="alert alert-success mb-2 alert-dismissible fade show" role="alert" sty><small>Image Has Been Selected</small><button type="button" class="close" data-dismiss="alert" aria-label="Close"><ion-icon name="close-outline"></ion-icon></button></div>');
    });
    $(document).ready(function(){
        Helper.loadingStop();
    })

    $(document).ready(function (e) {
        $.ajaxSetup({
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('#laravel-ajax-file-upload').submit(function(e) {
            Helper.loadingStart();
            e.preventDefault();
            var formData = new FormData(this);

            $.ajax({
                type:'POST',
                url: Helper.url('/b2b/profile/edit/' + $('#user_id').val()),
                data: formData,
                cache:false,
                contentType: false,
                processData: false,
                success: (data) => {
                    $('#success_message').append('<div class="alert alert-primary mb-2 alert-dismissible fade show" role="alert" sty><small>Profile Has Been Updated</small><button type="button" class="close" data-dismiss="alert" aria-label="Close"><ion-icon name="close-outline"></ion-icon></button></div>');
                    Helper.loadingStop();
                    window.location.href = Helper.redirectUrl('/b2b/profile/show');
                },
                error: function(error){
                    Helper.loadingStop();
                    $('#DialogIconedDanger').modal('show');
                    // console.log(data);
                }
            });
        });
    });
</script>
@endsection