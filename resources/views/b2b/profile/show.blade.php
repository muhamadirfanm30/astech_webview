@extends('b2b.master')
@section('content')
    <div class="appHeader bg-primary text-light">
        <div class="left">
            <a href="javascript:;" class="headerButton goBack">
                <ion-icon name="chevron-back-outline"></ion-icon>
            </a>
        </div>
        <div class="pageTitle">Profile</div>
        <!-- <div class="right">
            <a href="javascript:;" class="headerButton">
                <ion-icon name="mail-outline"></ion-icon>
            </a>
            <a href="javascript:;" class="headerButton">
                <ion-icon name="person-add-outline"></ion-icon>
            </a>
        </div> -->
    </div>


    <div class="section mt-2">
        <div class="profile-head">
            <div class="avatar">
                <img src="{{ url('storage/img-user/'. Auth::user()->image_teknisi) }}" alt="avatar" class="imaged w64 rounded">
            </div>
            <div class="in">
                <h3 class="name">{{ Auth::user()->name }}</h3>
                <!-- <h5 class="subtext">Programmer</h5> -->
            </div>
        </div>
    </div>
<hr>
    <div class="section mt-1 mb-2">
        <div class="profile-info">
            <div class=" bio">
                <div class="row">
                    <div class="col-3">
                        <label style="font-weight: bold; font-size:15px">Email </label>
                    </div>
                    <div class="col-9">
                        <h5 class="text-right">{{ Auth::user()->email }}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <label style="font-weight: bold; font-size:15px">Nomor Ponsel </label>
                    </div>
                    <div class="col-6">
                        <h5 class="text-right">{{ Auth::user()->phone }}</h5>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <a href="{{ url('/b2b/profile/update/'.Auth::user()->id) }}" class="btn btn-primary btn-block btn-sm" onclick="Helper.loadingStart()">Update Profile</a>
                    </div>
                </div>
                <hr>

                <!-- <div class="row">
                    <div class="col-md-12" style="margin-bottom: 15px;">
                        <input type="text" class="form-control" placeholder="Old Password">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" style="margin-bottom: 15px;">
                        <input type="text" class="form-control" placeholder="New Password">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" style="margin-bottom: 15px;">
                        <input type="text" class="form-control" placeholder="Confirm Password">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary btn-block btn-sm">Update Password</button>
                    </div>
                </div> -->
            </div>
            <!-- <div class="link">
                <a href="#">Paris</a>,
                <a href="#">France</a>
            </div> -->
        </div>
    </div>
    

@endsection
@section('script')
<script>
    $(document).ready(function(){
        Helper.loadingStop();
    })
</script>
@endsection