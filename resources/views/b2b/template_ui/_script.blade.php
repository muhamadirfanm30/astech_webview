<!-- ///////////// Js Files ////////////////////  -->
<!-- Jquery -->
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="{{ asset('assets/js/lib/jquery-3.4.1.min.js') }}"></script>
<!-- Bootstrap-->
<script src="{{ asset('assets/js/lib/popper.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/bootstrap.min.js') }}"></script>
<!-- Ionicons -->
<script type="module" src="{{ asset('https://unpkg.com/ionicons@5.0.0/dist/ionicons/ionicons.js') }}"></script>
<!-- Owl Carousel -->
<script src="{{ asset('assets/js/plugins/owl-carousel/owl.carousel.min.js') }}"></script>
<!-- jQuery Circle Progress -->
<script src="{{ asset('assets/js/plugins/jquery-circle-progress/circle-progress.min.js') }}"></script>
<!-- Base Js File -->
<script src="{{ asset('assets/js/base.js') }}"></script>
<!-- axios -->
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<!-- select2 -->
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<!-- Splide -->
<script src="{{ asset('assets/js/plugins/splide/splide.min.js') }}"></script>
<!-- ProgressBar js -->
<script src="{{ asset('assets/js/plugins/progressbar-js/progressbar.min.js') }}"></script>
<!-- loadash -->
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/lodash.js/0.10.0/lodash.min.js"></script>
<!-- Loader active -->
<script src="{{ asset('assets/js/bootbox.js') }}"></script>
<link rel="stylesheet" href="{{ asset('assets/css-loader.css') }}">

<!-- noty js -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

<!-- izitoast -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/css/iziToast.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/js/iziToast.min.js"></script>


<div id="loading-indikator" class="loader loader-default" data-text="LOADING...">
</div>

<script>
    var globalCRUD = {
        requestAjax: null,
        redirect: '/',
        successHandle: null,
        errorHandle: null,
        table: null,
        form: null,
        modal: null,
        select2Static: function(selector, url, callback = null) {
            Axios.get(url)
                .then(function(response) {
                    data = response.data.data;

                    if (callback !== null) {
                        res = $.map(data, callback);
                    } else {
                        res = $.map(data, function(item) {
                            return {
                                text: item.name,
                                id: item.id
                            }
                        });
                    }

                    $(selector).select2({
                        data: res
                    });
                })
                .catch(function(error) {
                    Helper.handleErrorResponse(error)
                });

            return this;
        },

        select2: function(selector, url = null, callback = null, visible = true) {
            if (url == null) {
                $(selector).select2();
                return this;
            }

            $(selector).select2({
                tags: false,
                ajax: {
                    type: "GET",
                    url: Helper.apiUrl(url),

                    data: function(params) {
                        return {
                            q: params.term
                        };
                    },
                    processResults: function(data) {
                        var res = $.map(data.data, function(item) {
                            return {
                                text: item.name,
                                id: item.id
                            }
                        });

                        if (callback !== null) {
                            var res = $.map(data.data, callback);
                        }
                        return {
                            results: res
                        };
                    }
                },
                closeOnSelect: true,

                containerCss: function (element) {
                    var style = $(element)[0].style;
                    return {
                        display: visible == false ? style.display : style.display.inline
                    };
                }

            });
            return this;
        },

        select2Tags: function(selector, url = null, callback = null) {
            if (url == null) {
                $(selector).select2({
                    tags: true
                });
                return this;
            }

            $(selector).select2({
                ajax: {
                    type: "GET",
                    url: Helper.apiUrl(url),
                    data: function(params) {
                        return {
                            q: params.term
                        };
                    },
                    processResults: function(data) {
                        var res = $.map(data.data, function(item) {
                            return {
                                text: item.name,
                                id: item.id
                            }
                        });

                        if (callback !== null) {
                            var res = $.map(data.data, callback);
                        }
                        return {
                            results: res
                        };
                    }
                },


                tags: true,


            });
            return this;
        },
    }

    var Helper = {
        thousandsSeparators: function(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        },
        loadingStart: function() {
            $('#loading-indikator').addClass('is-active');
        },
        loadingStop: function() {
            $('#loading-indikator').removeClass('is-active');
        },

        url: function(url = '') {
            return '{{ url("/") }}' + url;
        },

        apiUrl: function(url = '') {
            return '{{ url("/") }}/api' + url;
        },

        redirectUrl: function(url) {
            return '{{ url("/") }}' + url;
        },

        frontApiUrl: function(url) {
            return '{{ url("/") }}/admin' + url;
        },

        // axios handle error
        handleErrorResponse: function(error) {
            console.log('handleErrorResponse', error);
            this.loadingStop();
            if (error.response) {
                // console.log(error.response.data);
                // console.log(error.response.status);
                // console.log(error.response.headers);

                if (error.response.status == 422) {

                    messages = error.response.data.data;
                    // clean msg
                    $('.error-validation-mgs').remove();
                    _.each(messages, function(pesan, field) {
                        // append new message
                        $('[name="' + field + '"]').after('<strong><span class="error-validation-mgs" id="error-' + field + '" style="color:red; display:block;">' + pesan[0] + '</span></strong>');
                        // $('[name="' + field + '"]').after('<em class="error invalid-feedback" id="' + field + '-error">' + pesan[0] + '</em>');
                        // stop loop
                        return false;
                    })

                }
                this.errorNotif(error.response.data.msg);
            }

            return this;
        },

        confirm: function(callbackAction, option = null) {
            if (option == null) {
                option = {
                    message: "You sure about this?",
                };
            }
            bootbox.confirm({
                size: 'small',
                centerVertical: true,
                message: option.message,
                className: 'rubberBand animated',
                buttons: {
                    cancel: {
                        label: '<i class="fa fa-times"></i> Cancel'
                    },
                    confirm: {
                        label: '<i class="fa fa-check"></i> Confirm'
                    }
                },
                callback: function(result) {
                    if (result) {
                        callbackAction();
                    }
                }
            });
        },
        
        confirmDelete: function(callbackAction) {
            Helper.confirm(callbackAction, {
                title: "DeLete Data",
                message: "Do you want to delete this data?",
            })
        },

        confirmDeleteAll: function(callbackAction) {
            Helper.confirm(callbackAction, {
                title: "DeLete All Data",
                message: "Do you want to delete this data?",
            })
        },

        infoNotif: function(text) {
            this.loadingStop();

            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": true,
                "positionClass": "toast-top-right",
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": "100000",
                "hideDuration": "100000",
                "timeOut": "100000",
                "extendedTimeOut": "100000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }

            toastr.info(text)

            return this;
        },

        successNotif: function(text) {
            this.loadingStop();

            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": true,
                "positionClass": "toast-top-right",
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": "100000",
                "hideDuration": "100000",
                "timeOut": "100000",
                "extendedTimeOut": "100000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }

            toastr.success(text)

            return this;
        },

        errorNotif: function(text) {
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": true,
                "positionClass": "toast-top-right",
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": "100000",
                "hideDuration": "100000",
                "timeOut": "100000",
                "extendedTimeOut": "100000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }

            toastr.error(text)

            return this;
        },

        warningNotif: function(text) {
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": true,
                "positionClass": "toast-top-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "100000",
                "hideDuration": "100000",
                "timeOut": "100000",
                "extendedTimeOut": "100000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }

            toastr.warning(text)

            return this;
        },
    }

    // axios instance
    const Axios = axios.create({
        baseURL: Helper.url(),
        timeout: 100000,
        headers: {
            'X-Requested-With': 'XMLHttpRequest',
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<script>
    setTimeout(() => {
        notification('notification-welcome', 5000);
    }, 2000);
</script>