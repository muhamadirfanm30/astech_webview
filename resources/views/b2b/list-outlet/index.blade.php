@extends('b2b.master')
@section('content')
    <!-- App Header -->
    <!-- loader -->
    <div id="loader" hidden>
        <div class="spinner-border text-primary" role="status"></div>
    </div>
    <div class="appHeader">
        <div class="left">
            <a href="{{ url('b2b/dashboard') }}" class="headerButton" onclick="Helper.loadingStart()">
                <ion-icon name="chevron-back-outline"></ion-icon>
            </a>
        </div>
        <div class="pageTitle">{{ $getComp->name }}</div>
        <div class="right">
            <!-- <a href="#" class="headerButton">
                <ion-icon name="search-outline"></ion-icon>
            </a> -->
        </div>
    </div>
    <!-- * App Header -->

    <div class="accordion" id="accordionExample3">
        @foreach($showOutlet as $outlet)
            <div class="item">
                <div class="accordion-header">
                    <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#accordion00{{ $outlet->id }}">
                        <img src="{{ asset('shop.png') }}" width="30px" alt="">&nbsp;&nbsp;
                        {{ $outlet->type }} &nbsp; <small>{{ $outlet->name }}</small>
                    </button>
                </div>
                <div id="accordion00{{ $outlet->id }}" class="accordion-body collapse" data-parent="#accordionExample3">
                    <div class="accordion-content">
                        <a href="{{ url('b2b/list-outlet/detail/'. $outlet->id) }}">
                            <div class="row">
                                <div class="col-6"><label style="font-weight: bold; font-size:15px"> Jumlah Unit </label></div>
                                <div class="col-6 text-right">{{ $outlet->jumlah_unit }}</div>
                            </div>
                        </a>
                        
                        <div class="row">
                            <div class="col-6"><label style="font-weight: bold; font-size:15px"> Nama PIC </label></div>
                            <div class="col-6 text-right">{{ $outlet->nama_pic }}</div>
                        </div>
                        <div class="row">
                            <div class="col-6"><label style="font-weight: bold; font-size:15px"> Nomor PIC </label></div>
                            <div class="col-6 text-right">{{ $outlet->nomor_pic }}</div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <!-- href="{{ url('b2b/request-order/create') }}" -->
@endsection
@section('script')
<script>
    $(document).ready(function(){
        Helper.loadingStop();
    })
</script>
<script>
    $(document).on('click', '.create-order', function() {
        // console.log('klik')
        Helper.loadingStart();
        location.href = Helper.url('/b2b/request-order/create');
        
    })
</script>
@endsection