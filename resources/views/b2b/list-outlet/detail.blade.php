@extends('b2b.master')
@section('content')
    <!-- App Header -->
    <!-- loader -->
    <div id="loader" hidden>
        <div class="spinner-border text-primary" role="status"></div>
    </div>
    <div class="appHeader">
        <div class="left">
            <a href="{{ url('b2b/dashboard') }}" class="headerButton" onclick="Helper.loadingStart()">
                <ion-icon name="chevron-back-outline"></ion-icon>
            </a>
        </div>
        <div class="pageTitle">{{ !empty($getOutlet->name) ? $getOutlet->name : '-' }}</div>
        <div class="right">
            <!-- <a href="#" class="headerButton">
                <ion-icon name="search-outline"></ion-icon>
            </a> -->
        </div>
    </div>
    <!-- * App Header -->

    <div class="accordion" id="accordionExample3">
        @foreach($query as $val)
            <div class="item">
                <div class="accordion-header">
                    <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#accordion00{{ $val->id }}">
                        <img src="{{ asset('part-time.png') }}" width="30px" alt="">&nbsp;&nbsp;
                            {{ $val->merek }}
                    </button>
                </div>
                <div id="accordion00{{ $val->id }}" class="accordion-body collapse" data-parent="#accordionExample3">
                    <div class="accordion-content">
                        <a href="{{ url('b2b/list-outlet/detail/'. $val->id) }}">
                            <div class="row">
                                <div class="col-6"><label style="font-weight: bold; font-size:15px"> Unit Ke </label></div>
                                <div class="col-6 text-right">{{ $val->unit_ke }}</div>
                            </div>
                        </a>
                        <div class="row">
                            <div class="col-6"><label style="font-weight: bold; font-size:15px"> Type </label></div>
                            <div class="col-6 text-right">{{ !empty($val->get_type) ? $val->get_type->name : '-' }} {{ !empty($val->get_type) ? $val->get_type->desc : '-' }}</div>
                        </div>
                        <div class="row">
                            <div class="col-6"><label style="font-weight: bold; font-size:15px"> IMEI </label></div>
                            <div class="col-6 text-right">{{ $val->imei }}</div>
                        </div>
                        <div class="row">
                            <div class="col-6"><label style="font-weight: bold; font-size:15px"> Serial Number </label></div>
                            <div class="col-6 text-right">{{ $val->serial_number }}</div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <!-- href="{{ url('b2b/request-order/create') }}" -->
@endsection
@section('script')
<script>
    $(document).ready(function(){
        Helper.loadingStop();
    })
</script>
<script>
    $(document).on('click', '.create-order', function() {
        // console.log('klik')
        Helper.loadingStart();
        location.href = Helper.url('/b2b/request-order/create');
        
    })
</script>
@endsection