<?php

namespace App\Imports;

use App\Model\Master\MsZipcode;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ZipcodeImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new MsZipcode([
            'zip_no'         => $row['zip_no'],
            'ms_district_id' => $row['ms_district_id'],
        ]);
    }
}
