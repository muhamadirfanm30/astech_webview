<?php

namespace App\Imports;

use App\Model\Master\MsProvince;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ProvinceImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {

        return new MsProvince([
            'name'          => $row['name'],
            'ms_country_id' => $row['ms_country_id'],
            'iso_code'      => $row['iso_code'],
        ]);
    }
}
