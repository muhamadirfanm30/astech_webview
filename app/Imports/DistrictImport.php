<?php

namespace App\Imports;

use App\Model\Master\MsDistrict;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class DistrictImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new MsDistrict([
            'name'       => $row['name'],
            'ms_city_id' => $row['ms_city_id'],
        ]);
    }
}
