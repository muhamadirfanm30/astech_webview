<?php

namespace App\Imports;

use App\Model\Master\MsCountry;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class CountryImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new MsCountry([
            'name' => $row['name'],
            'code' => $row['code'],
        ]);
    }
}
