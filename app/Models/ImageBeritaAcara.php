<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ImageBeritaAcara extends Model
{
    protected $table = 'image_berita_acara';
    protected $fillable = [
        'type', 'image_berita_acara', 'ms_b2b_detail'
    ];
}
