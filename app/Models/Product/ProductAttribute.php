<?php

namespace App\Model\Product;

use Illuminate\Database\Eloquent\Model;

class ProductAttribute extends Model
{
    // protected $fillable = [
    //     'name',
    // ];
    protected $guarded = [];

    public function childs()
    {
        return $this->hasMany(ProductAttributeTerm::class, 'product_attribute_id', 'id');
    }

    // public function term()
    // {
    //     return $this->hasMany(ProductAttributeTerm::class, 'product_attribute_id', 'id');
    // }




}
