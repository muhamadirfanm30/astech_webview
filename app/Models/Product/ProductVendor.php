<?php

namespace App\Model\Product;

use App\Model\Master\Vendor;
use App\Model\Master\MsProduct;
use App\Model\Master\MsProductModel;
use App\Model\Master\MsProductCategory;
use Illuminate\Database\Eloquent\Model;

class ProductVendor extends Model
{
    // protected $fillable = [
    //     'name',
    //     'product_part_category_id',
    //     'code',
    // ];
    protected $table = 'product_vendors';

    protected $guarded = [];

    public function product_varian()
    {
        return $this->hasMany(ProductVarian::class, 'product_vendor_id', 'id');
    }

    public function productVarians()
    {
        return $this->hasMany(ProductVarian::class, 'product_vendor_id', 'id');
    }

    public function models()
    {
        return $this->belongsTo(MsProductModel::class, 'product_model_id', 'id');
    }

    // public function product()
    // {
    //     return $this->hasMany(MsProduct::class);
    // }

    public function product()
    {
        return $this->belongsTo(MsProduct::class, 'ms_product_id', 'id');
    }

    public function vendor()
    {
        return $this->belongsTo(Vendor::class, 'vendor_id');
    }
}
