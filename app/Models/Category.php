<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'slug', 'parent_id'
    ];

    public function childs()
    {
        return $this->hasMany(Category::class, 'parent_id', 'id');
    }

    /**
     * get recursive all sub categories of this category.
     *
     * @return mixed
     */
    public function childrens()
    {
        return $this->hasMany(Category::class, 'parent_id')->with('childs');
    }
}
