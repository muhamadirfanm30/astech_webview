<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Notification extends Model
{
    protected $table = 'notifications';

    public static function issetNew()
    {
        return DB::table('notifications')->limit(1)->whereNull('read_at')->where('notifiable_id', Auth::id())->count();
    }

    public static function getList()
    {
        return DB::table('notifications')->where('notifiable_id', Auth::id())->orderBy('created_at', 'desc')->paginate(50);
    }

    public static function unread()
    {
        return DB::table('notifications')->limit(5)->whereNull('read_at')->where('notifiable_id', Auth::id())->get();
    }

    public static function unreadCount($user_id)
    {
        return DB::table('notifications')->whereNull('read_at')->where('notifiable_id', $user_id)->count();
    }

    public static function readAndUnread()
    {
        return DB::table('notifications')->limit(5)->where('notifiable_id', Auth::id())->orderBy('created_at', 'desc')->get();
    }

    public static function markAsRead($id)
    {
        return DB::table('notifications')->where('id', $id)->update(['read_at' => now()]);
    }

    public static function markAsReadUser($id)
    {
        return DB::table('notifications')->where('notifiable_id', $id)->update(['read_at' => now()]);
    }

    public static function tiketUnread($user_id)
    {
        return DB::table('notifications')
            ->whereIn('type', ['App\Notifications\Ticket\TicketReplyNotification'])
            ->whereNull('read_at')
            ->where('notifiable_id', $user_id)
            ->count();
    }

    public static function sendToAdmin($notificationInstance)
    {
        $administrator = User::getAdminRole();
        foreach ($administrator as $user) {
            \Notification::send($user, $notificationInstance);
        }
    }

    public static function sendToUser($notificationInstance)
    {
        $user = User::where('id', Auth::id())->first();
        \Notification::send($user, $notificationInstance);
    }
}
