<?php

namespace App\Model\Master;


use Illuminate\Database\Eloquent\Model;

class EmailStatusOrder extends Model
{
    protected $guarded = [];
    protected $table = 'email_status_order';

    public function order_status()
    {
        return $this->belongsTo(OrdersStatus::class, 'orders_statuses_id', 'id');
    }

}
