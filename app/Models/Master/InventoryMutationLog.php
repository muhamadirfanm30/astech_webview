<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

use function App\Helpers\thousanSparator;

class InventoryMutationLog extends Model
{
    protected $table = 'inventories_mutation_logs';

    protected $guarded = [];

    public function batch_item()
    {
        return $this->belongsTo(BatchItem::class, 'ms_batch_item_id');
    }

    public function inventory_in()
    {
        return $this->belongsTo(Inventory::class, 'inventories_id_in');
    }

    public function inventory_out()
    {
        return $this->belongsTo(Inventory::class, 'inventories_id_out');
    }



}
