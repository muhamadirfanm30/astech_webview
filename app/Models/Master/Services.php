<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Services extends Model
{
    // use SoftDeletes;

    protected $table = 'ms_services';
    protected $guarded = [];
    protected $appends = ['link_image'];

    public static function getImagePathUpload()
    {
        return 'public/services';
    }

    public function getLinkImageAttribute()
    {
        return ($this->images == null) ? asset('/storage/services/service_not_found.jpeg') : asset('/storage/services/' . $this->images);
    }

    public function symptom()
    {
        return $this->hasMany(MsSymptom::class, 'ms_services_id', 'id');
    }

    public function symptoms()
    {
        return $this->hasMany(MsSymptom::class, 'ms_services_id', 'id');
    }

    public function order()
    {
        return $this->hasMany(Order::class, 'ms_services_id', 'id');
    }

    public function technician_sparepart()
    {
        return $this->hasMany(TechnicianSparepart::class, 'ms_services_id', 'id');
    }
}
