<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class BatchStatusShipment extends Model
{
    protected $guarded = [];
    // protected $table = 'batch_status_shipments';

    public function batch_shipment_history()
    {
        return $this->hasMany(BatchShipmentHistory::class);
    }

}
