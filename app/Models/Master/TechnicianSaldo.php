<?php

namespace App\Model\Master;

use App\Model\Technician\Technician;
use Illuminate\Database\Eloquent\Model;


class TechnicianSaldo extends Model
{
    protected $table = 'technician_saldo';
    protected $guarded = [];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function teknisi()
    {
        return $this->hasOne(Technician::class,'id');
    }

    public function orders()
    {
        return $this->hasMany(ServiceDetail::class, 'technicians_id')->whereHas('order', function ($query) {
            $query->where('transfer_status', 0)->whereMonth('updated_at', date('m'));
        })->with(['order.saldo', 'price_services.service_type'])->groupBy('id');
    }
}
