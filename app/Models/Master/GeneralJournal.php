<?php

namespace App\Model\Master;

use App\User;
use Illuminate\Database\Eloquent\Model;
use App\Model\Master\OrdersDataExcelLog;

class GeneralJournal extends Model
{
    protected $table = 'general_journal';

    protected $guarded = [];

    protected $appends = [
        'total_kredit',
        'total_debit',
        'total_balance'
    ];



    public static function getImagePathUpload()
    {
        return 'public/general-journal';
    }

    public function general_journal_detail()
    {
        return $this->hasMany(GeneralJournalDetail::class, 'general_journal_id', 'id');
    }

    public function general_journal_log()
    {
        return $this->hasMany(GeneralJournalLog::class, 'general_journal_id', 'id');
    }

    public function getTotalBalanceAttribute() {
        if($this->general_journal_detail()) {
            return $this->general_journal_detail()->orderBy('id', 'desc')->value('balance');
        }
        return 0;

    }
    public function getTotalKreditAttribute() {
        if($this->general_journal_detail()) {
            return $this->general_journal_detail()->sum('kredit');
        }
        return 0;

    }
    public function getTotalDebitAttribute() {
        if($this->general_journal_detail()) {
            return $this->general_journal_detail()->sum('debit');
        }
        return 0;

    }


}
