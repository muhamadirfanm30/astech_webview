<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class MsCity extends Model
{
    protected $table = 'ms_cities';

    protected $fillable = [
        'name', 'ms_province_id', 'meta'
    ];

    // protected $casts = [
    //     'meta' => 'array',
    // ];

    public function province()
    {
        return $this->belongsTo(MsProvince::class, 'ms_province_id');
    }

    public function districts()
    {
        return $this->hasMany(MsDistrict::class);
    }

    public function villages()
    {
        return $this->hasManyThrough(MsVillage::class, MsDistrict::class);
    }

}
