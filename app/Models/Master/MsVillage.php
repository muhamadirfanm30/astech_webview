<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class MsVillage extends Model
{
    protected $table = 'ms_villages';

    protected $fillable = [
        'name', 'meta', 'ms_district_id'
    ];

    // protected $casts = [
    //     'meta' => 'array',
    // ];

    public function district()
    {
        return $this->belongsTo(MsDistrict::class, 'ms_district_id');
    }
   
}
