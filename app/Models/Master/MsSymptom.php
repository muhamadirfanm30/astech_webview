<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MsSymptom extends Model
{
    // use SoftDeletes;

    protected $table = 'ms_symptoms';

    protected $guarded = [];

    protected $appends = ['link_image'];

    public static function getImagePathUpload()
    {
        return 'public/symptom';
    }

    public function getLinkImageAttribute()
    {
        return ($this->images == null) ? asset('/storage/services/service_not_found.jpeg') : asset('/storage/services-category/' . $this->images);
    }

    public function services()
    {
        // return $this->belongsTo(Services::class, 'ms_services_id')->withTrashed();
        return $this->belongsTo(Services::class, 'ms_services_id');
    }

    public function service_types()
    {
        return $this->hasMany(MsServicesType::class, 'ms_symptoms_id');
    }
}
