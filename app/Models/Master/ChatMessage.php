<?php

namespace App\Model\Master;

use App\User;
use Illuminate\Database\Eloquent\Model;

class ChatMessage extends Model
{
    protected $table = 'chat_messages';
    protected $guarded = [];

    protected $appends = ['order', 'attachment_src'];

    public function getOrderAttribute()
    {
        return json_decode($this->order_obj);
    }

    public function getAttachmentSrcAttribute()
    {
        return ($this->filename == null) ? '' : asset('storage/user/chat/' . $this->filename);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function chat()
    {
        return $this->belongsTo(Chat::class, 'chat_id');
    }
}
