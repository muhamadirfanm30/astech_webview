<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class WebSetting extends Model
{
    protected $fillable = [
        'title_setting',
        'metta_setting',
        'logo',
        'image_url'
    ];

    protected $appends = [
        'logo_src'
    ];

    public static function getImagePathUpload()
    {
        return 'public/web_setting_image/';
    }

    public function getLogoSrcAttribute()
    {
        return asset('public/web_setting_image/' . $this->logo);
    }
}
