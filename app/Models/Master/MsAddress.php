<?php

namespace App\Model\Master;

use App\User;
use Illuminate\Database\Eloquent\Model;

class MsAddress extends Model
{
    protected $table = 'ms_addresses';

    protected $fillable = [
        'address',
        'type',
        'latitude',
        'longitude',
        'location_google',
        'location_customer',
        'phone1',
        'phone2',
        'is_main',
        'is_teknisi',
        'user_id',
        'ms_city_id',
        'ms_district_id',
        'ms_village_id',
        'ms_zipcode_id',
        'ms_address_type_id',
        'cities',
        'province'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function city()
    {
        return $this->belongsTo(MsCity::class, 'ms_city_id');
    }

    public function district()
    {
        return $this->belongsTo(MsDistrict::class, 'ms_district_id');
    }

    public function village()
    {
        return $this->belongsTo(MsVillage::class, 'ms_village_id');
    }

    public function zipcode()
    {
        return $this->belongsTo(MsZipcode::class, 'zip_no');
    }

    public function types()
    {
        return $this->belongsTo(AddressType::class, 'ms_address_type_id');
    }
}
