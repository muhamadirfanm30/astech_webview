<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class ConfrimPayment extends Model
{
    protected $table = 'confirmation_payment';
    protected $fillable = [
        'id',
        'a_n_bank',
        'transfer_date',
        'nominal',
        'attachment',
        'note',
        'user_id',
        'ms_order_id'
    ];

    public static function getImagePathUpload()
    {
        return 'public/buktiTransfer';
    }
}
