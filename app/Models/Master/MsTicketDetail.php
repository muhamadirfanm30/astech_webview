<?php

namespace App\Model\Master;

use App\User;
use Illuminate\Database\Eloquent\Model;

class MsTicketDetail extends Model
{
    protected $table = 'ms_tickets_details';

    protected $guarded = [];

    protected $appends = [
        'attachment_src',
    ];

    public function ticket()
    {
        return $this->belongsTo(MsTicket::class, 'ms_ticket_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }

    public function getAttachmentSrcAttribute()
    {
        if ($this->attachment == null) {
            return null;
        }

        return asset('/storage/attachment/' . $this->attachment);
    }
}
