<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class ImageUpload extends Model
{
    protected $table = 'galery_image';

    protected $fillable = [
        'filename', 'alternatif_name', 'title', 'desc'
    ];

    protected $appends = ['src'];

    public function getSrcAttribute()
    {
        return asset('admin/storage/images/' . $this->filename);

        
    }
}
