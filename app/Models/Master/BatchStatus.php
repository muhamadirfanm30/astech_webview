<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class BatchStatus extends Model
{
    protected $fillable = [
        'name',
    ];

    public function batch_item()
    {
        return $this->hasMany(BatchItem::class);
    }

}
