<?php

namespace App\Model\Master;

use App\Model\Technician\Technician;
use App\Model\Master\TechnicianSaldo;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceDetail extends Model
{
    // use SoftDeletes;

    protected $table = 'service_details';

    protected $guarded = [];

    protected $appends = ['teknisi'];

    public function getTeknisiAttribute()
    {
        return json_decode($this->teknisi_json);
    }

    // mutator

    public function saldo()
    {
        return $this->hasOne(TechnicianSaldo::class, 'technician_id');
    }

    public function gettotalbersih()
    {
        return $this->hasMany(TechnicianSaldo::class, 'order_id', 'id');
    }

    public function setScheduleAttribute($value)
    {
        $this->attributes['schedule'] = date("Y-m-d H:i:s", strtotime($value));
    }

    public function price_services()
    {
        return $this->belongsTo(MsPriceService::class, 'ms_price_services_id');
    }

    public function ratings()
    {
        return $this->hasMany(Rating::class, 'service_detail_id');
    }

    public function reviews()
    {
        return $this->hasMany(Review::class, 'service_detail_id');
    }

    public function rating_customer()
    {
        return $this->hasOne(Rating::class, 'service_detail_id')->where('user_id', \Auth::id());
    }

    public function review_customer()
    {
        return $this->hasOne(Review::class, 'service_detail_id')->where('user_id', \Auth::id());
    }

    public function order()
    {
        return $this->belongsTo(Order::class, 'orders_id');
    }

    


    public function price_service()
    {
        return $this->belongsTo(MsPriceService::class, 'ms_price_services_id');
    }

    public function services_type()
    {
        return $this->belongsTo(MsServicesType::class, 'ms_services_types_id');
    }

    public function symptom()
    {
        return $this->belongsTo(MsSymptom::class, 'ms_symptoms_id');
    }

    public function service_status()
    {
        return $this->belongsTo(MsServiceStatus::class, 'ms_service_statuses_id');
    }

    public function technician()
    {
        return $this->belongsTo(Technician::class, 'technicians_id');
    }
}
