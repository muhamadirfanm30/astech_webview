<?php

namespace App\Model\Master;

use App\User;
use App\Model\Master\OrdersDataExcel;
use Illuminate\Database\Eloquent\Model;

class GeneralJournalLog extends Model
{
    protected $table = 'general_journal_logs';

    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

}
