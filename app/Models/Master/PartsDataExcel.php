<?php

namespace App\Model\Master;

use App\User;
use Illuminate\Database\Eloquent\Model;

class PartsDataExcel extends Model
{
    protected $table = 'part_data_excel';

    protected $guarded = [];

    public function order_data_excel()
    {
        return $this->belongsTo(OrdersDataExcel::class, 'orders_data_excel_id');
    }

    public function part_data_excel_bundle()
    {
        return $this->belongsTo(PartDataExcelStockBundle::class, 'part_data_excel_bundle_id');
    }

    public function part_data_stock_inventory()
    {
        return $this->belongsTo(PartDataExcelStockInventory::class, 'part_data_stock_inventories_id');
    }

    public function part_data_stock_bundle_details()
    {
        return $this->belongsTo(PartDataExcelStockBundleDetail::class, 'part_data_stock_bundle_details_id');
    }

    public function pds_inventory_mutation_histories()
    {
        return $this->hasMany(PDSMutationJoinHistories::class, 'part_data_excel_id', 'id');
    }



}
