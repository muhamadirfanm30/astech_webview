<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class Religion extends Model
{
    protected $fillable = [
        'name',
    ];
}
