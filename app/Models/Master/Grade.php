<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class Grade extends Model
{
    protected $table = 'grades';
    protected $fillable = [
        'name',
        'min_value',
        'max_value',
    ];
}
