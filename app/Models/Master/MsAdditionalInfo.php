<?php

namespace App\Models\Master;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class MsAdditionalInfo extends Model
{
    protected $fillable = [
        'ms_religion_id',
        'ms_marital_id',
        'first_name',
        'last_name',
        'date_of_birth',
        'place_of_birth',
        'gender',
        'picture',
        'user_id'
    ];

    protected $appends = [
        'avatar',
        'sex',
    ];

    protected $casts = [
        'date_of_birth' => 'date:Y-m-d',
    ];

    public static function getImagePathUpload()
    {
        return 'public/ImageProfile';
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function religion()
    {
        return $this->belongsTo(Religion::class, 'ms_religion_id');
    }

    public function marital()
    {
        return $this->belongsTo(Marital::class, 'ms_marital_id');
    }

    # append attribute
    # =================
    /**
     * Get Image full path
     *
     * @return string
     */
    public function getAvatarAttribute()
    {
        $src = asset('/storage/user/profile/avatar/' . $this->picture);

        return empty($this->picture) ? 'https://demo.dashboardpack.com/architectui-html-free/assets/images/avatars/9.jpg' : $src;
    }

    public function getSexAttribute(){
        switch ($this->gender) {
            case 0:
                return 'Male';
                break;

            case 1:
                return 'Female';
                break;

            case 2:
                return 'Another one';
                break;
            
            default:
                return '-';
                break;
        }
    }

}
