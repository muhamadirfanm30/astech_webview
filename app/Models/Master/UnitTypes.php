<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class UnitTypes extends Model
{
    protected $table = "unit_types";
    protected $guarded = [];

    public function price_item()
    {
        return $this->hasMany(MsPriceItems::class, 'ms_unit_type_id');
    }
}


