<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

use function App\Helpers\thousanSparator;

class MutationJoinHistories extends Model
{
    protected $table = 'mutation_join_histories';

    protected $guarded = [];

    public function history_out()
    {
        return $this->belongsTo(InventoryMutationHistories::class, 'history_out');
    }

    public function history_in()
    {
        return $this->belongsTo(InventoryMutationHistories::class, 'history_in');
    }

    public function mutation_join() {
        return $this->hasMany(InventoryMutationHistories::class, 'mutation_join_id');
    }





}
