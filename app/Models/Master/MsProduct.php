<?php

namespace App\Model\Master;

use App\Model\Master\Vendor;
use App\Model\Product\ProductBrand;
use App\Model\Master\MsProductModel;
use App\Model\Product\ProductVendor;
use App\Model\Product\ProductAttribute;
use Illuminate\Database\Eloquent\Model;
use App\Model\Product\ProductAdditional;
use App\Model\Product\ProductCategoryCombine;
use App\Model\Product\ProductImage;
use App\Model\Product\ProductStatus;
use App\Model\Product\ProductVarian;

use function App\Helpers\thousanSparator;

class MsProduct extends Model
{
    protected $table = 'ms_products';

    protected $casts = [
        'publish_at' => 'datetime:Y-m-d',
    ];

    protected $appends = ['product_category_name', 'main_image'];

    protected $guarded = [];

    public function model()
    {
        return $this->belongsTo(MsProductModel::class, 'product_model_id', 'id');
    }

    public function additional()
    {
        return $this->belongsTo(ProductAdditional::class, 'product_additionals_id');
    }

    public function product_vendor()
    {
        return $this->hasMany(ProductVendor::class, 'ms_product_id');
    }

    public function inventories()
    {
        return $this->hasMany(Inventory::class, 'product_id');
    }

    public function product_category_combine()
    {
        return $this->hasMany(ProductCategoryCombine::class, 'ms_product_id');
    }

    public function item_detail()
    {
        return $this->hasMany(ItemDetail::class, 'ms_product_id');
    }

    public function batch_items()
    {
        return $this->hasMany(BatchItem::class, 'product_id');
    }

    public function status()
    {
        return $this->belongsTo(ProductStatus::class, 'product_status_id', 'id');
    }

    public function gallery()
    {
        return $this->belongsTo(ImageUpload::class, 'galery_image_id', 'id');
    }

    public function product_images()
    {
        return $this->hasMany(ProductImage::class, 'product_id', 'id');
    }

    public function getProductAttributeHeaderAttribute($value)
    {
        return json_decode($value);
    }

    public function productVendors()
    {
        return $this->hasMany(ProductVendor::class, 'ms_product_id');
    }

    public function productCombineCategories()
    {
        return $this->hasMany(ProductCategoryCombine::class, 'ms_product_id');
    }

    public function getProductCategoryNameAttribute()
    {
        $name = '';
        $productCombineCategories = $this->productCombineCategories;
        foreach ($productCombineCategories as $productCombine) {
            $name .= isset($productCombine->product_category->name) ??  '' . ', ';
        }

        return trim($name, ', ');
    }

    public function getStockTotalAttribute()
    {
        $stock = $this->stock;
        $vendor_stock = 0;
        $varian_stock = 0;
        $vendors = $this->productVendors;
        foreach ($vendors as $productVendor) {
            $vendor_stock += $productVendor->stock;
            $varians = $productVendor->productVarians;
            foreach ($varians as $productVarians) {
                $varian_stock += $productVarians->stock;
            }
        }

        if ($varian_stock > 0) {
            return $varian_stock;
        }

        if ($vendor_stock > 0) {
            return $vendor_stock;
        }

        return $stock;
    }

    public function getMainImageAttribute()
    {
        if ($this->gallery == null) {
            return asset('/storage/user/profile/avatar/image-not-found.png');
        }

        if ($this->gallery->filename == null || $this->gallery->filename == '') {
            return asset('/storage/user/profile/avatar/image-not-found.png');
        }

        return asset('admin/storage/image/' . $this->gallery->filename);
    }

    public static function getNameProduct($product_id, $product_varian_id, $product_vendor_id)
    {
        $product = MsProduct::where('id', $product_id)->first();
        $name = $product->name;

        $productVendor = ProductVendor::with('vendor')->where('id', $product_vendor_id)->first();
        if ($productVendor != null) {
            $name .= ' ' . $productVendor->vendor->name;
        }

        $productVarians = ProductVarian::where('id', $product_varian_id)->first();
        if ($productVarians != null) {
            $variation = json_decode($productVarians->variation);
            foreach ($variation as $attribute => $term) {
                if (!is_numeric($attribute)) {
                    $name .= ' ' . $attribute . ' : ' . $term . ' ';
                }
            }
        }
        return ucfirst($name);
    }
}
