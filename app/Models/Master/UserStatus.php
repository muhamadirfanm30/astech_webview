<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class UserStatus extends Model
{
    protected $table = "users_status";
    protected $fillable =[
        'name'
    ];
}
