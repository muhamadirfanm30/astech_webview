<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class CurriculumSequence extends Model
{
    protected $table = 'curriculum_squence';
    protected $fillable = [
        'sequence_name',
        'dev_program_id',
        'curriculum_id'
    ];

    public function developmentprog()
    {
        return $this->belongsTo(DevelopmentProgram::class, 'dev_program_id');
    }

    public function curriculum()
    {
        return $this->belongsTo(Curriculum::class, 'curriculum_id');
    }
}
