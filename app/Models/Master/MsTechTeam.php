<?php

namespace App\Model\Master;

use App\Model\Master\TeamTechnician;
use App\Model\Technician\Technician;
use Illuminate\Database\Eloquent\Model;

class MsTechTeam extends Model
{
    protected $table = 'ms_tech_teams';

    protected $guarded = [];

    public function order()
    {
        return $this->belongsTo(Orders::class, 'order_id');
    }

    public function service_detail()
    {
        return $this->belongsTo(ServiceDetail::class, 'service_details_id');
    }

    public function technician()
    {
        return $this->belongsTo(Technician::class, 'technicians_id');
    }

    public function ticket()
    {
        return $this->belongsTo(MsTicket::class, 'ms_tickets_id');
    }

    public function team_technician()
    {
       return $this->hasMany(TeamTechnician::class, 'team_id');
    }

    public function team_invite() {
        return $this->hasMany(TeamInvite::class, 'team_id');
    }

}
