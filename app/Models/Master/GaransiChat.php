<?php

namespace App\Model\Master;

use App\User;
use Illuminate\Database\Eloquent\Model;

class GaransiChat extends Model
{
    protected $table = 'garansi_chats';

    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }

    public function garansi_details()
    {
        return $this->hasMany(GaransiChatDetail::class, 'garansi_chat_id');
    }
}
