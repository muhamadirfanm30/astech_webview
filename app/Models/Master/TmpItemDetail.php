<?php

namespace App\Model\Master;
use Illuminate\Database\Eloquent\Model;

class TmpItemDetail extends Model
{
    protected $table = 'tmp_item_details';

    protected $guarded = [];

    public function product()
    {
        return $this->belongsTo(MsProduct::class, 'ms_products_id');
    }

    public function inventory()
    {
        return $this->belongsTo(Inventory::class, 'inventory_id');
    }

    public function order()
    {
        return $this->belongsTo(Order::class, 'orders_id');
    }
}
