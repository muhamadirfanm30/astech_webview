<?php

namespace App\Model\Master;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Badge extends Model
{
    protected $table = 'badge';

    protected $guarded = [];

    protected $appends = [
        'user_emails', 'user_ids'
    ];

    public function getUserEmailsAttribute() {
        return User::where('badge_id', $this->id)->pluck('email');
    }

    public function getUserIdsAttribute() {
        return User::where('badge_id', $this->id)->pluck('id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'badge_id', 'id');
    }

}
