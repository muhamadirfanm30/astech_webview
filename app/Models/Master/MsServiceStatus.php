<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class MsServiceStatus extends Model
{
    protected $table = 'ms_service_statuses';
    protected $guarded = [];

    public function service_detail()
    {
        return $this->hasMany(ServiceDetail::class, 'ms_service_statuses_id');
    }
}
