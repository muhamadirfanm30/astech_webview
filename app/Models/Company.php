<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = 'companies';

    protected $fillable = [
        'name',
        'npwp',
        'email',
        'nomor_rekening',
        'nama_bank',
        'nama_pic',
        'alamat',
        'phone',
    ];
}
