<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\RequestOrderDetail;

class B2BAssignTeknisi extends Model
{
    protected $table = 'business_to_business_schedule_teknisi';

    protected $guarded = [];

    protected $appends = [
        'img_checkin_url',
        'img_checkout_url',
    ];

    public function getImgCheckinUrlAttribute()
    {
        return asset('storage/img-checkin/'.str_replace(' ', '%20', $this->attachment_checkin));
    }

    public function getImgCheckoutUrlAttribute()
    {
        return asset('storage/img-checkout/'.str_replace(' ', '%20', $this->attachment_checkout));
    }

    public function get_teknisi()
    {
        return $this->belongsTo(User::class, 'b2b_technician_id', 'id');
    }

    public function get_order()
    {
        return $this->belongsTo(RequestOrderDetail::class, 'order_id', 'id');
    }
}
