<?php

namespace App\Model\Technician;

use Illuminate\Database\Eloquent\Model;

class JobTitleCategory extends Model
{
    protected $fillable = [
        'name',
    ];
}
