<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\RequestOrderDetail;
use App\Models\BusinessToBusinessOutletDetailTransaction;

class RequestOrders extends Model
{
    protected $table = "ms_request_orders";
    protected $fillable = [
        'request_code',
        'ms_outlet_id',
        'is_tmp',
        'ms_request_order_detail',
        'user_id',
        'read_at',
    ];

    // public static function getImagePathUpload()
    // {
    //     return 'public/request_order_image/';
    // }
    public function get_user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function get_history_order()
    {
        return $this->hasMany(RequestOrderDetail::class, 'request_code', 'request_code');
    }

    // public function b2b_detail_trans()
    // {
    //     return $this->belongsTo(BusinessToBusinessOutletDetailTransaction::class, 'ms_transaction_b2b_detail', 'id');
    // }

    // public function b2b_outlet_detail_transactions()
    // {
    //     return $this->belongsTo(BusinessToBusinessOutletDetailTransaction::class, 'ms_b2b_detail', 'id');
    // }

}
