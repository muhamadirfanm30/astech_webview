<?php

namespace App\Models;

use App\Models\B2BDataTransactions;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use App\Models\B2BAssignTeknisi;

class RequestOrderDetail extends Model
{
    protected $table = "ms_request_order_detail";
    protected $fillable = [
        'request_code',
        'ms_outlet_id',
        'ms_transaction_b2b_detail',
        'service_type',
        'user_id',
        'read_at',
        'status_quotation',
        'ms_b2b_detail',
        'vidio',
        'remark',
        'image',
        'teknisi_id'
    ];

    protected $appends = [
        'url_vidio',
        'url_gambar',
    ];

    public function getUrlVidioAttribute()
    {
        return asset('/videos/'.str_replace(' ', '%20', $this->vidio));
    }

    public function getUrlGambarAttribute()
    {
        return asset('/storage/media-request-order/'.str_replace(' ', '%20', $this->image));
    }

    public function get_quot()
    {
        return $this->belongsTo(B2BDataTransactions::class, 'id', 'id_transaction');
    }

    public function get_b2b_order_detail()
    {
        return $this->belongsTo(BusinessToBusinessOutletDetailTransaction::class, 'ms_transaction_b2b_detail', 'id');
    }

    public function get_schedule()
    {
        return $this->belongsTo(B2BAssignTeknisi::class, 'id', 'order_id');
    }

    public function get_b2b_detail()
    {
        return $this->belongsTo(BusinessToBusinessOutletDetailTransaction::class, 'ms_b2b_detail', 'id');
    }

    public function get_outlet()
    {
        return $this->belongsTo(BusinessToBusinessOutletTransaction::class, 'ms_outlet_id', 'id');
    }

    public function get_user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
