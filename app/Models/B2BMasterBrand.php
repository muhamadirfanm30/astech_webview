<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class B2BMasterBrand extends Model
{
    protected $table = 'business_to_business_master_brand';

    protected $guarded = [];
}
