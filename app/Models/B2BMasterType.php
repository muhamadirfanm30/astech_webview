<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class B2BMasterType extends Model
{
    protected $table = 'business_to_business_master_type';

    protected $guarded = [];
}
