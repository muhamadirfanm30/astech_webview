<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ImageQuotation extends Model
{
    protected $table = 'image_quotation';
    protected $fillable = [
        'type', 'image_quot', 'ms_b2b_detail'
    ];
}
