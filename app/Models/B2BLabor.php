<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Models\RequestOrderDetail;
use App\Models\Master\JasaExcel;

class B2BLabor extends Model
{
    protected $table = 'bussiness_to_bussiness_labor';

    protected $guarded = [];

    public function get_labor_name()
    {
        return $this->belongsTo(JasaExcel::class, 'name', 'id');
    }
}
