<?php

namespace App\Services;

use App\Model\Menu;
use Illuminate\Database\QueryException;
use Spatie\Permission\Models\Permission;

class MenuService
{
    public $menu;

    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\Model\Menu
     *
     * @throws \Exception
     */
    public function create(array $data)
    {
        $menuData = [
            'name' => $data['name'],
            'url' => $data['url'],
            'module' => $data['module'],
            'icon' => $data['icon'],
            'permission_id' => 17, // 17 is permission for menu
            'parent_id' => $this->getParentId($data),
        ];

        try {
            return Menu::create($menuData);
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * update data
     *
     * @param array $data
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function update(array $data)
    {
        $menuData = [
            'name' => $data['name'],
            'url' => $data['url'],
            'module' => $data['module'],
            'icon' => $data['icon'],
        ];

        $menu = $this->getMenu();

        try {
            return $menu->update($menuData);
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function delete()
    {
        $menu = $this->getMenu();

        if ($menu->childs->count() > 0) {
            throw new \Exception('tidak bisa dihapus terdapat childs pada menu ini');
        }

        try {
            return $menu->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function checkPermission(int $permission_id)
    {
        $permission = Permission::where('id', $permission_id)->first();
        if ($permission == null) {
            throw new \Exception('permission yg dipilih tidak ada');
        }

        return $permission->id;
    }

    public function getParentId(array $data)
    {
        $parent_id = null;
        if (isset($data['parent_id'])) {
            $menu = Menu::where('id', $data['parent_id'])->first();
            if ($menu) {
                $parent_id = $menu->id;
            }
        }
        return $parent_id;
    }

    public function setMenu(Menu $menu)
    {
        $this->menu = $menu;
        return $this;
    }

    public function getMenu()
    {
        return $this->menu;
    }

    public function updateMoveMenu($request) {

        $data = json_decode($request->data);
        $readbleArray = $this->parseJsonArray($data);

        foreach($readbleArray as $key => $row){
            $row['parentID'] = $row['parentID'] == 0 ? null : $row['parentID'];
            $update = Menu::where('id',$row['id'])->update(['parent_id' => $row['parentID'], 'display_order' => $key]);
        }
    }

    public function parseJsonArray($jsonArray, $parentID = 0) {
        $return = array();
        foreach ($jsonArray as $subArray) {
          $returnSubSubArray = array();
          if (isset($subArray->children)) {
               $returnSubSubArray = $this->parseJsonArray($subArray->children, $subArray->id);
          }
          $return[] = array('id' => $subArray->id, 'parentID' => $parentID);
          $return = array_merge($return, $returnSubSubArray);
        }
        return $return;
    }

    public function buildMenu($menu, $parentid = 0)
	{
	  $result = null;
	  foreach ($menu as $item)
	    if ($item->parent_id == $parentid) {
	      $result .= "<li class='dd-item nested-list-item' data-order='{$item->display_order}' data-id='{$item->id}'>
	      <div class='dd-handle nested-list-handle'>
	        <span class='fa fa-arrows-alt'></span>
	      </div>
	      <div class='nested-list-content'>{$item->name}
	        <div class='pull-right'>
	          <a href='#editModal' class='update_toggle' rel='{$item->id}'  data-toggle='modal' data-backdrop='false'>Edit</a> |
	          <a href='#' class='delete_toggle' rel='{$item->id}'>Delete</a>
	        </div>
	      </div>".$this->buildMenu($menu, $item->id) . "</li>";
	    }
	  return $result ?  "\n<ol class=\"dd-list\">\n$result</ol>\n" : null;
	}

	// Getter for the HTML menu builder
	public function getHTML($items)
	{
		return $this->buildMenu($items);
    }
}
