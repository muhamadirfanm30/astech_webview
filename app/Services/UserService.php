<?php

namespace App\Services;

use App\Events\ResetPasswordEvent;
use App\Events\TeknisiApprovalEvent;
use App\Events\TeknisiRejectedEvent;
use App\Events\UserRegister;
use App\Exceptions\SendErrorMsgException;
use App\Models\Master\DevPlant;
use App\Models\Master\Ewallet;
use App\Models\Master\MsAdditionalInfo;
use App\Models\Master\TechnicianSaldo;
use App\Models\Master\MsAddress;
use App\Models\Master\MsPriceService;
use App\Models\Master\MsTechTeam;
use App\Models\Master\MsVillage;
use App\Models\Master\Order;
use App\Models\Master\Rating;
use App\Models\Master\RatingAndReview;
use App\Models\Master\Review;
use App\Models\Master\TeamInvite;
use App\Models\Master\TeamTechnician;
use App\Models\Master\TechnicianSparepart;
use App\Models\PasswordReset;
use App\Models\Technician\JobExperience;
use App\Models\Technician\JobTitle;
use App\Services\User\UserLoginService;
use App\Services\User\UserStoreService;
use App\Services\User\UserUpdateService;
use App\Models\User;
use App\UserB2b;
use DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\ImageManagerStatic as Image;

class UserService
{
    public $token_name = 'jasaWeb';
    public $user;
    public $notif    = true;
    public $redirect = '/';
    public $status;

    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\User
     *
     * @throws \Exception
     */
    public function create(array $data)
    {
        $storeService = new UserStoreService();

        $user = $storeService->handle($data);

        $this->setUser($user)
            // ->setBearerToken()
            ->sendNotificationActivication();

        return $user;
    }

    public function createFromAdmin(array $data)
    {
        $storeService = new UserStoreService();

        $user = $storeService->createFromAdmin($data);

        $this->setUser($user)
            ->sendNotificationActivication();

        return $user;
    }

    /**
     * kirim aktivikasi user melalui otp dan email
     *
     * @param \App\User $user
     *
     * @return void
     */
    public function sendNotificationActivication($user = null)
    {
        if ($this->notif) {
            $user = ($user == null) ? $this->getUser() : $user;

            if ($user) {
                // if ($user->phone !== null) {

                //     $otp = (new OtpService())->create();

                //     $otp->setLabel($user->id);

                //     $token = $otp->now();

                //     $msg = (new OtpService())->sendSMS([
                //         'to' => $user->phone,
                //         'text' => $token,
                //     ]);

                //     $user->update([
                //         'otp_code' => $token,
                //         'otp_url' => $otp->getProvisioningUri(),
                //     ]);

                //     return $msg;
                // } else {
                $this->sendEmailVerification($user);
                // }
            }
        }
    }

    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\User
     *
     * @throws \Exception
     */

    public function sendingOtp(array $data)
    {

        $otp   = (new OtpService())->create();
        $token = $otp->now();

        $msg = (new OtpService())->sendSMS([
            'to'   => $data['phone'], //6281318737506, //
            'text' => $token,
        ]);

        return $msg;
    }

    // =========Teknisi Approve===============

    public function sentNotificationApprove($user = null)
    {
        $this->sendEmailVerificationApprove($user);
    }

    public function sendEmailVerificationApprove($user)
    {
        event(new TeknisiApprovalEvent($user));
    }

    // =============Teknisi Reject=============

    public function sentNotificationReject($user = null, $request)
    {
        $this->sendEmailVerificationReject($user, $request);
    }

    public function sendEmailVerificationReject($user, Request $request)
    {
        event(new TeknisiRejectedEvent($user, $request));
    }

    //===============forgot pass================

    public function sentPasswordReset($user = null, $request)
    {
        $this->sendEmailPasswordReset($user, $request);
    }

    public function sendEmailPasswordReset($user, Request $request)
    {
        $resetPass = PasswordReset::where('email', $request->email)->first();
        if ($resetPass == null) {
            $resetPass = PasswordReset::create([
                'email' => $request->email,
                'token' => md5($request->token),
            ]);
        } else {
            $resetPass->update([
                'token' => md5($request->token),
            ]);
        }
        // isert ke pass reset

        event(new ResetPasswordEvent($user, $request, $resetPass));
    }

    public function dontSendNotif()
    {
        $this->notif = false;
        return $this;
    }

    public function sendEmailVerification($user)
    {
        event(new UserRegister($user));
    }

    /**
     * set token auth
     */
    public function setBearerToken($user = null)
    {
        $user = ($user == null) ? $this->getUser() : $user;

        if ($user) {
            // get access token
            $token = $user->createToken($this->getTokenName())->accessToken;

            $user->update([
                'auth_token' => $token,
            ]);
        }

        return $this;
    }

    /**
     * update data
     *
     * @param array $data
     * @param \App\User $data
     *
     * @return \App\User
     *
     * @throws \Exception
     */
    public function update(array $data, User $user)
    {
        $updateService = new UserUpdateService();

        return $updateService->setUser($user)->handle($data);
    }

    /**
     * login user
     *
     * @param array $credentials
     *
     * @return bool
     */
    public function login(array $credentials)
    {
        $loginService = new UserLoginService();
        
        $login = $loginService->handle($credentials);
        
        if ($login) {
            // $this->setUser($loginService->getUser())->setBearerToken();
            $this->setUser($loginService->getUser());
        }

        return $login;
    }

    /**
     * vertifikasi user
     *
     * @return bool
     */
    public function isVerify()
    {
        $user = $this->getUser();

        if ($user->email_verified_at !== null) {
            return true;
        } else {
            $this->setRedirect('/verify');
        }

        if ($user->otp_verified_at !== null) {
            return true;
        } else {
            $this->setRedirect(route('verification.otp.form'));
        }

        return false;
    }

    /**
     * create or update ms additional info
     */
    public function createOrUpdateInfo($request, $user_id = null)
    {
        $id_user = $user_id == null ? $request->user_id : $user_id;
        $user = User::where('id', $id_user)->firstOrFail();

        $dataInfo = [
            'date_of_birth'  => date("Y-m-d", strtotime($request->date_of_birth)),
            'first_name'     => $request->first_name,
            'user_id'        => $id_user,
            'gender'         => $request->gender,
            'last_name'      => $request->last_name,
            'ms_marital_id'  => $request->ms_marital_id,
            'ms_religion_id' => $request->ms_religion_id,
            'place_of_birth' => $request->place_of_birth,
        ];
        DB::beginTransaction();
        try {
            if ($user->info == null) {
                $info = MsAdditionalInfo::create($dataInfo);
                User::where('id', $id_user)->update([
                    'name' => $request->first_name.' '.$request->last_name,
                ]);
            } else {
                MsAdditionalInfo::where('user_id', $id_user)->update($dataInfo);
                $info = MsAdditionalInfo::where('user_id', $id_user)->first();
                User::where('id', $id_user)->update([
                    'name' => $request->first_name.' '.$request->last_name,
                ]);
            }
            // commit db
            DB::commit();
        } catch (\Exception $e) {
            // rollback db
            DB::rollback();

            throw new SendErrorMsgException($e->getMessage());
        }

        $info->redirect = '/customer/profile';
        if (Auth::user()->hasRole('Technician')) {
            $info->redirect = '/teknisi/profile';
        }

        return $info;
    }

    /**
     * insert alamat
     */
    public function addressCreate($request, $user_id = null)
    {
        $id_user = $user_id == null ? $request->user_id : $user_id;
        $user    = User::where('id', $id_user)->firstOrFail();
        $village = MsVillage::where('id', $request->ms_village_id)->first();
        $main_address = MsAddress::where('user_id', $id_user)->where('is_main', 1)->first();
        $is_main = $main_address == null ? 1 : 0;

        $dataAddress = [
            'address'            => $request->address,
            'is_main'            => $is_main,
            'user_id'            => $id_user,
            'phone1'             => $request->phone1,
            'phone2'             => $request->phone2,
            'ms_address_type_id' => $request->ms_address_type_id,
            'ms_village_id'      => $village == null ? null : $village->id,
            'ms_district_id'     => $village == null ? null : $village->district->id,
            // 'ms_city_id'         => $village == null ? null : $village->district->city->id,
            'ms_zipcode_id'      => $village == null ? null : $village->district->zipcode->id,
            'ms_city_id'         => $request->ms_cities_id,
            'latitude'           => $request->lat,
            'longitude'          => $request->long,
            'cities'             => $request->cities,
            'province'           => $request->province,
            'location_google'    => $request->search_map,
        ];
        DB::beginTransaction();
        try {
            $address = MsAddress::create($dataAddress);
            // commit db
            DB::commit();
        } catch (\Exception $e) {
            // rollback db
            DB::rollback();

            throw new SendErrorMsgException($e->getMessage());
        }

        return $address;
    }

    /**
     * update alamat
     */
    public function updateAddress($request, $user_id = null)
    {
        $id_user = $user_id == null ? $request->user_id : $user_id;
        $address = MsAddress::where('id', $request->id)->where('user_id', $id_user)->firstOrFail();
        $user    = User::where('id', $id_user)->firstOrFail();
        $village = MsVillage::where('id', $request->ms_village_id)->first();

        $dataAddress = [
            'address'            => $request->address,
            'phone1'             => $request->phone1,
            'phone2'             => $request->phone2,
            'ms_address_type_id' => $request->ms_address_type_id,
            'ms_village_id'      => $village == null ? null: $village->id,
            'ms_district_id'     => $village == null ? null: $village->district->id,
            // 'ms_city_id'      => $village == null ? null: $village->district->city->id,
            'ms_zipcode_id'      => $village == null ? null: $village->district->zipcode->id,
            'ms_city_id'         => $request->ms_cities_id,
            'latitude'           => $request->lat,
            'longitude'          => $request->long,
            'cities'             => $request->cities,
            'province'           => $request->province,
            'location_google'    => $request->search_map,
        ];
        DB::beginTransaction();
        try {
            $address->update($dataAddress);
            // commit db
            DB::commit();
        } catch (\Exception $e) {
            // rollback db
            DB::rollback();

            throw new SendErrorMsgException($e->getMessage());
        }

        return $address;
    }

    /**
     * set default alamat
     */
    public function mainAddress($id, $user_id = null)
    {
        if ($user_id == null) {
            $alamat = MsAddress::where('id', $id)->firstOrFail();
        } else {
            $alamat = MsAddress::where('id', $id)->where('user_id', $user_id)->firstOrFail();
        }

        DB::beginTransaction();
        try {
            MsAddress::where('user_id', $alamat->user_id)->update([
                'is_main' => 0,
            ]);

            $alamat->update([
                'is_main' => 1,
            ]);
            // commit db
            DB::commit();
        } catch (\Exception $e) {
            // rollback db
            DB::rollback();

            throw new SendErrorMsgException($e->getMessage());
        }

        return $alamat;
    }

    /**
     * delete alamat
     */
    public function deleteAddress($id, $user_id)
    {
        $alamat = MsAddress::where('id', $id)->where('user_id', $user_id)->firstOrFail();
        
        DB::beginTransaction();
        try {
            $alamat->delete();
            // commit db
            DB::commit();
        } catch (\Exception $e) {
            // rollback db
            DB::rollback();

            throw new SendErrorMsgException($e->getMessage());
        }

        return $alamat;
    }

    /**
     * ganti avatar
     */
    public function updateProfileImage($request, $user_id = null)
    {
        $id_user = $user_id == null ? $request->user_id : $user_id;
        $info = MsAdditionalInfo::where('user_id', $id_user)->first();
        $user = User::where('id', $id_user)->firstOrFail();

        if ($request->hasFile('image')) {
            $image      = $request->file('image');
            $file_name   = time() . '.' . $image->getClientOriginalExtension();

            $img = Image::make($image->getRealPath());
            $img->resize(505, 505, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->stream(); // <-- Key point

            Storage::disk('local')->put('public/user/profile/avatar/' . $file_name, $img);

            if ($info !== null) {
                $exists = Storage::disk('local')->has('public/user/profile/avatar/' . $info->picture);
                if ($exists) {
                    Storage::delete('public/user/profile/avatar/' . $info->picture);
                }
            }            
        }

        DB::beginTransaction();
        try {
            if ($info == null) {
                MsAdditionalInfo::create([
                    'first_name' => $user->name,
                    'picture' => $file_name,
                    'user_id' => $user->id
                ]);
            } else {
                $info->update([
                    'picture' => $file_name,
                ]);
            }

            // commit db
            DB::commit();
        } catch (\Exception $e) {
            // rollback db
            DB::rollback();

            throw new SendErrorMsgException($e->getMessage());
        }

        return $file_name;
    }

    /**
     * hapus user
     */
    public function deleteAll($id)
    {
        $user   = User::where(['id' => $id])->firstOrFail();
        $vendor = \App\Models\Master\Vendor::where('user_id', $user->id)->get();
        if (count($vendor) >= 1) {
            throw new SendErrorMsgException('this user has vendor relasion, cannot delete');
        }

        $technician = $user->technician;

        DB::beginTransaction();
        try {
            // alamat
            \App\Models\Master\MsAddress::where('user_id', $user->id)->delete();

            // info
            MsAdditionalInfo::where('user_id', $user->id)->delete();

            // tiket
            $ticket = \App\Models\Master\MsTicket::where('users_id', $user->id)->get();
            \App\Models\Master\MsTicketDetail::whereIn('ms_ticket_id', $ticket->pluck('id'))->delete();
            \App\Models\Master\MsTicket::whereIn('id', $ticket->pluck('id'))->delete();

            // devplan
            DevPlant::where('user_id', $user->id)->delete();

            // wallte
            Ewallet::where('user_id', $user->id)->delete();

            // Order
            // Order::where('users_id', $user->id)->update([
            //     'users_id' => null
            // ]);

            if ($technician != null) {
                // rating & review
                $rating = Rating::where('technician_id', $technician->id)->get();
                if (count($rating) > 0) {
                    RatingAndReview::where('rating_id', $rating->pluck('id'))->delete();
                    Review::where('technician_id', $technician->id)->delete();
                    Rating::whereIn('id', $rating->pluck('id'))->delete();
                }

                TechnicianSparepart::where('technicians_id', $technician->id)->update([
                    'technicians_id' => null
                ]);
                MsPriceService::where('technicians_id', $technician->id)->delete();

                // team
                MsTechTeam::where('technicians_id', $technician->id)->delete();
                TeamInvite::where('technicians_id', $technician->id)->delete();
                TeamTechnician::where('technicians_id', $technician->id)->delete();

                // teknisi info
                \App\Models\Master\TeknisiInfo::where('user_id', $user->id)->delete();
                JobExperience::where('user_id', $technician->user_id)->delete();
                $technician->delete();
                JobTitle::where('user_id', $technician->user_id)->delete();
                TechnicianSaldo::where('technician_id', $technician->id)->delete();
            }

            // delete order
            Order::where('users_id', $user->id)->delete();

            $user->delete();
            // commit db
            DB::commit();
        } catch (\Exception $e) {
            // rollback db
            DB::rollback();

            throw new SendErrorMsgException($e->getMessage());
        }
    }

    public function profile($user_id)
    {
        return User::where('id', $user_id)->with('info', 'roles')->first();
    }

    public function setTokenName(string $name)
    {
        $this->token_name = $name;
        return $this;
    }

    public function getTokenName(): string
    {
        return $this->token_name;
    }

    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setRedirect($redirect)
    {
        $this->redirect = $redirect;
        return $this;
    }

    public function getRedirect()
    {
        return $this->redirect;
    }
}
