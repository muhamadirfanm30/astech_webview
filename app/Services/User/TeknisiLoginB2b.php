<?php

namespace App\Services\User;

use App\User;
use App\UserB2b;

class TeknisiLoginB2b
{
    public $user;

    /**
     * handle login user
     *
     * @param array $credentials
     * 
     * @return bool
     */
    public function handle(array $credentials)
    {
        if (is_numeric($credentials['email'])) {
            return $this->login([
                'phone' => $credentials['email'],
                'password' => $credentials['password'],
            ]);
        } else {
            return $this->login([
                'email' => $credentials['email'],
                'password' => $credentials['password'],
            ]);
        }
    }

    

    /**
     * login user
     *
     * @param array $credentials
     * 
     * @return bool
     */
    public function login(array $credentials)
    {
        // dd(auth()->user());
        if (auth()->attempt($credentials)) {
            $user = User::with('roles', 'info')->where('id', auth()->user()->id)->first();
            // set user
            if($user->is_b2b_users == 1 && $user->is_b2b_teknisi == 1){
                $this->setUserb2b($user);
                return true;
            }else{
                return false;
            }
            
        } else {
            return false;
        }
    }

    public function setUserb2b(User $user)
    {
        $this->user = $user;
        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }
}
