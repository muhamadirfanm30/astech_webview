<?php

namespace App\Services\User;

use App\User;
use DB;
use Spatie\Permission\Models\Role;

class UserB2bUpdateService
{
    public $user;

    /**
     * handle update data
     *
     * @param array $data
     * @param \App\User $user
     *
     * @return \App\User
     *
     * @throws \Exception
     */
    public function handle(array $credentials)
    {
        if ($this->getUser() == null) {
            throw new \Exception('user belum diset');
        }
        return $this->update($credentials);
    }

    /**
     * update data
     *
     * @param array $data
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function update(array $data)
    {
        $user  = $this->getUser();
        $phone1 = str_replace(' ', '', $data['phone1']);
        $phone2 = str_replace(' ', '', $data['phone2']);
        $phone3 = str_replace(' ', '', $data['phone3']);
        // data for update
        $userData = [
            'name'   => $data['name'],
            'username'  => '-',
            'email'  => $data['email'],
            'phone1'     => $phone1,
            'phone2'     => $phone2,
            'phone3'     => $phone3,
            'address'  => $data['address'],
            'ms_company_id'  => $data['ms_company_id'],
            'status' => isset($data['status']) ? 1 : 0,
        ];

        DB::beginTransaction();
        try {
            // update data
            $update = $user->update($userData);

            DB::commit();
        } catch (\Exception $e) {
            // rollback db
            DB::rollback();

            throw new \Exception($e->getMessage());
        }

        return $update;
    }

    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }
}