<?php

namespace App\Services\User;

use App\Model\Technician\Technician;
use App\User;
use DB;
use Spatie\Permission\Models\Role;

class UserUpdateService
{
    public $user;

    /**
     * handle update data
     *
     * @param array $data
     * @param \App\User $user
     *
     * @return \App\User
     *
     * @throws \Exception
     */
    public function handle(array $credentials)
    {
        if ($this->getUser() == null) {
            throw new \Exception('user belum diset');
        }
        return $this->update($credentials);
    }

    /**
     * update data
     *
     * @param array $data
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function update(array $data)
    {
        $user  = $this->getUser();
        $phone = str_replace(' ', '', $data['phone']);

        // data for update
        $userData = [
            'name'   => $data['name'],
            'email'  => $data['email'],
            'phone'  => $phone,
            'status' => isset($data['status']) ? 1 : 0,
        ];

        // roles
        $roles                  = Role::whereIn('id', $data['role_id'])->get();
        $teknisiRole            = $roles->where('name', 'Technician')->first();
        $adminRole              = $roles->where('name', 'admin')->first();
        $userCurrentRole        = $user->roles;
        $userCurrentRoleTeknisi = $userCurrentRole->where('name', 'Technician')->first();

        DB::beginTransaction();
        try {
            // update data
            $update = $user->update($userData);

            // syncRoles role
            if ($adminRole != null) {
                $user->syncRoles(
                    Role::where('id', $adminRole->id)->get()
                );
            } else {
                $user->syncRoles(
                    Role::whereIn('id', $data['role_id'])->where('name', '!=', 'admin')->get()
                );
            }

            // buat teknisi
            if ($teknisiRole != null) {
                $teknisi = Technician::where('user_id', $user->id)->first();
                if ($teknisi == null) {
                    Technician::create([
                        'user_id' => $user->id,
                    ]);
                } else {
                    $teknisi->update([
                        'status' => 1,
                    ]);
                }
            }

            // jika user sebelum nya puya role teknisi dan akan diganti role teknisinya
            if ($userCurrentRoleTeknisi != null && $teknisiRole == null) {
                $userTeknisi = Technician::where('user_id', $user->id)->first();
                if ($userTeknisi != null) {
                    $userTeknisi->update([
                        'status' => 0,
                    ]);
                }
            }

            // jika role admin set teknisi status ke 0 / un aktif
            if ($adminRole != null) {
                $userTeknisi = Technician::where('user_id', $user->id)->first();
                if ($userTeknisi != null) {
                    $userTeknisi->update([
                        'status' => 0,
                    ]);
                }
            }

            // commit db
            DB::commit();
        } catch (\Exception $e) {
            // rollback db
            DB::rollback();

            throw new \Exception($e->getMessage());
        }

        return $update;
    }

    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }
}
