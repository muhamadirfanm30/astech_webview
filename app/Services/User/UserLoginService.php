<?php

namespace App\Services\User;

use App\Models\User;
use App\UserB2b;

class UserLoginService
{
    public $user;

    /**
     * handle login user
     *
     * @param array $credentials
     * 
     * @return bool
     */
    public function handle(array $credentials)
    {
        if (is_numeric($credentials['email'])) {
            return $this->login([
                'phone' => $credentials['email'],
                'password' => $credentials['password'],
            ]);
        } else {
            return $this->login([
                'email' => $credentials['email'],
                'password' => $credentials['password'],
            ]);
        }
    }

    

    /**
     * login user
     *
     * @param array $credentials
     * 
     * @return bool
     */
    public function login(array $credentials)
    {
        // dd(auth()->attempt($credentials));
        if (auth()->attempt($credentials)) {
        // dd(auth()->user()->id);
            $user = User::with('roles', 'info')->where('id', auth()->user()->id)->first();
            // set user
            // dd($user);
            $this->setUser($user);

            return true;
        } else {
            return false;
        }
    }

    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }

    
}
