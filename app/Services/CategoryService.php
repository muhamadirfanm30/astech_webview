<?php

namespace App\Services;

use App\Model\Category;
use Illuminate\Database\QueryException;
use Illuminate\Support\Str;

class CategoryService
{
    public $category;

    public function find($id)
    {
        return Category::where('id', $id)->first();
    }

    /**
     * create data
     *
     * @param array $data
     * 
     * @return \App\Model\Category
     * 
     * @throws \Exception
     */
    public function create(array $data)
    {
        $categosyData = [
            'name' => $data['name'],
            'slug' => Str::slug($data['name']),
            'parent_id' => $this->getParentId($data),
        ];

        try {
            return Category::create($categosyData);
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * update data
     *
     * @param array $data
     * 
     * @return bool
     * 
     * @throws \Exception
     */
    public function update(array $data)
    {
        $categosyData = [
            'name' => $data['name'],
            'slug' => $data['name'],
            'parent_id' => $this->getParentId($data),
        ];

        $category = $this->getCategory();

        try {
            return $category->update($categosyData);
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     * 
     * @return bool|null
     * 
     * @throws \Exception
     */
    public function delete()
    {
        $category = $this->getCategory();

        if ($category->childs->count() > 0) {
            throw new \Exception('tidak bisa dihapus terdapat childs pada category ini');
        }

        try {
            return $category->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function getParentId(array $data)
    {
        $parent_id = null;
        if (isset($data['parent_id'])) {
            $category = Category::where('id', $data['parent_id'])->first();
            if ($category) {
                $parent_id = $category->id;
            }
        }
        return $parent_id;
    }

    public function setCategory(Category $category)
    {
        $this->category = $category;
        return $this;
    }

    public function getCategory()
    {
        return $this->category;
    }
}
