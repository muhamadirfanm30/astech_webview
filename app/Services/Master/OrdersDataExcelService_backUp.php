<?php

namespace App\Services\Master;

use Carbon\Carbon;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use App\Model\Master\PartsDataExcel;
use Illuminate\Support\Facades\Auth;
use App\Exceptions\HasChildException;
use App\Http\Controllers\ApiController;
use App\Model\Master\EngineerCodeExcel;
use App\Model\Master\OrdersDataExcel;
use Illuminate\Database\QueryException;
use App\Model\Master\OrdersDataExcelLog;
use App\Model\Master\PartDataExcelStock;
use App\Model\Master\StatusExcel;

class OrdersDataExcelService
{
    public $orders_data_excel;

    /**
     * get list query
     *
     * @return App\Model\Master\OrdersDataExcel;
     */
    public function list()
    {
        return OrdersDataExcel::with('part_data_excel')->orderBy('id', 'DESC');
    }

    public function datatables($request = null)
    {
        $query = $this->list();
        if(!empty($request->type_date)) {
            $by_date = $request->type_date == 'request_date' ? 'date' : 'repair_completed_date';
        }
        if(!empty($request->date_from) && !empty($request->date_to)) {
            $date_from = Carbon::parse($request->date_from)->format('Y-m-d');
            $date_to = Carbon::parse($request->date_to)->format('Y-m-d');
            $query->where($by_date, '>=', $date_from)
            ->where($by_date, '<=', $date_to);
        }
        $query = $query->get();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\Model\Master\OrdersDataExcel
     *
     * @throws \Exception
     */
    public function create(array $row)
    {
        DB::beginTransaction();
        $apicontroller = new ApiController();
        try {
            $insert_ode = OrdersDataExcel::create($this->generateData($row));
            // $this->insertAutoCompleteField($row);
            foreach ($row['code_material'] as $key => $value) {
                // dd($value);
                $check_part = !empty($row['code_material'][$key]) && !empty($row['part_description'][$key]) && !empty($row['quantity'][$key]);
                if ($check_part) {
                    PartsDataExcel::create([
                        'orders_data_excel_id' => $insert_ode->id,
                        'part_data_stock_id' => $row['code_material_id'][$key],
                        'code_material' => $row['code_material'][$key],
                        'part_description' => $row['part_description'][$key],
                        'quantity' => $row['quantity'][$key],
                    ]);
                    $decrease_stock = $this->decreaseStock(($row['code_material'][$key]), ($row['quantity'][$key]));
                    if($decrease_stock == false) {
                        return $apicontroller->errorResponse(
                            $decrease_stock['stock_now'],
                            'Code Material = '.$row['code_material'][$key].'. Quantity melebihi jumlah stock terupdate sekarang = '.$decrease_stock['stock_now'].'',
                            400
                        );
                    }
                }
            }
            $this->logs($insert_ode, 1);
            DB::commit();
            return $apicontroller->successResponse(
                $row,
                $apicontroller->successStoreMsg(),
                201
            );
        } catch (QueryException $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }
    }

    public function generateData(array $row)
    {
        return $orders_data_excel = [
            "service_order_no" => $row['service_order_no'],
            "asc_name" => $row['asc_name'],
            "customer_name" => $row['customer_name'],
            "type_job" => $row['type_job'],
            "defect_type" => $row['defect_type'],
            "engineer_code" => $row['engineer_code'],
            "engineer_name" => $row['engineer_name'],
            "assist_engineer_name" => $row['assist_engineer_name'],
            "merk_brand" => $row['merk_brand'],
            "model_product" => $row['model_product'],
            "status" => $row['status'],
            "reason" => $row['reason'],
            "remark_reason" => $row['remark_reason'],
            "pending_aging_days" => $row['pending_aging_days'],
            "street" => $row['street'],
            "city" => $row['city'],
            "phone_no_mobile" => $row['phone_no_mobile'],
            "phone_no_home" => $row['phone_no_home'],
            "phone_no_office" => $row['phone_no_office'],
            "month" => $row['month'],
            "date" => Carbon::parse($row['date'])->format('Y-m-d'),
            "request_time" => Carbon::parse($row['request_time'])->format('H:i:s'),
            // "created_times" => $row['created_times'],
            "engineer_picked_order_date" => Carbon::parse($row['engineer_picked_order_date'])->format('Y-m-d'),
            "engineer_picked_order_time" => Carbon::parse($row['engineer_picked_order_time'])->format('H:i:s'),
            "admin_assigned_to_engineer_date" => Carbon::parse($row['admin_assigned_to_engineer_date'])->format('Y-m-d'),
            "admin_assigned_to_engineer_time" => Carbon::parse($row['admin_assigned_to_engineer_time'])->format('H:i:s'),
            "engineer_assigned_date" => Carbon::parse($row['engineer_assigned_date'])->format('Y-m-d'),
            "engineer_assigned_time" => Carbon::parse($row['engineer_assigned_time'])->format('H:i:s'),
            "tech_1st_appointment_date" => Carbon::parse($row['tech_1st_appointment_date'])->format('Y-m-d'),
            "tech_1st_appointment_time" => Carbon::parse($row['tech_1st_appointment_time'])->format('H:i:s'),
            "1st_visit_date" => Carbon::parse($row['1st_visit_date'])->format('Y-m-d'),
            "1st_visit_time" => Carbon::parse($row['1st_visit_time'])->format('H:i:s'),
            "repair_completed_date" => Carbon::parse($row['repair_completed_date'])->format('Y-m-d'),
            "repair_completed_time" => Carbon::parse($row['repair_completed_time'])->format('H:i:s'),
            "closed_date" => Carbon::parse($row['closed_date'])->format('Y-m-d'),
            "closed_time" => Carbon::parse($row['closed_time'])->format('H:i:s'),
            "rating" => $row['rating'],
        ];
    }

    /**
     * update data
     *
     * @param array $data
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function update(array $data)
    {
        DB::beginTransaction();

        try {
            // $this->insertAutoCompleteField($data);
            $update = $this->getData()->update($this->generateData($data));
            $this->logs($this->getData(), 2);
            DB::commit();
            return $update;

        } catch (QueryException $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function deleteX()
    {
        $orders_data_excel = $this->getData();
        DB::beginTransaction();

        try {
            $this->logs($orders_data_excel, 3);
            $delete = $orders_data_excel->delete();
            DB::commit();


        } catch (QueryException $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\Model\Master\OrdersDataExcel
     *
     * @throws \Exception
     */

    public function setData(OrdersDataExcel $orders_data_excel)
    {
        $this->orders_data_excel = $orders_data_excel;
        return $this;
    }

    public function getData()
    {
        return $this->orders_data_excel;
    }

    public function deleteById(array $ids)
    {
        try {
            return OrdersDataExcel::whereIn('id', $ids)->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function logs($data, $type) { // type created/changed/deleted
        return $log = OrdersDataExcelLog::create([
            'orders_data_excel_id' => $data->id,
            'service_order_no' => $data->service_order_no,
            'user_id' => Auth::user()->id,
            'type' => $type,
        ]);

    }

    // public function insertAutoCompleteField(array $row) {
    //     $c_engineer_code = EngineerCodeExcel::where('engineer_code',$row['engineer_code'])->count();
    //     if($c_engineer_code != 0) {
    //         EngineerCodeExcel::create([
    //             'engineer_code' => $row['engineer_code'],
    //             'engineer_name' => $row['engineer_name'],
    //         ]);
    //     }
    //     $c_status = StatusExcel::where('status', $row['status'])->count();
    //     if($c_status != 0) {
    //         StatusExcel::create([
    //             'status' => $row['status']
    //         ]);
    //     }
    // }

    public function decreaseStock($code_material, $quantity) {
        $q_stock = PartDataExcelStock::where('code_material', $code_material);
        $result = [];
        if($q_stock->count() > 0) {
            $q_stock = $q_stock->value('stock');
            $result['stock_now'] = $q_stock;
            if($quantity > $q_stock) {
                $result['status'] = false;
                return $result;
            }
            $update_stock = PartDataExcelStock::where('code_material', $code_material)->update(['stock' => ($q_stock - $quantity)]);
            $result['status'] = true;
            return $result;
        }


    }
}
