<?php

namespace App\Services\Master;

use App\Model\Master\Curriculum;
use Illuminate\Database\QueryException;
use App\Http\Controllers\AdminController;

class CurriculumService
{
    public $curiculum;

    /**
     * get list query
     *
     * @return App\Model\Master\Curriculum;
     */
    public function list()
    {
        return Curriculum::query();
    }

    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\Model\Master\Curriculum
     *
     * @throws \Exception
     */
    public function create(array $data)
    {
        $curriculumCreate = [
            'curriculum_name' => $data['curriculum_name'],
        ];

        try {
            return Curriculum::create($curriculumCreate);
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * update data
     *
     * @param array $data
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function update(array $data)
    {
        try {
            return $this->getCurriculum()->update($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function delete()
    {
        try {
            return  $this->getCurriculum()->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function setCurriculum(Curriculum $curiculum)
    {
        $this->curiculum = $curiculum;
        return $this;
    }

    public function generateData(array $data)
    {
        return [
            'curriculum_name' => $data['curriculum_name'],
        ];
    }

    public function getCurriculum()
    {
        return $this->curiculum;
    }
}
