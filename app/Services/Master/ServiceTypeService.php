<?php

namespace App\Services\Master;

use App\Model\Master\MsPriceService;
use App\Model\Master\MsServicesType;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class ServiceTypeService
{
    /**
     * dipakai saat delete data
     * akan mengecek ke semua relasi
     * pada model ini
     * untuk mencari jika model ini sudah di pakai di model lain
     * note: lumayan berat karna harus cek ke semua table
     */
    public $use_relation_check = true;

    public $service_type;
    /**
     * get list query
     *
     * @return App\Model\Master\MsServicesType;
     */
    public function list()
    {
        return MsServicesType::query();
    }

    public function select2($request, $symptom_id = null)
    {
        if ($symptom_id != null) {
            return MsServicesType::where('name', 'like', '%' . $request->q . '%')
                ->where('ms_symptoms_id', $symptom_id)
                ->limit(20)
                ->get();
        }
        return MsServicesType::where('name', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();
    }

    public function datatables()
    {
        $query = $this->list()->with(['symptom'])
            ->whereHas('symptom', function ($q) {
                $q->whereNull('deleted_at');
            });
        return DataTables::of($query)->addIndexColumn()->toJson();
    }

    public function create(array $data)
    {
        try {
            return MsServicesType::create($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function update(array $data)
    {
        try {
            return $this->getServiceTypes()->update($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }


    public function delete($id)
    {
        try {
            return $this->relasiCheck()->delete();
            // $name = MsServicesType::where('id', $id)->value('name');
            // $rand = rand(999999, 000000);
            // $update_name = MsServicesType::where('id', $id)->update(['name' => 'deleted-' . $name . '-' . $rand]);
            // return $this->relasiCheck()->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function setServiceTypes(MsServicesType $service_type)
    {
        $this->service_type = $service_type;
        return $this;
    }

    public function generateData(array $data)
    {
        return [
            'ms_symptoms_id' => $data['ms_symptoms_id'],
            'name' => $data['name'],
            'images' => $data['images']
        ];
    }

    public function getServiceTypes()
    {
        return $this->service_type;
    }

    /**
     * check relasi saat delete data
     */
    public function relasiCheck()
    {
        $row = $this->getServiceTypes();

        if ($this->use_relation_check) {
            // cek ke produk
            $priceService = MsPriceService::where('ms_services_types_id', $row->id)->first();
            if ($priceService != null) {
                throw new \Exception('already used in Technician Price Service module)');
            }
        }

        return $row;
    }
}
