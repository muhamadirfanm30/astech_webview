<?php
namespace App\Services\Master;

use App\Model\Master\ConfrimPayment;
use App\Model\Master\Order;
use Illuminate\Database\QueryException;
use Auth;
use App\Helpers\ImageUpload;

class ConfrimPaymentService
{
    public $confrimPayment;

    /**
     * get list query
     * 
     * @return App\Model\Product\ConfrimPayment;
     */
    public function list()
    {
        $userlogin = Auth::user()->id;
        return ConfrimPayment::where('user_id', $userlogin)->get();
    }
    
    /**
     * create data
     *
     * @param array $data
     * 
     * @return \App\Model\Master\ConfrimPayment
     * 
     * @throws \Exception
     */

    public function create(array $data)
    {
        
        try{
            return ConfrimPayment::create($this->generateData($data));
        }catch(QueryException $e){
            throw new \Exception($e->getMessage());
        }
    }

    public function update(array $data)
    {
        try{
            return $this->getConfrimPayment()->update($this->generateData($data));
        }catch(QueryException $e){
            throw new \Exception($e->getMessage());
        }
    }

    public function delete()
    {
        try{
            return $this->getConfrimPayment()->delete();
        }catch(QueryException $e){
            throw new \Exception($e->getMessage());
        }
    }


    public function generateData(array $data)
    {
        $user =  auth()->user();
        $path = ConfrimPayment::getImagePathUpload();
    
        // dd($path);
        $filename = null;

        if ($data['attachment']) {
            $image = (new ImageUpload)->upload($data['attachment'], $path);
            $filename = $image->getFilename();
        }

        $data = [
            // 'id' => 1,
            'a_n_bank' => $data['a_n_bank'],
            'transfer_date' => $data['transfer_date'],
            'nominal' => $data['nominal'],
            'note' => $data['note'],
            'ms_order_id' => $data['ms_order_id'],
            'user_id' => $data['user_id'],
            'attachment' => $filename,
        ];

        Order::where('id', $data['ms_order_id'])->update([
            'orders_statuses_id'    => 8,
        ]);


        // return array_merge($data);
    }

    public function setConfrimPayment(ConfrimPayment $confrimPayment)
    {
        $this->confrimPayment = $confrimPayment;
        return $this;
    }

    public function getConfrimPayment()
    {
        return $this->confrimPayment;
    }
}