<?php

namespace App\Services\Master;

use Illuminate\Http\Request;
use App\Model\Master\UnitTypes;
use Yajra\DataTables\DataTables;
use App\Exceptions\HasChildException;
use Illuminate\Database\QueryException;

class UnitTypesService
{
    public $unitTypes;

    /**
     * get list query
     *
     * @return App\Model\Master\UnitTypes;
     */
    public function list()
    {
        return UnitTypes::query();
    }

    /**
     * datatables data
     *
      * @return \App\Model\Master\UnitTypes
     */
    public function datatables()
    {
        $query = $this->list();
        return
        DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * select2 data
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Model\Master\UnitTypes
     */
    public function select2(Request $request)
    {
        return UnitTypes::where('unit_name', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();
    }

    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\Model\Master\UnitTypes
     *
     * @throws \Exception
     */
    public function create(array $data)
    {
        try{
            return UnitTypes::create($this->generateData($data));
        }catch(QueryException $e){
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * update data
     *
     * @param array $data
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function update(array $data)
    {
        try{
            return $this->getUnitType()->update($this->generateData($data));
        }catch(QueryException $e){
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     *
     * @return bool|null
     *
     * @throws \Exception
     */

     public function delete()
     {
        $unit_type = $this->getUnitType();

        if ($unit_type->price_item->count() > 0) {
            throw new HasChildException('tidak bisa delete sudah terpakai di Price Item pada Unit Type ini');
        }
        try{
            return $this->getUnitType()->delete();
        }catch(QueryException $e){
            throw new \Exception($e->getMessage());
        }
     }

    //  public function deleteById(array $ids)
    // {
    //     try {
    //         return UnitTypes::whereIn('id', $ids)->delete();
    //     } catch (QueryException $e) {
    //         throw new \Exception($e->getMessage());
    //     }
    // }

    public function generateData(array $data)
    {
        return [
            'unit_name' => $data['unit_name'],
        ];
    }

    public function setUnitType(UnitTypes $unitType)
    {
        $this->unitType = $unitType;
        return $this;
    }

    public function getUnitType()
    {
        return $this->unitType;
    }
}
