<?php

namespace App\Services\Master;

use App\Exceptions\SendErrorMsgException;
use App\Http\Requests\Master\BatchRequest;
use Carbon\Carbon;
use App\Model\Master\Batch;
use Illuminate\Http\Request;
use App\Model\Master\BatchItem;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use App\Model\Product\ProductVarian;
use App\Model\Master\BatchItemReason;
use Doctrine\DBAL\Query\QueryException;
use App\Model\Master\BatchStatusShipment;
use App\Model\Master\BatchShipmentHistory;
use App\Model\Master\Inventory;
use App\Model\Master\MsProduct;
use Illuminate\Database\Eloquent\Collection;

use function App\Helpers\onlyNumber;

class BatchService implements \IteratorAggregate
{
    public $batch;
    public $datatables = false;

    public function list()
    {
        return Batch::with('items', 'last_history.batch_status_shipment');
    }

    public function listShipmentHistories($batch_id)
    {
        return BatchShipmentHistory::with('batch', 'batch_status_shipment')->where('batch_id', $batch_id);
    }

    public function show($id)
    {
        $batch = Batch::where('id', $id)
            ->with(['items.product', 'items.productVendor.vendor', 'items.varian.productVarianAttributes', 'items.status'])
            ->first();

        return $batch;
    }

    public function infoTemplate($batch)
    {
        return view('admin.master.batch._batch_info_template', [
            'batch' => $batch
        ])->render();
    }

    public function items($batch_id)
    {
        $items = BatchItem::where('batch_id', $batch_id)
            ->where('is_inventory', 0)
            ->where('is_accepted', 1)
            ->with(['product', 'productVendor.vendor', 'varian.productVarianAttributes', 'status']);

        if ($this->getDatatables()) {
            return $this->datatables($items);
        }

        return $items->get();
    }

    public function select2(Request $request)
    {
        return Batch::with('items')->where('batch_no', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();
    }

    public function select2child(Request $request)
    {
        return BatchItem::with('product')->where('product_id', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();
    }

    public function select2Accepted(Request $request)
    {
        return Batch::where('batch_no', 'like', '%' . $request->q . '%')
            ->with([
                'items' => function ($query) {
                    $query->where('is_accepted', 1)->where('is_inventory', 0);
                }
            ])
            ->whereHas('items.product')
            ->whereHas('items', function($query){
                $query->where('is_accepted', 1)->where('is_inventory', 0);
            })
            ->limit(20)
            ->get();
    }

    public function datatables($query = null)
    {
        if ($query == null) {
            $query = $this->list();
        }

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function datatablesShipmentHistories($batch_id)
    {
        $query = $this->listShipmentHistories($batch_id);
        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function datatablesBatchItemEditOrder($batch_id)
    {
        $query = BatchItem::with('batch', 'product')->where('batch_id', $batch_id)->get();

        $data = new Collection();
        foreach ($query as $key => $val) {
            $reason_image = $this->batchItemReason($val['id']);
            $data->push([
                'id'              => $val['id'],
                'product_name'    => isset($val['product']['name']) ? $val['product']['name'] : '',
                'quantity'        => $val['quantity'],
                'value'           => $val['value'],
                'quantity_return' => $val['quantity_return'],
                'is_accepted'     => $val['is_accepted'],
                'is_confirmed'    => $val['is_confirmed'],
                'reason'          => $val['reason'],
                'reason_image'    => $reason_image
            ]);
        }
        return DataTables::of($data)
            ->addIndexColumn()
            ->rawColumns(['reason_image'])
            ->make(true);
    }


    public function searchDatatables(Request $request)
    {
        $query = $this->list();

        if (!empty($request->date_from) && !empty($request->date_to)) {
            $query->where('batch_date', '>=', $request->date_from)
                ->where('batch_date', '<=', $request->date_to);
        }
        if (!empty($request->batch_date_range)) {
            $range = explode('-', $request->batch_date_range);
            $date_from = date('Y-m-d', strtotime($range[0]));
            $date_to = date("Y-m-d", strtotime($range[1]));
            $query->where('batch_date', '>=', $date_from)
                ->where('batch_date', '<=', $date_to);
        }


        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function create(array $data)
    {
        DB::beginTransaction();
        try {
            // insert batch
            $batch = Batch::create($this->generateBatchData($data));
            // insert batch item
            BatchItem::insert($this->generateBatchItemData($data, $batch));
            // commit db
            DB::commit();
        } catch (\Exception $e) {
            // rollback db
            DB::rollback();

            throw new SendErrorMsgException($e->getMessage());
        }
    }

    public function update(array $data)
    {
        $batch = $this->getBatch();
        if (count($batch->batch_shipment_history) > 0) {
            throw new SendErrorMsgException('Cannot Edit');
        }
        
        DB::beginTransaction();
        try {
            // update batch
            $this->getBatch()->update($this->generateBatchData($data));
            // update batch item
            $this->updateOrInsertBatchItemData($data, $this->getBatch());
            // commit db
            DB::commit();
        } catch (\Exception $e) {
            // rollback db
            DB::rollback();

            throw new SendErrorMsgException($e->getMessage());
        }
    }

    public function delete()
    {
        $batch = $this->getBatch();

        DB::beginTransaction();
        try {
            BatchItemReason::whereIn('batch_item_id', $batch->items->pluck('id'))->delete();
            BatchShipmentHistory::where('batch_id', $batch->id)->delete();
            // delete item batch
            BatchItem::where('batch_id', $batch->id)->delete();
            // delete batch
            $batch->delete();
            // commit db
            DB::commit();
        } catch (\Exception $e) {
            // rollback db
            DB::rollback();

            throw new SendErrorMsgException($e->getMessage());
        }
    }

    private function generateBatchItemData(array $data, Batch $batch)
    {
        $vendor_id = $data['vendor_id'];
        $quantity = $data['quantity'];
        $value = $data['value'];
        $product_id = $data['product_id'];
        $product_varian_id = $data['product_varian_id'];
        $product_vendor_id = $data['product_vendor_id'];
        // $status = $data['status'];
        $status = $data['batch_status_id'];
        $is_consigment = $data['is_consigment'];

        $hasil = [];
        foreach ($quantity as $key => $qty) {
            if ($product_id[$key] != 0) {
                $hasil[] = [
                    'item_name' => MsProduct::getNameProduct($product_id[$key], $product_varian_id[$key], $product_vendor_id[$key]),
                    'batch_id' => $batch->id,
                    'product_id' => $product_id[$key],
                    // 'vendor_id' => $vendor_id[$key],
                    'product_vendor_id' => ($product_vendor_id[$key] == 0) ? null : $product_vendor_id[$key],
                    'product_varian_id' => ($product_varian_id[$key] == 0) ? null : $product_varian_id[$key],
                    'quantity' => onlyNumber($quantity[$key]),
                    'value' => onlyNumber($value[$key]),
                    // 'status' => $status[$key],
                    'batch_status_id' => $status[$key],
                    'is_consigment' => $is_consigment[$key],
                ];
            }
        }

        return $hasil;
    }

    public function generateBatchData(array $data)
    {
        $dataBatch = [
            'batch_no' => $data['batch_no'],
            'shipping_cost' => $data['shipping_cost'],
            'extra_cost' => $data['extra_cost'],
            'batch_date' => $data['batch_date'],
            // 'batch_status_id' => $data['batch_status_id'],
        ];

        return $dataBatch;
    }


    private function updateOrInsertBatchItemData(array $data, Batch $batch)
    {
        $vendor_id = $data['vendor_id'];
        $quantity = $data['quantity'];
        $value = $data['value'];
        $product_id = $data['product_id'];
        $batch_item_id = $data['batch_item_id'];
        $product_vendor_id = $data['product_vendor_id'];
        $product_varian_id = $data['product_varian_id'];
        // $status = $data['status'];
        $status = $data['batch_status_id'];
        $is_consigment = $data['is_consigment'];

        foreach ($quantity as $key => $qty) {
            $updateItemData = [
                'item_name' => MsProduct::getNameProduct($product_id[$key], $product_varian_id[$key], $product_vendor_id[$key]),
                'batch_id' => $batch->id,
                'product_id' => $product_id[$key],
                // 'vendor_id' => $vendor_id[$key],
                'product_vendor_id' => ($product_vendor_id[$key] == 0) ? null : $product_vendor_id[$key],
                'product_varian_id' => ($product_varian_id[$key] == 0) ? null : $product_varian_id[$key],
                'quantity' => $quantity[$key],
                'value' => $value[$key],
                // 'status' => $status[$key],
                'batch_status_id' => $status[$key],
                'is_consigment' => $is_consigment[$key],
            ];

            if ($batch_item_id[$key] == 0) {
                BatchItem::create($updateItemData);
            } else {
                BatchItem::where('id', $batch_item_id[$key])->update($updateItemData);
            }
        }
    }

    public function setBatch(Batch $batch)
    {
        $this->batch = $batch;
        return $this;
    }

    public function getBatch()
    {
        return $this->batch;
    }

    public function setDatatables(bool $value)
    {
        $this->datatables = $value;
        return $this;
    }

    public function getDatatables()
    {
        return $this->datatables;
    }

    public function getIterator()
    {
        return new \ArrayIterator($this->getBatch());
    }

    public static function generateBatchNo()
    {
        $q = Batch::count();
        $prefix = 'B'; //prefix = B-16032020-00001
        $separator = '-';
        $date = date('dmY');
        $number = 1; //format
        $new_number = sprintf("%05s", $number);
        $code = $prefix . ($separator) . $date . ($separator) . $new_number;
        $batch_no = $code;
        if ($q > 0) {
            $last = Batch::orderBy('id', 'desc')->value('batch_no');
            $last = explode("-", $last);
            if ($last[1] == $date) {
                $batch_no = $prefix . ($separator) . $last[1] . ($separator) . (sprintf("%05s", $last[2] + 1));
            }
        }
        return $batch_no;
    }

    public function updateItemOrder($request)
    {
        DB::beginTransaction();
        try {
            $returned = [];
            foreach ($request->batch_item_id as $key => $val) {
                request()->validate([
                    'filename-' . $val . '.*' => 'mimes:jpeg,jpg,png'
                ]);
                $is_returned = 0;
                if($request->quantity == $request->quantity_return) {
                    $is_returned = 1;
                    $returned [] = TRUE;
                } else {
                    $returned [] = FALSE;
                }
                $name_file = null;
                if ($request->hasfile('filename-' . $val . '')) {
                    $file = $request->file('filename-' . $val . '');
                    foreach ($file as $image) {
                        $name_file = rand() . $image->getClientOriginalName();
                        $image_data = new BatchItemReason;
                        $image_data->batch_item_id = $val;
                        $image_data->filename = $name_file;
                        $image_data->save();
                        $image->storeAs('public/file_import_all/', $name_file);
                    }
                }
                $data = [
                    'quantity_return' => $request->quantity_return[$key],
                    'reason'          => $request->reason[$key],
                    'is_returned'     => $is_returned,
                    'is_confirmed'    => 1
                ];
                $data_inventory = [
                    'stock_ret' => $request->quantity_return[$key]
                ];
                Inventory::where('ms_batch_item_id', $val)->update($data_inventory);
                BatchItem::find($val)->update($data);
            }
            if(array_sum($returned) == count($returned)) {
                BatchShipmentHistory::create([
                    'batch_id' => $request->batch_id,
                    'batch_status_shipment_id' => 3,
                    'datetime' => Carbon::now()
                ]);
            }
            DB::commit();
        } catch (\Exception $e) {
            // rollback db
            DB::rollback();
            throw new SendErrorMsgException($e->getMessage());
        }
    }

    public function updateShipmentHistory($request)
    {
        $data = [
            'batch_id'  => $request->batch_id,
            'batch_status_shipment_id' => $request->status_shipment,
            'datetime' => Carbon::now()
        ];
        if ($request->status_shipment == 2) {
            $this->updateIsAccepted($request->batch_id);
            $this->updateIsConfirmed($request->batch_id);
        }

        return BatchShipmentHistory::create($data);
    }

    public function updateIsAccepted($batch_id)
    {
        return BatchItem::where('batch_id', $batch_id)->update(['is_accepted' => 1]);
    }

    public function updateIsConfirmed($batch_id)
    {
        return Batch::where('id', $batch_id)->update(['is_confirmed' => 1]);
    }

    public function batchItemReason($batch_item_id)
    {
        $reason_image = BatchItemReason::where('batch_item_id', $batch_item_id)->pluck('filename');
        $html_image = [];
        $no = 1;
        if ($reason_image->count() > 0) {
            foreach ($reason_image as $images) {
                $link = '<a download data-fancybox href="' . url('admin/storage/other/' . $images . '') . '">' . $no . '</a>';
                array_push($html_image, $link);
                $no++;
            }
            return implode(" / ", $html_image);
        }
        return null;
    }

    public function validasi(BatchRequest $request)
    {
        // $vendor_id = $request['vendor_id'];
        $quantity = $request->quantity;
        $value = $request->value;
        $product_id = $request->product_id;
        // $batch_item_id = $data['batch_item_id'];
        // $product_vendor_id = $data['product_vendor_id'];
        // $product_varian_id = $data['product_varian_id'];
        // // $status = $data['status'];
        $status = $request->batch_status_id;
        // $is_consigment = $data['is_consigment'];
        foreach ($product_id as $key => $val) {
            if ($product_id[$key] == 0) {
                throw new SendErrorMsgException('product <' . ($key + 1) . '> required');
            }

            if ($status[$key] == 0) {
                throw new SendErrorMsgException('status <' . ($key + 1) . '> required');
            }

            if (empty($quantity[$key])) {
                throw new SendErrorMsgException('quantity <' . ($key + 1) . '> required');
            }

            if (empty($value[$key])) {
                throw new SendErrorMsgException('value <' . ($key + 1) . '> required');
            }
        }

        return true;
    }
}
