<?php
namespace App\Services\Master;

use App\Model\Master\AddressType;
use Illuminate\Database\QueryException;
use Auth;

class AddressTypeService
{
    public $addressType;

    /**
     * get list query
     * 
     * @return App\Model\Product\AddressType;
     */
    public function list()
    {
        $userlogin = Auth::user()->id;
        return AddressType::where('user_id', $userlogin)->get();
    }
    
    /**
     * create data
     *
     * @param array $data
     * 
     * @return \App\Model\Master\AddressType
     * 
     * @throws \Exception
     */

    public function create(array $data)
    {
        
        try{
            return AddressType::create($this->generateData($data));
        }catch(QueryException $e){
            throw new \Exception($e->getMessage());
        }
    }

    public function update(array $data)
    {
        try{
            return $this->getAddressType()->update($this->generateData($data));
        }catch(QueryException $e){
            throw new \Exception($e->getMessage());
        }
    }

    public function delete()
    {
        try{
            return $this->getAddressType()->delete();
        }catch(QueryException $e){
            throw new \Exception($e->getMessage());
        }
    }


    public function generateData(array $data)
    {
        $user =  auth()->user();

        $data = [
            'name' => $data['name'],
            'user_id' => $user->id,
        ];

        return array_merge($data);
    }

    public function setAddressType(AddressType $addressType)
    {
        $this->addressType = $addressType;
        return $this;
    }

    public function getAddressType()
    {
        return $this->addressType;
    }
}