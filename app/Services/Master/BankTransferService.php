<?php

namespace App\Services\Master;

use App\Model\Master\BankTransfer;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class BankTransferService
{
    public $bankTransfer;
    /**
     * get list query
     *
     * @return App\Model\Master\BankTransfer;
     */
    public function list()
    {
        return BankTransfer::query();
    }

    /**
     * datatables data
     *
     * @return \App\Model\Master\BankTransfer
     */
    public function datatables()
    {
        $query = BankTransfer::whereNotIn('id', [1,2]); //$this->list();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * select2 data
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Model\Master\BankTransfer
     */
    public function select2(Request $request)
    {
        return BankTransfer::where('bank_name', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get()
            ->groupBy('bank_name');
    }

    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\Model\Master\BankTransfer
     *
     * @throws \Exception
     */
    public function create(array $data)
    {
        try {
            return BankTransfer::create($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * update data
     *
     * @param array $data
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function update(array $data)
    {
        try {
            return $this->getBankTransfer()->update($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function delete()
    {
        try {
            return  $this->getBankTransfer()->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function deleteById(array $ids)
    {
        try {
            return BankTransfer::whereIn('id', $ids)->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function generateData(array $data)
    {
        $generate = [
            'bank_name' => $data['bank_name'],
            'name' => $data['name'],
            'virtual_code' => $data['virtual_code'],
            'desc' => $data['desc'],
            'is_parent' => 1
        ];
        if (isset($data['icon'])) {
            $generate['icon'] = $data['icon'];
        }
        return $generate;
    }

    public function setBankTransfer(BankTransfer $bankTransfer)
    {
        $this->bankTransfer = $bankTransfer;
        return $this;
    }

    public function getBankTransfer()
    {
        return $this->bankTransfer;
    }
}
