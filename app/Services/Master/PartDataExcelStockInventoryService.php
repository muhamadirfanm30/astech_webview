<?php

namespace App\Services\Master;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Model\Master\ReportHpp;
use Yajra\DataTables\DataTables;
use App\Model\Master\PDESHistory;
use Illuminate\Support\Facades\DB;
use App\Model\Master\PartsDataExcel;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\ApiController;
use Illuminate\Database\QueryException;
use App\Model\Master\GeneralJournalDetail;
use App\Model\Master\PartDataExcelStockInventory;

class PartDataExcelStockInventoryService
{
    public $pdesi;

    /**
     * get list query
     *
     * @return App\Model\Master\PartDataExcelStockInventory;
     */
    public function list()
    {

        return PartDataExcelStockInventory::with('part_data_stock.asc')->orderBy('id', 'DESC');
    }

    /**
     * datatables data
     *
     * @return \App\Model\Product\PartDataExcelStockInventory
     */
    public function datatables($request = null)
    {
        $query = $this->list();

        if(!empty($request->date_from) && !empty($request->date_to)) {
            $date_from = Carbon::parse($request->date_from)->format('Y-m-d');
            $date_to = Carbon::parse($request->date_to)->addDays(1)->format('Y-m-d');
            $query->whereBetween('created_at', array($date_from, $date_to));
        }

        if(!empty($request->asc_id)) {
            $query->whereHas('part_data_stock', function ($q) use ($request) {
                $q->where('asc_id', $request->asc_id);
            });
        }

        $query = $query->get();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * select2 data
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Model\Product\PartDataExcelStockInventory
     */
    public function select2($search_by, $asc_id=null, $filter_stock=null, Request $request)
    {
        $query = PartDataExcelStockInventory::with('part_data_stock');
        $query = $query->whereHas('part_data_stock', function ($q) use ($request,$search_by,$asc_id) {
                    $q->where($search_by, 'like', '%' . $request->q . '%');
                    if ($asc_id != null) {
                        $q->where('asc_id', $asc_id);
                    }
                });
                if($filter_stock == 'filter_petty') {
                    $query->where('stock','>=', 0);
                }
                if($filter_stock == 'filter_order') {
                    $query->where('stock','>', 0);
                }
        return $query = $query->get();
    }
    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\Model\Master\PartDataExcelStockInventory
     *
     * @throws \Exception
     */
    public function create(array $data)
    {
        try {
            $create = PartDataExcelStockInventory::create($this->generateData($data));
            if($create->stock > 0) {
                $stock_inventory_service = new PartDataExcelStockInventoryService();
                $data_history_inventory = [
                    'date' => Carbon::parse($create->created_at)->format('Y-m-d'),
                    'pdesi_id_out' => null,
                    'pdesi_id_in' => ($create->id),
                    'amount' => ($create->stock),
                    'type_amount' =>  1,
                    'type_mutation' =>  6,
                    'general_journal_details_id' => null,
                    'part_data_excel_id' => null
                ];
                $stock_inventory_service->historyLogProcess($data_history_inventory);
                // $this->historyLogProcess(null, $create->id, $create->stock, 1, 8, null, null);
            }
            return $create;
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * update data
     *
     * @param array $data
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function update(array $data)
    {
        try {
            return $this->getPartDataExcelStockInventory()->update($data);
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function delete()
    {
        try {
            return  $this->getPartDataExcelStockInventory()->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function deleteById(array $ids)
    {
        try {
            return PartDataExcelStockInventory::whereIn('id', $ids)->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function generateData(array $data)
    {
        return [
            'part_data_stock_id' => $data['part_data_stock_id'],
            'stock' => $data['stock'] != '' ? $data['stock'] : 0,
            'selling_price' => $data['selling_price'],
        ];
    }

    public function setPartDataExcelStockInventory(PartDataExcelStockInventory $pdesi)
    {
        $this->pdesi = $pdesi;
        return $this;
    }

    public function getPartDataExcelStockInventory()
    {
        return $this->pdesi;
    }

    public function decreaseStock($pdesi_id, $quantity) {
        $q_stock = PartDataExcelStockInventory::where('id', $pdesi_id);
        $result = [];
        if($q_stock->count() > 0) {
            $q_stock = $q_stock->value('stock');
            $result['stock_now'] = $q_stock;
            if($quantity > $q_stock) {
                $result['status'] = false;
                return $result;
            }
            $update_stock = PartDataExcelStockInventory::where('id', $pdesi_id)->update(['stock' => ($q_stock - $quantity)]);
            $result['status'] = true;
            return $result;
        }
    }

    public function decreaseStockUpdate($pdesi_id, $part_data_excel_id, $quantity) {
        $q_stock = PartDataExcelStockInventory::where('id', $pdesi_id);
        $last_value_pde = PartsDataExcel::where('id', $part_data_excel_id)->first();
        $last_q_stock = PartDataExcelStockInventory::where('id', $last_value_pde->part_data_stock_inventories_id)->value('stock');
        $result = [];
        if($last_value_pde->part_data_stock_id != $pdesi_id) {
            $restore_stock = PartDataExcelStockInventory::where('id', $last_value_pde->part_data_stock_inventories_id)->update(['stock' => ($last_q_stock + $quantity)]);
            $q_stock = $q_stock->value('stock');
            $update_stock = PartDataExcelStockInventory::where('id', $pdesi_id)->update(['stock' => ($q_stock - $quantity)]);
            $result['status'] = true;
            return $result;
        } else {
            if($q_stock->count() > 0) {
                $q_stock = $q_stock->value('stock');
                $result['stock_now'] = $q_stock;
                if($quantity > $q_stock) {
                    $result['status'] = false;
                    return $result;
                }
                $update_stock = PartDataExcelStockInventory::where('id', $pdesi_id)->update(['stock' => ($q_stock - ($quantity - $last_value_pde->quantity))]);
                $result['status'] = true;
                return $result;
            }
        }
    }

    public function restoreStock($pdesi_id, $quantity) {
        $q_stock = PartDataExcelStockInventory::where('id', $pdesi_id);
        $q_stock = $q_stock->value('stock');
        $update_stock = PartDataExcelStockInventory::where('id', $pdesi_id)->update(['stock' => ($q_stock + $quantity)]);

    }

    public function increasePartDataStock($pdesi_id, $quantity) {
        $q_stock = PartDataExcelStockInventory::where('id', $pdesi_id);
        if($q_stock->count() > 0) {
            $q_stock = $q_stock->value('stock');
            $result['stock_now'] = $q_stock;
            $update_stock = PartDataExcelStockInventory::where('id', $pdesi_id)->update(['stock' => ($q_stock + $quantity)]);
        }
    }

    public function increasePartDataStockUpdate($pdesi_id, $general_journal_id, $quantity) {
        $q_stock = PartDataExcelStockInventory::where('id', $pdesi_id);
        $last_value_gjd = GeneralJournalDetail::where('id', $general_journal_id)->first();
        $last_q_stock = PartDataExcelStockInventory::where('id', $last_value_gjd->part_data_stock_inventories_id)->value('stock');
        if($last_value_gjd->part_data_stock_inventories_id != $pdesi_id) {
            $restore_stock = PartDataExcelStockInventory::where('id', $last_value_gjd->part_data_stock_inventories_id)->update(['stock' => ($last_q_stock - $quantity)]);
            $q_stock = $q_stock->value('stock');
            $update_stock = PartDataExcelStockInventory::where('id', $pdesi_id)->update(['stock' => ($q_stock + $quantity)]);
        } else {
            if($q_stock->count() > 0) {
                $q_stock = $q_stock->value('stock');
                $result['stock_now'] = $q_stock;
                $update_stock = PartDataExcelStockInventory::where('id', $pdesi_id)->update(['stock' => ($q_stock - ($last_value_gjd->quantity - $quantity))]);
            }
        }
    }

    public function restoreStockIfOperational($general_journal_id, $quantity) {
        $last_value_gjd = GeneralJournalDetail::where('id', $general_journal_id)->first();
        $last_q_stock = PartDataExcelStockInventory::where('id', $last_value_gjd->part_data_stock_inventories_id)->value('stock');
        if($last_value_gjd->part_data_stock_id != null) {
            $restore_stock = PartDataExcelStockInventory::where('id', $last_value_gjd->part_data_stock_inventories_id)->update(['stock' => ($last_q_stock - $quantity)]);
            $stock_inventory_service = new PartDataExcelStockInventoryService();
            $data_history_inventory = [
                'date' => $last_value_gjd->date,
                'pdesi_id_out' => $last_value_gjd->part_data_stock_inventories_id,
                'pdesi_id_in' => null,
                'amount' => $last_value_gjd->quantity,
                'type_amount' =>  0,
                'type_mutation' =>  2,
                'general_journal_details_id' => $general_journal_id,
                'part_data_excel_id' => null
            ];
            return $stock_inventory_service->historyLogProcess($data_history_inventory);
            // return $pdes_service->historyLogProcess($last_value_gjd->part_data_stock_id, null, $last_value_gjd->quantity, 0, 6, $general_journal_id, null);

        }

        return false;
    }

    // public function selectAutoInventory($pdes_id) {
    //     $date = PartDataExcelStockInventory::select([
    //         DB::raw('(MIN(CAST(year AS UNSIGNED))) AS min_year'),
    //         DB::raw('(MAX(CAST(year AS UNSIGNED))) AS max_year'),
    //         DB::raw('(MIN(CAST(month AS UNSIGNED))) AS min_month'),
    //         DB::raw('(MAX(CAST(month AS UNSIGNED))) AS max_month'),
    //     ])->first();
    //     $min_month = $date->min_month;
    //     $max_month = $date->max_month;
    //     $min_year = $date->min_year;
    //     $max_year = $date->max_year;
    //     foreach (array_reverse(range($min_year, $max_year)) as $year) {
    //         foreach (array_reverse(range($min_month,$max_month)) as $month) {
    //             $data = PartDataExcelStockInventory::where('part_data_stock_id', $pdes_id)->where('month', $month)->where('year', $year);
    //             if($data->count() > 0) {
    //                 if($data->first()->stock >= 0) {
    //                     return $data = $data->first();
    //                 }
    //             }
    //         }
    //     }
    //     return null;
    // }

    public function historyLogProcess(array $data) {
        // $pds_id_out=null, $pds_id_in=null, $amount, $type_amount, $type_mutation=null, $general_journal_detail_id =null, $part_data_excel_id=null

        // $type_amount = 1 in; 0 out;
        // $type_mutation adalah berikut:
        // general_journal, orders_data_excel
        // 0 = add_general_journal;
        // 1 = edit_general_journal;
        // 2 = delete_general_journal;
        // 3 = add_order_data_excel;
        // 4 = edit_order_data_excel;
        // 5 = delete_order_data_excel;
        // 6 = add_inventory_part_stock

        $id = null;
        if($data['type_amount'] == 1) { //in
            if(
                ($data['type_mutation'] == 0 && isset($data['general_journal_details_id']))||
                ($data['type_mutation'] == 1 && isset($data['general_journal_details_id']))||
                ($data['type_mutation'] == 4 && isset($data['part_data_excel_id']))||
                ($data['type_mutation'] == 5 && isset($data['part_data_excel_id']))||
                ($data['type_mutation'] == 6)
            ) {
                $id = $data['pdesi_id_in'];
            }
        } else if ($data['type_amount'] == 0) { // out
            if(
                ($data['type_mutation'] == 1 && isset($data['general_journal_details_id']))||
                ($data['type_mutation'] == 2 && isset($data['general_journal_details_id']))||
                ($data['type_mutation'] == 3 && isset($data['part_data_excel_id']))||
                ($data['type_mutation'] == 4 && isset($data['part_data_excel_id']))
            ) {
              $id = $data['pdesi_id_out'];
            }
        }
        $q = PartDataExcelStockInventory::with('part_data_stock')->where('id',$id)->first();
        $row = [
            'general_journal_details_id'        => ($data['general_journal_details_id'] ?? null),
            'part_data_excel_id'                => ($data['part_data_excel_id'] ?? null),
            'part_data_stock_inventories_id'    => $id,
            'part_data_stock_bundle_details_id' => ($data['part_data_stock_bundle_details_id'] ?? null),
            'asc_name'                          => $q->part_data_stock->asc_name,
            'part_description'                  => $q->part_data_stock->part_description,
            'stock_before'                      => $q->stock_before,
            'amount'                            => $data['amount'],
            'type_amount'                       => $data['type_amount'],
            'type_mutation'                     => $data['type_mutation'],
            'date'                              => $data['date'],
            'user_id'                           => Auth::user()->id,
            'service_order_no'                  => ($data['service_order_no'] ?? null),
            'is_deleted'                        => (($data['type_mutation'] == 2 || $data['type_mutation'] == 5) ? 1 : 0)
        ];
        //simpan PDES Histories ;
        return $created_mutation_histories = $this->createMutationHistories($row);

    }

    // $data mutasi ;
    // $type_amount == 0=minus; 1=plus ;
    public function createMutationHistories(array $data) {
        $stock_after = ($data['type_amount'] === 1 ? ($data['stock_before'] + $data['amount']) : ($data['stock_before'] - $data['amount']));
        $data = [
            'general_journal_details_id'        => $data['general_journal_details_id'],
            'part_data_excel_id'                => $data['part_data_excel_id'],
            'part_data_stock_inventories_id'    => $data['part_data_stock_inventories_id'],
            'part_data_stock_bundle_details_id' => $data['part_data_stock_bundle_details_id'],
            'asc_name'                          => $data['asc_name'],
            'part_description'                  => $data['part_description'],
            'amount'                            => $data['amount'],
            'type_amount'                       => $data['type_amount'],
            'type_mutation'                     => $data['type_mutation'],
            'stock_before'                      => $data['stock_before'],
            'stock_after'                       => $stock_after,
            'date'                              => $data['date'],
            'user_id'                           => Auth::user()->id,
            'service_order_no'                  => $data['service_order_no'],
            'is_deleted'                        => $data['is_deleted'],

        ];
        return PDESHistory::create($data);
    }

    public function deleteMutationHistories($pde_id, $pdesi_id) {
        $data = [];
        return PDESHistory::where('part_data_stock_inventories_id', $pdesi_id)
        ->where('part_data_excel_id', $pde_id)->deleteMutationHistories->delete();
    }

    public function getSellingPrice($pdesi_id) {
        $data = PartDataExcelStockInventory::where('id', $pdesi_id);
        if($data->count() > 0) {
            return $data->value('selling_price');
        }
        return 0;
    }

    public function setSellingPrice(array $row) {
        try {
            $data = PartDataExcelStockInventory::where('id', $row['pdesi_id']);
            if($data->count() > 0) {
                return $data->update(['selling_price' => $row['selling_price']]);
            }
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function saveReportHpp($amount, $type_amount, $type_mutation, $pdesi_id, $gjd_id = null, $pde_id = null) {
        // $type_mutation {
            // 0 = add_general_journal;
            // 1 = edit_general_journal;
            // 2 = delete_general_journal;
            // 3 = add_order_data_excel;
            // 4 = edit_order_data_excel;
            // 5 = delete_order_data_excel;
            // 6 = add_inventory_part_stock
        // }
        $out_stock = PartsDataExcel::where('part_data_stock_inventories_id', $pdesi_id)->sum('quantity');
        $data = GeneralJournalDetail::select([
            DB::raw('SUM(general_journal_details.kredit) AS total_kredit'),
            DB::raw('SUM(general_journal_details.quantity) AS total_quantity'),
            'part_data_stock_inventories_id'
        ])
        ->join('ode_part_data_stock_inventories', 'ode_part_data_stock_inventories.id', '=', 'general_journal_details.part_data_stock_inventories_id')
        ->where('part_data_stock_inventories_id', $pdesi_id)
        ->where('category', 'material')
        ->groupBy('part_data_stock_inventories_id')->whereNotNull('part_data_stock_inventories_id')->first();

        if($data) {
            $cur_total_kredit = $data->total_kredit;
            $cur_sisa_qty = $data->total_quantity - $out_stock;
            $hpp = @($data->total_kredit / $data->total_quantity);
            $out_mul_hpp = 0;
            $get_report_hpp = ReportHpp::where('part_data_stock_inventories_id', $pdesi_id)->orderBy('id', 'DESC')->limit(1)->first();
            $kredit_latest = GeneralJournalDetail::where('part_data_stock_inventories_id', $pdesi_id)->orderBy('id', 'DESC')->limit(1)->first(['kredit'])->kredit;
            if($get_report_hpp) {
                $out_mul_hpp =  $type_amount == 0 ? ($get_report_hpp->hpp * $amount) :  0;
                $cur_total_kredit = $type_amount == 0 ? ($get_report_hpp->cur_total_kredit - $out_mul_hpp) :  (($get_report_hpp->type_amount == 1 && $get_report_hpp->type_mutation == 3) ? $data->total_kredit : $get_report_hpp->cur_total_kredit + $kredit_latest);

                if($type_mutation == 5) { // delete_orders_data_excel
                    $get_report_hpp_latest = ReportHpp::where('part_data_stock_inventories_id', $pdesi_id)->orderBy('id', 'DESC')->limit(1)->first();
                    $get_report_hpp = ReportHpp::where('part_data_stock_inventories_id', $pdesi_id)->where('type_mutation', 3)->where('part_data_excel_id', $pde_id)->orderBy('id', 'DESC')->limit(1)->first();
                    if($get_report_hpp) {
                        $cur_total_kredit = $get_report_hpp_latest->cur_total_kredit + $get_report_hpp->out_mul_hpp;
                        $cur_sisa_qty = $get_report_hpp_latest->cur_sisa_qty + $amount;
                    }
                    $cur_total_kredit = $get_report_hpp_latest->hpp + ($get_report_hpp_latest->hpp * $amount);

                }
                if($type_mutation == 2) { // delete_general_journal
                    $kredit = GeneralJournalDetail::where('part_data_stock_inventories_id', $pdesi_id)->where('id', $gjd_id)->first(['kredit'])->kredit;
                    $cur_total_kredit = $get_report_hpp->cur_total_kredit - $kredit;
                    $out_mul_hpp =  $get_report_hpp->out_mul_hpp;

                }

                $cur_sisa_qty = $type_amount == 0 ? ($get_report_hpp->cur_sisa_qty - $amount) :  ($get_report_hpp->cur_sisa_qty + $amount);
                $hpp = ($cur_sisa_qty != 0) ? @($cur_total_kredit / $cur_sisa_qty) : 0;
                if($hpp < 0) {
                    $hpp = 0;
                }

            }
            // if($cur_total_kredit <= 0) {
            //     $cur_total_kredit = 0;
            //     $cur_sisa_qty = 0;
            //     $hpp = 0;
            // }
            // if(($data->total_quantity - $out_stock) <= 0) {
            //     $cur_total_kredit = 0;
            //     $cur_sisa_qty = 0;
            //     $hpp = 0;
            // }

            return ReportHpp::create([
                'cur_total_kredit' => $cur_total_kredit,
                'cur_sisa_qty' => $cur_sisa_qty,
                'amount' => $amount,
                'type_amount' => $type_amount,
                'type_mutation' => $type_mutation,
                'hpp' => $hpp,
                'out_mul_hpp' => $out_mul_hpp,
                'part_data_stock_inventories_id' => $pdesi_id,
                'part_data_excel_id' => $pde_id,
                'user_id' => Auth::user()->id
            ]);

        }

    }
}
