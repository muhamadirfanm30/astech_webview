<?php

namespace App\Services\Master;

use App\Exceptions\HasChildException;
use App\Model\Master\MsCity;
use App\Model\Master\MsProvince;
use Illuminate\Database\QueryException;

class CityService
{
    public $city;

    /**
     * get list query
     *
     * @return App\Model\Master\MsCity;
     */
    public function list()
    {
        return MsCity::with('province');
    }

    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\Model\Master\MsCity
     *
     * @throws \Exception
     */
    public function create(array $data)
    {
        try {
            return MsCity::create($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * update data
     *
     * @param array $data
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function update(array $data)
    {
        try {
            return $this->getCity()->update($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function delete()
    {
        $city = $this->getCity();

        if ($city->districts->count() > 0) {
            throw new HasChildException('tidak bisa delete terdapat districts pada kota ini');
        }

        try {
            return $city->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function generateData(array $data)
    {
        $this->checkProvinsi($data);

        $data = [
            'name' => $data['name'],
            'ms_province_id' => $data['ms_province_id'],
        ];

        return $data;
    }

    public function checkProvinsi(array $data)
    {
        $provinsi = MsProvince::where('id', $data['ms_province_id'])->firstOrfail();

        return $provinsi;
    }

    public function setCity(MsCity $city)
    {
        $this->city = $city;
        return $this;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function deleteById(array $ids)
    {
        try {
            return MsCity::whereIn('id', $ids)->delete();
        } catch (QueryException $e) {
            throw new \Exception('Terdapat Beberapa Daerah Pada Kota Ini');
        }
    }
}
