<?php

namespace App\Services\Master;

use App\Model\Master\MsDistrict;
use App\Model\Master\MsZipcode;
use Illuminate\Database\QueryException;

class ZipcodeService
{
    public $zipcode;

    /**
     * get list query
     *
     * @return App\Model\Master\MsZipcode;
     */
    public function list()
    {
        return MsZipcode::with('district');
    }

    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\Model\Master\MsZipcode
     *
     * @throws \Exception
     */
    public function create(array $data)
    {
        $this->checkDistrik($data);

        $data = [
            'zip_no' => $data['zip_no'],
            'ms_district_id' => $data['ms_district_id'],
        ];

        try {
            return MsZipcode::create($data);
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * update data
     *
     * @param array $data
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function update(array $data)
    {
        $this->checkDistrik($data);

        $data = [
            'zip_no' => $data['zip_no'],
            'ms_district_id' => $data['ms_district_id'],
        ];

        $zipcode = $this->getZipcode();

        try {
            return $zipcode->update($data);
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function delete()
    {
        $zipcode = $this->getZipcode();

        try {
            return $zipcode->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function checkDistrik(array $data)
    {
        $distrik = MsDistrict::where('id', $data['ms_district_id'])->firstOrFail();

        if ($distrik == null) {
            throw new \Exception('distrik yg dipilih tidak ditemukan');
        }

        return $distrik;
    }

    public function setZipcode(MsZipcode $zipcode)
    {
        $this->zipcode = $zipcode;
        return $this;
    }

    public function getZipcode()
    {
        return $this->zipcode;
    }

    public function deleteById(array $ids)
    {
        try {
            return MsZipcode::whereIn('id', $ids)->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }
}
