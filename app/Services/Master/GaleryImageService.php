<?php

namespace App\Services\Master;

use App\User;
use App\Model\Master\ImageUpload;
use Illuminate\Database\QueryException;
use Auth;

class GaleryImageService
{
    public $galeryImages;

    /**
     * get list query
     * 
     * @return App\Model\Master\ImageUpload;
     */
    public function list()
    {
        return ImageUpload::query();
    }

    /**
     * update data
     *
     * @param array $data
     * 
     * @return bool
     * 
     * @throws \Exception
     */
    public function update(array $data)
    {
        $dataUpdate = [
            'alternatif_name'   => $data['alternatif_name'],
            'title'             => $data['title'],
            'desc'              => $data['desc'],
        ];

        $galery = $this->getGaleryImage();

        try {
            return $galery->update($dataUpdate);
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }


    public function setGaleryImage(ImageUpload $galeryImages)
    {
        $this->galeryImages = $galeryImages;
        return $this;
    }

    public function getGaleryImage()
    {
        return $this->galeryImages;
    }
}
