<?php

namespace App\Services\Master;

use Carbon\Carbon;
use Illuminate\Support\Arr;
use Yajra\DataTables\DataTables;
use App\Model\Master\ExcelIncome;
use Illuminate\Support\Facades\DB;
use App\Model\Master\ExcelIncomeLog;
use Illuminate\Support\Facades\Auth;
use App\Model\Master\ExcelIncomeDetail;
use Illuminate\Database\QueryException;
use App\Model\Master\ExcelIncomeDetailDescription;

class ExcelIncomeService
{
    public $excel_income;

    /**
     * get list query
     *
     * @return App\Model\Master\ExcelIncome;
     */
    public function list()
    {
        return ExcelIncome::with('excel_income_detail.details_desc');
    }

    public function datatables($request = null)
    {
        $query = $this->list();
        if(!empty($request->date_from) && !empty($request->date_to)) {
            $query->whereHas('excel_income_detail', function ($q) use ($request) {
                $date_from = Carbon::parse($request->date_from)->format('Y-m-d');
                $date_to = Carbon::parse($request->date_to)->format('Y-m-d');
                $q->where('date_transaction', '>=', $date_from)
                ->where('date_transaction', '<=', $date_to);
            });
        }
        $query = $query->orderBy('id', 'desc')->get();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\Model\Master\ExcelIncome
     *
     * @throws \Exception
     */
    public function create($row)
    {
        DB::beginTransaction();
        try {
            $insert_ei = ExcelIncome::create($this->generateData($row));
            $this->logs($insert_ei, 1);
            DB::commit();
            return $insert_ei;
        } catch (QueryException $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }
    }

    public function createDetail($data, $row) {
        DB::beginTransaction();
        try {
            $excel_income_detail = ExcelIncomeDetail::create([
                'excel_income_id'  => $data->id,
                'source'           => $row['source'],
                'date_transaction' => Carbon::parse($row['date_transaction'])->format('Y-m-d'),
                'date_transfer'    => Carbon::parse($row['date_transfer'])->format('Y-m-d'),
                'in_nominal'       => $row['in_nominal'],
                'refund'           => $row['refund'],
                'branch'           => $row['branch'],
            ]);
            foreach($row['description'] as $desc) {
                ExcelIncomeDetailDescription::create([
                    'excel_income_detail_id' => $excel_income_detail->id,
                    'description' => $desc
                ]);
            }
            // $this->logs($data, 1);
            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }

    }

    public function updateDetail($data, $row) {
        DB::beginTransaction();
        try {
            $excel_income_detail = ExcelIncomeDetail::where('id',$data->id)->update([
                'source'           => $row['source'],
                'date_transaction' => Carbon::parse($row['date_transaction'])->format('Y-m-d'),
                'date_transfer'    => Carbon::parse($row['date_transfer'])->format('Y-m-d'),
                'in_nominal'       => $row['in_nominal'],
                'refund'           => $row['refund'],
                'branch'           => $row['branch'],
            ]);
            $this->updateDescription($row,$data->id);
            // $this->logs($data, 1);
            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }

    }

    public function updateDescription($row, $detail_id)
    {

        $description_update = [];
        $description_insert = [];

        $description_id = $row->description_id;
        $description = $row->description;
        // loop request
        foreach ($description_id as $key => $value) {
            // check request
            $check_description = !empty($description[$key]);
            if ($check_description) {
                // jika id tidak 0 == update
                if ($value != 0) {
                    $description_update[] = [
                        'id' => $value,
                        'description' => $row['description'][$key],
                    ];
                } else { // jika id 0 == insert
                    $description_insert[] = [
                        'excel_income_detail_id' => $detail_id,
                        'description' => $row['description'][$key],
                    ];
                }
            }
        }

        DB::beginTransaction();
        try {
            // update sparrepart
            if (count($description_update) > 0) {
                foreach ($description_update as $data) {
                    ExcelIncomeDetailDescription::where('id', $data['id'])->update([
                        'description' => $data['description'],
                    ]);
                }
            }

            // insert part_datas
            if (count($description_insert) > 0) {
                ExcelIncomeDetailDescription::insert($description_insert);
            }
            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }
    }

    public function generateData($row)
    {

        $data_gj = [
            "code" => $row['code'],
            "year" => $row['year'],
            "month" => $row['month'],
            "desc" => $row['desc']
        ];
        return $data_gj;
    }

    /**
     * update data
     *
     * @param array $data
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function updateX($data)
    {
        DB::beginTransaction();

        try {
            $update = $this->getData()->update($this->generateData($data));
            // $this->updateDetails($data);
            $this->logs($this->getData(), 2);
            DB::commit();
            return $update;

        } catch (QueryException $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function deleteX()
    {
        $excel_income = $this->getData();
        DB::beginTransaction();

        try {
            $this->logs($excel_income, 3);
            $delete = $excel_income->delete();
            DB::commit();


        } catch (QueryException $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\Model\Master\ExcelIncome
     *
     * @throws \Exception
     */

    public function setData(ExcelIncome $excel_income)
    {
        $this->excel_income = $excel_income;
        return $this;
    }

    public function getData()
    {
        return $this->excel_income;
    }

    public function deleteById(array $ids)
    {
        try {
            return ExcelIncome::whereIn('id', $ids)->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function generateCode()
    {
        $q          = ExcelIncome::count();
        $prefix     = 'EI'; //prefix = GJ-000001
        $separator  = '-';
        $number     = 1; //format
        $new_number = sprintf("%06s", $number);
        $code       = $prefix . ($separator) . ($new_number);
        $gj_code   = $code;
        if ($q > 0) {
            $last     = ExcelIncome::orderBy('id', 'desc')->value('code');
            $last     = explode("-", $last);
            $gj_code = $prefix . ($separator) . (sprintf("%06s", $last[1] + 1));
        }
        return $gj_code;
    }

    public function logs($data, $type) { // type created/changed/deleted
        return $log = ExcelIncomeLog::create([
            'excel_income_id' => $data->id,
            'code' => $data->code,
            'user_id' => Auth::user()->id,
            'type' => $type,
        ]);

    }


}
