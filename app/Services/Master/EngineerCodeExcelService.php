<?php

namespace App\Services\Master;

use Illuminate\Http\Request;
use App\Model\Master\Services;
use Yajra\DataTables\DataTables;
use Illuminate\Database\QueryException;
use App\Model\Master\EngineerCodeExcel;

class EngineerCodeExcelService
{
    public $ece;

    /**
     * get list query
     *
     * @return App\Model\Master\EngineerCodeExcel;
     */
    public function list()
    {
        return EngineerCodeExcel::query();
    }

    /**
     * datatables data
     *
     * @return \App\Model\Product\EngineerCodeExcel
     */
    public function datatables()
    {
        $query = $this->list();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * select2 data
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Model\Product\EngineerCodeExcel
     */
    public function select2(Request $request)
    {
        return EngineerCodeExcel::where('engineer_code', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();
    }

    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\Model\Master\EngineerCodeExcel
     *
     * @throws \Exception
     */
    public function create(array $data)
    {
        try {
            return EngineerCodeExcel::create($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * update data
     *
     * @param array $data
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function update(array $data)
    {
        try {
            return $this->getEngineerCodeExcel()->update($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function delete()
    {
        try {
            return  $this->getEngineerCodeExcel()->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function deleteById(array $ids)
    {
        try {
            return EngineerCodeExcel::whereIn('id', $ids)->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function generateData(array $data)
    {
        return [
            'engineer_code' => $data['engineer_code'],
            'engineer_name' => $data['engineer_name'],
        ];
    }

    public function setEngineerCodeExcel(EngineerCodeExcel $ece)
    {
        $this->ece = $ece;
        return $this;
    }

    public function getEngineerCodeExcel()
    {
        return $this->ece;
    }
}
