<?php

namespace App\Services\Master;

use Illuminate\Http\Request;
use App\Model\Master\Services;
use Yajra\DataTables\DataTables;
use Illuminate\Database\QueryException;
use App\Model\Master\StatusExcel;

class StatusExcelService
{
    public $status_excel;

    /**
     * get list query
     *
     * @return App\Model\Master\StatusExcel;
     */
    public function list()
    {
        return StatusExcel::query();
    }

    /**
     * datatables data
     *
     * @return \App\Model\Product\StatusExcel
     */
    public function datatables()
    {
        $query = $this->list();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * select2 data
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Model\Product\StatusExcel
     */
    public function select2(Request $request)
    {
        return StatusExcel::where('status', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();
    }

    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\Model\Master\StatusExcel
     *
     * @throws \Exception
     */
    public function create(array $data)
    {
        try {
            return StatusExcel::create($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * update data
     *
     * @param array $data
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function update(array $data)
    {
        try {
            return $this->getStatusExcel()->update($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function delete()
    {
        try {
            return  $this->getStatusExcel()->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function deleteById(array $ids)
    {
        try {
            return StatusExcel::whereIn('id', $ids)->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function generateData(array $data)
    {
        return [
            'status' => $data['status'],
        ];
    }

    public function setStatusExcel(StatusExcel $status_excel)
    {
        $this->status_excel = $status_excel;
        return $this;
    }

    public function getStatusExcel()
    {
        return $this->status_excel;
    }
}
