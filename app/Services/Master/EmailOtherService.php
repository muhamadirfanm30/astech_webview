<?php

namespace App\Services\Master;

use App\Model\Master\EmailStatusOrder;
use App\Model\Master\EmailOther;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class EmailOtherService
{
    public $email;
    /**
     * get list query
     *
     * @return App\Model\Master\EmailStatusOrder;
     */
    public function list()
    {
        return EmailStatusOrder::with('order_status')->whereNotNull('orders_statuses_id')->groupBy('orders_statuses_id');
    }

    public function listEmailOther()
    {
        return EmailOther::groupBy('name_module');
    }

    /**
     * datatables data
     *
     * @return \App\Model\Master\EmailStatusOrder
     */
    public function datatables()
    {
        $query = $this->list();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function datatablesEmailOther()
    {
        $query = $this->listEmailOther();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }



}
