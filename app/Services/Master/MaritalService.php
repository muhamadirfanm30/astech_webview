<?php

namespace App\Services\Master;

use App\Model\Master\Marital;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class MaritalService
{
    public $marital;
    /**
     * get list query
     *
     * @return App\Model\Master\Marital;
     */
    public function list()
    {
        return Marital::query();
    }

    /**
     * datatables data
     *
     * @return \App\Model\Master\Marital
     */
    public function datatables()
    {
        $query = $this->list();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * select2 data
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Model\Master\Marital
     */
    public function select2(Request $request)
    {
        return Marital::where('status', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();
    }

    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\Model\Master\Marital
     *
     * @throws \Exception
     */
    public function create(array $data)
    {
        try {
            return Marital::create($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * update data
     *
     * @param array $data
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function update(array $data)
    {
        try {
            return $this->getMarital()->update($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function delete()
    {
        try {
            return  $this->getMarital()->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function deleteById(array $ids)
    {
        try {
            return Marital::whereIn('id', $ids)->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function generateData(array $data)
    {
        return [
            'status' => $data['status'],
        ];
    }

    public function setMarital(Marital $marital)
    {
        $this->marital = $marital;
        return $this;
    }

    public function getMarital()
    {
        return $this->marital;
    }
}
