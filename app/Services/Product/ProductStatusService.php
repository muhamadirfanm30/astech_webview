<?php

namespace App\Services\Product;

use App\Model\Product\ProductStatus;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class ProductStatusService
{
    public $status;

    /**
     * get list query
     * 
     * @return App\Model\Product\ProductStatus;
     */
    public function list()
    {
        return ProductStatus::query();
    }

    /**
     * datatables data
     * 
     * @return \App\Model\Product\ProductBrand
     */
    public function datatables()
    {
        $query = $this->list();

        return DataTables::of($query)
            ->toJson();
    }

    /**
     * select2 data
     *
     * @param \Illuminate\Http\Request $request
     * 
     * @return \App\Model\Product\ProductStatus
     */
    public function select2(Request $request)
    {
        return ProductStatus::where('name', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();
    }

    /**
     * create data
     *
     * @param array $data
     * 
     * @return \App\Model\Product\ProductStatus
     * 
     * @throws \Exception
     */
    public function create(array $data)
    {
        try {
            return ProductStatus::create($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * update data
     *
     * @param array $data
     * 
     * @return bool
     * 
     * @throws \Exception
     */
    public function update(array $data)
    {
        try {
            return $this->getStatus()->update($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     * 
     * @return bool|null
     * 
     * @throws \Exception
     */
    public function delete()
    {
        try {
            return $this->getStatus()->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     * 
     * @param array ids
     * 
     * @return bool|null
     * 
     * @throws \Exception
     */
    public function deleteById(array $ids)
    {
        try {
            return ProductStatus::whereIn('id', $ids)->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function generateData(array $data)
    {
        return [
            'name' => $data['name'],
        ];
    }

    public function setStatus(ProductStatus $status)
    {
        $this->status = $status;
        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }
}
