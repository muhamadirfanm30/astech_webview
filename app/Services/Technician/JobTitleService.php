<?php

namespace App\Services\Technician;

use App\Model\Technician\JobExperience;
use App\Model\Technician\JobTitle;
use App\Model\Technician\Technician;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class JobTitleService
{
    public $job;

    /**
     * get list query
     * 
     * @return App\Model\Technician\JobTitle;
     */
    public function list()
    {
        return JobTitle::with([
            'job_title_category',
            'user'
        ]);
    }

    /**
     * datatables data
     * 
     * @return \App\Model\Technician\JobTitle
     */
    public function datatables()
    {
        $query = $this->list();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * select2 data
     *
     * @param \Illuminate\Http\Request $request
     * 
     * @return \App\Model\Technician\JobTitle
     */
    public function select2(Request $request)
    {
        return JobTitle::where('description', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();
    }

    /**
     * find data
     *
     * @param int $id
     * 
     * @return \App\Model\Technician\JobTitle
     */
    public function find(int $id)
    {
        return JobTitle::where('id', $id)->first();
    }

    /**
     * create data
     *
     * @param array $data
     * 
     * @return \App\Model\Technician\JobTitle
     * 
     * @throws \Exception
     */
    public function create(array $data)
    {
        try {
            return JobTitle::create($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * update data
     *
     * @param array $data
     * 
     * @return bool
     * 
     * @throws \Exception
     */
    public function update(array $data)
    {
        try {
            return $this->getJob()->update($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     * 
     * @return bool|null
     * 
     * @throws \Exception
     */
    public function delete()
    {
        $tek = Technician::where('job_title_id', $this->getJob()->id)->count();
        if ($tek > 0) {
            throw new \Exception('Cannot Delete');
        }

        $exp = JobExperience::where('job_title_id', $this->getJob()->id)->count();
        if ($exp > 0) {
            throw new \Exception('Cannot Delete');
        }

        try {
            return $this->getJob()->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function deleteById(array $ids)
    {
        try {
            return JobTitle::whereIn('id', $ids)->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function generateData(array $data)
    {
        return [
            'description' => $data['description'],
            'user_id' => $data['user_id'],
            'job_title_category_id' => $data['job_title_category_id'],
        ];
    }

    public function setJob(JobTitle $job)
    {
        $this->job = $job;
        return $this;
    }

    public function getJob()
    {
        return $this->job;
    }
}
