<?php

namespace App\Services\Technician;

use App\Model\Technician\JobTitle;
use App\Model\Technician\JobTitleCategory;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class JobTitleCategoryService
{
    public $category;

    /**
     * get list query
     * 
     * @return App\Model\Technician\JobTitleCategory;
     */
    public function list()
    {
        return JobTitleCategory::query();
    }

    /**
     * datatables data
     * 
     * @return \App\Model\Technician\JobTitleCategory
     */
    public function datatables()
    {
        $query = $this->list();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * select2 data
     *
     * @param \Illuminate\Http\Request $request
     * 
     * @return \App\Model\Technician\JobTitleCategory
     */
    public function select2(Request $request)
    {
        return JobTitleCategory::where('name', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();
    }

    /**
     * find data
     *
     * @param int $id
     * 
     * @return \App\Model\Technician\JobTitleCategory
     */
    public function find(int $id)
    {
        return JobTitleCategory::where('id', $id)->first();
    }

    /**
     * create data
     *
     * @param array $data
     * 
     * @return \App\Model\Technician\JobTitleCategory
     * 
     * @throws \Exception
     */
    public function create(array $data)
    {
        try {
            return JobTitleCategory::create($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * update data
     *
     * @param array $data
     * 
     * @return bool
     * 
     * @throws \Exception
     */
    public function update(array $data)
    {
        try {
            return $this->getCategory()->update($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     * 
     * @return bool|null
     * 
     * @throws \Exception
     */
    public function delete()
    {
        $job_tit = JobTitle::where('job_title_category_id', $this->getCategory()->id)->count();
        if ($job_tit > 0) {
            throw new \Exception('Cannot Delete');
        }

        try {
            return $this->getCategory()->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function deleteById(array $ids)
    {
        try {
            return JobTitleCategory::whereIn('id', $ids)->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function generateData(array $data)
    {
        return [
            'name' => $data['name'],
        ];
    }

    public function setCategory(JobTitleCategory $category)
    {
        $this->category = $category;
        return $this;
    }

    public function getCategory()
    {
        return $this->category;
    }
}
