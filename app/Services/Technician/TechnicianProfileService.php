<?php

namespace App\Services\Technician;

use App\Exceptions\SendErrorMsgException;
use App\Model\GeneralSetting;
use App\Model\Master\MsPriceService;
use App\Model\Technician\JobExperience;
use App\Model\Technician\JobTitle;
use App\Model\Technician\JobTitleCategory;
use App\Model\Technician\Technician;
use App\Services\Technician\JobExperienceService;
use Auth;
use DB;
use Yajra\DataTables\DataTables;

class TechnicianProfileService
{
    public $technician;

    /**
     * price service list
     */
    public function priceServiceList()
    {
        $user    = Auth::user();
        $teknisi = Technician::where('user_id', $user->id)->first();
        if ($teknisi == null) {
            $query = [];
        } else {
            // $query = MsPriceService::with(['service_type.symptom.services'])
            //     ->whereHas('service_type', function ($q) {
            //         $q->whereNull('deleted_at');
            //     })
            //     ->whereHas('service_type.symptom', function ($q) {
            //         $q->whereNull('deleted_at');
            //     })
            //     ->whereHas('service_type.symptom.services', function ($q) {
            //         $q->whereNull('deleted_at');
            //     })
            //     ->where('technicians_id', $teknisi->id);

            $query = MsPriceService::with(['product_group', 'service_type.symptom.services'])
                ->where('technicians_id', $teknisi->id);
        }

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * price service list
     */
    public function priceServiceDataList()
    {
        $user    = Auth::user();
        $teknisi = Technician::where('user_id', $user->id)->firstOrFail();
        $price_services = MsPriceService::with(['product_group', 'service_type.symptom.services'])
            ->where('technicians_id', $teknisi->id)
            ->paginate(10);

        return $price_services;
    }

    /**
     * price service insert data
     */
    public function priceServiceStore($data)
    {
        $setting = GeneralSetting::where('name', 'commission_type_rate')->firstOrFail();

        $user    = Auth::user();
        $teknisi = Technician::where('user_id', $user->id)->first();

        if ($teknisi == null) {
            throw new SendErrorMsgException('Please complete technician job first');
        }

        if (empty($teknisi->rekening_bank_name) || empty($teknisi->rekening_number) || empty($teknisi->rekening_name)) {
            throw new SendErrorMsgException('Please complete technician bank account first');
        }

        $priceService = MsPriceService::where('ms_services_types_id', $data['ms_services_types_id'])
            ->where('technicians_id', $teknisi->id)
            ->where('product_group_id', $data['product_group_id'])
            ->first();

        if ($priceService != null) {
            $priceService->update([
                'value'            => $data['value'],
                'commission'       => $setting->value,
                'commission_value' => $setting->type == 2 ? ($data['value'] * $setting->value / 100) : $setting->value,
                'ms_services_types_id' => $data['ms_services_types_id'],
                'product_group_id' => $data['product_group_id'],
            ]);

            return $priceService;
        }
        return MsPriceService::create([
            'product_group_id' => $data['product_group_id'],
            'ms_services_types_id' => $data['ms_services_types_id'],
            'value'                => $data['value'],
            'commission'       => $setting->value,
            'commission_value' => $setting->type == 2 ? ($data['value'] * $setting->value / 100) : $setting->value,
            'technicians_id'       => $teknisi->id,
        ]);
    }

    /**
     * price service update data
     */
    public function priceServiceUpdate($id, $data)
    {
        $setting = GeneralSetting::where('name', 'commission_type_rate')->firstOrFail();

        $user    = Auth::user();
        $teknisi = Technician::where('user_id', $user->id)->first();

        if ($teknisi == null) {
            throw new SendErrorMsgException('Please complete technician job first');
        }

        $cekDobel = MsPriceService::where('ms_services_types_id', $data['ms_services_types_id'])
            ->where('technicians_id', $teknisi->id)
            ->where('product_group_id', $data['product_group_id'])
            ->where('id', '!=', $id)
            ->first();

        if ($cekDobel != null) {
            throw new SendErrorMsgException('Price Service Already Exsits');
        }

        $priceService = MsPriceService::where('id', $id)
            ->where('technicians_id', $teknisi->id)
            ->first();

        if ($priceService != null) {
            $priceService->update([
                'value'                => $data['value'],
                'commission'       => $setting->value,
                'commission_value' => $setting->type == 2 ? ($data['value'] * $setting->value / 100) : $setting->value,
                'ms_services_types_id' => $data['ms_services_types_id'],
                'product_group_id' => $data['product_group_id'],
            ]);

            return $priceService;
        }

        return false;
    }

    /**
     * price service delte data
     */
    public function priceServiceDestroy($id)
    {
        $user    = Auth::user();
        $teknisi = Technician::where('user_id', $user->id)->first();
        return MsPriceService::where('id', $id)->where('technicians_id', $teknisi->id)->delete();
    }

    /**
     * job experience insert data
     */
    public function profileJobExperience(array $data)
    {
        $user = Auth::user();

        DB::beginTransaction();
        try {
            $JobTitleCategory = $this->createOrGetJobTitleCatgery($data['job_title_category_name']);

            // data job title
            $jobTitle = JobTitle::create([
                'user_id'               => $user->id,
                'description'           => $data['job_title_description_ex'],
                'job_title_category_id' => $JobTitleCategory->id,
            ]);

            // data job experience
            $JobExperience = JobExperience::create([
                'position'     => $data['position'],
                'company_name' => $data['company_name'],
                'period_start' => $data['period_start'],
                'period_end'   => $data['period_end'],
                'type'         => $data['type'],
                'user_id'      => $user->id,
                'job_title_id' => $jobTitle->id,
            ]);

            // commit db
            DB::commit();
        } catch (\Exception $e) {
            // rollback db
            DB::rollback();

            throw new SendErrorMsgException($e->getMessage());
        }

        return $JobExperience;
    }

    /**
     * job experience list data
     */
    public function jobExperienceList()
    {
        $user = Auth::user();

        $query = (new JobExperienceService())->list()->where('user_id', $user->id);

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * job experience delete data
     */
    public function jobExperienceDestroy($id)
    {
        $user = Auth::user();

        return JobExperience::where('id', $id)->where('user_id', $user->id)->delete();
    }

    /**
     * job profile create / update data
     */
    public function profileJobInfo(array $data)
    {
        DB::beginTransaction();
        try {
            // insert data
            $this->updateOrCreateJobInfo($data);
            // commit db
            DB::commit();
        } catch (\Exception $e) {
            // rollback db
            DB::rollback();

            throw new SendErrorMsgException($e->getMessage());
        }

        return $this->getTechnician();
    }

    /**
     * rek info create / update data
     */
    public function rekInfo(array $data)
    {
        $user = Auth::user();
        $teknisi = $user->technician;

        DB::beginTransaction();
        try {
            $teknisi->update([
                'rekening_bank_name' => $data['rekening_bank_name'],
                'rekening_number' => $data['rekening_number'],
                'rekening_name' => $data['rekening_name'],
            ]);
            // commit db
            DB::commit();
        } catch (\Exception $e) {
            // rollback db
            DB::rollback();

            throw new SendErrorMsgException($e->getMessage());
        }

        return $this->getTechnician();
    }

    /**
     * job profile create / update data
     */
    public function updateOrCreateJobInfo(array $data)
    {
        $user             = Auth::user();
        $JobTitleCategory = $this->createOrGetJobTitleCatgery($data['job_title_category_name']);

        if ($user->technician == null) {
            // data job title
            $jobTitle = JobTitle::create([
                'user_id'               => $user->id,
                'description'           => $data['job_title_description'],
                'job_title_category_id' => $JobTitleCategory->id,
            ]);

            // data teknisi
            $teknisi = Technician::create([
                'skill'        => $data['skil'],
                'user_id'      => $user->id,
                'job_title_id' => $jobTitle->id,
            ]);
        } else {
            if ($user->technician->job_title == null) {
                // data job title
                $jobTitle = JobTitle::create([
                    'user_id'               => $user->id,
                    'description'           => $data['job_title_description'],
                    'job_title_category_id' => $JobTitleCategory->id,
                ]);
                $job_title_id = $jobTitle->id;
            } else {
                $job_title_id = $user->technician->job_title_id;
                // data job title
                $jobTitle = JobTitle::where('id', $user->technician->job_title_id)->update([
                    'description'           => $data['job_title_description'],
                    'job_title_category_id' => $JobTitleCategory->id,
                ]);
            }

            $teknisi = $user->technician;
            $teknisi->update([
                'job_title_id' => $job_title_id,
                'skill' => $data['skil'],
            ]);
        }

        $this->setTechnician($teknisi);
        return $this;
    }

    /**
     * job title categosy create / get data
     */
    public function createOrGetJobTitleCatgery(string $name)
    {
        $JobTitleCategory = JobTitleCategory::where('name', $name)->first();
        if ($JobTitleCategory == null) {
            $JobTitleCategory = JobTitleCategory::create([
                'name' => $name,
            ]);
        }

        return $JobTitleCategory;
    }

    public function setTechnician(Technician $technician)
    {
        $this->technician = $technician;
        return $this;
    }

    public function getTechnician()
    {
        return $this->technician;
    }
}
