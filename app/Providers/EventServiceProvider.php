<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        'App\Events\UserRegister' => [
            'App\Listeners\User\SendEmailVerification',
        ],

        'App\Events\TeknisiApprovalEvent' => [
            'App\Listeners\SendSuccessApproval',
         ],

         'App\Events\TeknisiRejectedEvent' => [
            'App\Listeners\SendFailedApproval',
         ],

         'App\Events\ResetPasswordEvent' => [
            'App\Listeners\SendSuccessResetPass',
         ],

         'App\Events\CustomerRequestPenawaranEvent' => [
            'App\Listeners\TeknicialRequestListener',
         ],

         'App\Events\TechnicianAcceptRequestEvent' => [
            'App\Listeners\TechnicianAcceptRequestListener',
         ],

         'App\Events\OrderEvent' => [
            'App\Listeners\OrderListener',
         ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
