<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Storage;

class ImageUpload
{
    public $image;
    public $filename;

    public function upload($image, $path = 'public/files')
    {
        // set image
        $this->setImage($image);

        // generate name
        $filename = $this->generateName();

        // set filenma
        $this->setFilename($filename);

        // store image
        $image->storeAs($path, $filename);

        return $this;
    }

    public function deleteDuplicate($path, $name)
    {
        if (substr($path, -1) !== '/') {
            $path = $path . '/';
        }
        
        if (Storage::exists($path . $name)) {
            Storage::delete($path . $name);
        }
    }

    public function generateName()
    {
        $filename = uniqid(). '.' . $this->getImage()->getClientOriginalExtension();

        return $filename;
    }

    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setFilename($filename)
    {
        $this->filename = $filename;
        return $this;
    }

    public function getFilename()
    {
        return $this->filename;
    }
}
