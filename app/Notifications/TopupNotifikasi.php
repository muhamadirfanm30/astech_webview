<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use App\Mail\CustomerRequestJobEmail;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class TopupNotifikasi extends Notification
{
    use Queueable;
    public $opsi;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($opsi = [])
    {
        $this->opsi = $opsi;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */

    public function toMail($notifiable)
    {
        return true;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $opsi = $this->opsi;
        $data = [
            'name' => $opsi['name'],
            'description' => $opsi['description'],
            'icon' => 'fa fa-exchange',
            'color' => 'border-warning bg-warning',
            'url' => $opsi['url'],
            'type' => 'topup',
            'ewallet_history_id' => $opsi['ewallet_history_id'],
        ];
        return $data;
    }
}
