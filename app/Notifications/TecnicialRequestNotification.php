<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\Mail\TechnicialRequestEmail;
use App\Model\Master\ServiceDetail;

class TecnicialRequestNotification extends Notification
{
    use Queueable;

    public $serviceDetail;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(ServiceDetail $serviceDetail)
    {
        $this->serviceDetail = $serviceDetail;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new TechnicialRequestEmail($this->serviceDetail))->to($notifiable);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
