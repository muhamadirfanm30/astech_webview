<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\Mail\SendPasswordReset;
use App\User;
use Illuminate\Http\Request;

class PasswordResetRequestNotification extends Notification // implements ShouldQueue
{
    use Queueable;

    public $user;
    public $request;
    public $resetPass;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user, Request $request, $resetPass)
    {
        $this->user = $user;
        $this->request = $request;
        $this->resetPass = $resetPass;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $token = md5($this->user->id);
        $url = url('/create-password-form?token=' . $this->resetPass->token );

        return (new SendPasswordReset($this->user, $this->request, $url))
            // ->line('You are receiving this email because we received a password reset request for your account.')
            // ->action('Reset Password', url($url))
            // ->line('If you did not request a password reset, no further action is required.')
            ->to($notifiable);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
