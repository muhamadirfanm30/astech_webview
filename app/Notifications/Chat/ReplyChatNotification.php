<?php

namespace App\Notifications\Chat;

use App\Model\Master\ChatMessage;
use App\User;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\Model\Notification as Noty;

class ReplyChatNotification extends Notification
{
    use Queueable;

    public $user;
    public $chatMessage;
    public $content;
    public $list_chat;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user, ChatMessage $chatMessage, $content, $list_chat = '')
    {
        $this->user = $user;
        $this->chatMessage = $chatMessage;
        $this->content = $content;
        $this->list_chat = $list_chat;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line('The introduction to the notification.')
            ->action('Notification Action', url('/'))
            ->line('Thank you for using our application!');
    }

    public function toArray($notifiable)
    {
        $detail = $this->chatMessage;
        return [
            'name' => 'Reply From ' . $detail->user->name,
            'description' => $detail->message,
            'icon' => 'fa fa-envelope',
            'color' => 'border-danger bg-danger',
            'url' => route('customer.chat'),
            'content' => [
                'reply' => $this->content,
                'list' => $this->list_chat
            ],
        ];
    }

    public function toDatabase($notifiable)
    {
        $detail = $this->chatMessage;
        return [
            'name' => 'Reply From ' . $detail->user->name,
            'description' => $detail->message,
            'icon' => 'fa fa-envelope',
            'color' => 'border-danger bg-danger',
            'url' => route('customer.chat'),
            'data' => $detail,
            'content' => [
                'reply' => $this->content,
                'list' => $this->list_chat
            ],
        ];
    }

    public function broadcastOn()
    {
        return new PrivateChannel('chats.reply.' . $this->user->id);
    }

    public function broadcastAs()
    {
        return 'chats.reply';
    }

    public function toBroadcast($notifiable)
    {
        $detail = $this->chatMessage;

        return (new BroadcastMessage([
            'name' => 'Reply From ' . $detail->user->name,
            'description' => $detail->message,
            'icon' => 'fa fa-envelope',
            'color' => 'border-danger bg-danger',
            'url' => route('customer.chat'),
            'content' => [
                'count_unread' => Noty::unreadCount($this->user->id),
                'reply' => $this->content,
                'id' => $detail->chat_id,
                'list' => $this->list_chat
            ],
        ]))->onConnection('sync');
    }
}
