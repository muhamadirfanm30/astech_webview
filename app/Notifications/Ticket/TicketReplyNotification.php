<?php

namespace App\Notifications\Ticket;

use App\Model\Master\MsTicketDetail;
use App\User;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class TicketReplyNotification extends Notification
{
    use Queueable;

    public $ticketDetail;
    public $user;
    public $content;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(MsTicketDetail $msTicketDetail, User $user, $content = '')
    {
        $this->ticketDetail = $msTicketDetail;
        $this->user = $user;
        $this->content = $content;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line('The introduction to the notification.')
            ->action('Notification Action', url('/'))
            ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $detail = $this->ticketDetail;
        return [
            'name' => '(TICKET) Reply Ticket ' . $detail->ticket->ticket_no,
            'description' => $detail->message,
            'icon' => 'fa fa-envelope',
            'color' => 'border-danger bg-danger',
            'url' => route('tickets.detail', ['id' => $detail->ticket->id]),
            'data' => $detail,
            'content' => $this->content,
        ];
    }

    public function toDatabase($notifiable)
    {
        $detail = $this->ticketDetail;
        return [
            'name' => '(TICKET) Reply Ticket ' . $detail->ticket->ticket_no,
            'description' => $detail->message,
            'icon' => 'fa fa-envelope',
            'color' => 'border-danger bg-danger',
            'url' => route('tickets.detail', ['id' => $detail->ticket->id]),
            'data' => $detail,
            'content' => $this->content,
        ];
    }

    public function broadcastOn()
    {
        return new PrivateChannel('tickets.reply.' . $this->user->id);
    }

    public function broadcastAs()
    {
        return 'tickets.reply';
    }

    public function toBroadcast($notifiable)
    {
        $detail = $this->ticketDetail;

        return (new BroadcastMessage([
            'name' => 'Reply Ticket ' . $detail->ticket->ticket_no,
            'description' => $detail->message,
            'icon' => 'fa fa-envelope',
            'color' => 'border-danger bg-danger',
            'url' => route('tickets.detail', ['id' => $detail->ticket->id]),
            'data' => $detail,
            'content' => $this->content,
        ]))->onConnection('sync');
    }
}
