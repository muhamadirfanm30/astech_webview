<?php

namespace App\Http\Controllers;

use DB;
use PDF;
use App\User;
use DateTime;
use Carbon\Carbon;
use App\Services\OrderService;
use App\Models\Master\Order;
use Illuminate\Http\Request;
use App\Models\GeneralSetting;
use App\Models\Master\Ewallet;
use App\Models\Master\Services;
use App\Models\Master\Inventory;
use App\Models\Master\MsProduct;
use App\Models\Master\ItemDetail;
use App\Models\Master\MsTechTeam;
use App\Models\Master\TeamInvite;
use Illuminate\Support\Collection;
use App\Models\Master\ServiceDetail;
use App\Models\Master\TmpItemDetail;
use App\Models\Master\EwalletHistory;
use App\Models\Master\EwalletPayment;
use App\Models\Master\MsPriceService;
use App\Http\Requests\JobDoneRequest;
use Illuminate\Support\Facades\Validator;
use App\Models\Master\TmpSparepartDetail;
use App\Models\Master\TeamTechnician;
use App\Models\Technician\Technician;
use Illuminate\Support\Facades\Auth;
use App\Models\Master\SparepartDetail;
use App\Models\Master\TechnicianSaldo;
use App\Models\Master\MsOrderComplaint;
use App\Http\Controllers\ApiController;
use App\Models\Master\OrderMediaProblem;
use Illuminate\Database\QueryException;
use App\Models\Master\TechnicianSchedule;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Controllers\AdminController;
use App\Models\Master\TechnicianSparepart;
use App\Http\Requests\JobsDoneRequest;
use App\Models\Master\MsAdditionalInfo;
use App\Models\Master\TeknisiInfo;
use App\Models\Master\MsAddress;

class TeknisiController extends AdminController
{

    public function dashboard(Request $request)
    {
        $additionalInfo = MsAdditionalInfo::where('user_id', Auth::user()->id)->with(['religion', 'marital'])->first();
        $teknisiInfo = TeknisiInfo::where('user_id', Auth::user()->id)->first();
        $address = MsAddress::where('user_id', Auth::user()->id)->with(['city', 'types'])->first();
        $userLogin   = Auth::user()->id;
        $getIdTechnician = User::where('id', $userLogin)->with('technician')->first();
        $updateUsers = User::with(['info', 'technician'])->where('id', $userLogin)->first();
        if (!empty($getIdTechnician->technician)) {
            // return 'tifak kosong';
            $getTotaltechnicianOrder = ServiceDetail::with('order.sparepart_detail')
                ->where('technicians_id', $getIdTechnician->technician->id)
                ->whereMonth('created_at', '=', date('m'))
                ->get();
            // return $getTotaltechnicianOrder;
            $getTotalService = 0;
            foreach ($getTotaltechnicianOrder as $serviceOnly) {
                $getTotalService += $serviceOnly->price;
            }

            $getTotalPart = SparepartDetail::where('technicians_id',  $getIdTechnician->technician->id)
                ->whereMonth('created_at', '=', date('m'))
                ->get();
            // return $getTotalPart;
            $totalPart = 0;
            foreach ($getTotalPart as $part) {
                $totalPart += $part->price;
            }

            if ($updateUsers->technician != null) {
                $balance    = TechnicianSaldo::where('technician_id', $updateUsers->technician->id)
                    ->where('transfer_status', 0)
                    ->get();
            }

            $totalOrderCompleted = ServiceDetail::with('order.sparepart_detail')
                ->whereHas('order', function ($query) use ($request) {
                    $query->where('orders_statuses_id', 10);
                })
                ->where('technicians_id', $getIdTechnician->technician->id)
                ->whereMonth('created_at', '=', date("m"))
                ->get();
            // return $totalOrderCompleted;
            $completeOrder = 0;
            foreach ($totalOrderCompleted as $part) {
                $completeOrder += $part->price;
            }
        } else {
            // return 'kosong';
            $balance = 0;
            $totalPart = 0;
            $getTotalService = 0;
            $completeOrder = 0;
        }

        // return ['updateUsers'=> $updateUsers->technician];
        if ($updateUsers->info == null) {
            // return 'teknisi info';
            return $this->viewAdmin('users.teknisiInfo');
        } else {
            if ($updateUsers->technician != null) {
                // return 'teknisi tidak kosong';
                return $this->viewAdmin('admin.teknisi.users.dashboard', [
                    'title' => 'Dashboard',
                    'balance' => $balance,
                    'getTotalService' => $getTotalService,
                    'totalPart' => $totalPart,
                    'completeOrder' => $completeOrder,
                    'additionalInfo' => $additionalInfo,
                    'teknisiInfo' => $teknisiInfo,
                    'address' => $address,
                ]);
            } else {
                return $this->viewAdmin('admin.teknisi.users.dashboard', [
                    'title' => 'Dashboard',
                    'balance' => $balance,
                    'getTotalService' => $getTotalService,
                    'totalPart' => $totalPart,
                    'completeOrder' => $completeOrder,
                    'additionalInfo' => $additionalInfo,
                    'teknisiInfo' => $teknisiInfo,
                    'address' => $address,
                ]);
            }
        }
    }

    public function service_price()
    {
        $settings = GeneralSetting::all();
        // return $settings;
        return $this->viewAdmin('admin.teknisi.users.service_price', [
            'settings' => $settings
        ]);
    }

    /**
     * list job order teknisi
     */
    public function listRequestJob()
    {
        return $this
            ->viewAdmin('admin.teknisi.job_request.index', [
                'title' => 'LIST JOB REQUEST',
            ]);
    }

    public function garansiIndex()
    {
        return $this
            ->viewAdmin('admin.teknisi.garansi.index', [
                'title' => 'List Claim Garansi',
            ]);
    }

    public function garansiIndexDatatables(OrderService $orderService, Request $request)
    {
        $query = Order::with([
            'orderpayment',
            'user.ewallet',
            'service',
            'ticket',
            'symptom',
            'sparepart_detail',
            'item_detail',
            'service_detail.symptom',
            'service_detail.services_type.product_group',
            'service_detail.technician.user',
            'service_detail.price_service.service_type.symptom.services',
            'history_order.order_status',
            'order_status',
            'user',
            'garansi',
            'payment_method'
        ])
            ->whereHas('garansi', function ($query) {
                $query->where('status', '!=', 'done')->where('customer_can_reply', 1);
            })
            ->whereHas('service_detail.technician', function ($query) use ($request) {
                $query->where('user_id', Auth::user()->id);
            });

        if (!empty($request->order_status_id)) {
            $query->whereIn('orders_statuses_id', explode(',', $request->order_status_id));
        }

        if (!empty($request->payment_type)) {
            $query->where('payment_type', $request->payment_type);
        }

        if (!empty($request->order_date_range)) {
            $range = explode(' - ', $request->order_date_range);
            $date_from = date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $range[0])));
            $date_to = date("Y-m-d H:i:s", strtotime(str_replace('/', '-', $range[1])));
            $query->where('schedule', '>=', $date_from)
                ->where('schedule', '<=', $date_to);
        }

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function listGaransiJob(OrderService $orderService, Request $request)
    {
        $query = Order::with([
            'orderpayment',
            'user.ewallet',
            'service',
            'ticket',
            'symptom',
            'sparepart_detail',
            'item_detail',
            'service_detail.symptom',
            'service_detail.services_type.product_group',
            'service_detail.technician.user',
            'service_detail.price_service.service_type.symptom.services',
            'history_order.order_status',
            'order_status',
            'user',
            'garansi',
            'payment_method'
        ])
            ->whereHas('garansi', function ($query) {
                $query->where('status', '!=', 'done')->where('customer_can_reply', 1);
            })
            ->whereHas('service_detail.technician', function ($query) use ($request) {
                $query->where('user_id', Auth::user()->id);
            });

        if (!empty($request->order_status_id)) {
            $query->whereIn('orders_statuses_id', explode(',', $request->order_status_id));
        }

        if (!empty($request->payment_type)) {
            $query->where('payment_type', $request->payment_type);
        }

        if (!empty($request->order_date_range)) {
            $range = explode(' - ', $request->order_date_range);
            $date_from = date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $range[0])));
            $date_to = date("Y-m-d H:i:s", strtotime(str_replace('/', '-', $range[1])));
            $query->where('schedule', '>=', $date_from)
                ->where('schedule', '<=', $date_to);
        }

        return $this->successResponse($query->paginate(10), 'ok');
    }

    public function listRequestJobAwaiting()
    {
        return $this
            ->viewAdmin('admin.teknisi.job_request.jobRequestStatus.awaitingJobs');
    }

    public function listRequestJobOnProgress()
    {
        return $this
            ->viewAdmin('admin.teknisi.job_request.jobRequestStatus.onProgress');
    }

    public function listRequestJobDone()
    {
        return $this
            ->viewAdmin('admin.teknisi.job_request.jobRequestStatus.jobsDone');
    }

    public function listRequestCancel()
    {
        return $this
            ->viewAdmin('admin.teknisi.job_request.jobRequestStatus.cancel');
    }

    public function balanceDetail()
    {
        return $this
            ->viewAdmin('admin.teknisi.users.historyTransaction');
    }

    public function allOrders(Request $request)
    {
        // return $this
        //     ->viewAdmin('admin.order.allOrder');
        return $this->viewAdmin('admin.master.email.create', [
            'title' => 'asd'
        ]);
    }

    /**
     * list job teknisi datatables api
     */
    public function datatables(OrderService $orderService, Request $request)
    {
        return $orderService->datatablesTeknisi($request);
    }

    public function complaintListDatatables($query = null, Request $request)
    {
        $query   = Order::with([
            'user',
            'unread_complaint',
            'user.address',
            'service',
            'symptom',
            'product_group',
            'order_status',
            'service_detail.symptom',
            'service_detail.services_type',
            'service_detail.price_service',
            'service_detail.service_status',
            'service_detail.technician',

        ])->whereHas('service_detail.technician', function ($query) use ($request) {
            $query->where('user_id', Auth::user()->id);
        })->whereIn('orders_statuses_id', [11]);

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function complaintListIndex()
    {
        $query   = Order::with([
            'user',
            'unread_complaint',
            'user.address',
            'service',
            'symptom',
            'product_group',
            'order_status',
            'service_detail.symptom',
            'service_detail.services_type',
            'service_detail.price_service',
            'service_detail.service_status',
            'service_detail.technician',

        ])->whereHas('service_detail.technician', function ($query) {
            $query->where('user_id', Auth::user()->id);
        })->whereIn('orders_statuses_id', [11]);

        return $this->successResponse($query->paginate(10), 'ok');
    }

    public function detailComplaintJson($order_id, Request $request)
    {
        $order_by       = empty($request->order_by) ? 'id' : $request->order_by;
        $sort_by        = in_array($request->sort_by, ['asc', 'desc']) ? $request->sort_by : 'desc';

        $order = Order::where('id', $order_id)
            ->whereHas('service_detail.technician', function ($query) {
                $query->where('user_id', Auth::user()->id);
            })
            ->firstOrFail();

        MsOrderComplaint::where('ms_orders_id', $order_id)->where('user_id', '!=', Auth::id())->update([
            'read_at' => now()
        ]);

        $complaints = MsOrderComplaint::with(['user'])
            ->where('ms_orders_id', $order_id)
            ->orderBy($order_by, $sort_by)
            ->get();

        $resp = [
            'order' => $order,
            'complaints' => $complaints
        ];

        return $this->successResponse($resp, 'ok', 200);
    }

    public function service_detail($order_id)
    {
        $order = Order::where('id', $order_id)
            ->with([
                'rating_and_review.rating',
                'rating_and_review.review',
                'product_group',
                'user.ewallet',
                'user.address',
                'service',
                'symptom',
                'order_status',
                'sparepart_detail',
                'service_detail.symptom',
                'service_detail.services_type',
                'service_detail.price_service',
                'user.ewallet'
            ])
            ->whereHas('service_detail.technician', function ($query) {
                $query->where('user_id', Auth::user()->id);
            })
            ->firstOrFail();

        $order->markAsRead();

        $walletBalanceCustomer = EwalletHistory::select(array(DB::raw('SUM(nominal) as Total')))
            ->where('user_id', $order->user->id)
            ->where('transfer_status_id', 1)
            ->first();

        // return $order;

        $sparepart_details = SparepartDetail::where('orders_id', $order_id)
            ->with([
                'order',
                'technician_sparepart'
            ])->get();

        $item_details = ItemDetail::where('orders_id', $order_id)
            ->with(['order'])->get();

        $technicians_id    = Technician::where('user_id', Auth::user()->id)->value('id');
        $astech_spareparts = MsProduct::where('ms_services_id', $order->service->id)->get();
        $tech_spareparts   = TechnicianSparepart::where('technicians_id', $technicians_id)->get();
        $sparepart_details = SparepartDetail::where('orders_id', $order_id)
            ->with([
                'order',
                'technician_sparepart'
            ])->get();

        $tmp_spareparts = TmpSparepartDetail::where('orders_id', $order_id)->get();
        $tmp_item_detail = TmpItemDetail::where('orders_id', $order_id)->with(['order'])->get();
        // return $tmp_item_detail;
        $item_details = ItemDetail::where('orders_id', $order_id)->with(['order'])->get();
        // return $tmp_item_detail;
        $order_media_problem = OrderMediaProblem::where('orders_id', $order_id)->get();
        $schedules = TechnicianSchedule::where(['technician_id' => $technicians_id, 'schedule_date' =>  Carbon::parse($order->schedule)->format('Y-m-d')])->get(['start_work', 'end_work']);

        $jam_kerja_teknisi = [];
        foreach ($schedules as $key => $schedule) {
            for ($i = (int) Carbon::parse($schedule->start_work)->format('H'); $i <= (int) Carbon::parse($schedule->end_work)->format('H'); $i++) {
                $jam_kerja_teknisi[] = $i;
            }
        }
        return $this
            ->viewAdmin('admin.teknisi.job_request.service_detail', [
                'title'                 => 'ACCEPTED JOB TEKNISI',
                'order'                 => $order,
                'order_id'              => $order_id,
                'astech_spareparts'     => $astech_spareparts,
                'tech_spareparts'       => $tech_spareparts,
                'sparepart_details'     => $sparepart_details,
                'item_details'          => $item_details,
                'technicians_id'        => $technicians_id,
                'sparepart_details'     => $sparepart_details,
                'item_details'          => $item_details,
                'order_media_problem'   => $order_media_problem,
                'walletBalanceCustomer' => $walletBalanceCustomer,
                'jam_teknisi'           => json_encode($jam_kerja_teknisi),
                'tmp_spareparts'        => $tmp_spareparts,
                'tmp_item_detail'        => $tmp_item_detail,
            ]);
    }

    public function datatablesListJob($query = null, Request $request)
    {
        $query   = Order::with([
            'user',
            'symptom',
            'service',
            'garansi',
            'product_group',
            'order_status',
            'service_detail.symptom',
            'service_detail.services_type',
            'service_detail.technician',
        ])->whereHas('service_detail.technician', function ($query) use ($request) {
            $query->where('user_id', Auth::user()->id);
        });

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function rejectJob(Request $request, OrderService $orderService, $order_id)
    {
        $resp = $orderService
            ->setOrder($order_id)
            ->onlyJobPendingStatus() // hanya status 2
            ->checkTeknisiHasOrder() // login user harus sama dengan order teknisi nya
            ->changeToCancel($request);

        return $this->successResponse($resp, 'This Order Has Been Rejected');
    }

    public function acceptedJobSave($order_id, Request $request, OrderService $orderService)
    {
        $resp = $orderService
            ->setOrder($order_id)
            ->onlyJobPendingStatus() // hanya status 2
            ->checkTeknisiHasOrder() // login user harus sama dengan order teknisi nya
            ->changeToWaitingApproval($request);

        return $this->successResponse($resp, 'order success acepted');
    }

    public function reviewList()
    {
        return $this
            ->viewAdmin('admin.teknisi.review.index', [
                'title'     => 'LIST REVIEW',
            ]);
    }

    public function reviewListDatatables(OrderService $orderService)
    {
        $query = $orderService->hasReview()->whereHas('service_detail.technician', function ($query) {
            $query->where('user_id', Auth::id());
        });;

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function reviewListIndex(OrderService $orderService)
    {
        $query = $orderService->hasReview()->whereHas('service_detail.technician', function ($query) {
            $query->where('user_id', Auth::id());
        });;

        return $query->paginate(10);
    }

    public function profile(Request $request)
    {
        // dd('sad');
        $user = User::with(['technician.job_title.job_title_category', 'info.religion', 'info.marital'])->where('id', Auth::id())
            ->firstOrFail();
        // return $user;
        $prefixUrl = $request->route()->getPrefix();

        return $this
            ->viewAdmin('admin.teknisi.technician.profile', [
                'title'     => 'TECHNICIAN',
                'user'      => $user,
                'prefixUrl' => $prefixUrl,
            ]);
    }

    public function inventoryCompanySelect2(Request $request)
    {
        if ($request->inventory_id != null) {
            $inventory_ids = $request->inventory_id;
            if (!is_array($inventory_ids)) {
                $inventory_ids = [$request->inventory_id];
            }
            if (count($inventory_ids) > 0) {
                $inventory = Inventory::with([
                    'unit_type',
                    'warehouse',
                    'product'
                ])
                    ->whereHas('batch_item.product', function ($query) use ($request) {
                        $query->where('name', 'like', '%' . $request->q . '%')->where('ms_warehouse_id', $request->warehouse_id);
                    })
                    // ->where(DB::raw('(stock - stock_out - stock_ret)'), '>', 0)
                    ->where('stock_now', '>', 0)
                    ->whereNotIn('id', $inventory_ids)
                    ->limit(20)
                    ->get();

                return (new ApiController())->successResponse($inventory, 'success', 201);
            }
        }
        $inventory = Inventory::with([
            'unit_type',
            'warehouse',
            'product'
        ])->whereHas('batch_item.product', function ($query) use ($request) {
            $query->where('name', 'like', '%' . $request->q . '%')->where('ms_warehouse_id', $request->warehouse_id);
        })
            // ->where(DB::raw('(stock - stock_out - stock_ret)'), '>', 0)
            ->where('stock_now', '>', 0)
            ->limit(20)
            ->get();

        return (new ApiController())->successResponse($inventory, 'success', 201);
    }

    public function inventoryCompanySelect2Mobile(Request $request)
    {
        $q = $request->q;
        $warehouse_id = $request->warehouse_id;
        $paginate = $request->paginate;

        $inventory = Inventory::with(['unit_type', 'warehouse', 'product'])
            ->where('stock', '>', 0);

        if (!empty($warehouse_id)) {
            $inventory->whereHas('warehouse', function ($query) use ($request) {
                $query->where('ms_warehouse_id', $request->warehouse_id);
            });
        }

        if (!empty($q)) {
            $inventory->where('item_name', $request->q);
        }

        if ($paginate == 'no_paginate') {
            $response = $inventory->limit(20)->get();
        } else {
            $response = $inventory->paginate(10);
        }

        return (new ApiController())->successResponse($response, 'success', 200);
    }

    public function jobsDone(JobsDoneRequest $request, $order_id, OrderService $orderService)
    {
        $ids = TmpSparepartDetail::where('orders_id', $order_id)->pluck('id');
        $idItems = TmpItemDetail::where('orders_id', $order_id)->pluck('id');
        if (!empty($ids)) {
            TmpSparepartDetail::whereIn('id', $ids)->delete();
        }

        if (!empty($idItems)) {
            TmpItemDetail::whereIn('id', $idItems)->delete();
        }

        $resp = $orderService->setOrder($order_id)
            ->checkTeknisiHasOrder()
            ->onlyJobWorkingStatus()
            ->changeToDone($request);

        // get data order stelah diupdate
        $order = $orderService->getNewOrder();

        //email status order
        $this->sendEmailOrder($order, 'review', 1);

        //Order Logs
        $orderService->orderLog(null, null, $order->id, $order->order_status->name);

        // send response
        return $this->successResponse($resp, 'ok', 200);
    }

    public function onWorking($order_id, OrderService $orderService, Request $request) //teknisi
    {
        $resp = $orderService->setOrder($order_id)
            ->checkTeknisiHasOrder()
            ->onlyJobProsesStatus()
            ->changeToOnWorking($request);

        // get data order stelah diupdate
        $order = $orderService->getOrder();

        //Order Logs
        $orderService->orderLog(null, null, $order->id, $order->order_status->name);

        return $this->successResponse($resp, $this->successStoreMsg(), 200);
    }

    public function complaintList()
    {
        return $this->viewAdmin('admin.teknisi.complaintList.index');
    }

    public function detailComplaint($order_id)
    {
        $query   = Order::with([
            'user.technician',
            'symptom',
            'service',
            'order_status',
            'service_detail.symptom',
            'service_detail.services_type',
            'service_detail.technician',
            'service_detail.technician.user'
        ])->where('id', $order_id)->first();

        MsOrderComplaint::where('ms_orders_id', $order_id)->where('user_id', '!=', Auth::id())->update([
            'read_at' => now()
        ]);

        $user = Auth::user()->id;
        $showComplaint = MsOrderComplaint::with(['user', 'order'])->where('ms_orders_id', $order_id)->get();

        return $this->viewAdmin('admin.customer.complaint.detailComplain', compact('query', 'showComplaint', 'user'));
    }

    public function techniciansTeamList()
    {
        $count_invitation = $this->countInvitation();
        return $this->viewAdmin('admin.teknisi.teams.list', [
            'title' => 'Technician Teams',
            'count_invitation' => $count_invitation
        ]);
    }

    public function mySchedules()
    {

        $technician_id = $this->getTechnicianId();
        $my_schedule = TechnicianSchedule::with('order')->where('technician_id', $technician_id)->get();
        $event = new Collection;

        foreach ($my_schedule as $key => $val) {
            $color = '#3788D8';
            $desc = '';
            if($val->order != null){
                if ($val->order->orders_statuses_id == 10) {
                    $color = '#00d890';
                } else if ($val->order->orders_statuses_id == 5) {
                    $color = '#d82400';
                }
                $desc = '#' . $val->order->code . '<br/> asdasd adasdas ';
            }
            $event->push([
                'type'     => 'shift',
                'color'    => $color,
                'title'    => '(' . Carbon::parse($val->start_work)->format('H:i') . ' - ' . Carbon::parse($val->end_work)->format('H:i') . ') ',
                'description' => $desc,
                'start'    => $val->schedule_date,
                'allDay'   => true,
            ]);
        }

        return $this->viewAdmin('admin.teknisi.my-schedules.index', [
            'title' => 'Technician Schedules',
            'event' => $event
        ]);
    }

    public function mySchedulesApi(Request $request)
    {
        $technician_id = $this->getTechnicianId();
        $date_from = empty($request->date_from) ? date('Y-m-d') : $request->date_from;
        $date_to = empty($request->date_to) ? date('Y-m-d') : $request->date_to;

        $my_schedule = TechnicianSchedule::with([
            'order' => function ($query) {
                $query->select('id', 'code', 'orders_statuses_id', 'symptom_name', 'product_group_name');
            }
        ])
            ->where('technician_id', $technician_id)
            ->where('schedule_date', '>=', $date_from)
            ->where('schedule_date', '<=', $date_to)
            ->get();

        return $this->successResponse(
            $my_schedule->map(function ($row) {
                $color = '#3788D8';
                $order = null;
                $calendar = [
                    'color' => $color,
                    'title' => '',
                    'description' => '',
                ];
                if($row->order != null){
                    if ($row->order->orders_statuses_id == 10) {
                        $color = '#00d890';
                    } else if ($row->order->orders_statuses_id == 5) {
                        $color = '#d82400';
                    }

                    $order = [
                        'id' => $row->order->id,
                        'code' => $row->order->code,
                        'orders_statuses_id' => $row->order->orders_statuses_id,
                    ];

                    $calendar = [
                        'color' => $color,
                        'title' => $row->order->code,
                        'description' => $row->order->symptom_name . ' ' . $row->order->product_group_name,
                    ];
                }
                
                return [
                    'id' => $row->id,
                    'order_id' => $row->order_id,
                    'technician_id' => $row->technician_id,
                    'start_work' => $row->start_work,
                    'end_work' => $row->end_work,
                    'schedule_date' => $row->schedule_date,
                    'order' => $order,
                    'calendar' => $calendar
                ];
            })
        );
    }

    public function teamsDatatables()
    {
        // return $request->all();
        $query = MsTechTeam::with([
            'technician.user',
            'team_technician.technician.user',
            'team_invite.technician.user'
        ])->where('technicians_id', '=', $this->getTechnicianId(Auth::user()->id));

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function memberDatatables()
    {
        // return $request->all();
        $query = MsTechTeam::with([
            'technician.user',
            'team_technician.technician.user'

        ])->whereHas('team_technician', function ($q) {
            $q->where('technicians_id', '=', $this->getTechnicianId(Auth::user()->id));
        });

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function memberSelect2(Request $request)
    {

        $query = Technician::with('user')
            ->whereHas('user', function ($q) use ($request) {
                $q->where('email', 'like', '%' . $request->q . '%');
            })->where('user_id', '!=', Auth::user()->id)
            ->limit(1)
            ->get()->pluck('user.email');
        return $this->successResponse($query);
    }

    public function teamsCreate(Request $request)
    {
        // return $request->all();
        DB::beginTransaction();
        try {
            $create_team = MsTechTeam::create([
                'name'           => $request->get('name'),
                'description'    => $request->get('description'),
                'technicians_id' => $this->getTechnicianId(Auth::user()->id)
            ]);
            $technician_email = explode(",", $request->get('technicians_email'));
            foreach ($technician_email as $key => $val) {
                $attr = $this->getTechnicianUserbyEmail(str_replace(' ', '', $val));
                if ($attr != 0) {
                    $is_leader = MsTechTeam::where('technicians_id', $attr->id)->count();
                    if ($is_leader > 0) {
                        return $this->errorResponse($is_leader, '' . $val . ', this technicians are already the team leader on other teams!', 400);
                    }
                    $invite_team_technicians = TeamInvite::create([
                        'team_id'        => $create_team->id,
                        'technicians_id' => $attr->id,
                        'type'           => 'invite'
                    ]);
                } else {
                    return $this->errorResponse($val, '' . $val . ', this email technician is not registered!', 400);
                }
            }
            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }

        return $this->successResponse($create_team, 'success', 201);
    }

    public function teamsUpdate($team_id, Request $request)
    {
        DB::beginTransaction();
        try {
            $user = Auth::user();
            $teknisi = Technician::where('user_id', $user->id)->first();
            if ($teknisi == null) {
                throw new \Exception('Please complete technician job first');
            }

            $team = MsTechTeam::where('id', $team_id)
                ->first();
            if ($team != null) {
                $team->update([
                    'name' => $request->get('name'),
                    'description' => $request->get('description')
                ]);
            }

            $arr_email_id = TeamInvite::with(['technician.user'])
                ->where('team_id', $team_id)->get()->pluck('user.email')
                ->toArray();
            $arr_email_id_text = implode('', $arr_email_id);
            $technicians_email = explode(",", $request->get('technicians_email'));
            $technicians_email_text = implode('', $technicians_email);
            if ($arr_email_id_text != $technicians_email_text) {
                $q = TeamInvite::where('team_id', '=', $team_id);
                if ($q->get()->count() >  0) {
                    $q->delete();
                }
                if (count($technicians_email) > 0) {
                    foreach ($technicians_email as $key => $val) {
                        $attr = $this->getTechnicianUserbyEmail(str_replace(' ', '', $val));
                        if ($attr != 0) {
                            $insert = TeamInvite::create([
                                'team_id' => $team_id,
                                'technicians_id' => $attr->id,
                                'type' => 'invite'
                            ]);
                        } else {
                            return $this->errorResponse($val, '' . $val . ', this email technician is not registered!', 400);
                        }
                    }
                }
            }
            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }

        return $this->successResponse($team, 'success', 200);
    }

    public function teamsDelete($team_id)
    {
        $delete = MsTechTeam::where('id', $team_id)->delete();
        $this->successResponse($delete, 'success', 200);
    }

    public function teamsLeave($team_id)
    {
        $leave = TeamTechnician::where('team_id', $team_id)->where('technicians_id', $this->getTechnicianId(Auth::user()->id))->delete();
        $this->successResponse($leave, 'success', 200);
    }

    public function invitationDatatables()
    {
        $query = TeamInvite::with([
            'tech_team.technician',
            'technician.user',
        ])->where('technicians_id', '=', $this->getTechnicianId(Auth::user()->id));

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function approveInvitation(Request $request)
    {
        // return $request->all();
        DB::beginTransaction();
        try {
            $data = TeamInvite::where('id', $request->id)->first();
            $accept = TeamTechnician::create([
                'team_id' => $data->team_id,
                'technicians_id' => $data->technicians_id
            ]);
            $delete = TeamInvite::where('id', $request->id)->delete();
            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }

        return $this->countInvitation();
    }

    public function countInvitation()
    {
        $count_invitation = TeamInvite::where('technicians_id', $this->getTechnicianId(Auth::user()->id))->count();
        return $count_invitation;
    }

    public function datatablesListBalance($query = null, Request $request)
    {
        $query   = TechnicianSaldo::with(['order.service_detail', 'order.sparepart_detail'])
            ->where('technician_id', Auth::user()->technician->id)
            ->whereNotNull('total');

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function balanceDetailList($query = null, Request $request)
    {
        $query   = TechnicianSaldo::with(['order.service_detail', 'order.sparepart_detail'])
            ->where('technician_id', Auth::user()->technician->id)
            ->whereHas('order', function ($query) use ($request) {
                $query->whereNotNull('id');
            })
            ->where('transfer_status', 0)
            ->orderBy('id', 'desc');

        return $this->successResponse($query->paginate(10), 'ok', 200);
    }

    public function balanceTotal($query = null, Request $request)
    {
        $query   = TechnicianSaldo::with(['order.service_detail', 'order.sparepart_detail'])
            ->where('technician_id', Auth::user()->technician->id)
            ->whereHas('order')
            ->where('transfer_status', 0)
            ->orderBy('id', 'desc')
            ->sum('total');

        return $this->successResponse($query, 'ok', 200);
    }

    public function workmanshipReportPdf($order_id)
    {
        // // Fetch all customers from database
        // $data = Customer::get();
        // Send data to the view using loadView function of PDF facade
        $order = Order::where('id', $order_id)->first();
        $pdf = PDF::loadView('admin.teknisi.printpdf', compact('order'));
        // If you want to store the generated pdf to the server then you can use the store function
        // $pdf->save(storage_path().'_filename.pdf');
        // Finally, you can download the file using download function
        return $pdf->stream('workmanship_report.pdf');
    }
}
