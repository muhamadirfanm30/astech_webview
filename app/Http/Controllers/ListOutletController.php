<?php

namespace App\Http\Controllers;

use App\Models\BusinessToBusinessOutletDetailTransaction;
use App\Models\BusinessToBusinessOutletTransaction;
use App\Models\Company;
use Illuminate\Http\Request;
use Auth;

class ListOutletController extends Controller
{
    public function index()
    {
        if(!empty(Auth::user())){
            $comp_id = Auth::user()->ms_company_id;
        }else{
            return response()->json([
                'data' => 'error',
                'msg' => 'userid tidak ada',
                'status' => 401,
            ], 401);
            return $this->errorResponse('User ID Tidak Ditemukan');
        }
        $getComp = Company::where('id', $comp_id)->first();
        $showOutlet = BusinessToBusinessOutletTransaction::with([
                        'business_to_business_transaction',
                        'business_to_business_transaction.company',
                        'business_to_business_outlet_detail_transactions.get_type',
                    ])
                    ->whereHas('business_to_business_transaction', function($q) use ($comp_id) {
                        $q->where('company_id',  $comp_id);
                    })
                    ->get();
        return view('b2b.list-outlet.index', [
            'showOutlet' => $showOutlet,
            'getComp' => $getComp,
        ]);
    }

    public function detail($id)
    {
        $getOutlet = BusinessToBusinessOutletTransaction::where('id', $id)->first();
        $query = BusinessToBusinessOutletDetailTransaction::with([
                    'user_create', 
                    'user_update', 
                    'get_type'
                ])
                ->whereNull('is_parent_b2b_outlet_id')
                ->where('business_to_business_outlet_transaction_id', $id)
                ->get();
        return view('b2b.list-outlet.detail', [
            'query' => $query,
            'getOutlet' => $getOutlet,
        ]);
    }
}
