<?php

namespace App\Http\Controllers;

use Safe\stream;
use Illuminate\Http\Request;
use App\Models\B2BAssignTeknisi;
use App\Models\RequestOrderDetail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;


class B2BSchedulesController extends Controller
{
    public function index()
    {
        // dd(Auth::user()->id);
        $data = B2BAssignTeknisi::with(['get_teknisi', 'get_order.get_outlet', 'get_order.get_b2b_order_detail.get_type'])
        ->where('b2b_technician_id', Auth::user()->id)
        ->orderBy('schedule_date')
        ->get();
        return view('b2b.schedules.index', [
                'showData' => $data,
                'title' => 'Teknisi All Schedules',

            ]);
    }
    public function checkin()
    {
        // dd(Auth::user()->id);
        $data = B2BAssignTeknisi::with(['get_teknisi', 'get_order.get_outlet', 'get_order.get_b2b_order_detail.get_type'])
        ->where('b2b_technician_id', Auth::user()->id)
        ->orderBy('schedule_date')
        ->whereNull('checkin')
        ->whereNull('checkout')
        ->get();
        return view('b2b.schedules.checkin', [
                'showData' => $data,
                'title' => 'List CheckIn',

            ]);
    }
    public function checkout()
    {
        // dd(Auth::user()->id);
        $data = B2BAssignTeknisi::with(['get_teknisi', 'get_order.get_outlet', 'get_order.get_b2b_order_detail.get_type'])
        ->where('b2b_technician_id', Auth::user()->id)
        ->orderBy('schedule_date')
        ->whereNotNull('checkin')
        ->whereNull('checkout')
        ->get();
        return view('b2b.schedules.checkout', [
                'showData' => $data,
                'title' => 'List CheckOut',

            ]);
    }

    public function checkinTeknisi($id)
    {
        // dd(Auth::user()->id);
        $detailOrder = B2BAssignTeknisi::with(['get_teknisi', 'get_order.get_outlet', 'get_order.get_b2b_order_detail.get_type'])
        ->where('b2b_technician_id', Auth::user()->id)
        ->orderBy('schedule_date')
        ->whereNull('checkin')
        ->whereNull('checkout')
        ->where('id', $id)
        ->first();
        return view('b2b.schedules.checkin-teknisi', [
                'id' => $id,
                'detailOrder' => $detailOrder,
                'title' => 'CheckIn Post',
            ]);
    }

    public function checkoutTeknisi($id)
    {
        // dd(Auth::user()->id);
        $detailOrder = B2BAssignTeknisi::with(['get_teknisi', 'get_order.get_outlet', 'get_order.get_b2b_order_detail.get_type'])
        ->where('b2b_technician_id', Auth::user()->id)
        ->orderBy('schedule_date')
        ->whereNotNull('checkin')
        ->whereNull('checkout')
        ->where('id', $id)
        ->first();
        return view('b2b.schedules.checkout-teknisi', [
                'id' => $id,
                'detailOrder' => $detailOrder,
                'title' => 'CheckOut Post',
            ]);
    }

    public function checkinTeknisiPost(Request $request)
    {
        // dd(Auth::user()->id);
        $checkinSchedule = B2BAssignTeknisi::where('id', $request->id)->first();
        $filename = null;

        if(!empty($checkinSchedule)){

            if(!empty($request->attachment_checkin)){
                if($image = $request->attachment_checkin){
                    $name = $image->getClientOriginalName();
                    $img = Image::make($image);
                    $img->stream(); // <-- Key point
                    Storage::disk('local')->put('public/teknisi_checkin_image/' . $name, $img);
                    $filename = $name;
                }

                $checkinSchedule->update([
                    'checkin' => date('Y-m-d H:i:s'),
                    'status' => 'Tiba Ditempat',
                    'attachment_checkin' => $filename
                ]);

                return $this->successResponse($checkinSchedule, 'CheckIn Success !');
            }else{
                return $this->errorResponse(null, 'Bukti Foto Wajib di Upload.');
            }
        }else{
            return $this->errorResponse(null, 'Data Tidak Ditemukan.');
        }
    }


    public function checkoutTeknisiPost(Request $request)
    {
        $checkout = B2BAssignTeknisi::where('id', $request->id)->first();
        // dd($checkout);
        if(!empty($checkout)){
            if(!empty($request->status)){
                if(!empty($request->remark)){
                    if(!empty($request->attachment_checkout)){
                        $filename = null;
                        if($image = $request->attachment_checkout){
                            $name = $image->getClientOriginalName();
                            $img = Image::make($image);
                            $img->stream(); // <-- Key point
                            Storage::disk('local')->put('public/teknisi_checkout_image/' . $name, $img);
                            $filename = $name;
                        }

                        $checkout->update([
                            'checkout' => date('Y-m-d H:i:s'),
                            'status' => $request->status,
                            'remark' => $request->remark,
                            'reschedule' => $request->reschedule,
                            'attachment_checkout' => $filename,

                        ]);

                        return $this->successResponse($checkout,'Checkout Success !');
                    }else{
                        return $this->errorResponse(null, 'Bukti Foto Wajib di Upload.');
                    }
                }else{
                    return $this->errorResponse(null, 'Remark is Required');
                }
            }else{
                return $this->errorResponse(null, 'Status is Required');
            }
        }else{
            return $this->errorResponse(null, 'Data Tidak Ditemukan');
        }
    }

}
