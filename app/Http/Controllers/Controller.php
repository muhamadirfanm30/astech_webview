<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\Midtrans\Config;
use App\Models\Master\GeneralSetting;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $data = [];

    public function getRedirection($condition, $data)
    {
        if (is_string($data)) {
            $route = $data;
        }

        if (is_array($data)) {
            $route = $data['route'];
        }

        if ($condition) {
            // redirect
            return redirect($route)
                ->with('success', 'sukses');
        }

        // redirect
        return redirect($route)
            ->with('error', 'gagal');
    }



    public function setData(array $data)
    {
        foreach ($data as $key => $value) {
            $this->data[$key] = $value;
        }
        return $this;
    }

    public function getData(): array
    {
        $defaultData = $this->getDefaulData();
        return array_merge($defaultData, $this->data);
    }

    protected function initPaymentGetway(){
        // Set your Merchant Server Key
        $getKeyMidtrans = GeneralSetting::where('id', 7)->first();
        Config::$serverKey = $getKeyMidtrans->value;
        // Config::$serverKey = 'SB-Mid-server-7Q3bB3s0bd8oTo9iZVGghcqA';

        // Set to Development/Sandbox Environment (default). Set to true for Production Environment (accept real transaction).
        Config::$isProduction = false;
        // Set sanitization on (default)
        Config::$isSanitized = true;
        // Set 3DS transaction for credit card to true
        Config::$is3ds = true;
    }

    public function successResponse($data, string $msg = 'success', $status_code = 200)
    {
        return response()->json([
            'data' => $data,
            'msg' => $msg,
            'status_code' => $status_code,
            'success' => true,
        ], $status_code);
    }

    public function errorResponse($data, string $msg = 'terjadi kesalahan', $status_code = 400)
    {
        return response()->json([
            'data' => $data,
            'msg' => $msg,
            'status_code' => $status_code,
            'success' => false
        ], $status_code);
    }

    public function successStoreMsg()
    {
        return 'Data successfully saved';
    }
    public function successUpdateMsg()
    {
        return 'Data successfully updated';
    }
    public function successDeleteMsg()
    {
        return 'Data successfully deleted';
    }
}
