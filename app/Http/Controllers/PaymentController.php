<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\AdminController;
use App\Models\Master\EwalletHistory;
use App\Models\Master\Ewallet;
use App\Models\Master\Payment;
use App\Models\Master\GeneralSetting;
use App\Midtrans\Notification;
use App\Services\NotifikasiService;
use Illuminate\Http\Request;
use Auth;

class PaymentController extends AdminController
{
    public function notification(Request $request)
    {
        $payload = $request->getContent();
        // dd($payload);
        $notification = json_decode($payload);

        $this->initPaymentGetway();
        $statusCode = null;

        $paymentNotification = new Notification();
        // dd($paymentNotification);
        $order = EwalletHistory::with(['user', 'voucer'])->where('order_code', $notification->order_id)->firstOrFail();
        // dd($order);
        $getCode = Ewallet::where('user_id', $order->user_id)->firstOrFail();

        if ($order->isPaid()) {
            return response(['message' => 'This Order Has been Paid before'], 422);
        }

        $transaction = $paymentNotification->transaction_status;
        $type = $paymentNotification->payment_type;
        $orderId = $paymentNotification->order_id;
        $fraud = $paymentNotification->fraud_status;

        $vaNumber = null;
        $vendorName = null;
        if (!empty($paymentNotification->va_numbers[0])) {
            $vaNumber = $paymentNotification->va_numbers[0]->va_number;
            $vendorName = $paymentNotification->va_numbers[0]->bank;
        }

        $paymentStatus = null;
        if ($transaction == 'capture') {
            // For credit card transaction, we need to check whether transaction is challenge by FDS or not
            if ($type == 'credit_card') {
                if ($fraud == 'challenge') {
                    // TODO set payment status in merchant's database to 'Challenge by FDS'
                    // TODO merchant should decide whether this transaction is authorized or not in MAP
                    $paymentStatus =  Payment::CHALLENGE;
                } else {
                    // TODO set payment status in merchant's database to 'Success'
                    $paymentStatus =  Payment::SUCCESS;
                }
            }
        } else if ($transaction == 'settlement') {
            // TODO set payment status in merchant's database to 'Settlement'
            $paymentStatus =  Payment::SETTLEMENT;
        } else if ($transaction == 'pending') {
            // TODO set payment status in merchant's database to 'Pending'
            $paymentStatus =  Payment::PENDING;
        } else if ($transaction == 'deny') {
            // TODO set payment status in merchant's database to 'Denied'
            $paymentStatus =  Payment::DENY;
        } else if ($transaction == 'expire') {
            // TODO set payment status in merchant's database to 'expire'
            $paymentStatus =  Payment::EXPIRE;
        } else if ($transaction == 'cancel') {
            // TODO set payment status in merchant's database to 'Denied'
            $paymentStatus =  Payment::CANCEL;
        }

        $paymentParams = [
            'wallet_code' => $getCode->topup_code,
            'wallet_id' => $order->id,
            'amount' => $paymentNotification->gross_amount,
            'method' => 'Midtrans',
            'status' => $paymentStatus,
            'token' => $paymentNotification->transaction_id,
            'payloads' => $payload,
            'payment_type' => $paymentNotification->payment_type,
            'va_number' => $vaNumber,
            'vendor_name' => $vendorName,
            'biller_code' => $paymentNotification->biller_code,
            'bill_key' => $paymentNotification->bill_key,
        ];

        $payment = Payment::create($paymentParams);

        if ($paymentStatus && $payment) {
            \DB::transaction(
                function () use ($order, $payment, $getCode, $paymentNotification) {
                    if (in_array($payment->status, [Payment::SUCCESS, Payment::SETTLEMENT])) {
                        if (!empty($order->voucer)) {
                            if ($order->voucer->type == 1) {
                                $getCashback = $order->voucer->max_nominal;
                            } else {
                                $getCashback = $order->voucer->value;
                            }
                            if ($order->voucer->voucher_type == 1) {
                                $order->payment_status = EwalletHistory::PAID;
                                $order->status = EwalletHistory::CONFIRMED;
                                $order->payment_metod_id = 'MIDTRANS';
                                $order->payment_type_id =   2;
                                $order->transfer_status_id = 1;
                                $order->nominal = $order->nominal + $getCashback;
                                $order->saldo = $order->saldo + $getCashback;
                                $order->payment_metod_type = str_replace("_", " ", $paymentNotification->payment_type);

                                $order->save();

                                $getCode->update([
                                    'nominal' => $getCode->nominal + $order->nominal
                                ]);
                            } else {
                                $order->payment_status = EwalletHistory::PAID;
                                $order->status = EwalletHistory::CONFIRMED;
                                $order->payment_metod_id = 'MIDTRANS';
                                $order->payment_type_id =   2;
                                $order->transfer_status_id = 1;
                                $order->payment_metod_type = str_replace("_", " ", $paymentNotification->payment_type);
                                $order->save();

                                $getCode->update([
                                    'nominal' => $getCode->nominal + $order->nominal
                                ]);
                            }
                        } else {
                            $order->payment_status = EwalletHistory::PAID;
                            $order->status = EwalletHistory::CONFIRMED;
                            $order->payment_metod_id = 'MIDTRANS';
                            $order->payment_type_id =   2;
                            $order->transfer_status_id = 1;
                            $order->payment_metod_type = str_replace("_", " ", $paymentNotification->payment_type);
                            $order->save();

                            $getCode->update([
                                'nominal' => $getCode->nominal + $order->nominal
                            ]);
                        }
                    }

                    if (in_array($payment->status, [Payment::CANCEL, Payment::EXPIRE])) {
                        $order->payment_status = EwalletHistory::UNPAID;
                        $order->status = EwalletHistory::CANCEL;
                        $order->payment_metod_id = 'MIDTRANS';
                        $order->payment_type_id =   2;
                        $order->payment_metod_type = str_replace("_", " ", $paymentNotification->payment_type);
                        $order->save();
                    }

                    if (in_array($payment->status, [Payment::UNPAID, Payment::PENDING])) {
                        $order->payment_type_id =   2;
                        $order->payment_metod_type = str_replace("_", " ", $paymentNotification->payment_type);
                        $order->save();
                    }
                }
            );
        }

        $dataPush = [
            'type' => 'topup',
            'title' => "ASTECH TOPUP",
            'body' => $paymentStatus . " Topup sebesar Rp. " . number_format($paymentNotification->gross_amount, 0, '.', '.') . ' ' . $vendorName,
        ];
        (new NotifikasiService)->firebasePushNotifikasi($order->user, $dataPush);

        $message = 'Payment Status is : ' . $paymentStatus;
        $response = [
            'code' => 200,
            'message' => $message
        ];

        return response($response, 200);
    }

    public function completed(Request $request)
    {
        # code...
    }

    public function failed(Request $request)
    {
        # code...
    }

    public function unFinish(Request $request)
    {
        # code...
    }
}
