<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Models\B2BAssignTeknisi;
use App\Models\RequestOrderDetail;
use App\Models\BusinessToBusinessOutletDetailTransaction;

class DashboardController extends Controller
{
    public function loginForm()
    {
        return view('login');
    }



    public function index()
    {
        $showListDataOrder = RequestOrderDetail::where('user_id', Auth::user()->id)
        ->with(['get_b2b_detail'])
        ->whereHas('get_b2b_detail', function ($q)  {
            $q->where('status_quotation', 'Waiting Payment');
        })->count();
        if (Auth::user()->is_b2b_users) {
            return view('b2b.dashboard', [
                'showListDataOrder' => $showListDataOrder
            ]);
        }
        if (Auth::user()->is_b2b_teknisi) {
            $countCheckin = B2BAssignTeknisi::with(['get_teknisi', 'get_order.get_outlet', 'get_order.get_b2b_order_detail.get_type'])
            ->where('b2b_technician_id', Auth::user()->id)
            ->orderBy('schedule_date')
            ->whereNull('checkin')
            ->whereNull('checkout')
            ->count();
            $countCheckout = B2BAssignTeknisi::with(['get_teknisi', 'get_order.get_outlet', 'get_order.get_b2b_order_detail.get_type'])
            ->where('b2b_technician_id', Auth::user()->id)
            ->orderBy('schedule_date')
            ->whereNotNull('checkin')
            ->whereNull('checkout')
            ->count();
            return view('b2b.dashboard-teknisi', [
                'showListDataOrder' => $showListDataOrder,
                'countCheckin' => $countCheckin,
                'countCheckout' => $countCheckout,
            ]);
        }
    }

    public function successView()
    {
        return view('b2b.success_view');
    }

    public function successCheckinout()
    {
        return view('b2b.success_checkinout');
    }

    public function cartView()
    {
        return view('b2b.cart_view');
    }
}
