<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Storage;
use App\Helpers\ImageUpload;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class ProfileController extends Controller
{
    public function index()
    {
        return view('b2b.profile.show');
    }

    public function update($id)
    {
        return view('b2b.profile.update', [
            'user' => User::where('id', $id)->first()
        ]);
    }

    public function edit(Request $request, $id)
    {
        // return $request->all();
        $cek = User::where('id', $id)->first();
        $password = Hash::make($request->password);
        if(!empty($request->image_teknisi)){
            $filenameWithExt = $request->file('image_teknisi')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('image_teknisi')->getClientOriginalExtension();
            $filenameSimpan = $filename.'_'.time();
            $path = $request->file('image_teknisi')->storeAs('public/user_img', $filenameSimpan);
            $storeTeknisi = User::find($id)->update([
                'name' => $request->name,
                'image_teknisi' => !empty($filenameSimpan) ? $filenameSimpan : null,
                'email' => $request->email,
                'phone' => $request->phone,
                'address' => $request->address,
                'api_token' => Str::random(80),
                'is_b2b_users' => 1,
            ]);
            return redirect()->route('profile.show')->with('success', 'Berhasil.!');
        }else{
            $storeTeknisi = User::find($id)->update([
                'name' => $request->name,
                'image_teknisi' => $cek->image_teknisi,
                'email' => $request->email,
                'phone' => $request->phone,
                'address' => $request->address,
                'api_token' => Str::random(80),
                'is_b2b_users' => 1,
            ]);
            return redirect()->route('profile.show')->with('success', 'Berhasil.!');
        }

        

        
    }
}
