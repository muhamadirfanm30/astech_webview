<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Model\Master\TechnicianSchedule;
use App\Models\RequestOrderTmp;
use App\Models\B2BAssignTeknisi;
use App\Models\BusinessToBusinessOutletDetailTransaction;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Storage;
use App\Models\BusinessToBusinessTransaction;
use App\Models\RequestOrderDetail;
use App\Models\RequestOrders;
use Illuminate\Http\Request;
use App\Http\Requests\OrderRequestB2b;
use Auth;

class CreateOrderController extends Controller
{
    public function index()
    {
        $showListDataOrder = RequestOrders::where('user_id', Auth::user()->id)
        ->with([
            'get_user',
            'get_history_order.get_b2b_detail.business_to_business_outlet_transaction.business_to_business_transaction.company',
            'get_history_order.get_schedule'
        ])->get();
        return view('b2b.request-order.index', [
            'showListDataOrder' => $showListDataOrder
        ]);
    }

    public function cart()
    {
        $hitungCart = RequestOrderTmp::where('user_id', Auth::user()->id)->count();
        $getCart = RequestOrderTmp::where('user_id', Auth::user()->id)->with(['tmp_unit', 'tmp_outlet'])->get();
        return view('b2b.cart.index', [
            'hitungCart' => $hitungCart,
            'getCart' => $getCart,
        ]);
    }

    public function create()
    {
        $showOutlet = BusinessToBusinessTransaction::where('company_id', Auth::user()->ms_company_id)->with('business_to_business_outlet_transactions')->first();
        $hitungCart = RequestOrderTmp::where('user_id', Auth::user()->id)->count();
        return view('b2b.request-order.create', [
            'showOutlet' => $showOutlet,
            'hitungCart' => $hitungCart
        ]);
    }

    public function detail($id)
    {
        $detailOrder = RequestOrderDetail::where('id', $id)
        ->with([
            'get_b2b_detail.business_to_business_outlet_transaction.business_to_business_transaction.company',
            'get_schedule',
        ])
        ->first();
        $getTeknisi = B2BAssignTeknisi::where('order_id', $id)->with('get_teknisi')->get();

        return view('b2b.request-order.detailOrder', [
            'detailOrder' => $detailOrder,
            'getTeknisi' => $getTeknisi
        ]);
    }


    // api request order

    public function select2Outlet()
    {
        $showOutlet = BusinessToBusinessTransaction::where('company_id', Auth::user()->ms_company_id)->with('business_to_business_outlet_transactions')->first();
        return $showOutlet;
        // return response()->json([
        //     'data' => $showOutlet,
        //     'msg' => 'data ditemukan',
        // ], 200);
    }

    public function select2Unit($id)
    {
        $showUnitWhereOutlet = BusinessToBusinessOutletDetailTransaction::where('business_to_business_outlet_transaction_id', $id)
            ->whereNull('is_parent_b2b_outlet_id')
            ->count();

        if($showUnitWhereOutlet > 0){
            $showUnitWhereOutlet = BusinessToBusinessOutletDetailTransaction::with('get_type')->where('business_to_business_outlet_transaction_id', $id)
            ->whereNull('is_parent_b2b_outlet_id')
            ->get();
            return response()->json([
                'data' => $showUnitWhereOutlet,
                'msg' => 'data ditemukan',
            ], 200);
        }else{
            return response()->json([
                'data' => null,
                'msg' => 'Tidak Ada Unit di Outlet Ini',
            ], 400);
            return $this->errorResponse('data', 'Tidak Ada Unit di Outlet Ini');
        }
    }

    public function addToCart(OrderRequestB2b $request)
    {
        if($request->hasFile('images')){
            $filenameWithExt = $request->file('images')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('images')->getClientOriginalExtension();
            $filenameSimpan = $filename.'_'.time();
            $path = $request->file('images')->storeAs('public/posts_image', $filenameSimpan);
            $getUnit = BusinessToBusinessOutletDetailTransaction::where('id', $request->unit_id)->first();

            $storeDetail = BusinessToBusinessOutletDetailTransaction::create([
                'business_to_business_outlet_transaction_id' => $request->outlet_id,
                'unit_ke' => $getUnit->unit_ke,
                'merek' => $getUnit->merek,
                'remark' => '-',
                'no_quotation' => '-',
                'status_quotation' => 'Requested',
                'no_po' => '-',
                'no_invoice' => '-',
                'nominal_quot' => 0,
                'tanggal_pembayaran' => null,
                'tanggal_quot' => null,
                'tanggal_po' => null,
                'tanggal_Invoice' => null,
                'tanggal_pengerjaan' => null,
                'file_berita_acara' => '-',
                'file_quot' => '-',
                'file_po' => '-',
                'file_invoice' => '-',
                'uniq_key' => '-',
                'user_created' => Auth::id(),
                'is_parent_b2b_outlet_id' => $getUnit->id,
            ]);

            $storeOrder = RequestOrderTmp::create([
                // 'request_code' => $reqOrder->request_code,
                'ms_outlet_id' => $request->outlet_id,
                'ms_transaction_b2b_detail' => $request->unit_id,
                'ms_b2b_detail' => $storeDetail->id,
                'service_type' => $request->service_type,
                'user_id' => Auth::user()->id,
                'created_at' => date('y-m-d H:i:s'),
                'vidio' => null,
                'image' => !empty($filenameSimpan) ? $filenameSimpan : null,
                'remark' => $request->remark,
            ]);
        }else{
            return 'tidak ada file yang diupload';
        }
    }

    public function checkout(OrderRequestB2b $request)
    {
        if($request->hasFile('images')){
            $filenameWithExt = $request->file('images')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('images')->getClientOriginalExtension();
            $filenameSimpan = $filename.'_'.time();
            $path = $request->file('images')->storeAs('public/posts_image', $filenameSimpan);
            $getUnit = BusinessToBusinessOutletDetailTransaction::where('id', $request->unit_id)->first();

            $storeDetail = BusinessToBusinessOutletDetailTransaction::create([
                'business_to_business_outlet_transaction_id' => $request->outlet_id,
                'unit_ke' => $getUnit->unit_ke,
                'merek' => $getUnit->merek,
                'remark' => '-',
                'no_quotation' => '-',
                'status_quotation' => 'Requested',
                'no_po' => '-',
                'no_invoice' => '-',
                'nominal_quot' => 0,
                'tanggal_pembayaran' => null,
                'tanggal_quot' => null,
                'tanggal_po' => null,
                'tanggal_Invoice' => null,
                'tanggal_pengerjaan' => null,
                'file_berita_acara' => '-',
                'file_quot' => '-',
                'file_po' => '-',
                'file_invoice' => '-',
                'uniq_key' => '-',
                'user_created' => Auth::id(),
                'is_parent_b2b_outlet_id' => $getUnit->id,
            ]);

            $reqOrder = RequestOrders::create([
                'request_code' => $this->generateOrderCode(),
                'user_id' => Auth::user()->id,
                'created_at' => date('Y-m-d H:i:s')
            ]);

            $storeOrder = RequestOrderDetail::create([
                // 'request_code' => $reqOrder->request_code,
                'ms_outlet_id' => $request->outlet_id,
                'service_type' => $request->service_type,
                'user_id' => Auth::user()->id,
                'ms_b2b_detail' => $storeDetail->id,
                'vidio' => null,
                'remark' => $request->remark,
                'ms_transaction_b2b_detail' => $request->unit_id,
                'created_at' => date('y-m-d H:i:s'),
                'image' => !empty($filenameSimpan) ? $filenameSimpan : null,
                'request_code' => $reqOrder->request_code
            ]);
        }else{
            return 'tidak ada file yang diupload';
        }
    }

    public function checkoutDataCart(Request $request)
    {
        if(!empty($request->id_checkout_item)){
            $cek = RequestOrderTmp::whereIn('id', $request->id_checkout_item)->get();
            $hitung = RequestOrderTmp::whereIn('id', $request->id_checkout_item)->count();
            if($hitung > 0){
                try{
                    // return '> 0';
                    $reqOrder = RequestOrders::create([
                        'request_code' => $this->generateOrderCode(),
                        'user_id' => Auth::user()->id,
                        'created_at' => date('Y-m-d H:i:s')
                    ]);

                    for ($i = 0; $i < $hitung; $i++) {
                        $storeDetailOrder = RequestOrderDetail::create([
                            'request_code' => $reqOrder->request_code,
                            'ms_outlet_id' => $cek[$i]->ms_outlet_id,
                            'ms_transaction_b2b_detail' => $cek[$i]->ms_transaction_b2b_detail,
                            'service_type' => $cek[$i]->service_type,
                            'user_id' => $cek[$i]->user_id,
                            'status_quotation' => $cek[$i]->status_quotation,
                            'ms_b2b_detail' => $cek[$i]->ms_b2b_detail,
                            'vidio' => $cek[$i]->vidio,
                            'image' => $cek[$i]->image,
                            'remark' => $cek[$i]->remark,
                        ]);
                    }

                    $this->deleteOrder($request);

                }catch (QueryException $e) {
                    throw new \Exception($e->getMessage());
                }
            }else{
                return response()->json([
                    'data' => null,
                    'msg' => 'Cart Empty',
                    'status' => 401,
                ], 401);
            }
        }else{
            return response()->json([
                'data' => null,
                'msg' => 'No data selected',
                'status' => 402,
            ], 402);
        }
    }

    public function deleteDataCart($id)
    {
        $cariData = RequestOrderTmp::where('id', $id)->first();
        if(!empty($cariData)){
            $cariData->delete();
            return response()->json([
                'data' => null,
                'msg' => 'Data Has Been Deleted',
                'status' => 200,
            ], 200);
        }else{
            return response()->json([
                'data' => null,
                'msg' => 'Data Not Found',
                'status' => 400,
            ], 400);
        }
    }

    public function deleteAllData()
    {
        $deleteData = RequestOrderTmp::where('user_id', Auth::user()->id)->get();
        $countData = RequestOrderTmp::where('user_id', Auth::user()->id)->count();
        for ($i = 0; $i < $countData; $i++) {
            $deleteTmp = RequestOrderTmp::find($deleteData[$i]->id)->delete();
        }
        return response()->json([
            'data' => null,
            'msg' => 'Data Has Been Deleted',
            'status' => 200,
        ], 200);
    }

    public function deleteOrder(Request $request)
    {
        $getCart = RequestOrderTmp::whereIn('id', $request->id_checkout_item)->with('business_to_business_outlet_detail_transactions')->get();
        $delete = RequestOrderTmp::whereIn('id', $request->id_checkout_item)->get('id');
        $coundTmp = RequestOrderTmp::whereIn('id', $request->id_checkout_item)->count();
        for ($i = 0; $i < $coundTmp; $i++) {
            $deleteTmp = RequestOrderTmp::find($getCart[$i]->id)->delete();
        }
    }

    public function generateOrderCode()
    {
        $q          = RequestOrders::count();
        $prefix     = 'O'; //prefix = O-16032020-00001
        $separator  = '-';
        $date       = date('dmY');
        $number     = 1; //format
        $new_number = sprintf("%06s", $number);
        $code       = 'ORD-' . $date . ($separator) . ($new_number);
        $order_code   = $code;
        if ($q > 0) {
            $last     = RequestOrders::orderBy('id', 'desc')->value('request_code');
            $last     = explode("-", $last);
            $order_code = 'ORD-' . $date . ($separator) . (sprintf("%06s", $last[2] + 1));
        }
        return $order_code;
    }
}
