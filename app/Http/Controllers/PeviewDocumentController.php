<?php

namespace App\Http\Controllers;

use App\Models\B2BDataTransactions;
use Illuminate\Http\Request;

class PeviewDocumentController extends Controller
{
    public function quot($id)
    {
        $showQuotation = B2BDataTransactions::with(['get_part', 'get_type',  'get_labor'])->where('id_transaction', $id)->orderBy('id', 'Desc')->first();
        return view('b2b.document.quotation', [
            'showQuotation' => $showQuotation
        ]);
    }

    public function invoice($id)
    {
        $showInv = B2BDataTransactions::with(['get_part', 'get_type',  'get_labor'])->where('id_transaction', $id)->orderBy('id', 'Desc')->first();
        return view('b2b.document.invoice', [
            'showInv' => $showInv
        ]);
    }
}
