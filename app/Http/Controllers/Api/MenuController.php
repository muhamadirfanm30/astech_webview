<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Http\Requests\MenuRequest;
use App\Model\Menu;
use App\Services\MenuService;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class MenuController extends ApiController
{
    public function index()
    {
        $response = Menu::with('childs')->paginate(10);

        return $this->successResponse($response);
    }

    public function select2(Request $request)
    {
        $response = Menu::with('childs')
            ->orderBy('display_order', 'ASC')
            ->where('name', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();

        return $this->successResponse($response);
    }

    public function datatables()
    {
        $query = Menu::whereNull('parent_id')->orderBy('display_order', 'ASC')->with('childs');

        return DataTables::of($query)
            ->toJson();
    }

    public function store(MenuRequest $request, MenuService $menuService)
    {
        // validasi
        $request->validated();

        return $this->successResponse(
            $menuService->create($request->all()),
            'success',
            201
        );
    }

    public function show(Menu $menu)
    {
        $response = $menu;

        return $this->successResponse($response);
    }

    public function update(MenuRequest $request, Menu $menu, MenuService $menuService)
    {
        // validasi
        $request->validated();

        return $this->successResponse(
            $menuService->setMenu($menu)->update($request->all()),
            'success',
            200
        );
    }

    public function destroy(Menu $menu, MenuService $menuService)
    {
        return $this->successResponse(
            $menuService->setMenu($menu)->delete(),
            'success',
            204
        );
    }

    public function updateMoveMenu(Request $request, MenuService $MenuService)
    {
        return $MenuService->updateMoveMenu($request);
    }
}
