<?php

namespace App\Http\Controllers\Api\Technician;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Technician\JobExperienceRequest;
use App\Services\Technician\JobExperienceService as Service;
use App\Model\Technician\JobExperience;
use Illuminate\Http\Request;

class JobExperienceController extends ApiController
{
    /**
     * show list with paginate
     */
    public function index(Service $service)
    {
        $response = $service->list()->paginate(10);

        return $this->successResponse($response);
    }

    /**
     * show list with select2 structur
     */
    public function select2(Request $request, Service $service)
    {
        return $this->successResponse($service->select2($request));
    }

    /**
     * show list with datatables structur
     */
    public function datatables(Service $service)
    {
        return $service->datatables();
    }

    /**
     * insert data
     */
    public function store(JobExperienceRequest $request, Service $service)
    {
        return $this->successResponse(
            $service->create($request->all()),
            'success',
            201
        );
    }

    /**
     * show one data
     */
    public function show(JobExperience $experience)
    {
        return $this->successResponse($experience);
    }

    /**
     * update data
     */
    public function update(JobExperienceRequest $request, JobExperience $experience, Service $service)
    {
        return $this->successResponse(
            $service->setExperience($experience)->update($request->all()),
            'success'
        );
    }

    /**
     * delete data
     */
    public function destroy(JobExperience $experience, Service $service)
    {
        return $this->successResponse(
            $service->setExperience($experience)->delete(),
            'success',
            204
        );
    }
}
