<?php

namespace App\Http\Controllers\Api\Technician;

use Illuminate\Http\Request;
use App\Model\Technician\Technician;
use App\Http\Controllers\ApiController;
use App\Model\Master\TechnicianCurriculum;
use App\Http\Requests\Technician\TechnicianRequest;
use App\Services\Technician\TechnicianService as Service;

class TechnicianController extends ApiController
{
    /**
     * show list with paginate
     */
    public function index(Service $service)
    {
        $response = $service->list()->paginate(10);

        return $this->successResponse($response);
    }

    /**
     * show list with select2 structur
     */
    public function select2(Request $request, Service $service)
    {
        return $this->successResponse($service->select2($request));
    }

    /**
     * show list with datatables structur
     */
    public function datatables(Service $service)
    {
        return $service->datatables();
    }

    /**
     * insert data
     */
    public function store(TechnicianRequest $request, Service $service)
    {
        return $this->successResponse(
            $service->create($request->all()),
            'success',
            201
        );
    }

    /**
     * insert data
     */
    public function storeInfo($user_id, Request $request, Service $service)
    {
        return $this->successResponse(
            $service->createInfo($request),
            'success',
            201
        );
    }

    /**
     * insert data
     */
    public function storeExperience(Technician $technician, Request $request, Service $service)
    {
        return $this->successResponse(
            $service->setTechnician($technician)->createExperience($request->all()),
            'success',
            201
        );
    }

    /**
     * insert data
     */
    public function storeCurriculum(Technician $technician, Request $request, Service $service)
    {
        return $this->successResponse(
            $service->setTechnician($technician)->createCurriculum($request->all()),
            'success',
            201
        );
    }

    /**
     * insert data
     */
    public function storePriceService(Technician $technician, Request $request, Service $service)
    {
        return $this->successResponse(
            $service->setTechnician($technician)->storePriceService($request->all()),
            'success',
            201
        );
    }

    /**
     * show one data
     */
    public function show(Technician $technician)
    {
        return $this->successResponse($technician);
    }

    /**
     * show one data
     */
    public function experienceDatatables(Technician $technician, Service $service)
    {
         return $service->setTechnician($technician)->experienceDatatables();
    }

    /**
     * show one data
     */
    public function curriculumDatatables(Technician $technician, Service $service)
    {

         return $service->setTechnician($technician)->curriculumDatatables();
    }

    /**
     * show one data
     */
    public function priceServiceDatatables(Technician $technician, Service $service)
    {
         return $service->setTechnician($technician)->priceServiceDatatables();
    }

    /**
     * update data
     */
    public function updateJobInfo(Request $request, Technician $technician, Service $service)
    {
        return $this->successResponse(
            $service->setTechnician($technician)->updateJobInfo($request->all()),
            'success'
        );
    }

    /**
     * update data
     */
    public function updateRekInfo(Request $request, Technician $technician, Service $service)
    {
        return $this->successResponse(
            $service->setTechnician($technician)->updateRekInfo($request->all()),
            'success'
        );
    }

    /**
     * delete data
     */
    public function destroy(Technician $technician, Service $service)
    {
        return $this->successResponse(
            $service->setTechnician($technician)->delete(),
            'success',
            204
        );
    }

    /**
     * delete data
     */
    public function priceServiceDestroy($id, Service $service)
    {
        return $this->successResponse(
            $service->priceServiceDestroy($id),
            'success',
            204
        );
    }

    public function curriculumDestroy($id, Service $service)
    {
        return $this->successResponse(
            $service->curriculumDestroy($id),
            'success',
            204
        );
    }
}
