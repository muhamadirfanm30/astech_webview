<?php

namespace App\Http\Controllers\Api\Technician;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Technician\JobTitleRequest;
use App\Model\Technician\JobTitle;
use App\Services\Technician\JobTitleService as Service;
use Illuminate\Http\Request;

class JobTitleController extends ApiController
{
    /**
     * show list with paginate
     */
    public function index(Service $service)
    {
        $response = $service->list()->paginate(10);

        return $this->successResponse($response);
    }

    /**
     * show list with select2 structur
     */
    public function select2(Request $request, Service $service)
    {
        return $this->successResponse($service->select2($request));
    }

    /**
     * show list with datatables structur
     */
    public function datatables(Service $service)
    {
        return $service->datatables();
    }

    /**
     * insert data
     */
    public function store(JobTitleRequest $request, Service $service)
    {
        return $this->successResponse(
            $service->create($request->all()),
            'success',
            201
        );
    }

    /**
     * show one data
     */
    public function show(JobTitle $job)
    {
        return $this->successResponse($job);
    }

    /**
     * update data
     */
    public function update(JobTitleRequest $request, JobTitle $job, Service $service)
    {
        return $this->successResponse(
            $service->setJob($job)->update($request->all()),
            'success'
        );
    }

    /**
     * delete data
     */
    public function destroy(JobTitle $job, Service $service)
    {
        return $this->successResponse(
            $service->setJob($job)->delete(),
            'success',
            204
        );
    }
}
