<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Providers\RouteServiceProvider;
use App\Services\OtpService;
use App\Services\UserService;
use Illuminate\Auth\Events\Verified;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Http\Request;
use App\User;

class VerificationController extends ApiController
{
    /*
    |--------------------------------------------------------------------------
    | Email Verification Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling email verification for any
    | user that recently registered with the application. Emails may also
    | be re-sent if the user didn't receive the original email message.
    |
    */

    use VerifiesEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('signed')->only('verify');
        $this->middleware('throttle:6,1')->only('verify', 'resend');
    }

    /**
     * Show the email verification notice.
     *
     */
    public function show()
    {
        //
    }

    /**
     * Mark the authenticated user's email address as verified.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function verify(Request $request)
    {
        $user = $request->user();
        
        if ($user->hasVerifiedEmail()) {
            // send response
            return $this->successResponse([
                'user' => $user,
            ], 'akun sudah diverifikasi');
        }

        if ($request->route('id') == $user->getKey() && $user->markEmailAsVerified()) {
            // send response
            return $this->successResponse([
                'user' => $user,
            ], 'Email verified!');
        }

        // send response
        return $this->errorResponse(
            ['user' => $user],
            'login gagal'
        );
    }

    /**
     * Resend the email verification notification.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function resend(Request $request)
    {
        // dd('asdasd');
        if ($request->user()->hasVerifiedEmail()) {
            // send response
            return $this->errorResponse(
                [],
                'User already have verified email!',
                400
            );
        }

        $service = new UserService();

        // send mail
        $service->sendEmailVerification($request->user());

        // send response
        return $this->successResponse([], 'The notification has been resubmitted');
    }

    public function otpVerify(Request $request)
    {
        $user = auth()->user();

        $otp = new OtpService();

        if ($otp->verify($request->otp, $user->otp_url)) {
            $user->update([
                'otp_verified_at' => now()
            ]);

            // send response
            return $this->successResponse([], 'Success Verification');
        }

        // send response
        return $this->errorResponse(
            [],
            'Error Verification',
            400
        );
    }

    public function otpResend(Request $request)
    {
        $user = auth()->user();

        $otp = (new OtpService())->create();

        $otp->setLabel($user->id);

        $token = $otp->now();

        $url = $otp->getProvisioningUri();

        $msg = (new OtpService())->sendSMS([
            'to' => $user->phone,
            'text' => $token,
        ]);

        $user->update([
            'otp_code' => $token,
            'otp_url' => $url,
        ]);

        // send response
        return $this->successResponse([], 'kode berhasil dikirim kembali');
    }

    
}
