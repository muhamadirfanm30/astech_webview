<?php

namespace App\Http\Controllers\Api\Master;

use Auth;
use Illuminate\Http\Request;
use App\Model\Master\MsAddress;
use Yajra\DataTables\DataTables;
use App\Model\Master\TeknisiInfo;
use App\Http\Controllers\ApiController;
use App\Services\Master\AddressService;
use App\Http\Requests\Master\AddressRequest;

class AddressController extends ApiController
{
    /**
     * show list with paginate
     */
    public function index(AddressService $addressService)
    {

        $response = $addressService->list()->paginate(10);

        return $this->successResponse($response);
    }

    /**
     * show list with datatables structur
     */
    public function datatables(AddressService $addressService)
    {
        $query = $addressService->list();//->where('is_teknisi', null);

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * show list with datatables structur
     */
    public function datatablesUser(AddressService $addressService, $user_id)
    {
        $query = MsAddress::with('user', 'city', 'district', 'village', 'zipcode', 'types')->where('user_id', $user_id);

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function datatablesTeknisi(AddressService $addressService)
    {
        $query = $addressService->list();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * insert data
     */
    public function store(Request $request, AddressService $addressService)
    {
        // validasi
        // $request->validated();
        $user = Auth::user();
        $data = TeknisiInfo::where('user_id', $user->id)->get();
        // dd($data);
        if($user->hasRole('Technician')){
            $url = '/customer/dashboard';
        }

        if($user->hasRole('admin')){
            $url = '/admin/home';
        }

        if($user->hasRole('Customer')){
            $url = '/customer/address/show';
        }

        $response = [
            'address' => $addressService->create($request->all()),
            'redirect'=> $url
        ];
        return $this->successResponse(
            $response,
            'success',
            201
        );
    }

    /**
     * show one data
     */
    public function show(MsAddress $address)
    {
        $response = $address;

        return $this->successResponse($response);
    }

    /**
     * update data
     */
    public function update(Request $request, MsAddress $address, AddressService $addressService)
    {
        // return $request->all();
        // validasi
        // $request->validated();

        return $this->successResponse(
            $addressService->setAddress($address)->update($request->all()),
            'success'
        );
    }

    /**
     * set main data
     */
    public function setMain(MsAddress $address, AddressService $addressService)
    {
        return $this->successResponse(
            $addressService->setAddress($address)->setMain(),
            'success'
        );
    }

    /**
     * delete data
     */
    public function destroy(MsAddress $address, AddressService $addressService)
    {
        $is_main = MsAddress::where('id',$address->id)->where('is_main', 1)->count();
        if($is_main){
            return $this->errorResponse(
                $is_main,
                'Address utama tidak bisa di delete',
                400);
        }else{
            return $this->successResponse(
                $addressService->setAddress($address)->delete(),
                'success',
                204
            );
        }

    }
}
