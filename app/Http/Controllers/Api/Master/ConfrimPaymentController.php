<?php

namespace App\Http\Controllers\Api\Master;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\Services\Master\ConfrimPaymentService as Service;
use App\Http\Requests\Master\ConfrimPaymentRequest;
use App\Model\Master\ConfrimPayment;

class ConfrimPaymentController extends ApiController
{

    public function store(ConfrimPaymentRequest $request, Service $service)
    {
        $request->validated();
        return $this->successResponse(
            $service->create($request->all()),
            'success',
            400
        );
    }

}
