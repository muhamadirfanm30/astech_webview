<?php

namespace App\Http\Controllers\Api\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Services\Master\TopupWalletService as Service;
use App\Http\Requests\Master\TicketRequest;
use App\Model\Master\MsTicket as Ticket;
use App\Model\Master\TopupWallet;
use App\Model\Master\Ewallet;
use App\Helpers\ImageUpload;
use App\Http\Requests\Master\ConfrimPaymentRequest;
use App\Http\Requests\TopUpRequest;
use App\Model\Master\EwalletHistory;
use App\Model\Master\EwalletPayment;
use App\Model\Master\Payment;
use Auth;
use DB;
use Carbon\Carbon;
use Validator;
use Redirect;
use App\Midtrans\Snap;
use App\Model\Master\VoucherGeneral;
use App\Notifications\TopupNotifikasi;
use App\Services\NotifikasiService;
use App\User;

class TopupWalletController extends ApiController
{
    /**
     * show list with datatables structur
     */
    public function datatables(Service $service)
    {
        return $service->datatables();
    }

    public function adminListWallet(Service $service)
    {
        return $service->datatablesAdminListWallet();
    }

    /**
     * insert data
     */
    public function store(TopUpRequest $request)
    {
        // if (!in_array($request->nominal, ['10000', '50000', '200000', '500000'])) {
        //     return $this->errorResponse(null, 'terjadi kesalahan coba lagi ', 422);
        // }

        $length = 6;
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        $getWalletCustomer = Ewallet::where('user_id', Auth::user()->id)->first();

        if ($getWalletCustomer == null) {
            if ($request->nominal >= 10000) {
                $createWallet = Ewallet::create([
                    'nominal' => 0,
                    'topup_code' =>  "TRANS-" . date('dmy') . $randomString,
                    'user_id' => Auth::user()->id,
                ]);

                $createHistory = EwalletHistory::create([
                    'ms_wallet_id' => $createWallet->id,
                    'order_code' => date('ymd') . $randomString,
                    'type_transaction_id' => 0,
                    'transfer_status_id' => 0,
                    'kode_uniq_bank' => rand(100,999),
                    'saldo' => $request->nominal,
                    'nominal' => $request->nominal,
                    'user_id' => Auth::user()->id,
                    'status' =>  Payment::UNPAID,
                    'payment_status' =>  Payment::UNPAID,
                ]);

                return $this->successResponse($createHistory);
                // $checkout   = EwalletHistory::with('user')->where('id', $createHistory->id)->first();
                // return $checkout;
                // $getCode  = Ewallet::where('user_id', Auth::user()->id)->first();
                // $this->_generatePaymentToken($checkout, $getCode);

            } else {
                return $this->errorResponse(null, 'Minimal Top up Rp 10.000 ', 422);
            }
        } else {
            if ($request->nominal >= 10000) {
                $getSaldoAwal = EwalletHistory::where('user_id', Auth::user()->id)->orderBy('created_at', 'desc')->first();
                $getIdMaster = Ewallet::where('user_id', Auth::user()->id)->first();

                $updateUser = Ewallet::where('user_id', Auth::user()->id)->update([
                    'nominal' => $getIdMaster->nominal,
                    'topup_code' =>  "TRANS-" . date('dmy') . "-" . $randomString,
                    'user_id' => Auth::user()->id,
                ]);

                $createWallet = EwalletHistory::create([
                    'ms_wallet_id' => $getIdMaster->id,
                    'order_code' => date('ymd') . $randomString,
                    'type_transaction_id' => 0,
                    'transfer_status_id' => 0,
                    'kode_uniq_bank' => rand(100,999),
                    'saldo' => $getSaldoAwal !== null ? $getSaldoAwal->saldo + $request->nominal : $request->nominal,
                    'nominal' => $request->nominal,
                    'user_id' => Auth::user()->id,
                    'status' =>  Payment::UNPAID,
                    'payment_status' =>  Payment::UNPAID,
                ]);

                return $this->successResponse($createWallet);
            } else {
                return $this->errorResponse(null, 'Minimal Top up Rp 10.000 ', 422);
            }
        }
    }

    public function saved(Request $request)
    {
        if (!empty($request->nominal)) {
            if ($request->nominal >= $request->selisih) {
                $getSaldoAwal = EwalletHistory::where('user_id', Auth::user()->id)->orderBy('created_at', 'desc')->first();
                $getIdMaster = Ewallet::where('user_id', Auth::user()->id)->first();

                $length = 6;
                $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $charactersLength = strlen($characters);
                $randomString = '';
                for ($i = 0; $i < $length; $i++) {
                    $randomString .= $characters[rand(0, $charactersLength - 1)];
                }

                $getWalletCustomer = Ewallet::where('user_id', Auth::user()->id)->first();

                if ($getWalletCustomer == null) {
                    if ($request->nominal >= 10000) {
                        $createWallet = Ewallet::create([
                            'nominal' => 0,
                            'topup_code' =>  "TRANS-" . date('dmy') . $randomString,
                            'user_id' => Auth::user()->id,
                        ]);

                        $createHistory = EwalletHistory::create([
                            'ms_wallet_id' => $createWallet->id,
                            'order_code' => date('ymd') . $randomString,
                            'type_transaction_id' => 0,
                            'transfer_status_id' => 0,
                            'saldo' => $request->nominal,
                            'nominal' => $request->nominal,
                            'user_id' => Auth::user()->id,
                            'status' =>  Payment::UNPAID,
                            'payment_status' =>  Payment::UNPAID,
                        ]);


                        $checkout   = EwalletHistory::with('user')->where('id', $createWallet->id)->first();
                        $ApiController = new ApiController();
                        return $ApiController->successResponse($checkout, 201);
                    } else {
                        return $this->errorResponse(null, 'Minimal Top up Rp 10.000 ', 422);
                    }
                } else {
                    if ($request->nominal >= 10000) {
                        $getSaldoAwal = EwalletHistory::where('user_id', Auth::user()->id)->orderBy('created_at', 'desc')->first();
                        $getIdMaster = Ewallet::where('user_id', Auth::user()->id)->first();

                        $updateUser = Ewallet::where('user_id', Auth::user()->id)->update([
                            'nominal' => $getIdMaster->nominal,
                            'topup_code' =>  "TRANS-" . date('dmy') . "-" . $randomString,
                            'user_id' => Auth::user()->id,
                        ]);

                        $createWallet = EwalletHistory::create([
                            'ms_wallet_id' => $getIdMaster->id,
                            'order_code' => date('ymd') . $randomString,
                            'type_transaction_id' => 0,
                            'transfer_status_id' => 0,
                            'saldo' => $getSaldoAwal !== null ? $getSaldoAwal->saldo + $request->nominal : $request->nominal,
                            'nominal' => $request->nominal,
                            'user_id' => Auth::user()->id,
                            'status' =>  Payment::UNPAID,
                            'payment_status' =>  Payment::UNPAID,
                        ]);

                        $checkout   = EwalletHistory::with('user')->where('id', $createWallet->id)->first();
                        $ApiController = new ApiController();
                        return $ApiController->successResponse($checkout, 201);
                    } else {
                        return $this->errorResponse(null, 'Minimal Top up Rp 10.000 ', 422);
                    }
                }

                // $createWallet = EwalletHistory::create([
                //     'ms_wallet_id' => $getIdMaster->id,
                //     'type_transaction_id' => 0, // topup
                //     'order_code' => date('ymd') . $randomString,
                //     'transfer_status_id' => 0,
                //     'saldo' => $getSaldoAwal->saldo + $request->nominal,
                //     'nominal' => $request->nominal,
                //     'user_id' => Auth::user()->id,
                // ]);

                // $checkout   = EwalletHistory::with('user')->where('id', $createWallet->id)->first();
                // $getCode  = Ewallet::where('user_id', Auth::user()->id)->first();
                // $this->_generatePaymentToken($checkout, $getCode);

                // return $this->successResponse(EwalletHistory::where('id', $createWallet->id)->first());
            } else {
                return $this->errorResponse(null, 'Minimal Top up Rp.' . number_format($request->selisih), 422);
            }
        } else {
            return $this->errorResponse(null, 'Nominal Not Null', 422);
        }
    }

    public function adminStore(Request $request, Service $service)
    {
        $length = 6;
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        $getWalletCustomer = Ewallet::where('user_id', $request->user_id)->first();
        if (!empty($request->nominal)) {
            if (!empty($request->user_id)) {
                if ($getWalletCustomer == null) {
                    if ($request->nominal >= 10000) {
                        $createWallet = Ewallet::create([
                            'nominal' => 0,
                            'topup_code' =>  "TRANS-" . date('dmy') . $randomString,
                            'user_id' => $request->user_id,
                        ]);

                        $createHistory = EwalletHistory::create([
                            'ms_wallet_id' => $createWallet->id,
                            'order_code' => date('ymd') . $randomString,
                            'type_transaction_id' => 0,
                            'transfer_status_id' => 0,
                            'saldo' => $request->nominal,
                            'nominal' => $request->nominal,
                            'user_id' => $request->user_id,
                        ]);


                        $checkout   = EwalletHistory::with('user')->where('id', $createHistory->id)->first();
                        // return $checkout;
                        $getCode  = Ewallet::where('user_id', Auth::user()->id)->first();
                        $this->_generatePaymentToken($checkout, $getCode);
                    } else {
                        return $this->errorResponse(null, 'Minimal Top up Rp 10.000 ', 422);
                    }
                } else {
                    if ($request->nominal >= 10000) {
                        $getSaldoAwal = EwalletHistory::where('user_id', $request->user_id)->orderBy('created_at', 'desc')->first();
                        $getIdMaster = Ewallet::where('user_id', $request->user_id)->first();

                        $updateUser = Ewallet::where('user_id', $request->user_id)->update([
                            'nominal' => $request->nominal,
                            'topup_code' =>  "TRANS-" . date('dmy') . "-" . $randomString,
                            'user_id' => $request->user_id,
                        ]);

                        $createWallet = EwalletHistory::create([
                            'ms_wallet_id' => $getIdMaster->id,
                            'order_code' => date('ymd') . $randomString,
                            'type_transaction_id' => 0,
                            'transfer_status_id' => 0,
                            'saldo' => $getSaldoAwal !== null ? $getSaldoAwal->saldo + $request->nominal : $request->nominal,
                            'nominal' => $request->nominal,
                            'user_id' => $request->user_id,
                        ]);

                        $checkout   = EwalletHistory::with('user')->where('id', $createWallet->id)->first();
                        $getCode  = Ewallet::where('user_id', $request->user_id)->first();
                        return $this->_generatePaymentToken($checkout, $getCode);
                    } else {
                        return $this->errorResponse(null, 'Minimal Top up Rp 10.000 ', 422);
                    }
                }
            } else {
                return $this->errorResponse(null, 'username cannot be empty', 422);
            }
        } else {
            return $this->errorResponse(null, 'Nominal Not Null', 422);
        }
    }

    public function checkoutTopupWallet(Request $request, $id)
    {
        $checkout   = EwalletHistory::with('user')->where('id', $id)->first();
        // $this->_generatePaymentToken($checkout, $getCode);
        $checkout->update([
            'payment_type_id' => $request->payment_type_id,
            'payment_metod_id' => $request->payment_metod_id,
            'payment_metod_type' => 'Bank Transfer',
        ]);

        $ApiController = new ApiController();
        return $ApiController->successResponse($checkout, 'success', 200);
    }
    
    public function topupConfirmationDetail($id)
    {
        $resp = EwalletHistory::with(['bank', 'user'])->where('id', $id)->firstOrFail();

        $ApiController = new ApiController();
        return $ApiController->successResponse($resp, 'ok');
    }

    public function reedemVoucer(Request $request, $id)
    {
        // return $request->reedem_voucer;
        $checkout   = EwalletHistory::with('user')->where('user_id', Auth::id())->where('id', $id)->firstOrFail();
        $tanggalsekarang = date('Y-m-d');
        // return $checkout;
        $getIdVoucher = VoucherGeneral::where('code', $request->reedem_voucer)->first();

        if (!empty($getIdVoucher)) {
            if ($getIdVoucher->valid_at <= $tanggalsekarang) {
                if ($checkout->nominal >= $getIdVoucher->min_payment) {
                    if ($tanggalsekarang <= $getIdVoucher->valid_until) {
                        if ($getIdVoucher->voucher_type == 1) {
                            if ($getIdVoucher->type == 1) {
                                $ApiController = new ApiController();
                                return $ApiController->successResponse([$getIdVoucher, $checkout]);
                            } else {
                                $ApiController = new ApiController();
                                return $ApiController->successResponse([$getIdVoucher, $checkout]);
                            }
                        } else {
                            $ApiController = new ApiController();
                            return $ApiController->successResponse([$getIdVoucher, $checkout]);
                        }
                    } else {
                        $ApiController = new ApiController();
                        return $ApiController->errorResponse('error', 'This voucher Has Been Expired ');
                    }
                } else {
                    $ApiController = new ApiController();
                    return $ApiController->errorResponse('error', 'your total topup is less than Rp.' . number_format($getIdVoucher->min_payment));
                }
            } else {
                $ApiController = new ApiController();
                return $ApiController->errorResponse('error', 'This voucher cannot be used ');
            }
        } else {
            $ApiController = new ApiController();
            return $ApiController->errorResponse('error', 'Voucer not Found');
        }
    }

    public function checkoutWithMidtrans(Request $request, $id)
    {
        $checkout   = EwalletHistory::with(['user', 'voucer'])->where('id', $id)->first();
        $getCode  = Ewallet::where('user_id', Auth::user()->id)->first();
        $getIdVoucher = VoucherGeneral::where('code', $request->reedem_voucer)->first();

        try {
            // cek voucer valid atau tidak
            if (!empty($getIdVoucher)) {
                if ($getIdVoucher->type == 1) {
                    $totalDiscount = $checkout->nominal - $getIdVoucher->max_nominal;
                    $potonganPercent = $getIdVoucher->max_nominal;
                    if ($getIdVoucher->voucher_type == 1) {
                        // return 'type casback harus bayar nominal awal (percent)';
                        $checkout->update([
                            'nominal_pay_after_discount' => $checkout->nominal,
                            'number_of_pieces' => $potonganPercent
                        ]);
                    } else {
                        // return 'type potongan harus bayar setelah potongan (percent)';
                        $checkout->update([
                            'nominal_pay_after_discount' => $totalDiscount,
                            'number_of_pieces' => $potonganPercent
                        ]);
                    }
                } else {
                    $totalFix = $checkout->nominal - $getIdVoucher->value;
                    $potonganFix = $getIdVoucher->value;
                    if ($getIdVoucher->voucher_type == 1) {
                        // return 'type casback harus bayar nominal awal (fix)';
                        $checkout->update([
                            'nominal_pay_after_discount' => $checkout->nominal,
                            'number_of_pieces' => $potonganFix
                        ]);
                    } else {
                        // return 'type potongan harus bayar setelah potongan (fix)';
                        $checkout->update([
                            'nominal_pay_after_discount' => $totalFix,
                            'number_of_pieces' => $potonganFix
                        ]);
                    }
                }

                $this->initPaymentGetway();

                $customrtDetail = [
                    'first_name' => Auth::user()->name,
                    'email' => Auth::user()->email,
                    'phone_number' => Auth::user()->phone
                ];

                $params = [
                    'enable_payments' => Payment::PAYMENT_CHANNELS,
                    'transaction_details' => [
                        'order_id' => $checkout->order_code,
                        'topup_id' => $checkout->id,
                        'gross_amount' => $checkout->nominal_pay_after_discount,
                    ],
                    'customer_details' => $customrtDetail,
                    'expairy' => [
                        'start_time' => date('Y-m-d H:i'),
                        'unit' => Payment::EXPIRY_UNIT,
                        'duration' => Payment::EXPIRY_DURATION,
                    ]
                ];

                $snapToken = Snap::createTransaction($params);

                if ($snapToken->token) {
                    $updateTrans = EwalletHistory::where('id', $checkout->id)->update([
                        'payment_token' => $snapToken->token,
                        'payment_url' => $snapToken->redirect_url,
                        'voucher_id' => $getIdVoucher->id,
                        'voucher_type' => $getIdVoucher->voucher_type,
                        'qty' => 1,
                        'payment_type_id' => 2,
                        'payment_metod_id' => 'MIDTRANS',
                    ]);
                }

                $snapToken   = EwalletHistory::with(['user', 'voucer'])->where('id', $id)->first();
                $ApiController = new ApiController();
                return $ApiController->successResponse($snapToken);
            } else {
                // return 'update niasa';
                $this->_generatePaymentToken($checkout, $getCode);
                $checkout->update([
                    'payment_type_id' => 2,
                    'payment_metod_id' => 'MIDTRANS',
                ]);
                $snapToken   = EwalletHistory::with(['user', 'voucer'])->where('id', $id)->first();
                $ApiController = new ApiController();
                return $ApiController->successResponse($snapToken);
            }
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    private function _generatePaymentToken($checkout, $getCode)
    {
        // dd('asd');
        $this->initPaymentGetway();

        $customrtDetail = [
            'first_name' => Auth::user()->name,
            'email' => Auth::user()->email,
            'phone_number' => Auth::user()->phone
        ];

        $params = [
            'enable_payments' => Payment::PAYMENT_CHANNELS,
            'transaction_details' => [
                'order_id' => $checkout->order_code,
                'topup_id' => $checkout->id,
                'gross_amount' => $checkout->nominal,
            ],
            'customer_details' => $customrtDetail,
            'expairy' => [
                'start_time' => date('Y-m-d H:i'),
                'unit' => Payment::EXPIRY_UNIT,
                'duration' => Payment::EXPIRY_DURATION,
            ]
        ];

        $snapToken = Snap::createTransaction($params);

        if ($snapToken->token) {
            EwalletHistory::where('id', $checkout->id)->update([
                'payment_token' => $snapToken->token,
                'payment_url' => $snapToken->redirect_url,
            ]);
        }

        // dd($snapToken);
    }



    public function confirmationTopupWallet(ConfrimPaymentRequest $request, $id)
    {

        $user =  auth()->user();
        $path = EwalletHistory::getImagePathUpload();

        // dd($path);
        $filename = null;

        if ($request->attachment != null) {
            $image = (new ImageUpload)->upload($request->attachment, $path);
            $filename = $image->getFilename();
        }

        $confirmTopup   = EwalletHistory::where('id', $id)->first();
        $nominal = $confirmTopup->nominal;
        $id = $confirmTopup->id;
        $confirmTopup->update([
            'nama_rekening' => $request->nama_rekening,
            'attachment' => $filename,
            'updated_at' => date('Y-m-d H:i:s'),
            'payment_status' => 'processing',
            'status' => 'processing',
        ]);
        $userAdmin = User::getAdminRole();
        foreach ($userAdmin as $user) {
            $url = url('/admin/topup/detail/' . $id);
            $opsi = [
                'name' => 'new topup',
                'description' => 'new topup ' . $nominal,
                'url' => $url,
                'ewallet_history_id' => $id,
            ];
            $user->notify(new TopupNotifikasi($opsi));
        }
        
        $ApiController = new ApiController();
        return $ApiController->successResponse('success', 201);
    }

    public function approveTopupWallet(Request $request, $id)
    {
        // dd($request->ids);
        $confirmTopup   = EwalletHistory::where('id', $id)->first();
        $confirmTopup->update([
            'transfer_status_id' => 1,
            'payment_status' => 'paid',
            'status' => 'confrim'
        ]);
        // dd($confirmTopup->nominal);
        $updatEwallet = Ewallet::where('id', $confirmTopup->ms_wallet_id)->first();
        // return 
        $updatEwallet->update([
            'nominal' => $updatEwallet->nominal + $confirmTopup->nominal
        ]);

        $dataPush = [
            'type' => 'topup',
            'title' => "ASTECH TOPUP",
            'body' => "Sukses Topup sebesar Rp. " . number_format($confirmTopup->nominal, 0, '.', '.'),
        ];
        (new NotifikasiService)->firebasePushNotifikasi($confirmTopup->user, $dataPush);
    }
}
