<?php

namespace App\Http\Controllers\Api\Master;

use App\Http\Controllers\ApiController;
use App\Model\Master\StatusExcel;
use App\Services\Master\StatusExcelService as Service;
use Illuminate\Http\Request;

class StatusExcelController extends ApiController
{
    /**
     * show list with paginate
     */
    public function index(Service $service, Request $request)
    {
        $q = $request->q;
        $response = $service->list()
            ->when($q, function ($query, $q) {
                $query->where('name', 'like', '%' . $q . '%');
            })
            ->orderBy('name', 'asc')
            ->paginate(10);

        return $this->successResponse($response);
    }

    /**
     * show list with select2 structur
     */
    public function select2(Request $request, Service $service)
    {
        return $this->successResponse($service->select2($request));
    }


    /**
     * show list with datatables structur
     */
    public function datatables(Service $service)
    {
        return $service->datatables();
    }

    /**
     * insert data
     */
    public function store(Request $request, Service $service)
    {
        return $this->successResponse(
            $service->create($request->all()),
            $this->successStoreMsg(),
            201
        );
    }

    /**
     * show one data
     */
    public function show(StatusExcel $status_excel)
    {
        $response = $status_excel;

        return $this->successResponse($response);
    }

    /**
     * update data
     */
    public function update(Request $request, StatusExcel $status_excel, Service $service)
    {
        return $this->successResponse(
            $service->setStatusExcel($status_excel)->update($request->all()),
            $this->successUpdateMsg()
        );
    }

    /**
     * delete data
     */
    public function destroy(StatusExcel $status_excel, Service $service)
    {
        return $this->successResponse(
            $service->setStatusExcel($status_excel)->delete(),
            'success',
            204
        );
    }

    /**
     * delete data
     */
    public function destroyBatch(Request $request, Service $service)
    {
        return $this->successResponse(
            $service->deleteById($request->id),
            'success',
            204
        );
    }
}
