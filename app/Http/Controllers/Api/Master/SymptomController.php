<?php

namespace App\Http\Controllers\Api\Master;

use App\Helpers\ImageUpload;
use Illuminate\Http\Request;
use App\Model\Master\MsSymptom;
use App\Model\Master\MsServicesType;
use App\Http\Controllers\ApiController;
use App\Http\Requests\Master\SymptomRequest;
use App\Services\Master\SymptomService as Service;

class SymptomController extends ApiController
{
    public function index(Service $service, Request $request)
    {
        $q = $request->q;
        $ms_services_id = $request->ms_services_id;
        $paginate = $request->paginate;

        $response = $service->list()
            ->when($q, function ($query, $q) {
                $query->where('name', 'like', '%' . $q . '%');
            })
            ->when($ms_services_id, function ($query, $ms_services_id) {
                $query->where('ms_services_id', $ms_services_id);
            })
            ->orderBy('name', 'asc');
        
        if ($paginate == 'no_paginate') {
            $response = $response->get();
        }else{
            $response = $response->paginate(10);
        }
        
        return $this->successResponse($response);
    }

    public function datatables(Service $service)
    {
        return $service->datatables();
    }

    public function select2(Request $request, Service $service, $ms_services_id = null)
    {
        return $this->successResponse($service->select2($request, $ms_services_id));
    }

    public function serviceType(Request $request, Service $service, $id)
    {
        $resp  = MsServicesType::where('name', 'like', '%' . $request->q . '%')
            ->where('ms_symptoms_id', $id)
            ->limit(20)
            ->get();

        return $this->successResponse($resp);
    }

    public function store(Request $request, Service $service)
    {

        $user = auth()->user();
        $path = MsSymptom::getImagePathUpload();
        // dd($path);
        $filename = null;
        if ($request->hasFile('file')) {
            $image = (new ImageUpload)->upload($request->file('file'), $path);
            $filename = $image->getFilename();
        }
        $datas = [
            'ms_services_id' => $request->ms_services_id,
            'name'           => $request->name,
            'images'         => $filename
        ];

        return $this->successResponse(
            $service->create($datas),
            'success create',
            201
        );
    }

    public function show(Service $service, $id)
    {
        return $this->successResponse(
            MsSymptom::where('id', $id)->with('service_types.price_service')->first(),
            'success'
        );
    }

    public function showRelatable(Service $service, $id)
    {

        return $this->successResponse(
            MsSymptom::where('id', $id)->with('service_types.symptom')->first(),
            'success'
        );
    }

    public function update(Request $request, MsSymptom $Symptom, Service $service)
    {
        $user = auth()->user();
        $path = MsSymptom::getImagePathUpload();
        // dd($path);
        $filename = null;
        if ($request->hasFile('file')) {
            $image = (new ImageUpload)->upload($request->file('file'), $path);
            $filename = $image->getFilename();
        }
        $datas = [
            'ms_services_id' => $request->ms_services_id,
            'name'           => $request->name,
            'images'         => $filename
        ];
        if ($Symptom->images != null) {
            $file_path = storage_path('app/public/symptom/' . $Symptom->images);
            if (file_exists($file_path)) {
                unlink($file_path);
            };
        }
        return $this->successResponse(
            $service->setSymptoms($Symptom)->update($datas),
            'success'
        );
    }

    public function delete(MsSymptom $Symptom, Service $service)
    {
        if ($Symptom->images != null) {
            $file_path = storage_path('app/public/symptom/' . $Symptom->images);
            if (file_exists($file_path)) {
                unlink($file_path);
            };
        }
        return $this->successResponse(
            $service->setSymptoms($Symptom)->delete($Symptom->id),
            'success',
            201
        );
    }
}
