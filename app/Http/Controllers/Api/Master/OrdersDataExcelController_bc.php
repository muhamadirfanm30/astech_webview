<?php

namespace App\Http\Controllers\Api\Master;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Model\Master\StatusExcel;
use Illuminate\Support\Facades\DB;
use App\Model\Master\PartsDataExcel;
use Illuminate\Support\Facades\Auth;

use App\Model\Master\OrdersDataExcel;
use App\Http\Controllers\ApiController;
use App\Model\Master\EngineerCodeExcel;
use Illuminate\Database\QueryException;
use App\Model\Master\OrdersDataExcelLog;
use App\Model\Master\PartDataExcelStock;
use App\Services\Master\OrdersDataExcelService as Service;
use App\Services\Master\OrdersDataExcelService;

class OrdersDataExcelController extends ApiController
{

    public function datatables(Service $service)
    {
        return $service->datatables();
    }

    public function datatablesSearch(Service $service, Request $request)
    {
        return $service->datatables($request);
    }

    public function datatablesLogs($id, $type=null)
    {
        $query = OrdersDataExcelLog::with('user');
        if($id != '0') {
            $query->where('orders_data_excel_id', $id);
        }
        if(!empty($type)) {
            $query->where('type', $type);
        }
        $query->orderBy('created_at','DESC')->get();
        return DataTables::of($query)
        ->addIndexColumn()
        ->toJson();
    }

    /**
     * insert data
     */
    public function store(Request $request, Service $service)
    {


        return $service->create($request->all());

    }

    /**
     * show one data
     */
    public function show(OrdersDataExcel $marital)
    {
        $response = $marital;

        return $this->successResponse($response);
    }

    /**
     * update data
     */
    public function update(Request $request, OrdersDataExcel $orders_data_excel, Service $service)
    {
        return $this->successResponse(
            $service->setData($orders_data_excel)->update($request->all()),
            $this->successUpdateMsg()
        );
    }

    /**
     * delete data
     */
    public function destroy(OrdersDataExcel $orders_data_excel, Service $service)
    {
        return $this->successResponse(
            $service->setData($orders_data_excel)->deleteX(),
            'success',
            204
        );
    }

    public function destroyPartData(Request $request, $part_data_id)
    {
        $part = PartsDataExcel::where('id', $part_data_id)->firstOrFail();
        $part->delete();

        return $this->successResponse([
            'uniq' => time(),
        ], $this->successDeleteMsg(), 200);
    }

    public function updatePartData(Request $request, $orders_data_excel_id, Service $service)
    {
        $orders_data_excel = OrdersDataExcel::with(['part_data_excel'])
            ->where('id', $orders_data_excel_id)
            ->firstOrFail();

        $data_part_update = [];
        $data_part_insert = [];

        $part_data_id = $request->part_data_id;
        $code_material = $request->code_material;
        $part_description = $request->part_description;
        $quantity = $request->quantity;

        // loop request
        foreach ($part_data_id as $key => $value) {
            // check request
            $check_part = !empty($code_material[$key]) && !empty($part_description[$key]) && !empty($quantity[$key]);

            if ($check_part) {

                // jika id tidak 0 == update
                if ($value != 0) {
                    $data_part_update[] = [
                        'id' => $value,
                        'code_material' => $code_material[$key],
                        'part_description' => $part_description[$key],
                        'quantity' => $quantity[$key],
                    ];

                } else { // jika id 0 == insert
                    $data_part_insert[] = [
                        'orders_data_excel_id' => $orders_data_excel->id,
                        'code_material' => $code_material[$key],
                        'part_description' => $part_description[$key],
                        'quantity' => $quantity[$key],
                    ];
                }
            }
        }

        DB::beginTransaction();
        try {
            // update sparrepart
            if (count($data_part_update) > 0) {
                foreach ($data_part_update as $data) {
                    PartsDataExcel::where('id', $data['id'])->update([
                        'code_material' => $data['code_material'],
                        'part_description' => $data['part_description'],
                        'quantity' => $data['quantity'],
                    ]);
                }
            }

            // insert part_datas
            if (count($data_part_insert) > 0) {
                PartsDataExcel::insert($data_part_insert);
            }
            $service->logs($orders_data_excel, 2);
            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }
    }

    // public function updatePartData(Request $request, $orders_data_excel_id, Service $service)
    // {
    //     $orders_data_excel = OrdersDataExcel::with(['part_data_excel'])
    //         ->where('id', $orders_data_excel_id)
    //         ->firstOrFail();



    //     $data_part_update = [];
    //     $data_part_insert = [];

    //     $part_data_id = $request->part_data_id;
    //     $code_material = $request->code_material;
    //     $part_description = $request->part_description;
    //     $quantity = $request->quantity;
    //     $part_data_stock_id = $request->code_material_id;


    //     // loop request
    //     foreach ($part_data_id as $key => $value) {
    //         // check request
    //         $check_part = !empty($code_material[$key]) && !empty($part_description[$key]) && !empty($quantity[$key]);

    //         if ($check_part) {

    //             // jika id tidak 0 == update
    //             if ($value != 0) {
    //                 $data_part_update[] = [
    //                     'id' => $value,
    //                     'part_data_stock_id' => $part_data_stock_id[$key],
    //                     'code_material' => $code_material[$key],
    //                     'part_description' => $part_description[$key],
    //                     'quantity' => $quantity[$key],

    //                 ];

    //             } else { // jika id 0 == insert
    //                 $data_part_insert[] = [
    //                     'orders_data_excel_id' => $orders_data_excel->id,
    //                     'part_data_stock_id' => $part_data_stock_id[$key],
    //                     'code_material' => $code_material[$key],
    //                     'part_description' => $part_description[$key],
    //                     'quantity' => $quantity[$key],
    //                 ];
    //             }
    //         }
    //     }

    //     DB::beginTransaction();
    //     try {
    //         // update sparrepart
    //         if (count($data_part_update) > 0) {
    //             foreach ($data_part_update as $data) {
    //                 PartsDataExcel::where('id', $data['id'])->update([
    //                     'code_material' => $data['code_material'],
    //                     'part_description' => $data['part_description'],
    //                     'part_data_stock_id' => $data['part_data_stock_id'],
    //                     'quantity' => $data['quantity'],
    //                 ]);
    //                 $decrease_stock = $this->decreaseStock($data['code_material'],$data['quantity']);
    //                 if($decrease_stock['status'] == false) {
    //                     return $this->errorResponse(
    //                         $decrease_stock['stock_now'],
    //                         'Code Material = '.$data['code_material'].'. Quantity melebihi jumlah stock terupdate sekarang = '.$decrease_stock['stock_now'].'',
    //                         400
    //                     );
    //                 }
    //             }
    //         }

    //         // insert part_datas
    //         if (count($data_part_insert) > 0) {
    //             PartsDataExcel::insert($data_part_insert);
    //             foreach ($data_part_insert as $data) {
    //                 $decrease_stock = $this->decreaseStock($data['code_material'],$data['quantity']);
    //                 if($decrease_stock['status'] == false) {
    //                     return $this->errorResponse(
    //                         $decrease_stock['stock_now'],
    //                         'Code Material = '.$data['code_material'].'. Quantity melebihi jumlah stock terupdate sekarang = '.$decrease_stock['stock_now'].'',
    //                         400
    //                     );
    //                 }
    //             }
    //         }
    //         $service->logs($orders_data_excel, 2);
    //         DB::commit();
    //     } catch (QueryException $e) {
    //         DB::rollback();
    //         throw new \Exception($e->getMessage());
    //     }
    // }

    // public function decreaseStock($code_material,$quantity) {
    //     $service = new OrdersDataExcelService();
    //     return  $service->decreaseStock($code_material,$quantity);
    // }

    // public function autoCompleteField(Request $request)
    // {
    //     if ($request->namefield == 'engineer_code') {
    //         return $data = EngineerCodeExcel::where('engineer_code', 'like', '%'.strtolower($request->val).'%')->get();

    //     } else if($request->namefield == 'engineer_name') {
    //         return $data = EngineerCodeExcel::where('engineer_name', 'like', '%'.strtolower($request->val).'%')->get();

    //     } else if($request->namefield == 'status') {
    //         return $data = StatusExcel::where('status', 'like', '%'.strtolower($request->val).'%')->get();
    //     }

    // }



}
