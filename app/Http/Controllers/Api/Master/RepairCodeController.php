<?php

namespace App\Http\Controllers\Api\Master;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\Services\Master\RepairCodeService as Service;
use App\Http\Requests\Master\RepairCodeRequest;
use App\Model\Master\RepairCode;

class RepairCodeController extends ApiController
{
    public function index(Service $service, Request $request)
    {
        $q = $request->q;
        $paginate = $request->paginate;

        $response = $service->list()->when($q, function ($query, $q) {
            $query->where('name', 'like', '%' . $q . '%');
        });

        if ($paginate == 'no_paginate') {
            $response = $response->get();
        }else{
            $response = $response->paginate(10);
        }
        
        return $this->successResponse($response);
    }

    public function datatables(Service $service)
    {
        return $service->datatables();
    }

    public function select2(Request $request, Service $service)
    {
        return $this->successResponse($service->select2($request));
    }

    public function store(RepairCodeRequest $request, Service $service)
    {
        return $this->successResponse(
            $service->create($request->all()),
            'success',
            201
        );
    }

    public function update(RepairCodeRequest $request, RepairCode $repairCode, Service $service)
    {
        return $this->successResponse(
            $service->setRepairCode($repairCode)->update($request->all()),
            'success'
        );
    }

    public function delete(RepairCode $repairCode, Service $service)
    {
        return $this->successResponse(
            $service->setRepairCode($repairCode)->delete($repairCode->id),
            'success',
            201
        );
    }
}
