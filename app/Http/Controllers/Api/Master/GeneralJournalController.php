<?php

namespace App\Http\Controllers\Api\Master;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Model\Master\HppItem;
use Yajra\DataTables\DataTables;

use Illuminate\Support\Facades\DB;
use App\Model\Master\GeneralJournal;
use App\Model\Master\GeneralSetting;
use App\Model\Master\PartsDataExcel;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\ApiController;
use App\Model\Master\GeneralJournalLog;
use Illuminate\Database\QueryException;
use App\Model\Master\PartDataExcelStock;
use App\Model\Master\GeneralJournalDetail;
use Illuminate\Database\Eloquent\Collection;
use App\Model\Master\PartDataExcelStockInventory;
use App\Model\Master\PDESHistory;
use App\Services\Master\PartDataExcelStockService;
use App\Model\Master\PDSInventoryMutationHistories;
use App\Services\Master\GeneralJournalService as Service;
use App\Services\Master\PartDataExcelStockInventoryService;

class GeneralJournalController extends ApiController
{

    public function datatables(Service $service)
    {
        return $service->datatables();
    }

    public function datatablesSearch(Service $service, Request $request)
    {
        return $service->datatables($request);
    }

    public function datatablesLogs($id, $type=null)
    {
        $query = GeneralJournalLog::with('user');
        if($id != '0') {
            $query->where('general_journal_id', $id);
        }
        if(!empty($type)) {
            $query->where('type', $type);
        }
        $query->orderBy('created_at','DESC')->get();
        return DataTables::of($query)
        ->addIndexColumn()
        ->toJson();
    }

    /**
     * insert data
     */
    public function store(Request $request, Service $service)
    {
        return $this->successResponse(
            $service->create($request),
            $this->successStoreMsg(),
            201
        );
    }

    /**
     * show one data
     */
    public function show(GeneralJournal $general_journal)
    {
        $response = $general_journal;

        return $this->successResponse($response);
    }

    /**
     * update data
     */
    public function update(Request $request, GeneralJournal $general_journal, Service $service)
    {
        // return $request->all();
        if ($general_journal->image != null) {
            $file_path = storage_path('app/public/general_journal/' . $general_journal->image);
            if (file_exists($file_path)) {
                unlink($file_path);
            }
        }
        return $this->successResponse(
            $service->setData($general_journal)->updateX($request),
            $this->successUpdateMsg()
        );
    }

    /**
     * delete data
     */
    public function destroy(GeneralJournal $general_journal, Service $service)
    {
        $check_gjd_part = GeneralJournalDetail::where('general_journal_id', $general_journal->id)->get();
        foreach($check_gjd_part as $data) {
            if($data->part_data_stock_inventories_id != null) {
                return $this->errorResponse(
                    $data->part_data_stock_inventories_id,
                    'Tidak bisa di hapus, Karena terdapat detail yang memiliki part !',
                    400
                );
            }
        }
        return $this->successResponse(
            $service->setData($general_journal)->deleteX(),
            $this->successDeleteMsg(),
            204
        );
    }

    public function destroyDetailsData($gjd_id, Request $request)
    {
        $data = GeneralJournalDetail::where('id', $gjd_id)->firstOrFail();
        $find_data_excel = PartsDataExcel::where('part_data_stock_inventories_id', $data->part_data_stock_inventories_id)->count();
        if($find_data_excel > 0) {
            return $this->errorResponse(
                $find_data_excel,
                'Tidak bisa di delete, karena terdapat inventory stock Out yang digunakan!',
                400
            );
        }

        DB::beginTransaction();
        try {
            if($data->part_data_stock_inventories_id != null) {
                $update_log_gjd_deleted = PDESHistory::where('general_journal_details_id', $gjd_id)->update(['is_deleted' => 1]);
                $pdesi_service = new PartDataExcelStockInventoryService();
                // return $pde;
                $data_history_inventory = [
                    'date' => $data->date,
                    'pdesi_id_out' => $data->part_data_stock_inventories_id,
                    'pdesi_id_in' => null,
                    'amount' => $data->quantity,
                    'type_amount' =>  0,
                    'type_mutation' =>  2,
                    'general_journal_details_id' => $gjd_id,
                    'part_data_excel_id' => null
                ];
                $create_log = $pdesi_service->historyLogProcess($data_history_inventory);
                // $pdes_service->historyLogProcess($data->part_data_stock_id, null, $data->quantity, 0, 6, $general_journal_id, null);
                $pdesi_service->saveReportHpp($data->quantity, 0, 2, $data->part_data_stock_inventories_id, $data->id);

                $inv_stock = PartDataExcelStockInventory::where('id', $data->part_data_stock_inventories_id)->value('stock');
                $restore_stock = PartDataExcelStockInventory::where('id', $data->part_data_stock_inventories_id)->update(['stock' => ($inv_stock - $data->quantity)]);
            }
            if ($data->filename != null) {
                $file_path = storage_path('app/public/general-journal-detail/' . $data->filename);
                if (file_exists($file_path)) {
                    unlink($file_path);
                }
            }
            DB::commit();
            $data->delete();
            return $this->successResponse($data, $this->successDeleteMsg(), 204);
        } catch (QueryException $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }
    }

    public function hppItemDatatables(Request $request)
    {
        $month = date('n');
        $year = date('Y');
        if(!empty($request->month)) {
            $month = $request->month;
        }
        if(!empty($request->year)) {
            $year = $request->year;
        }

        $query = GeneralJournalDetail::select([
            DB::raw('(CEIL(SUM(general_journal_details.kredit) / SUM(general_journal_details.quantity))) AS hpp_average_month'),
            'ode_part_data_stock.part_description as part_description',
            'part_data_stock_id',
            ])
            ->with(['hpp_item' => function ($query) use($month, $year) {
                $query->where([
                    ['type', '=', 1], //type 1 mean Month
                    ['month', '=', $month],
                    ['year', '=', $year],
                ]);
            }])
            ->join('ode_part_data_stock', 'ode_part_data_stock.id', '=', 'general_journal_details.part_data_stock_id')
            ->where('category', 'material')
            ->whereMonth('general_journal_details.created_at', $month)
            ->whereYear('general_journal_details.created_at', $year)
            ->groupBy('part_data_stock_id')->whereNotNull('part_data_stock_id');
        $query = $query->get();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function hppItemDatatablesDetails($pdes_id,Request $request)
    {
        $month = date('n');
        $year = date('Y');
        if(!empty($request->month)) {
            $month = $request->month;
        }
        if(!empty($request->year)) {
            $year = $request->year;
        }

        $query = GeneralJournalDetail::select([
            DB::raw('( CEIL(general_journal_details.kredit / general_journal_details.quantity) ) AS hpp_average'),
            'ode_part_data_stock.part_description as part_description',
            'date',
            'part_data_stock_id',
            'quantity',
            'kredit',
            'margin_rate_manual',
            'suggest_to_sell_manual',
            'general_journal_details.id as gjd_id'
            ])
            ->join('ode_part_data_stock', 'ode_part_data_stock.id', '=', 'general_journal_details.part_data_stock_id')
            ->where('part_data_stock_id', $pdes_id)
            ->where('category', 'material')
            ->whereMonth('general_journal_details.created_at', $month)
            ->whereYear('general_journal_details.created_at', $year)
            ->whereNotNull('part_data_stock_id');
        $query = $query->get();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function hppItemDatatablesDetailsYearly(Request $request)
    {
        $data = GeneralJournalDetail::select([
                DB::raw('(MIN(YEAR(general_journal_details.created_at))) AS min_year'),
                DB::raw('(MAX(YEAR(general_journal_details.created_at))) AS max_year')
            ])->first();
        $min_month = 1;
        $max_month = 12;
        $min_year = $data->min_year;
        $max_year = $data->max_year;
        if(!empty($request->month)) {
            $min_month = $request->month;
            $max_month = $request->month;
        }
        if(!empty($request->year)) {
            $min_year = $request->year;
            $max_year = $request->year;
        }
        $collection = new Collection();
        foreach (range($min_year, $max_year) as $year) {
            foreach (range($min_month,$max_month) as $month) {
                $data = GeneralJournalDetail::select([
                    DB::raw('(CEIL(SUM(general_journal_details.kredit) / SUM(general_journal_details.quantity))) AS hpp_average_month'),
                    DB::raw('SUM(general_journal_details.quantity) AS quantity'),
                    DB::raw('SUM(general_journal_details.kredit) AS price'),
                    'general_journal_details.part_data_stock_id as part_data_stock_id',
                    'ode_part_data_stock.part_description as part_description'
                    ])
                    ->with(['hpp_item' => function ($query) use($month, $year) {
                        $query->where([
                            ['type', '=', 1], //type 1 mean Month
                            ['month', '=', $month],
                            ['year', '=', $year],
                        ]);
                    }])
                    ->join('ode_part_data_stock', 'ode_part_data_stock.id', '=', 'general_journal_details.part_data_stock_id')
                    ->where('part_data_stock_id', $request->pdes_id)
                    ->where('category', 'material')
                    ->whereMonth('general_journal_details.created_at', $month)
                    ->whereYear('general_journal_details.created_at', $year)
                    ->groupBy('part_data_stock_id')
                    ->whereNotNull('part_data_stock_id')
                    ->first();
                if($data){
                    $collection->push([
                        'part_data_stock_id' => $data->part_data_stock_id,
                        'part_description' => $data->part_description,
                        'quantity' => $data->quantity,
                        'price' => $data->price,
                        'hpp_average_month' => $data->hpp_average_month,
                        'month' => date("F", mktime(0, 0, 0, $month, 1)),
                        'month_number' => $month,
                        'year' => $year,
                        'margin_rate_auto' => $data->margin_rate_auto,
                        'margin_rate_manual' => ($data->hpp_item != null ? $data->hpp_item->margin_rate_manual : null),
                        'suggest_to_sell_manual' => ($data->hpp_item != null ? $data->hpp_item->suggest_to_sell_manual : null)
                    ]);
                }
            }
        }

        // Datatables::of($results)->make(true)
        return DataTables::of($collection)
            ->addIndexColumn()
            ->toJson();
    }

    public function updateMarginRateAuto(Request $request) {
        return GeneralSetting::where('name', 'margin_rate_auto')->update(['value' => $request->margin_rate_auto]);
    }

    public function updateHppAverageManualMonth(Request $request) {
        $whereData = [
            ['pdes_id', '=', $request->pdes_id],
            ['month', '=', $request->month],
            ['year', '=', $request->year],
            ['type', '=', 1]
        ];
        $cek = HppItem::where($whereData)->whereNotNull('hpp_average_month_manual');
        if($cek->count() > 0) {
            return HppItem::where([
                ['pdes_id', '=', $request->pdes_id],
                ['month', '=', $request->month],
                ['year', '=', $request->year],
                ['type', '=', 1]
            ])->update(['hpp_average_month_manual' => $request->hpp_average_month_manual]);
        } else {
            return HppItem::create([
                'pdes_id' => $request->pdes_id,
                'type' => 1,
                'month' => $request->month,
                'year' => $request->year,
                'hpp_average_month_manual' => $request->hpp_average_month_manual
            ]);
        }
    }

    public function updateHppAverageManualAll(Request $request) {
        $whereData = [
            ['pdes_id', '=', $request->pdes_id],
            ['type', '=', 2]
        ];
        $cek = HppItem::where($whereData)->whereNotNull('hpp_average_all_manual');
        if($cek->count() > 0) {
            return HppItem::where([
                    ['pdes_id', '=', $request->pdes_id],
                    ['type', '=', 2] //year
                ])
                ->whereNotNull('hpp_average_all_manual')
                ->update(['hpp_average_all_manual' => $request->hpp_average_all_manual]);
        } else {
            return HppItem::create([
                'pdes_id' => $request->pdes_id,
                'type' => 2, //year
                'hpp_average_all_manual' => $request->hpp_average_all_manual
            ]);
        }
    }



    public function updateMarginRateManual(Request $request) {
        $whereData = [
            ['pdes_id', '=', $request->pdes_id],
            ['month', '=', $request->month],
            ['year', '=', $request->year],
            ['type', '=', 1]
        ];
        $cek_update = HppItem::where($whereData)->count();
        $data = null;
        if($cek_update) {
            $data = HppItem::where($whereData)->update(['margin_rate_manual' => $request->margin_rate_manual]);
        } else {
            $data = HppItem::create([
                'pdes_id' => $request->pdes_id,
                'margin_rate_manual' => $request->margin_rate_manual,
                'pdes_id' => $request->pdes_id,
                'month' => $request->month,
                'year' => $request->year,
                'type' => 1
            ]);
        }
        return $this->successResponse(
            $data,
            $this->successUpdateMsg(),
            200
        );
    }
    public function updateSuggestToSellManual(Request $request) {
        $whereData = [
            ['pdes_id', '=', $request->pdes_id],
            ['month', '=', $request->month],
            ['year', '=', $request->year],
            ['type', '=', 1],
        ];
        $cek_update = HppItem::where($whereData)->count();

        $data = null;
        if($cek_update) {
            $data = HppItem::where($whereData)
                    ->update([
                        'suggest_to_sell_manual' => $request->suggest_to_sell_manual
                    ]);
        } else {
            $data = HppItem::create([
                'pdes_id' => $request->pdes_id,
                'suggest_to_sell_manual' => $request->suggest_to_sell_manual,
                'pdes_id' => $request->pdes_id,
                'month' => $request->month,
                'year' => $request->year,
                'type' => 1
            ]);
        }
        return $this->successResponse(
            $data,
            $this->successUpdateMsg(),
            200
        );
    }

    public function updateMarginRateManualDetail(Request $request) {
        $cek = GeneralJournalDetail::where('id', $request->gjd_id);
        $data = null;
        if($cek->count() > 0) {
            $data = GeneralJournalDetail::where('id', $request->gjd_id)->update(['margin_rate_manual' => $request->margin_rate_manual]);
        } else {
            return $this->errorResponse(
                null,
                'general_journal_detail_id tidak ditemukan',
                500
            );
        }
        return $this->successResponse(
            $data,
            $this->successUpdateMsg(),
            200
        );
    }
    public function updateSuggestToSellManualDetail(Request $request) {
        $cek = GeneralJournalDetail::where('id', $request->gjd_id);
        $data = null;
        if($cek->count() > 0) {
            $data = GeneralJournalDetail::where('id', $request->gjd_id)->update(['suggest_to_sell_manual' => $request->suggest_to_sell_manual]);
        } else {
            return $this->errorResponse(
                null,
                'general_journal_detail_id tidak ditemukan',
                500
            );
        }
        return $this->successResponse(
            $data,
            $this->successUpdateMsg(),
            200
        );
    }

    public function getHppAverageAll($pdesi_id, $out_stock, Service $service) {
        return $this->successResponse(
            $service->getHppAverageAll($pdesi_id, $out_stock),
            'get hpp_average_all success',
            200
        );

    }

}
