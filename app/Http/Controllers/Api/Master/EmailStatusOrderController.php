<?php

namespace App\Http\Controllers\Api\Master;


use Illuminate\Http\Request;
use App\Model\Master\EmailStatusOrder;
use App\Http\Controllers\ApiController;
use App\Http\Requests\Master\EmailStatusOrderRequest;
use App\Services\Master\EmailStatusOrderService as Service;

class EmailStatusOrderController extends ApiController
{
    /**
     * show list with paginate
     */
    public function index(Service $service)
    {
        $response = $service->list()->paginate(10);

        return $this->successResponse($response);
    }

    /**
     * show list with select2 structur
     */
    public function select2(Request $request, Service $service)
    {
        return $this->successResponse($service->select2($request));
    }

    /**
     * show list with datatables structur
     */
    public function datatables(Service $service)
    {
        return $service->datatables();
    }

    /**
     * update data
     */
    public function update(EmailStatusOrderRequest $request)
    {
        $dataInfo = [];
        foreach(json_decode($request->id_email) as $key => $val) {
            $dataInfo = [
                'title' => $request->{'title'.$key} ,
                'content' => $request->{'content'.$key},
                'orders_statuses_id' => $request->{'orders_statuses_id'.$key},
            ];
            EmailStatusOrder::where('id', $val)->update($dataInfo);
        }

        return $this->successResponse('Success Update !');
    }


}
