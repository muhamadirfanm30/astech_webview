<?php

namespace App\Http\Controllers\Api\Master;

use App\User;
use App\Model\Master\VoucherGeneral;
use App\Http\Controllers\ApiController;
use App\Http\Requests\Master\VoucherGeneralRequest;
use App\Services\Master\VoucherGeneralService as Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;

class VoucherController extends ApiController
{
    /**
     * show list with paginate
     */
    public function index(Service $service)
    {
        $response = $service->list()->paginate(10);
        return $this->successResponse($response);
    }

    public function listForCustomer(Service $service)
    {
        $response = $service->list()->orderBy('created_at', 'asc')->paginate(10);
        return $this->successResponse($response);
    }

    /**
     * show list with datatables structur
     */
    public function datatables(Service $service)
    {
        $query = $service->list();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function store(VoucherGeneralRequest $request, Service $service)
    {
        // validasi
        $request->validated();

        return $this->successResponse(
            $service->create($request->all()),
            'success',
            201
        );
    }

    public function update(VoucherGeneralRequest $request, VoucherGeneral $voucherGeneral, Service $service)
    {
        // validasi
        $request->validated();

        return $this->successResponse(
            $service->setGeneralVoucher($voucherGeneral)->update($request->all()),
            'success'
        );
    }

    public function destroy(VoucherGeneral $voucherGeneral, Service $service)
    {
        return $this->successResponse(
            $service->setGeneralVoucher($voucherGeneral)->delete(),
            'success',
            204
        );
    }

    public function createVoucherForNewUsers(VoucherGeneralRequest $request)
    {
        
        $showData = VoucherGeneral::where('is_new_register', 1)->first();
        
        if($showData == null){

            $length = 6;
            $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }

            VoucherGeneral::create([
                'code' => 'VCR'. date('ymd') . $randomString,
                'type' => $request->type,
                'value' => $request->value,
                'max_nominal' => $request->max_nominal,
                'min_payment' => $request->min_payment,
                'valid_at' => $request->valid_at,
                'valid_until' => $request->valid_until,
                'desc' => $request->desc,
                'is_new_register' => 1,
                'is_active' => $request->status,
            ]);

        }else{
            VoucherGeneral::where('is_new_register', 1)->update([
                'type' => $request->type,
                'value' => $request->value,
                'max_nominal' => $request->max_nominal,
                'min_payment' => $request->min_payment,
                'valid_at' => $request->valid_at,
                'valid_until' => $request->valid_until,
                'desc' => $request->desc,
                'is_new_register' => 1,
                'is_active' => $request->status,
            ]);
        }
    }
}
