<?php

namespace App\Http\Controllers\Api\Master;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Master\OrderStatusRequest;
use App\Model\Master\OrdersStatus;
use App\Services\Master\OrderStatusService as Service;
use Illuminate\Http\Request;

class OrderStatusController extends ApiController
{
    /**
     * show list with paginate
     */
    public function index(Service $service)
    {
        $response = $service->list()->paginate(10);

        return $this->successResponse($response);
    }

    /**
     * show list with select2 structur
     */
    public function select2(Request $request, Service $service)
    {
        return $this->successResponse($service->select2($request));
    }

    /**
     * show list with datatables structur
     */
    public function datatables(Service $service)
    {
        return $service->datatables();
    }

    /**
     * insert data
     */
    public function store(OrderStatusRequest $request, Service $service)
    {
        return $this->successResponse(
            $service->create($request->all()),
            'success',
            201
        );
    }

    /**
     * show one data
     */
    public function show(OrdersStatus $status)
    {
        $response = $status;

        return $this->successResponse($response);
    }

    /**
     * update data
     */
    public function update(OrderStatusRequest $request, OrdersStatus $status, Service $service)
    {
        return $this->successResponse(
            $service->setStatus($status)->update($request->all()),
            'success'
        );
    }

    /**
     * delete data
     */
    public function destroy(OrdersStatus $status, Service $service)
    {
        return $this->successResponse(
            $service->setStatus($status)->delete(),
            'success',
            204
        );
    }
}
