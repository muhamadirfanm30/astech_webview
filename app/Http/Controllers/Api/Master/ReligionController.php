<?php

namespace App\Http\Controllers\Api\Master;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Master\ReligionRequest;
use App\Model\Master\Religion;
use App\Services\Master\ReligionService as Service;
use Illuminate\Http\Request;

class ReligionController extends ApiController
{
    /**
     * show list with paginate
     */
    public function index(Service $service, Request $request)
    {
        $q = $request->q;
        $paginate = $request->paginate;

        $response = $service->list()
            ->when($q, function ($query, $q) {
                $query->where('name', 'like', '%' . $q . '%');
            })
            ->orderBy('name', 'asc');

            if ($paginate == 'no_paginate') {
                $response = $response->get();
            }else{
                $response = $response->paginate(10);
            }

        return $this->successResponse($response);
    }

    /**
     * show list with select2 structur
     */
    public function select2(Request $request, Service $service)
    {
        return $this->successResponse($service->select2($request));
    }

    /**
     * show list with datatables structur
     */
    public function datatables(Service $service)
    {
        return $service->datatables();
    }

    /**
     * insert data
     */
    public function store(ReligionRequest $request, Service $service)
    {
        return $this->successResponse(
            $service->create($request->all()),
            $this->successStoreMsg(),
            201
        );
    }

    /**
     * show one data
     */
    public function show(Religion $religion)
    {
        $response = $religion;

        return $this->successResponse($response);
    }

    /**
     * update data
     */
    public function update(ReligionRequest $request, Religion $religion, Service $service)
    {
        return $this->successResponse(
            $service->setReligion($religion)->update($request->all()),
            $this->successUpdateMsg()
        );
    }

    /**
     * delete data
     */
    public function destroy(Religion $religion, Service $service)
    {
        return $this->successResponse(
            $service->setReligion($religion)->delete(),
            'success',
            204
        );
    }

    /**
     * delete data
     */
    public function destroyBatch(Request $request, Service $service)
    {
        return $this->successResponse(
            $service->deleteById($request->id),
            'success',
            204
        );
    }
}
