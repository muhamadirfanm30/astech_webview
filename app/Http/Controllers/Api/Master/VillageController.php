<?php

namespace App\Http\Controllers\Api\Master;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Master\VillageRequest;
use App\Model\Master\MsVillage;
use App\Services\Master\VillageService;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Traits\MasterImportTrait;

class VillageController extends ApiController
{
    /**
     * show list with paginate
     */
    use MasterImportTrait;
    public function index(VillageService $villageService)
    {
        $response = $villageService->list()->paginate(10);

        return $this->successResponse($response);
    }

    /**
     * show list with select2 structur
     */
    public function select2(Request $request, VillageService $villageService)
    {
        $response = $villageService->list()->where('name', 'like', '%' . $request->q . '%')->limit(10)->get();

        return $this->successResponse($response);
    }

    /**
     * show list with datatables structur
     */
    public function datatables(VillageService $villageService)
    {
        $query = $villageService->list()->with('district.city');

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * insert data
     */
    public function store(VillageRequest $request, VillageService $villageService)
    {
        // validasi
        $request->validated();

        return $this->successResponse(
            $villageService->create($request->all()),
            'success',
            201
        );
    }

    /**
     * show one data
     */
    public function show(MsVillage $village)
    {
        $response = $village;

        return $this->successResponse($response);
    }

    /**
     * update data
     */
    public function update(VillageRequest $request, MsVillage $village, VillageService $villageService)
    {
        // validasi
        $request->validated();

        return $this->successResponse(
            $villageService->setVillage($village)->update($request->all()),
            'success'
        );
    }

    /**
     * delete data
     */
    public function destroy(MsVillage $village, VillageService $villageService)
    {
        return $this->successResponse(
            $villageService->setVillage($village)->delete(),
            'success',
            204
        );
    }

    public function importExcel(Request $request)
	{
        return $this->importExcelTraits('village', $request); // country is type of controller name
    }

    public function destroyBatch(Request $request, VillageService $villageService)
    {
        return $this->successResponse(
            $villageService->deleteById($request->id),
            'success',
            204
        );
    }

}
