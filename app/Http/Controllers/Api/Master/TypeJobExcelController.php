<?php

namespace App\Http\Controllers\Api\Master;

use App\Http\Controllers\ApiController;
use App\Model\Master\TypeJobExcel;
use App\Services\Master\TypeJobExcelService as Service;
use Illuminate\Http\Request;

class TypeJobExcelController extends ApiController
{
    /**
     * show list with paginate
     */
    public function index(Service $service, Request $request)
    {
        $q = $request->q;
        $response = $service->list()
            ->when($q, function ($query, $q) {
                $query->where('name', 'like', '%' . $q . '%');
            })
            ->orderBy('name', 'asc')
            ->paginate(10);

        return $this->successResponse($response);
    }

    /**
     * show list with select2 structur
     */
    public function select2(Request $request, Service $service)
    {
        return $this->successResponse($service->select2($request));
    }


    /**
     * show list with datatables structur
     */
    public function datatables(Service $service)
    {
        return $service->datatables();
    }

    /**
     * insert data
     */
    public function store(Request $request, Service $service)
    {
        return $this->successResponse(
            $service->create($request->all()),
            $this->successStoreMsg(),
            201
        );
    }

    /**
     * show one data
     */
    public function show(TypeJobExcel $type_job_excel)
    {
        $response = $type_job_excel;

        return $this->successResponse($response);
    }

    /**
     * update data
     */
    public function update(Request $request, TypeJobExcel $type_job_excel, Service $service)
    {
        return $this->successResponse(
            $service->setTypeJobExcel($type_job_excel)->update($request->all()),
            $this->successUpdateMsg()
        );
    }

    /**
     * delete data
     */
    public function destroy(TypeJobExcel $type_job_excel, Service $service)
    {
        return $this->successResponse(
            $service->setTypeJobExcel($type_job_excel)->delete(),
            'success',
            204
        );
    }

    /**
     * delete data
     */
    public function destroyBatch(Request $request, Service $service)
    {
        return $this->successResponse(
            $service->deleteById($request->id),
            'success',
            204
        );
    }
}
