<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Http\Requests\RoleRequest;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Yajra\DataTables\DataTables;

class RoleController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->successResponse(Role::with('permissions')->get());
    }

    public function select2(Request $request)
    {
        $response = Role::where('name', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();

        return $this->successResponse($response);
    }

    /**
     * use datatables json structure
     *
     * @return \Illuminate\Http\Response
     */
    public function datatables()
    {
        return DataTables::of(Role::query())
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoleRequest $request)
    {
        // validasi
        $request->validated();

        // get all permission
        $permissions = Permission::whereIn('id', $request->permissions)->get();

        DB::beginTransaction();
        try {
            // create role
            $role = Role::create([
                'name' => $request->name,
            ]);

            // sync Permissions
            $role->syncPermissions($permissions);

            // commit db
            DB::commit();

            return $this->successResponse(
                $role,
                'success',
                201
            );
        } catch (QueryException $ex) {
            // rollback db
            DB::rollback();

            return $this->errorResponse(
                $ex,
                'gagal insert'
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->successResponse(Role::where(['id' => $id])->first());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RoleRequest $request, $id)
    {
        // validasi
        $request->validated();

        // get data
        $role = Role::findOrFail($id);

        // get all permission
        $permissions = Permission::whereIn('id', $request->permissions)->get();

        DB::beginTransaction();
        try {
            // create role
            $role->update([
                'name' => $request->name
            ]);

            // sync Permissions
            $role->syncPermissions($permissions);

            // commit db
            DB::commit();

            return $this->successResponse($role, 'success update');
        } catch (\Exception $ex) {
            // rollback db
            DB::rollback();

            return $this->errorResponse($request, 'gagal update');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->successResponse(
            Role::where(['id' => $id])->first()->delete(),
            'success delete',
            204
        );
    }
}
