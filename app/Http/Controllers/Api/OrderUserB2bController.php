<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Models\RequestOrders;
use App\Models\RequestOrderDetail;
use Auth;
use Illuminate\Support\Facades\DB;
use App\User;
use Carbon\Carbon;
use App\Models\ImageBeritaAcara;
use App\Http\Requests\EditProfileRequest;
use App\Models\ImageInvoice;
use App\Models\B2BDataCostEstimations;
use App\Models\B2BDataTransactions;
use App\Models\B2BAssignTeknisi;
use App\Models\ImagePurchaseOrder;
use App\Models\RequestOrderImage;
use App\Models\ImageQuotation;
use App\Models\BusinessToBusinessOutletDetailTransaction;
use App\Models\B2BLabor;
use App\Models\BusinessToBusinessTransaction;
use App\Models\BusinessToBusinessOutletTransaction;
use App\Helpers\ImageUpload;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class OrderUserB2bController extends ApiController
{

    public function showBeritaAcara($type, $id)
    {
        $berita = ImageBeritaAcara::where('type', 'file_berita_acara')->where('ms_b2b_detail', $id)->get();
        return $this->successResponse($berita, $this->successUpdateMsg());
    }

    public function deleteBeritaAcara($id)
    {
        $getBeritaAcara = ImageBeritaAcara::where('id', $id)->first();
        $exists = Storage::disk('local')->has('public/btb/media/' . $getBeritaAcara->image_berita_acara);
        if ($exists) {
            Storage::delete('public/btb/media/' . $getBeritaAcara->image_berita_acara);
        }

        $berita = ImageBeritaAcara::where('id', $id)->delete();
        return $this->successResponse($berita, $this->successUpdateMsg());
    }

    public function showQuotation($type, $id)
    {
        $quot = ImageQuotation::where('type', 'image_quot')->where('ms_b2b_detail', $id)->get();
        return $this->successResponse($quot, $this->successUpdateMsg());
    }

    public function deleteQuotation($id)
    {
        $quot = ImageQuotation::where('id', $id)->first();
        $exists = Storage::disk('local')->has('public/btb/media/' . $quot->image_quot);
        if ($exists) {
            Storage::delete('public/btb/media/' . $quot->image_quot);
        }

        $berita = ImageQuotation::where('id', $id)->delete();
        return $this->successResponse($berita, $this->successUpdateMsg());
    }

    public function showInvoice($type, $id)
    {
        $quot = ImageInvoice::where('type', 'image_invoice')->where('ms_b2b_detail', $id)->get();
        return $this->successResponse($quot, $this->successUpdateMsg());
    }

    public function deleteInvoice($id)
    {
        $getInv = ImageInvoice::where('id', $id)->first();
        $exists = Storage::disk('local')->has('public/btb/media/' . $getInv->image_quot);
        if ($exists) {
            Storage::delete('public/btb/media/' . $getInv->image_quot);
        }

        $berita = ImageInvoice::where('id', $id)->delete();
        return $this->successResponse($berita, $this->successUpdateMsg());
    }

    public function showPo($type, $id)
    {
        $quot = ImagePurchaseOrder::where('type', 'image_po')->where('ms_b2b_detail', $id)->get();
        return $this->successResponse($quot, $this->successUpdateMsg());
    }

    public function showImageOrder($code)
    {
        $orderImg = RequestOrderDetail::where('id', $code)->get();
        return $this->successResponse($orderImg, $this->successUpdateMsg());
    }

    public function deletePo($id)
    {
        $getPo = ImagePurchaseOrder::where('id', $id)->first();
        $exists = Storage::disk('local')->has('public/btb/media/' . $getPo->image_po);
        if ($exists) {
            Storage::delete('public/btb/media/' . $getPo->image_po);
        }

        $berita = ImagePurchaseOrder::where('id', $id)->delete();
        return $this->successResponse($berita, $this->successUpdateMsg());
    }

    public function datatables()
    {
        // $user = RequestOrders::with(['business_to_business_outlet_transaction.business_to_business_transaction.company', 'b2b_detail_trans'])->orderBy('id', 'desc');
        $user = RequestOrders::with(['get_history_order.get_outlet', 'get_history_order.get_schedule', 'get_history_order.get_b2b_detail', 'get_user.get_comp'])->orderBy('created_at', 'desc');

        return DataTables::of($user)
            ->addIndexColumn()
            ->toJson();
    }



    public function previewProfile()
    {
        if(!empty(Auth::user()->id)){
            $viewTeknisi = User::where('is_b2b_teknisi', 1)->where('id', Auth::user()->id)->first();
            return $this->successResponse($viewTeknisi, $this->successUpdateMsg());
        }else{
            return $this->errorResponse('data', 'Teknisi Tidak Ditemukan.');
        }

    }

    public function previewUserB2b()
    {
        if(!empty(Auth::user()->id)){
            $viewUser = User::where('is_b2b_users', 1)->where('id', Auth::user()->id)->first();
            return $this->successResponse($viewUser, $this->successUpdateMsg());
        }else{
            return $this->errorResponse('data', 'Teknisi Tidak Ditemukan.');
        }
    }

    public function editProfileTeknisi(EditProfileRequest $request)
    {
        if(!empty(Auth::user()->id)){
            $viewTeknisi = User::where('is_b2b_teknisi', 1)->where('id', Auth::user()->id)->first();
            if(!empty($request->image_teknisi)){
                if ($request->hasFile('image_teknisi')) {
                    $image      = $request->file('image_teknisi');
                    $file_name   = time() .'1'. '.' . $image->getClientOriginalExtension();
                    $img = Image::make($image);
                    $img->stream(); // <-- Key point
                    Storage::disk('local')->put('public/user/profile/avatar/' . $file_name, $img);

                    $exists1 = Storage::disk('local')->has('public/user/profile/avatar/' . $viewTeknisi->image_teknisi);
                    if ($exists1) {
                        Storage::delete('public/user/profile/avatar/' . $viewTeknisi->image_teknisi);
                    }
                }
            }else{
                $file_name = $viewTeknisi->image_teknisi;
            }
            $viewTeknisi->update([
                'id' => $request->id,
                'name' => $request->name,
                'email' => $request->email,
                'phone' => $request->phone,
                'phone1' => $request->phone1,
                'address' => $request->address,
                'image_teknisi' => $file_name,
            ]);
            return $this->successResponse($viewTeknisi, $this->successUpdateMsg());
        }else{
            return $this->errorResponse('data', 'Teknisi Tidak Ditemukan.');
        }
    }

    public function editUserB2b(EditProfileRequest $request)
    {
        if(!empty(Auth::user()->id)){
            $viewTeknisi = User::where('is_b2b_users', 1)->where('id', Auth::user()->id)->first();
            if(!empty($request->image_teknisi)){
                if ($request->hasFile('image_teknisi')) {
                    $image      = $request->file('image_teknisi');
                    $file_name   = time() .'1'. '.' . $image->getClientOriginalExtension();
                    $img = Image::make($image);
                    $img->stream(); // <-- Key point
                    Storage::disk('local')->put('public/user/profile/avatar/' . $file_name, $img);

                    $exists1 = Storage::disk('local')->has('public/user/profile/avatar/' . $viewTeknisi->image_teknisi);
                    if ($exists1) {
                        Storage::delete('public/user/profile/avatar/' . $viewTeknisi->image_teknisi);
                    }
                }
            }else{
                $file_name = $viewTeknisi->image_teknisi;
            }

            $viewTeknisi->update([
                'id' => $request->id,
                'name' => $request->name,
                'email' => $request->email,
                'phone' => $request->phone,
                'phone1' => $request->phone1,
                'address' => $request->address,
                'image_teknisi' => $file_name,
            ]);
            return $this->successResponse($viewTeknisi, $this->successUpdateMsg());
        }else{
            return $this->errorResponse('data', 'Teknisi Tidak Ditemukan.');
        }
    }

    public function assignTeknisi(Request $request, $id)
    {
        $end_work = Carbon::parse($request->tech_schedule)->addHour($request->estimation_hours);
        $getOrder = RequestOrderDetail::where('id', $id)->with('get_b2b_detail')->first();

        if(!empty($getOrder->get_b2b_detail)){
            $storeSchedule = B2BAssignTeknisi::create([
                'order_id' => $getOrder->id,
                'b2b_technician_id' => $request->b2b_technician_id,
                'schedule_date' => $request->schedule,
                'start_work' => $request->tech_schedule,
                'end_work' => $end_work->format('Y-m-d H:i:s'),
            ]);

            $updateStatusB2b = BusinessToBusinessOutletDetailTransaction::where('id', $getOrder->get_b2b_detail->id)->update([
                'status_quotation' => 'Process',
            ]);

            return $this->successResponse($storeSchedule, $this->successUpdateMsg());
        }else{
            return $this->errorResponse('data', 'Assignt Technician Failed');
        }

    }

    public function updateScheduleTeknisi(Request $request, $id)
    {
        // return $request->all();
        $end_work = Carbon::parse($request->tech_schedule)->addHour($request->estimation_hours);
        $getOrder = RequestOrderDetail::where('id', $request->get_id_order)->with('get_b2b_detail')->first();
        // return [
        //     'end work' => $end_work,
        //     'get order' => $getOrder,
        //     'show data' => B2BAssignTeknisi::where('id', $request->id_teknisi)->first(),
        //     'id' => $id,
        //     'id teknisi' => $request->id_teknisi,
        //     'id order' => $request->get_id_order,
        // ];

        if(!empty($getOrder->get_b2b_detail)){
            $storeSchedule = B2BAssignTeknisi::where('id', $request->id_teknisi)->update([
                'order_id' => $getOrder->id,
                'b2b_technician_id' => $request->b2b_technician_id,
                'schedule_date' => $request->schedule,
                'start_work' => $request->tech_schedule,
                'end_work' => $end_work->format('Y-m-d H:i:s'),
                'is_deleted' => 0,
            ]);

            $updateStatusB2b = BusinessToBusinessOutletDetailTransaction::where('id', $getOrder->get_b2b_detail->id)->update([
                'status_quotation' => 'Process',
            ]);

            return $this->successResponse($storeSchedule, $this->successUpdateMsg());
        }else{
            return $this->errorResponse('data', 'Update Schedule Technician Failed');
        }
    }

    public function scheduleListTeknisi($id)
    {
        $list = B2BAssignTeknisi::with('get_teknisi')->where('order_id', $id)->get();
        return DataTables::of($list)
            ->addIndexColumn()
            ->toJson();
    }

    public function scheduleByStatusNew()
    {
        $id_teknisi = Auth::user()->id;
        if(!empty($id_teknisi)){
            $user_id = Auth::user()->id;
            $list = B2BAssignTeknisi::with('get_teknisi')->where('b2b_technician_id', $user_id)->whereNull('status')->get();
            if(count($list) > 0){
                return $this->successResponse($list, 'Data Berhasil Ditemukan');
            }else{
                return $this->successResponse('data', 'Data Kosong');
            }
        }else{
            return $this->errorResponse('data', 'Tidak ditemukan data Untuk Userid Ini');
        }
    }

    public function scheduleByStatusAll()
    {
        $id_teknisi = Auth::user()->id;
        if(!empty($id_teknisi)){
            $user_id = Auth::user()->id;
            $list = B2BAssignTeknisi::with('get_teknisi')->where('b2b_technician_id', $user_id)->get();
            if(count($list) > 0){
                return $this->successResponse($list, 'Data Berhasil Ditemukan');
            }else{
                return $this->successResponse('data', 'Data Kosong');
            }
        }else{
            return $this->errorResponse('data', 'Tidak ditemukan data Untuk Userid Ini');
        }
    }

    public function listTechnicianSchedule()
    {
        // dd(Auth::user()->id);
        $teknisiSchedule = B2BAssignTeknisi::with(['get_teknisi', 'get_order.get_outlet', 'get_order.get_b2b_order_detail.get_type'])
        ->where('b2b_technician_id', Auth::user()->id)
        ->orderBy('schedule_date')
        ->whereNull('checkout')
        ->get();
        return $this->successResponse($teknisiSchedule, $this->successUpdateMsg());
    }

    public function checkoutTeknisi(Request $request)
    {
        $checkout = B2BAssignTeknisi::where('id', $request->id)->first();
        // dd($checkout);
        if(!empty($checkout)){
            if(!empty($request->status)){
                if(!empty($request->remark)){
                    if(!empty($request->attachment_checkout)){
                        $filename = null;
                        if($image = $request->attachment_checkout){
                            $name = $image->getClientOriginalName();
                            $img = Image::make($image);
                            $img->stream(); // <-- Key point
                            Storage::disk('local')->put('public/teknisi_checkout_image/' . $name, $img);
                            $filename = $name;
                        }

                        $checkout->update([
                            'checkout' => date('Y-m-d H:i:s'),
                            'status' => $request->status,
                            'remark' => $request->remark,
                            'reschedule' => $request->reschedule,
                            'attachment_checkout' => $filename,

                        ]);

                        return $this->successResponse($checkout, $this->successUpdateMsg());
                    }else{
                        return $this->errorResponse(null, 'Bukti Foto Wajib di Upload.');
                    }
                }else{
                    return $this->errorResponse(null, 'Remark is Required');
                }
            }else{
                return $this->errorResponse(null, 'Status is Required');
            }
        }else{
            return $this->errorResponse(null, 'Data Tidak Ditemukan');
        }

    }

    public function checkinTeknisi(Request $request)
    {
        // dd(Auth::user()->id);
        $checkinSchedule = B2BAssignTeknisi::where('id', $request->id)->first();
        $filename = null;

        if(!empty($checkinSchedule)){

            if(!empty($request->attachment_checkin)){
                if($image = $request->attachment_checkin){
                    $name = $image->getClientOriginalName();
                    $img = Image::make($image);
                    $img->stream(); // <-- Key point
                    Storage::disk('local')->put('public/teknisi_checkin_image/' . $name, $img);
                    $filename = $name;
                }

                $checkinSchedule->update([
                    'checkin' => date('Y-m-d H:i:s'),
                    'status' => 'Tiba Ditempat',
                    'attachment_checkin' => $filename
                ]);

                return $this->successResponse($checkinSchedule, $this->successUpdateMsg());
            }else{
                return $this->errorResponse(null, 'Bukti Foto Wajib di Upload.');
            }
        }else{
            return $this->errorResponse(null, 'Data Tidak Ditemukan.');
        }


    }

    public function quotationCode()
    {
        $q          = B2BDataTransactions::count();
        $prefix_1    = 'AST';
        $prefix_2    = 'QUOT';
        $prefix_3    = 'AOS';
        $separator  = '/';
        $date       = date('dmY');
        $number     = 1; //format
        $new_number = sprintf("%04s", $number);
        $code       = $prefix_1 . $separator . $prefix_2 . $separator . $prefix_3 . $separator . ($new_number);
        $order_code   = $code;
        if ($q > 0) {
            $last     = B2BDataTransactions::orderBy('id', 'desc')->value('quotation_code');
            $last     = explode("/", $last);
            $order_code = $prefix_1 . $separator . $prefix_2 . $separator . $prefix_3 . $separator . (sprintf("%04s", $last[3] + 1));
        }
        return $order_code;
    }

    public function saveQuotationOrder(Request $request)
    {
        // return $request->id_b2b_order;
        if(!empty($request->nama_jasa) && empty($request->harga_jasa)){
            return $this->errorResponse('data', 'HARGA jasa tidak boleh di kosongkan');
        }elseif(empty($request->nama_jasa) && !empty($request->harga_jasa)){
            return  $this->errorResponse('data', 'NAMA jasa tidak boleh di kosongkan');
        }elseif(empty($request->nama_jasa) && empty($request->harga_jasa) || !empty($request->nama_jasa) && !empty($request->harga_jasa)){
            // return $request->all();
            DB::beginTransaction();
            try {
                $sparepart_detail_id = $request->sparepart_detail_id;
                $tmp_sparepart_id = $request->tmp_sparepart_id;
                $teknisi_sparepart_stok = $request->part_data_stock_id;
                $teknisi_sparepart_part_desc = $request->part_description;
                $teknisi_sparepart_qty = $request->qty;
                $teknisi_sparepart_selling_price = $request->selling_price;
                $teknisi_sparepart_satuan = $request->satuan;
                $teknisi_sparepart_total = $request->sub_total_details;
                $teknisi_sparepart_hpp = $request->hpp_average;
                $teknisi_sparepart_stoks = $request->stok;

                $editB2bDetail = BusinessToBusinessOutletDetailTransaction::where('id', $request->id_b2b_order)->first();

                $saveDataQuot = B2BDataTransactions::create([
                    'quotation_code' => $this->quotationCode(),
                    'company' => $request->company,
                    'address' => $request->address,
                    'phone' => $request->phone,
                    'validity' => $request->validity,
                    'min_order' => $request->min_order,
                    'status_process' => $request->status_process,
                    'payment' => $request->payment,
                    'deliver' => $request->deliver,
                    'product_name' => $request->product_name,
                    'type_produk' => $request->type_produk,
                    'remark' => $request->remark,
                    'id_transaction' => $request->id_transaction,
                    'unit_ke' => $request->unit_ke,
                    'nama_jasa' => $request->nama_jasa,
                    'harga_jasa' => $request->harga_jasa,
                    'revisi_ke' => 000,
                    'status' => 1,
                ]);

                if(!empty($editB2bDetail)){
                    $editB2bDetail->update([
                        'no_quotation' => $saveDataQuot->quotation_code,
                        'no_invoice' => str_replace('QUOT', 'INV', $saveDataQuot->quotation_code),
                    ]);
                }
                // return $request->all();
                foreach ($teknisi_sparepart_stok as $key => $value) {
                    B2BDataCostEstimations::create([
                        'b2b_data_quotations' => $saveDataQuot->id,
                        'part_data_stock_id' => $teknisi_sparepart_stok[$key],
                        'part_description' => $teknisi_sparepart_part_desc[$key],
                        'qty' => $teknisi_sparepart_qty[$key],
                        'selling_price' => $teknisi_sparepart_selling_price[$key],
                        'satuan' => '-',
                        'sub_total_details' => $teknisi_sparepart_qty[$key] * $teknisi_sparepart_selling_price[$key],
                        'hpp' => $teknisi_sparepart_hpp[$key],
                        'stok' => $teknisi_sparepart_stoks[$key],
                    ]);
                    // $sparepart_detail_total_insert += $teknisi_sparepart_price[$key] * $teknisi_sparepart_qty[$key];
                }

                if($request->service_price != null){
                    $saveLabor = B2BLabor::create([
                        'name' => $request->service_name,
                        'service_price' => $request->service_price,
                        'id_b2b_quot' => $saveDataQuot->id,
                    ]);
                }
                DB::commit();
            } catch (QueryException $e) {
                DB::rollback();
                throw new SendErrorMsgException($e->getMessage());
            }
        }
    }

    public function deleteShedule($id)
    {
        $getOldSchedule = B2BAssignTeknisi::where('id', $id)->update([
            'is_deleted' => 1,
        ]);
        return $this->successResponse($getOldSchedule, $this->successUpdateMsg());
    }

    public function deletePartQuot(Request $request, $id)
    {
        $delete = B2BDataCostEstimations::where('id', $id)->delete();
        return $this->successResponse($delete, $this->successUpdateMsg());
    }

    public function editAndCreateQuotationOrder(Request $request, $id)
    {
        if(!empty($request->nama_jasa) && empty($request->harga_jasa)){
            return $this->errorResponse('data', 'HARGA jasa tidak boleh di kosongkan');
        }elseif(empty($request->nama_jasa) && !empty($request->harga_jasa)){
            return  $this->errorResponse('data', 'NAMA jasa tidak boleh di kosongkan');
        }elseif(empty($request->nama_jasa) && empty($request->harga_jasa) || !empty($request->nama_jasa) && !empty($request->harga_jasa)){
            DB::beginTransaction();
            try {
                $getCodeRevisi = B2BDataTransactions::where('id_transaction', $request->id_trans)->orderBy('id', 'desc')->first();
                $sparepart_detail_id = $request->sparepart_detail_id;
                $tmp_sparepart_id = $request->tmp_sparepart_id;
                $teknisi_sparepart_stok = $request->part_data_stock_id;
                $teknisi_sparepart_part_desc = $request->part_description;
                $teknisi_sparepart_qty = $request->qty;
                $teknisi_sparepart_selling_price = $request->selling_price;
                $teknisi_sparepart_satuan = $request->satuan;
                $teknisi_sparepart_total = $request->sub_total_details;
                $teknisi_sparepart_stoks = $request->stok;
                $teknisi_sparepart_hpp = $request->hpp_average;

                // if(!empty($data->)){

                // }

                $editB2bDetail = BusinessToBusinessOutletDetailTransaction::where('id', $request->id_b2b_details)->first();

                $saveDataQuot = B2BDataTransactions::create([
                    'quotation_code' => $getCodeRevisi->quotation_code,
                    'company' => $request->company,
                    'address' => $request->address,
                    'phone' => $request->phone,
                    'validity' => $request->validity,
                    'min_order' => $request->min_order,
                    'status_process' => $request->status_process,
                    'payment' => $request->payment,
                    'deliver' => $request->deliver,
                    'product_name' => $request->product_name,
                    'type_produk' => $request->type_produk,
                    'remark' => $request->remark,
                    'id_transaction' => $request->id_trans,
                    'unit_ke' => $request->unit_ke,
                    'nama_jasa' => $request->nama_jasa,
                    'harga_jasa' => $request->harga_jasa,
                    'revisi_ke' => sprintf("%03s", $getCodeRevisi->revisi_ke + 1),
                    'status' => 1,
                ]);

                if(!empty($editB2bDetail)){
                    $editB2bDetail->update([
                        'no_quotation' => $saveDataQuot->quotation_code,
                        'no_invoice' => str_replace('QUOT', 'INV', $saveDataQuot->quotation_code),
                    ]);
                }


                // return $request->all();
                foreach ($teknisi_sparepart_part_desc as $key => $value) {
                    B2BDataCostEstimations::create([
                        'b2b_data_quotations' => $saveDataQuot->id,
                        'part_data_stock_id' => $teknisi_sparepart_stok[$key],
                        'part_description' => $teknisi_sparepart_part_desc[$key],
                        'qty' => $teknisi_sparepart_qty[$key],
                        'selling_price' => $teknisi_sparepart_selling_price[$key],
                        'satuan' => '-',
                        'sub_total_details' =>  $teknisi_sparepart_qty[$key] * $teknisi_sparepart_selling_price[$key],
                        'stok' => $teknisi_sparepart_stoks[$key],
                        'hpp' => $teknisi_sparepart_hpp[$key],
                    ]);
                    // $sparepart_detail_total_insert += $teknisi_sparepart_price[$key] * $teknisi_sparepart_qty[$key];
                }

                if($request->service_price != null){
                    $saveLabor = B2BLabor::create([
                        'name' => $request->service_name,
                        'service_price' => $request->service_price,
                        'id_b2b_quot' => $saveDataQuot->id,
                    ]);
                }
                DB::commit();
            } catch (QueryException $e) {
                DB::rollback();
                throw new SendErrorMsgException($e->getMessage());
            }
        }

    }

    public function listQuotationOrder($id)
    {
        $listQuotation = B2BDataTransactions::where('id_transaction', $id)->orderBy('created_at', 'desc');

        return DataTables::of($listQuotation)
            ->addIndexColumn()
            ->toJson();
    }



    public function datatablesTransDetail($request_code)
    {
        $user = RequestOrderDetail::where('request_code', $request_code)
        ->with([
            'get_schedule',
            'get_outlet',
            'get_b2b_detail',
            'get_user'
        ])
        ->orderBy('created_at', 'desc');

        return DataTables::of($user)
            ->addIndexColumn()
            ->toJson();
    }

    public function historyOrder()
    {
        if(!empty(Auth::user())){
            $user_id = Auth::user()->id;
        }else{
            return $this->errorResponse('User ID Tidak Ditemukan');
        }

        $data = RequestOrders::with([
            'get_history_order.get_b2b_detail.business_to_business_outlet_transaction.business_to_business_transaction.company',
            'get_history_order.get_quot',
            'get_history_order.get_schedule.get_teknisi',
            'get_history_order.get_b2b_detail.get_type',
        ])
        ->where('user_id', Auth::user()->id)
        ->orderBy('id', 'desc')
        ->get();



        // $tes = BusinessToBusinessOutletDetailTransaction::with([
        //         'business_to_business_outlet_transaction.business_to_business_transaction.company',
        //         'b2b_detail_trans.get_user'
        //     ])->whereHas('b2b_detail_trans.get_user', function($q) use ($user_id){
        //         $q->where('user_id',  $user_id);
        //     })
        //     ->get();

        return $this->successResponse($data, $this->successUpdateMsg());
    }

    public function getOutletByCompany(Request $request)
    {

        if(!empty(Auth::user())){
            $comp_id = Auth::user()->ms_company_id;
        }else{
            return $this->errorResponse('User ID Tidak Ditemukan');
        }


        $showOutlet = BusinessToBusinessOutletTransaction::with([
                        'business_to_business_transaction',
                        'business_to_business_transaction.company',
                        'business_to_business_outlet_detail_transactions.get_type',
                        // => function ($query) {
                        //     $query->whereNull('is_parent_b2b_outlet_id');
                        // },
                    ])
                    ->whereHas('business_to_business_transaction', function($q) use ($comp_id) {
                        $q->where('company_id',  $comp_id);
                    })
                    ->get();

        return $this->successResponse($showOutlet);
    }

    public function getUnitByOutlet(Request $request, $id)
    {
        if(!empty(Auth::user())){
            $comp_id = Auth::user()->ms_company_id;
        }else{
            return $this->errorResponse('User ID Tidak Ditemukan');
        }


        $outlet = BusinessToBusinessOutletDetailTransaction::with([
            'business_to_business_outlet_transaction.business_to_business_transaction.company'
        ])
        ->where('business_to_business_outlet_transaction_id', $id)
        ->whereHas('business_to_business_outlet_transaction.business_to_business_transaction', function($q) use ($comp_id) {
            $q->where('company_id',  $comp_id);
        })
        ->whereNull('is_parent_b2b_outlet_id')
        ->get();

        return $this->successResponse($outlet);
    }

    public function getUnitDetail(Request $request, $id)
    {
        if(!empty(Auth::user())){
            $comp_id = Auth::user()->ms_company_id;
        }else{
            return $this->errorResponse('User ID Tidak Ditemukan');
        }

        $outletUnitDetail = BusinessToBusinessOutletDetailTransaction::with([
            'business_to_business_outlet_transaction.business_to_business_transaction.company'
        ])
        ->where('is_parent_b2b_outlet_id', $id)
        ->whereHas('business_to_business_outlet_transaction.business_to_business_transaction', function($q) use ($comp_id) {
            $q->where('company_id',  $comp_id);
        })
        ->whereNotNull('is_parent_b2b_outlet_id')
        ->get();

        return $this->successResponse($outletUnitDetail);
    }



    public function unitWhereOutlet(Request $request, $id)
    {
        $unit = BusinessToBusinessOutletDetailTransaction::where('id', $id)->get();

        // if (!empty($showList)) {
            return $this->successResponse($unit);
        // } else {
        //     return $this->errorResponse('Data Tidak Ditemukan');
        // }

    }

    public function historyOrderSearch(Request $request)
    {
        if(!empty(Auth::user())){
            $user_id = Auth::user()->id;
        }else{
            return $this->errorResponse('User ID Tidak Ditemukan');
        }

        $company       = empty($request->company) ? 'id' : $request->company;
        $outlet       = empty($request->outlet) ? 'id' : $request->outlet;
        $limit        = empty($request->limit) ? 10 : $request->limit;

        // dd($outlet);
        $query = BusinessToBusinessOutletDetailTransaction::with([
            'business_to_business_outlet_transaction.business_to_business_transaction.company',
            'b2b_detail_trans.get_user'
            // 'business_to_business_outlet_transactions' => function ($query) use ($request) {
            //     if (!empty($request->outlet_name)) {
            //         $query->where('name', $request->outlet_name);
            //     }
            // },
            ])->whereHas('b2b_detail_trans.get_user', function($q) use ($user_id) {
                $q->where('user_id',  $user_id);
            });

        if (!empty($request->status)) {
            $query->where('status_quotation', $request->status);
        }

        if (!empty($request->company)) {
            $query->whereHas('business_to_business_outlet_transaction.business_to_business_transaction.company', function($q) use ($company) {
                $q->where('name',  $company);
            });
            // $query->whereHas('business_to_business_outlet_transaction.business_to_business_transaction.company');
        }

        if (!empty($request->outlet)) {
            $query->whereHas('business_to_business_outlet_transaction', function($q) use ($outlet) {
                $q->where('name',  $outlet);
            });
            // $query->whereHas('business_to_business_outlet_transaction.business_to_business_transaction.company');
        }

        return $this->successResponse($query->paginate($limit));
    }
}
