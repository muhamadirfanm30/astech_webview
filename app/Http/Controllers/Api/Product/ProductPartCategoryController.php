<?php

namespace App\Http\Controllers\Api\Product;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Product\ProductPartCategoryRequest;
use App\Model\Product\ProductPartCategory;
use App\Services\Product\ProductPartCategoryService as Service;
use Illuminate\Http\Request;

class ProductPartCategoryController extends ApiController
{
    /**
     * show list with paginate
     */
    public function index(Service $service)
    {
        $response = $service->list()->paginate(10);

        return $this->successResponse($response);
    }

    /**
     * show list with select2 structur
     */
    public function select2(Request $request, Service $service)
    {
        return $this->successResponse($service->select2($request));
    }

    /**
     * show list with datatables structur
     */
    public function datatables(Service $service)
    {
        return $service->datatables();
    }

    /**
     * insert data
     */
    public function store(ProductPartCategoryRequest $request, Service $service)
    {
        return $this->successResponse(
            $service->create($request->all()),
            $this->successStoreMsg(),
            201
        );
    }

    /**
     * show one data
     */
    public function show(ProductPartCategory $category)
    {
        $response = $category;

        return $this->successResponse($response);
    }

    /**
     * update data
     */
    public function update(ProductPartCategoryRequest $request, ProductPartCategory $category, Service $service)
    {
        return $this->successResponse(
            $service->setCategory($category)->update($request->all()),
            $this->successUpdateMsg()
        );
    }

    /**
     * delete data
     */
    public function destroy(ProductPartCategory $category, Service $service)
    {
        return $this->successResponse(
            $service->setCategory($category)->delete(),
            'success',
            204
        );
    }
}
