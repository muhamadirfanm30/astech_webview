<?php

namespace App\Http\Controllers\Api\Product;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Product\ProductModelRequest;
use App\Model\Master\MsProductModel;
use App\Services\Product\ProductModelService;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class ProductModelController extends ApiController
{
    /**
     * show list with paginate
     */
    public function index(ProductModelService $productModelService)
    {
        $response = $productModelService->list()->paginate(10);

        return $this->successResponse($response);
    }

    /**
     * show list with select2 structur
     */
    public function select2(Request $request)
    {
        $response = MsProductModel::where('name', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();
        return $this->successResponse($response);
    }

    /**
     * show list with select2 structur
     */
    public function select2p($id, Request $request)
    {
        $response = MsProductModel::where('name', 'like', '%' . $request->q . '%')
            ->where('product_brands_id', $id)
            ->limit(20)
            ->get();
        return $this->successResponse($response);
    }

    /**
     * show list with datatables structur
     */
    public function datatables(ProductModelService $productModelService)
    {
        $query = $productModelService->list();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * insert data
     */
    public function store(ProductModelRequest $request, ProductModelService $productModelService)
    {
        return $this->successResponse(
            $productModelService->create($request->all()),
            $this->successStoreMsg(),
            201
        );
    }

    /**
     * show one data
     */
    public function show(MsProductModel $productModel)
    {
        $response = $productModel;

        return $this->successResponse($response);
    }

    /**
     * update data
     */
    public function update(ProductModelRequest $request, MsProductModel $productModel, ProductModelService $productModelService)
    {
        return $this->successResponse(
            $productModelService->setProductModel($productModel)->update($request->all()),
            $this->successUpdateMsg()
        );
    }

    /**
     * delete data
     */
    public function destroy(MsProductModel $productModel, ProductModelService $productModelService)
    {
        return $this->successResponse(
            $productModelService->setProductModel($productModel)->delete(),
            'success',
            204
        );
    }
}
