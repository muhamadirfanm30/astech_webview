<?php

namespace App\Http\Controllers\Api\Product;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Product\ProductAttributeRequest;
use App\Http\Requests\Product\ProductAttributeUpdateBatchRequest;
use App\Model\Product\ProductAttribute;
use App\Model\Product\ProductAttributeTerm;
use App\Services\Product\ProductAttributeService;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class ProductAttributeController extends ApiController
{
    /**
     * show list with paginate
     */
    public function index(ProductAttributeService $productAttributeService)
    {
        $response = $productAttributeService->list()->paginate(10);

        return $this->successResponse($response);
    }

    /**
     * show list with select2 structur
     */
    public function select2(Request $request)
    {
        $response = ProductAttribute::with(['childs'])
            ->where('name', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();

        if ($request->no_limit) {
            $response = ProductAttribute::with(['childs'])->get();
        }

        return $this->successResponse($response);
    }

    /**
     * show list with datatables structur
     */
    public function datatables(ProductAttributeService $productAttributeService)
    {
        $query = $productAttributeService->list();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * insert data
     */
    public function store(ProductAttributeRequest $request, ProductAttributeService $productAttributeService)
    {
        return $this->successResponse(
            $productAttributeService->create($request->all()),
            $this->successStoreMsg(),
            201
        );
    }

    /**
     * show one data
     */
    public function show(ProductAttribute $attribute)
    {
        $response = $attribute;

        return $this->successResponse($response);
    }

    /**
     * show childs attribute data
     */
    public function childs($product_attribute_id)
    {
        $response = ProductAttributeTerm::where('product_attribute_id', $product_attribute_id)
            ->get();

        return $this->successResponse($response);
    }

    /**
     * update data
     */
    public function update(ProductAttributeRequest $request, ProductAttribute $attribute, ProductAttributeService $productAttributeService)
    {
        return $this->successResponse(
            $productAttributeService->setAttribute($attribute)->update($request->all()),
            $this->successUpdateMsg()
        );
    }

    /**
     * update data
     */
    public function updateBatch(ProductAttributeUpdateBatchRequest $request, ProductAttributeService $productAttributeService)
    {
        return $this->successResponse(
            $productAttributeService->updateBatch($request->all()),
            $this->successUpdateMsg()
        );
    }

    /**
     * delete data
     */
    public function destroy(ProductAttribute $attribute, ProductAttributeService $productAttributeService)
    {
        return $this->successResponse(
            $productAttributeService->setAttribute($attribute)->delete(),
            'success',
            204
        );
    }

    /**
     * delete data
     */
    public function destroyChild(ProductAttributeTerm $term, ProductAttributeService $productAttributeService)
    {
        return $this->successResponse(
            $productAttributeService->deleteChild($term),
            'success',
            204
        );
    }
}
