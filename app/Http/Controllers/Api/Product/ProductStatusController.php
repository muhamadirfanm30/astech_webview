<?php

namespace App\Http\Controllers\Api\Product;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Product\ProductStatusRequest;
use App\Model\Product\ProductStatus;
use App\Services\Product\ProductStatusService as Service;
use Illuminate\Http\Request;

class ProductStatusController extends ApiController
{
    /**
     * show list with paginate
     */
    public function index(Service $service)
    {
        $response = $service->list()->paginate(10);

        return $this->successResponse($response);
    }

    /**
     * show list with select2 structur
     */
    public function select2(Request $request, Service $service)
    {
        return $this->successResponse($service->select2($request));
    }

    /**
     * show list with datatables structur
     */
    public function datatables(Service $service)
    {
        return $service->datatables();
    }

    /**
     * insert data
     */
    public function store(ProductStatusRequest $request, Service $service)
    {
        return $this->successResponse(
            $service->create($request->all()),
            $this->successStoreMsg(),
            201
        );
    }

    /**
     * show one data
     */
    public function show(ProductStatus $status)
    {
        return $this->successResponse($status);
    }

    /**
     * update data
     */
    public function update(ProductStatusRequest $request, ProductStatus $status, Service $service)
    {
        return $this->successResponse(
            $service->setStatus($status)->update($request->all()),
            $this->successUpdateMsg()
        );
    }

    /**
     * delete data
     */
    public function destroy(ProductStatus $status, Service $service)
    {
        return $this->successResponse(
            $service->setStatus($status)->delete(),
            'success',
            204
        );
    }
}
