<?php

namespace App\Http\Controllers\Api\Product;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Product\ProductBrandRequest;
use App\Model\Product\ProductBrand;
use App\Services\Product\ProductBrandService;
use Illuminate\Http\Request;

class ProductBrandController extends ApiController
{
    /**
     * show list with paginate
     */
    public function index(ProductBrandService $productBrandService)
    {
        $response = $productBrandService->list()->paginate(10);

        return $this->successResponse($response);
    }

    /**
     * show list with select2 structur
     */
    public function select2(Request $request, ProductBrandService $productBrandService)
    {
        return $this->successResponse($productBrandService->select2($request));
    }

    /**
     * show list with datatables structur
     */
    public function datatables(ProductBrandService $productBrandService)
    {
        return $productBrandService->datatables();
    }

    /**
     * insert data
     */
    public function store(ProductBrandRequest $request, ProductBrandService $productBrandService)
    {
        return $this->successResponse(
            $productBrandService->create($request->all()),
            $this->successStoreMsg(),
            201
        );
    }

    /**
     * show one data
     */
    public function show(ProductBrand $brand)
    {
        $response = $brand;

        return $this->successResponse($response);
    }

    /**
     * update data
     */
    public function update(ProductBrandRequest $request, ProductBrand $brand, ProductBrandService $productBrandService)
    {
        return $this->successResponse(
            $productBrandService->setBrand($brand)->update($request->all()),
            $this->successUpdateMsg()
        );
    }

    /**
     * delete data
     */
    public function destroy(ProductBrand $brand, ProductBrandService $productBrandService)
    {
        return $this->successResponse(
            $productBrandService->setBrand($brand)->delete(),
            'success',
            204
        );
    }
}
