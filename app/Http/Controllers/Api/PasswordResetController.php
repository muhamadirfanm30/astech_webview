<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Model\PasswordReset;
use App\Notifications\PasswordResetRequestNotification;
use App\Notifications\PasswordResetSuccessNotification;
use App\Services\User\ResetPasswordService;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class PasswordResetController extends ApiController
{
    /**
     * Create token password reset
     */
    public function create(Request $request, ResetPasswordService $resetPasswordService)
    {
        // validasi
        $request->validate([
            'email' => 'required',
        ]);

        // request reset password
        $user = $resetPasswordService->request($request->all());

        // send response
        return $this->successResponse($user, 'We have e-mailed your password reset link!');
    }

    /**
     * Find token password reset
     */
    public function find($token, ResetPasswordService $resetPasswordService)
    {
        if ($resetPasswordService->isValidToken($token) == false) {
            return $this->errorResponse(null, 'This password reset token is invalid.', 404);
        }

        // send response
        return $this->successResponse($resetPasswordService->getPasswordReset(), 'We have e-mailed your password reset link!');
    }

    /**
     * Reset password
     */
    public function change(Request $request, ResetPasswordService $resetPasswordService)
    {
        // validation
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string|confirmed',
            'token' => 'required|string'
        ]);

        // ganti password
        $user = $resetPasswordService->changePassword($request->all());
            
        // send response
        return $this->successResponse($user, 'success');
    }
}
