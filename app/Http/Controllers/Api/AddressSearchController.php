<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Model\Master\MsCity;
use App\Model\Master\MsCountry;
use App\Model\Master\MsDistrict;
use App\Model\Master\MsProvince;
use App\Model\Master\MsVillage;
use App\Model\Master\MsZipcode;
use Illuminate\Http\Request;
use DB;

class AddressSearchController extends ApiController
{
    public function findCountry(Request $request, $country_id = null)
    {
        if ($country_id) {
            return $this->successResponse(
                MsCountry::with('provinces')->find($country_id)
            );
        }

        $response = MsCountry::where('name', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();

        return $this->successResponse($response);
    }

    public function findProvince(Request $request, $province_id = null)
    {
        if ($province_id) {
            return $this->successResponse(
                MsProvince::with('cities')->find($province_id)
            );
        }

        $response = MsProvince::with('country')
            ->where('name', 'like', '%' . $request->q . '%')
            ->when($request->country_id, function ($query, $country_id) {
                return $query->where('ms_country_id', $country_id);
            })
            ->limit(20)
            ->get();

        return $this->successResponse($response);
    }

    public function findCity(Request $request, $city_id = null)
    {
        if ($city_id) {
            return $this->successResponse(
                MsCity::with('province', 'districts')->find($city_id)
            );
        }

        $response = MsCity::with('province')
            ->where('name', 'like', '%' . $request->q . '%')
            ->when($request->province_id, function ($query, $province_id) {
                return $query->where('ms_province_id', $province_id);
            })
            ->limit(20)
            ->get();

        return $this->successResponse($response);
    }

    public function findDistrict(Request $request, $district_id = null)
    {
        if ($district_id) {
            return $this->successResponse(
                MsDistrict::with('villages', 'city', 'zipcode')->find($district_id)
            );
        }

        $response = MsDistrict::with('zipcode')
            ->where('name', 'like', '%' . $request->q . '%')
            ->when($request->city_id, function ($query, $city_id) {
                return $query->where('ms_city_id', $city_id);
            })
            ->limit(20)
            ->get();

        return $this->successResponse($response);
    }

    public function findVillage(Request $request, $village_id = null)
    {
        if ($village_id) {
            return $this->successResponse(
                MsVillage::with('district')->find($village_id)
            );
        }

        $response = MsVillage::where('name', 'like', '%' . $request->q . '%')
            ->when($request->district_id, function ($query, $district_id) {
                return $query->where('ms_district_id', $district_id);
            })
            ->limit(20)
            ->get();

        return $this->successResponse($response);
    }

    public function findZipcode(Request $request, $zipcode_id = null)
    {
        if ($zipcode_id) {
            return $this->successResponse(
             MsZipcode::with('district')->find($zipcode_id)->groupBy('zip_no asc')
            );
        }
        $response = MsZipcode::where('zip_no', 'like', '%' . $request->q . '%')
            ->when($request->district_id, function ($query, $district_id) {
                return $query->where('ms_district_id', $district_id);
            })
            ->groupBy('zip_no')
            ->limit(20)
            ->get();

        return $this->successResponse($response);
    }


}
