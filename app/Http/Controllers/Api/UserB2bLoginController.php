<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\UserB2b;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Http\Requests\UserLoginB2bRequest;
use App\Services\UserB2bService;

class UserB2bLoginController extends ApiController
{
    public function login(UserLoginB2bRequest $request, UserB2bService $userService)
    {
        // return 'asd';
        if ($userService->login($request->all())) {
            // get user login
            $user = $userService->getUser();

            // check user verify
            // if ($userService->isVerify() == false) {
            //     // send response
            //     return $this->errorResponse($user, 'your account has not been validated');
            // }

            // get access token
            $token = $user->createToken($userService->getTokenName())->accessToken;

            // send response
            return $this->successResponse([
                'token' => $token,
                'user' => $user,
            ], 'login sukses');
        }

        // send response
        return $this->errorResponse(
            [],
            'login gagal',
            400
        );
    }

    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }
}
