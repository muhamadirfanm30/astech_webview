<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Model\Company;
use App\Model\BusinessToBusinessOutletTransaction;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class CompanyController extends ApiController
{
    /**
     * show list with select2 structur
     */
    public function select2(Request $request)
    {
        $resp =  Company::where('name', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();

        return $this->successResponse($resp);
    }

    /**
     * show list with datatables structur
     */
    public function datatables()
    {
        $query = Company::query();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function datatablesOutletByCompany($id)
    {
        $query = BusinessToBusinessOutletTransaction::where('business_to_business_transaction_id', $id);
        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * insert data
     */
    public function store(Request $request)
    {
        return $this->successResponse(
            Company::create([
                'name' => $request->name,
                'npwp' => $request->npwp,
                'email' => $request->email,
                'nomor_rekening' => $request->nomor_rekening,
                'phone' => $request->phone,
                'nama_bank' => $request->nama_bank,
                'nama_pic' => $request->nama_pic,
                'alamat' => $request->alamat,
            ]),
            $this->successStoreMsg(),
            201
        );
    }

    /**
     * show one data
     */
    public function show(Company $company)
    {
        $response = $company;

        return $this->successResponse($response);
    }

    /**
     * update data
     */
    public function update(Request $request, Company $company)
    {
        return $this->successResponse(
            $company->update([
                'name' => $request->name,
                'npwp' => $request->npwp,
                'email' => $request->email,
                'nomor_rekening' => $request->nomor_rekening,
                'phone' => $request->phone,
                'nama_bank' => $request->nama_bank,
                'nama_pic' => $request->nama_pic,
                'alamat' => $request->alamat,
            ]),
            $this->successUpdateMsg()
        );
    }

    /**
     * delete data
     */
    public function destroy(Company $company)
    {
        return $this->successResponse(
            $company->delete(),
            'success',
            204
        );
    }
}
