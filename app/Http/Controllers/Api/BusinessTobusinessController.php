<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\SendErrorMsgException;
use App\Http\Controllers\ApiController;
use App\Model\BusinessToBusinessOutletDetailTransaction;
use App\Model\BusinessToBusinessOutletTransaction;
use App\Model\BusinessToBusinessTransaction;
use App\Model\BusinessToBusinessTransactionLog;
use App\Model\ImageBeritaAcara;
use App\Model\ImageInvoice;
use App\Model\ImagePurchaseOrder;
use App\Model\ImageQuotation;
use App\Model\RequestOrders;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use stdClass;
use Yajra\DataTables\DataTables;

class BusinessToBusinessController extends ApiController
{
    public function beritaAcara($id)
    {
        return ImageBeritaAcara::where('ms_b2b_detail', $id)->count();
    }

    public function apiMobileSearch(Request $request)
    {
        $company_id       = empty($request->company_id) ? 'id' : $request->company_id;
        $limit            = empty($request->limit) ? 10 : $request->limit;
        
        $query = BusinessToBusinessTransaction::with([
            'user_create',
            'user_update',
            'company',
            'business_to_business_outlet_transactions' => function ($query) use ($request) {
                if (!empty($request->outlet_name)) {
                    $query->where('name', $request->outlet_name);
                }
            },
            'business_to_business_outlet_transactions.user_create',
            'business_to_business_outlet_transactions.user_update',
            'business_to_business_outlet_transactions.business_to_business_outlet_detail_transactions',
        ]);

        if (!empty($request->company_id)) {
            $query->where('company_id', $request->company_id);
        }

        return $this->successResponse($query->paginate($limit));
    }

    public function apiMobileList(Request $request)
    {
        $q = $request->q;
        $paginate = $request->paginate;

        $response = BusinessToBusinessTransaction::with([
            'user_create',
            'user_update',
            'company',
            'business_to_business_outlet_transactions' => function ($query) use ($request) {
                if (!empty($request->outlet_name)) {
                    $query->where('name', $request->outlet_name);
                }
            },
            'business_to_business_outlet_transactions.user_create',
            'business_to_business_outlet_transactions.user_update',
            'business_to_business_outlet_transactions.business_to_business_outlet_detail_transactions',
        ])->when($q, function ($query, $q) {
            $query->where('name', 'like', '%' . $q . '%');
        });

        if ($paginate == 'no_paginate') {
            $response = $response->get();
        }else{
            $response = $response->paginate(10);
        }
        
        return $this->successResponse($response);
    }

    public function apiMobileStore(Request $request)
    {
        // dd(Auth::user());
        DB::beginTransaction();
        $no_transaksi = $this->createNomorTransaksi();
        try {
            $btb = BusinessToBusinessTransaction::create([
                'no_transaksi' => $no_transaksi,
                'company_id' => $request->company_id,
                'user_created' => Auth::id()
            ]);

            $newData = BusinessToBusinessTransaction::with('user_create', 'user_update', 'company')->where('id', $btb->id)->firstOrFail();

            BusinessToBusinessTransactionLog::create([
                'no_transaksi' => $no_transaksi,
                'playload_after' => json_encode($newData),
                'user_id' => Auth::id(),
                'business_to_business_transaction_id' => $btb->id,
                'log_type' => 'create',
                'log_modul' => 'header'
            ]);

            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            throw new SendErrorMsgException($e->getMessage());
        }

        return $this->successResponse($btb);
    }

    public function apiMobileShowLog(Request $request)
    {
        $limit = $request->limit;
        $offset = $request->offset;


        $logQuery = BusinessToBusinessTransactionLog::query();
        $logQueryCount = BusinessToBusinessTransactionLog::query();

        if (!empty($request->business_to_business_transaction_id)) {
            $logQuery->where('business_to_business_transaction_id', $request->business_to_business_transaction_id);
            $logQueryCount->where('business_to_business_transaction_id', $request->business_to_business_transaction_id);
        }

        if (!empty($request->business_to_business_outlet_transaction_id)) {
            $logQuery->where('business_to_business_outlet_transaction_id', $request->business_to_business_outlet_transaction_id)->whereNull('business_to_business_outlet_detail_transaction_id');
            $logQueryCount->where('business_to_business_outlet_transaction_id', $request->business_to_business_outlet_transaction_id)->whereNull('business_to_business_outlet_detail_transaction_id');
        }

        if (!empty($request->business_to_business_outlet_detail_transaction_id)) {
            $logQuery->where('business_to_business_outlet_detail_transaction_id', $request->business_to_business_outlet_detail_transaction_id);
            $logQueryCount->where('business_to_business_outlet_detail_transaction_id', $request->business_to_business_outlet_detail_transaction_id);
        }

        $logData = $logQuery->with('user')
            ->orderBy('id', 'desc')
            ->limit($limit)
            ->offset($offset)
            ->get();

        return [
            'rows' => $logData,
            'total' => $logQueryCount->count(),
        ];

            // dd($logData);

        // $this->successResponse('asd');

        // $this->successResponse([
        //     'rows' => '$logData',
        //     'total' => $logQueryCount->count(),
        // ]);
    }

    public function apiMobileStoreOutlet(Request $request)
    {
        DB::beginTransaction();
        try {
            $detail = BusinessToBusinessOutletTransaction::create([
                'business_to_business_transaction_id' => $request->business_to_business_transaction_id,
                'name' => $request->name,
                'alamat' => $request->alamat,
                'jumlah_unit' => $request->jumlah_unit,
                'searchMap' => $request->searchMap,
                'nomor_pic' => $request->nomor_pic,
                'nama_pic' => $request->nama_pic,
                'lat' => $request->lat,
                'long' => $request->long,
                'type' => $request->type,
                'user_created' => Auth::id()
            ]);

            $newData = BusinessToBusinessOutletTransaction::with('user_create', 'user_update', 'business_to_business_transaction.company')->where('id', $detail->id)->firstOrFail();

            BusinessToBusinessTransactionLog::create([
                'no_transaksi' => $newData->business_to_business_transaction->no_transaksi,
                'playload_after' => json_encode($newData),
                'user_id' => Auth::id(),
                'business_to_business_transaction_id' => $request->business_to_business_transaction_id,
                'business_to_business_outlet_transaction_id' => $detail->id,
                'log_type' => 'create',
                'log_modul' => 'outlet'
            ]);

            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            throw new SendErrorMsgException($e->getMessage());
        }

        return $this->successResponse($detail);
    }

    public function apiMobileEditOutlet(Request $request, BusinessToBusinessOutletTransaction $headerOurlet)
    {
        // dd($headerOurlet);
        DB::beginTransaction();
        try {
            $beforeData = BusinessToBusinessOutletTransaction::with('user_create', 'user_update', 'business_to_business_transaction.company')
                ->where('id', $headerOurlet->id)
                ->firstOrFail();

            $headerOurlet->update([
                'business_to_business_transaction_id' => $request->business_to_business_transaction_id,
                'name' => $request->name,
                'alamat' => $request->alamat,
                'jumlah_unit' => $request->jumlah_unit,
                'searchMap' => $request->searchMap,
                'nomor_pic' => $request->nomor_pic,
                'nama_pic' => $request->nama_pic,
                'lat' => $request->lat,
                'long' => $request->long,
                'type' => $request->type,
                'user_updated' => Auth::id()
            ]);

            $newData = BusinessToBusinessOutletTransaction::with('user_create', 'user_update', 'business_to_business_transaction.company')
                ->where('id', $headerOurlet->id)
                ->firstOrFail();

            BusinessToBusinessTransactionLog::create([
                'no_transaksi' => $newData->business_to_business_transaction->no_transaksi,
                'playload_before' => json_encode($beforeData),
                'playload_after' => json_encode($newData),
                'user_id' => Auth::id(),
                'business_to_business_transaction_id' => $request->business_to_business_transaction_id,
                'business_to_business_outlet_transaction_id' => $headerOurlet->id,
                'log_type' => 'edit',
                'log_modul' => 'outlet'
            ]);

            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            throw new SendErrorMsgException($e->getMessage());
        }

        return $this->successResponse($headerOurlet);
    }

    public function apiMobileShowDataEditOutlet(Request $request, BusinessToBusinessOutletTransaction $headerOurlet)
    {
        return $this->successResponse($headerOurlet);
    }

    public function apiMobileDestroyOutlet(Request $request, BusinessToBusinessOutletTransaction $headerOurlet)
    {
        DB::beginTransaction();
        try {
            $beforeData = BusinessToBusinessOutletTransaction::with('user_create', 'user_update', 'business_to_business_transaction.company')->where('id', $headerOurlet->id)->firstOrFail();

            $headerOurlet->delete();

            BusinessToBusinessTransactionLog::create([
                'no_transaksi' => $beforeData->business_to_business_transaction->no_transaksi,
                'playload_before' => json_encode($beforeData),
                'user_id' => Auth::id(),
                'business_to_business_transaction_id' => $beforeData->business_to_business_transaction_id,
                'business_to_business_outlet_transaction_id' => $beforeData->id,
                'log_type' => 'delete',
                'log_modul' => 'outlet'
            ]);

            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            throw new SendErrorMsgException($e->getMessage());
        }

        return $this->successResponse(
            $beforeData,
            $this->successStoreMsg(),
            204
        );
    }

    public function apiMobileStoreDetailOutlet(Request $request)
    {
        $uniq_key = $request->business_to_business_outlet_transaction_id . '_' . uniqid();

        $file_berita_acara_name = null;
        $file_berita_acara      = $request->file('file_berita_acara');
        if ($file_berita_acara) {
            $file_berita_acara_name = rand() . $file_berita_acara->getClientOriginalName();
            $file_berita_acara->storeAs('public/btb/media/', $file_berita_acara_name);
        }

        $file_quot_name = null;
        $file_quot      = $request->file('file_quot');
        if ($file_quot) {
            $file_quot_name = rand() . $file_quot->getClientOriginalName();
            $file_quot->storeAs('public/btb/media/', $file_quot_name);
        }

        $file_po_name = null;
        $file_po      = $request->file('file_po');
        if ($file_po) {
            $file_po_name = rand() . $file_po->getClientOriginalName();
            $file_po->storeAs('public/btb/media/', $file_po_name);
        }

        $file_invoice_name = null;
        $file_invoice      = $request->file('file_invoice');
        if ($file_invoice) {
            $file_invoice_name = rand() . $file_invoice->getClientOriginalName();
            $file_invoice->storeAs('public/btb/media/', $file_invoice_name);
        }

        DB::beginTransaction();
        try {
            $store_detail = BusinessToBusinessOutletDetailTransaction::create([
                'business_to_business_outlet_transaction_id' => $request->business_to_business_outlet_transaction_id,
                'unit_ke' => $request->unit_ke,
                'merek' => $request->merek,
                'remark' => $request->remark,
                'no_quotation' => $request->no_quotation,
                'status_quotation' => $request->status_quotation,
                'no_po' => $request->no_po,
                'no_invoice' => $request->no_invoice,
                'nominal_quot' => $request->nominal_quot,
                'tanggal_pembayaran' => $request->tanggal_pembayaran,
                'tanggal_quot' => $request->tanggal_quot,
                'tanggal_po' => $request->tanggal_po,
                'tanggal_Invoice' => $request->tanggal_Invoice,
                'file_berita_acara' => $file_berita_acara_name,
                'file_quot' => $file_quot_name,
                'file_po' => $file_po_name,
                'file_invoice' => $file_invoice_name,
                'uniq_key' => $uniq_key,
                'user_created' => Auth::id()
            ]);

            $newData = BusinessToBusinessOutletDetailTransaction::with('user_create', 'user_update', 'business_to_business_outlet_transaction.business_to_business_transaction.company')
                ->where('id', $store_detail->id)
                ->firstOrFail();

            BusinessToBusinessTransactionLog::create([
                'no_transaksi' => $newData->business_to_business_outlet_transaction->business_to_business_transaction->no_transaksi,
                'playload_after' => json_encode($newData),
                'user_id' => Auth::id(),
                'business_to_business_transaction_id' => $newData->business_to_business_outlet_transaction->business_to_business_transaction_id,
                'business_to_business_outlet_transaction_id' => $newData->business_to_business_outlet_transaction->id,
                'business_to_business_outlet_detail_transaction_id' => $store_detail->id,
                'log_type' => 'create',
                'log_modul' => 'outlet_detail'
            ]);

            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            throw new SendErrorMsgException($e->getMessage());
        }

        return $this->successResponse($store_detail);
    }

    public function apiMobileEditDetailOutlet(Request $request, BusinessToBusinessOutletDetailTransaction $detailOutlet)
    {
        $uniq_key = $detailOutlet->uniq_key;

        $file_berita_acara_name =  $detailOutlet->file_berita_acara;
        $file_berita_acara      = $request->file('file_berita_acara');
        if ($file_berita_acara) {
            $file_berita_acara_name = rand() . $file_berita_acara->getClientOriginalName();
            $file_berita_acara->storeAs('public/btb/media/', $file_berita_acara_name);

            Storage::delete('public/btb/media/' . $detailOutlet->file_berita_acara);
        }

        $file_quot_name = $detailOutlet->file_quot;
        $file_quot      = $request->file('file_quot');
        if ($file_quot) {
            $file_quot_name = rand() . $file_quot->getClientOriginalName();
            $file_quot->storeAs('public/btb/media/', $file_quot_name);

            Storage::delete('public/btb/media/' . $detailOutlet->file_quot);
        }

        $file_po_name = $detailOutlet->file_po;
        $file_po      = $request->file('file_po');
        if ($file_po) {
            $file_po_name = rand() . $file_po->getClientOriginalName();
            $file_po->storeAs('public/btb/media/', $file_po_name);

            Storage::delete('public/btb/media/' . $detailOutlet->file_po);
        }

        $file_invoice_name = $detailOutlet->file_invoice;
        $file_invoice      = $request->file('file_invoice');
        if ($file_invoice) {
            $file_invoice_name = rand() . $file_invoice->getClientOriginalName();
            $file_invoice->storeAs('public/btb/media/', $file_invoice_name);

            Storage::delete('public/btb/media/' . $detailOutlet->file_invoice);
        }

        DB::beginTransaction();
        try {
            $beforeData = BusinessToBusinessOutletDetailTransaction::with('user_create', 'user_update', 'business_to_business_outlet_transaction.business_to_business_transaction.company')
                ->where('id', $detailOutlet->id)
                ->firstOrFail();

            $detailOutlet->update([
                'merek' => $request->merek,
                'unit_ke' => $request->unit_ke,
                'remark' => $request->remark,
                'no_quotation' => $request->no_quotation,
                'status_quotation' => $request->status_quotation,
                'no_po' => $request->no_po,
                'no_invoice' => $request->no_invoice,
                'nominal_quot' => $request->nominal_quot,
                'tanggal_pembayaran' => $request->tanggal_pembayaran,
                'tanggal_quot' => $request->tanggal_quot,
                'tanggal_po' => $request->tanggal_po,
                'tanggal_Invoice' => $request->tanggal_Invoice,
                'tanggal_pengerjaan' => $request->tanggal_pengerjaan,
                'file_berita_acara' => $file_berita_acara_name,
                'file_quot' => $file_quot_name,
                'file_po' => $file_po_name,
                'file_invoice' => $file_invoice_name,
                'uniq_key' => $uniq_key,
                'user_updated' => Auth::id()
            ]);

            $newData = BusinessToBusinessOutletDetailTransaction::with('user_create', 'user_update', 'business_to_business_outlet_transaction.business_to_business_transaction.company')
                ->where('id', $detailOutlet->id)
                ->firstOrFail();

            BusinessToBusinessTransactionLog::create([
                'no_transaksi' => $newData->business_to_business_outlet_transaction->business_to_business_transaction->no_transaksi,
                'playload_before' => json_encode($beforeData),
                'playload_after' => json_encode($newData),
                'user_id' => Auth::id(),
                'business_to_business_transaction_id' => $newData->business_to_business_outlet_transaction->business_to_business_transaction_id,
                'business_to_business_outlet_transaction_id' => $newData->business_to_business_outlet_transaction->id,
                'business_to_business_outlet_detail_transaction_id' => $newData->id,
                'log_type' => 'edit',
                'log_modul' => 'outlet_detail'
            ]);

            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            throw new SendErrorMsgException($e->getMessage());
        }

        return $this->successResponse($detailOutlet);
    }

    public function apiMobileShowDetailOutlet(Request $request, BusinessToBusinessOutletDetailTransaction $detailOutlet)
    {
        return $this->successResponse($detailOutlet);
    }

    public function apiMobileDeleteDetailOutlet(Request $request, BusinessToBusinessOutletDetailTransaction $detailOutlet)
    {
        DB::beginTransaction();
        try {
            $beforeData = BusinessToBusinessOutletDetailTransaction::with('user_create', 'user_update', 'business_to_business_outlet_transaction.business_to_business_transaction.company')
            ->where('id', $detailOutlet->id)
            ->firstOrFail();
            $newData = BusinessToBusinessOutletDetailTransaction::with('user_create', 'user_update', 'business_to_business_outlet_transaction.business_to_business_transaction.company')
                ->where('id', $detailOutlet->id)
                ->firstOrFail();
            // dd($beforeData);
            $detailOutlet->delete();

            BusinessToBusinessTransactionLog::create([
                'no_transaksi' => $newData->business_to_business_outlet_transaction->business_to_business_transaction->no_transaksi,
                'playload_before' => json_encode($beforeData),
                'user_id' => Auth::id(),
                'business_to_business_transaction_id' => $newData->business_to_business_outlet_transaction->business_to_business_transaction_id,
                'log_type' => 'delete',
                'log_modul' => 'header'
            ]);

            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            throw new SendErrorMsgException($e->getMessage());
        }

        return $this->successResponse(
            $beforeData,
            $this->successStoreMsg(),
            204
        );
    }

    public function apiMobileShowDetail($id)
    {
        $detailBtb = BusinessToBusinessTransaction::with(['company'])->where('id', $id)->firstOrFail();
        return $this->successResponse($detailBtb);
        
    }

    public function apiGetStatus()
    {
        $statuses = BusinessToBusinessOutletDetailTransaction::whereNotNull('status_quotation')->groupBy('status_quotation')->get();
        return $this->successResponse($statuses);
    }

    public function bootstrapTable(Request $request)
    {
        // $btbQuery = BusinessToBusinessTransaction::query();
        $btbQuery = BusinessToBusinessTransaction::whereNull('is_deleted');

        if (!empty($request->company_id)) {
            $btbQuery->where('company_id', $request->company_id);
        }

        if (!empty($request->outlet_name)) {
            $btbQuery->whereHas('business_to_business_outlet_transactions', function ($query) use ($request) {
                if (!empty($request->outlet_name)) {
                    $query->where('name', $request->outlet_name);
                }
            });
        }

        $btbQuery->with([
            'user_create',
            'user_update',
            'company',
            'business_to_business_outlet_transactions' => function ($query) use ($request) {
                if (!empty($request->outlet_name)) {
                    $query->where('name', $request->outlet_name);
                }
            },
            'business_to_business_outlet_transactions.user_create',
            'business_to_business_outlet_transactions.user_update',
            'business_to_business_outlet_transactions.business_to_business_outlet_detail_transactions'=> function ($query) use ($request) {
                $query->where('status_quotation', '-');
            },
        ]);

        return DataTables::of($btbQuery)
            ->addIndexColumn()
            ->toJson();
    }


    public function bootstrapTableX(Request $request)
    {
        $limit = $request->limit;
        $offset = $request->offset;
        $btbQuery = BusinessToBusinessTransaction::query();

        if (!empty($request->company_id)) {
            $btbQuery->where('company_id', $request->company_id);
        }

        $btbData = $btbQuery;
        $btbCount = $btbQuery;

        $btb = $btbData->with(
            'user_create',
            'user_update',
            'company',
            'business_to_business_outlet_transactions.user_create',
            'business_to_business_outlet_transactions.user_update',
            'business_to_business_outlet_transactions.business_to_business_outlet_detail_transactions',
        )
            ->limit($limit)
            ->offset($offset)
            ->get();

        $newFormat = [];
        foreach ($btb as $row) {
            $offset++;
            $parenRow = new stdClass();
            $parenRow->number = $offset;
            $parenRow->business_to_business_transaction = $row;
            $parenRow->business_to_business_outlet_transaction = null;
            $parenRow->business_to_business_outlet_detail_transaction = null;
            $parenRow->id = $row->id;
            $parenRow->company_name = $row->company->name;
            $parenRow->pid = 0;
            $parenRow->is_parent = true;
            $newFormat[] = $parenRow;
            foreach ($row->business_to_business_outlet_transactions as $k => $detail) {
                $childRow = new stdClass();
                $childRow->number = $offset . '.' . ($k + 1);
                $childRow->business_to_business_transaction = $row;
                $childRow->business_to_business_outlet_transaction = $detail;
                $childRow->business_to_business_outlet_detail_transaction = null;
                $childRow->id = $row->id . '-' . $detail->id;
                $childRow->company_name = $row->company->name;
                $childRow->pid = $row->id;
                $childRow->is_parent = false;
                $newFormat[] = $childRow;
            }
        }
        return [
            'rows' => $newFormat,
            'total' => $btbCount->count(),
        ];
    }

    public function bootstrapTableGetOutlet($btb_id, Request $request)
    {
        $limit = $request->limit;
        $offset = $request->offset;

        $btbQuery = BusinessToBusinessOutletTransaction::where('business_to_business_transaction_id', $btb_id);
        $btbCount = BusinessToBusinessOutletTransaction::where('business_to_business_transaction_id', $btb_id);

        if (!empty($request->search)) {
            $btbQuery->where(function ($query) use ($request) {
                $query->where('name', 'like', '%' . $request->search . '%')
                    ->OrWhereHas('business_to_business_outlet_detail_transactions', function ($queryWhere) use ($request) {
                        $queryWhere->where('merek', 'like', '%' . $request->search . '%')
                            ->orWhere('remark', 'like', '%' . $request->search . '%')
                            ->orWhere('no_quotation', 'like', '%' . $request->search . '%')
                            ->orWhere('no_po', 'like', '%' . $request->search . '%')
                            ->orWhere('no_invoice', 'like', '%' . $request->search . '%')
                            ->orWhere('status_quotation', 'like', '%' . $request->search . '%');
                    });
            });
            $btbCount->where(function ($query) use ($request) {
                $query->where('name', 'like', '%' . $request->search . '%')
                    ->OrWhereHas('business_to_business_outlet_detail_transactions', function ($queryWhere) use ($request) {
                        $queryWhere->where('merek', 'like', '%' . $request->search . '%')
                            ->orWhere('remark', 'like', '%' . $request->search . '%')
                            ->orWhere('no_quotation', 'like', '%' . $request->search . '%')
                            ->orWhere('no_po', 'like', '%' . $request->search . '%')
                            ->orWhere('no_invoice', 'like', '%' . $request->search . '%')
                            ->orWhere('status_quotation', 'like', '%' . $request->search . '%');
                    });
            });
        }

        if (!empty($request->status_quotation)) {
            $btbQuery->whereHas('business_to_business_outlet_detail_transactions', function ($query) use ($request) {
                $query->where('status_quotation', $request->status_quotation)->where('is_parent_b2b_outlet_id', 62);
            });
            $btbCount->whereHas('business_to_business_outlet_detail_transactions', function ($query) use ($request) {
                $query->where('status_quotation', $request->status_quotation)->where('is_parent_b2b_outlet_id', 62);
            });
        }

        $btbData = $btbQuery;

        $btb = $btbData->with([
            'user_create',
            'user_update',
            'business_to_business_outlet_detail_transactions' => function ($query) use ($request) {
                $query->whereNull('is_parent_b2b_outlet_id');
                if (!empty($request->status_quotation)) {
                    $query->where('status_quotation', $request->status_quotation);
                }

                if (!empty($request->search)) {
                    $query->where(function ($queryWhere) use ($request) {
                        $queryWhere->where('merek', 'like', '%' . $request->search . '%')
                            ->orWhere('remark', 'like', '%' . $request->search . '%')
                            ->orWhere('no_quotation', 'like', '%' . $request->search . '%')
                            ->orWhere('no_po', 'like', '%' . $request->search . '%')
                            ->orWhere('no_invoice', 'like', '%' . $request->search . '%')
                            ->orWhere('status_quotation', 'like', '%' . $request->search . '%');
                    });
                }
            },
            'business_to_business_outlet_detail_transactions.user_create',
            'business_to_business_outlet_detail_transactions.user_update',
        ])
            ->limit($limit)
            ->offset($offset)
            ->orderBy('name', 'ASC')
            ->get();

            // return $btb;

        $newFormat = [];
        foreach ($btb as $row) {
            $offset++;
            $parenRow = new stdClass();
            $parenRow->number = $offset;
            $parenRow->business_to_business_outlet_transaction = $row;
            $parenRow->business_to_business_outlet_detail_transaction = null;
            $parenRow->id = $row->id;
            $parenRow->name = $row->name;
            $parenRow->pid = 0;
            $parenRow->is_parent = true;
            $newFormat[] = $parenRow;

            foreach ($row->business_to_business_outlet_detail_transactions as $k => $detail) {
                $childRow = new stdClass();
                $childRow->number = $offset . '.' . ($k + 1);
                $childRow->business_to_business_outlet_transaction = $row;
                $childRow->business_to_business_outlet_detail_transaction = $detail;
                $childRow->id = uniqid();
                $childRow->name = $row->name;
                $childRow->pid = $row->id;
                $childRow->is_parent = false;
                $newFormat[] = $childRow;
            }
        }
        return [
            'rows' => $newFormat,
            'total' => $btbCount->count(),
        ];
    }

    public function bootstrapTableGetOutletDetail($outlet_id)
    {
        $query = BusinessToBusinessOutletDetailTransaction::with(['user_create', 'user_update', 'get_type'])->whereNull('is_parent_b2b_outlet_id')->where('business_to_business_outlet_transaction_id', $outlet_id);

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function bootstrapTableGetOutletDetailAddParent($outlet_id)
    {
        $query = BusinessToBusinessOutletDetailTransaction::with(['user_create', 'user_update'])->where('is_parent_b2b_outlet_id', $outlet_id);

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function datatablesLog(Request $request)
    {
        $limit = $request->limit;
        $offset = $request->offset;

        $logQuery = BusinessToBusinessTransactionLog::query();
        $logQueryCount = BusinessToBusinessTransactionLog::query();

        if (!empty($request->business_to_business_transaction_id)) {
            $logQuery->where('business_to_business_transaction_id', $request->business_to_business_transaction_id);
            $logQueryCount->where('business_to_business_transaction_id', $request->business_to_business_transaction_id);
        }

        if (!empty($request->business_to_business_outlet_transaction_id)) {
            $logQuery->where('business_to_business_outlet_transaction_id', $request->business_to_business_outlet_transaction_id)->whereNull('business_to_business_outlet_detail_transaction_id');
            $logQueryCount->where('business_to_business_outlet_transaction_id', $request->business_to_business_outlet_transaction_id)->whereNull('business_to_business_outlet_detail_transaction_id');
        }

        if (!empty($request->business_to_business_outlet_detail_transaction_id)) {
            $logQuery->where('business_to_business_outlet_detail_transaction_id', $request->business_to_business_outlet_detail_transaction_id);
            $logQueryCount->where('business_to_business_outlet_detail_transaction_id', $request->business_to_business_outlet_detail_transaction_id);
        }

        $logData = $logQuery->with('user')
            ->orderBy('id', 'desc')
            ->limit($limit)
            ->offset($offset)
            ->get();

        return [
            'rows' => $logData,
            'total' => $logQueryCount->count(),
        ];
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        $no_transaksi = $this->createNomorTransaksi();
        try {
            $btb = BusinessToBusinessTransaction::create([
                'no_transaksi' => $no_transaksi,
                'company_id' => $request->company_id,
                'user_created' => Auth::id()
            ]);

            $newData = BusinessToBusinessTransaction::with('user_create', 'user_update', 'company')->where('id', $btb->id)->firstOrFail();

            BusinessToBusinessTransactionLog::create([
                'no_transaksi' => $no_transaksi,
                'playload_after' => json_encode($newData),
                'user_id' => Auth::id(),
                'business_to_business_transaction_id' => $btb->id,
                'log_type' => 'create',
                'log_modul' => 'header'
            ]);

            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            throw new SendErrorMsgException($e->getMessage());
        }

        return $this->successResponse(
            $newData,
            $this->successStoreMsg(),
            201
        );
    }

    public function storeOutlet(Request $request)
    {
        // dd($request->business_to_business_transaction_id);
        $cek = BusinessToBusinessTransaction::where('company_id', $request->business_to_business_transaction_id)->count();
        // dd($cek);
        if($cek == 0){
            $create = BusinessToBusinessTransaction::create([
                'company_id' => $request->business_to_business_transaction_id,
                'user_created' => Auth::user()->id,
                'no_transaksi' => $this->createNomorTransaksi()
            ]);
        }
        DB::beginTransaction();
        try {
            
            
            $detail = BusinessToBusinessOutletTransaction::create([
                'business_to_business_transaction_id' => $request->business_to_business_transaction_id,
                'name' => $request->name,
                'alamat' => $request->alamat,
                'jumlah_unit' => $request->jumlah_unit,
                'searchMap' => $request->searchMap,
                'nomor_pic' => $request->nomor_pic,
                'nama_pic' => $request->nama_pic,
                'lat' => $request->lat,
                'long' => $request->long,
                'type' => $request->type,
                'user_created' => Auth::id()
            ]);

            $newData = BusinessToBusinessOutletTransaction::with('user_create', 'user_update', 'business_to_business_transaction.company')->where('id', $detail->id)->firstOrFail();

            BusinessToBusinessTransactionLog::create([
                'no_transaksi' => $newData->business_to_business_transaction->no_transaksi,
                'playload_after' => json_encode($newData),
                'user_id' => Auth::id(),
                'business_to_business_transaction_id' => $request->business_to_business_transaction_id,
                'business_to_business_outlet_transaction_id' => $detail->id,
                'log_type' => 'create',
                'log_modul' => 'outlet'
            ]);

            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            throw new SendErrorMsgException($e->getMessage());
        }

        return $this->successResponse(
            $newData,
            $this->successStoreMsg(),
            201
        );
    }

    public function storeOutletDetail(Request $request)
    {
        $uniq_key = $request->business_to_business_outlet_transaction_id . '_' . uniqid();

        $file_berita_acara_name = null;
        $file_berita_acara      = $request->file('file_berita_acara');
        if ($file_berita_acara) {
            $file_berita_acara_name = rand() . $file_berita_acara->getClientOriginalName();
            $file_berita_acara->storeAs('public/btb/media/', $file_berita_acara_name);
        }

        $file_quot_name = null;
        $file_quot      = $request->file('file_quot');
        if ($file_quot) {
            $file_quot_name = rand() . $file_quot->getClientOriginalName();
            $file_quot->storeAs('public/btb/media/', $file_quot_name);
        }

        $file_po_name = null;
        $file_po      = $request->file('file_po');
        if ($file_po) {
            $file_po_name = rand() . $file_po->getClientOriginalName();
            $file_po->storeAs('public/btb/media/', $file_po_name);
        }

        $file_invoice_name = null;
        $file_invoice      = $request->file('file_invoice');
        if ($file_invoice) {
            $file_invoice_name = rand() . $file_invoice->getClientOriginalName();
            $file_invoice->storeAs('public/btb/media/', $file_invoice_name);
        }

        DB::beginTransaction();
        try {
            $detail = BusinessToBusinessOutletDetailTransaction::create([
                'business_to_business_outlet_transaction_id' => $request->business_to_business_outlet_transaction_id,
                'unit_ke' => !empty($request->unit_ke) ? $request->unit_ke : '-',
                'merek' => !empty($request->merek) ? $request->merek : '-',
                'type' => !empty($request->type) ? $request->type : '-',
                'remark' => !empty($request->remark) ? $request->remark : '-',
                'no_quotation' => !empty($request->no_quotation) ? $request->no_quotation : '-',
                'status_quotation' => !empty($request->status_quotation) ? $request->status_quotation : '-',
                'no_po' => !empty($request->no_po) ? $request->no_po : '-',
                'no_invoice' => !empty($request->no_invoice) ? $request->no_invoice : '-',
                'nominal_quot' => !empty($request->nominal_quot) ? $request->nominal_quot : '0',
                'tanggal_pembayaran' => !empty($request->tanggal_pembayaran) ? $request->tanggal_pembayaran : null,
                'tanggal_quot' => !empty($request->tanggal_quot) ? $request->tanggal_quot : null,
                'tanggal_po' => !empty($request->tanggal_po) ? $request->tanggal_po : null,
                'tanggal_Invoice' => !empty($request->tanggal_Invoice) ? $request->tanggal_Invoice : null,
                'tanggal_pengerjaan' => !empty($request->tanggal_pengerjaan) ? $request->tanggal_pengerjaan : null,
                'serial_number' => !empty($request->serial_number) ? $request->serial_number : null,
                'imei' => !empty($request->imei) ? $request->imei : null,
                // 'file_berita_acara' => $file_berita_acara_name,
                // 'file_quot' => $file_quot_name,
                // 'file_po' => $file_po_name,
                // 'file_invoice' => $file_invoice_name,
                'uniq_key' => $uniq_key,
                'user_created' => Auth::id()
            ]);

            
                $imageBeritaAcara = ImageBeritaAcara::create([
                    'image_berita_acara' => $file_berita_acara_name,
                    'type' => 'file_berita_acara',
                    'ms_b2b_detail' => $detail->id,
                ]);
            

            
                $imageInvoice = ImageInvoice::create([
                    'image_invoice' => $file_invoice_name,
                    'type' => 'image_invoice',
                    'ms_b2b_detail' => $detail->id,
                ]);
            
            

            
                $imagePurchaseOrder = ImagePurchaseOrder::create([
                    'image_po' => $file_po_name,
                    'type' => 'image_po',
                    'ms_b2b_detail' => $detail->id,
                ]);
            
           

            
                $imageQuotation = ImageQuotation::create([
                    'image_quot' => $file_quot_name,
                    'type' => 'image_quot',
                    'ms_b2b_detail' => $detail->id,
                ]);
            

            $newData = BusinessToBusinessOutletDetailTransaction::with('user_create', 'user_update', 'business_to_business_outlet_transaction.business_to_business_transaction.company')
                ->where('id', $detail->id)
                ->firstOrFail();

            BusinessToBusinessTransactionLog::create([
                'no_transaksi' => $newData->business_to_business_outlet_transaction->business_to_business_transaction->no_transaksi,
                'playload_after' => json_encode($newData),
                'user_id' => Auth::id(),
                'business_to_business_transaction_id' => $newData->business_to_business_outlet_transaction->business_to_business_transaction_id,
                'business_to_business_outlet_transaction_id' => $newData->business_to_business_outlet_transaction->id,
                'business_to_business_outlet_detail_transaction_id' => $detail->id,
                'log_type' => 'create',
                'log_modul' => 'outlet_detail'
            ]);

            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            throw new SendErrorMsgException($e->getMessage());
        }

        return $this->successResponse(
            $newData,
            $this->successStoreMsg(),
            201
        );
    }

    public function storeOutletDetailAddParent(Request $request)
    {
        $uniq_key = $request->business_to_business_outlet_transaction_id . '_' . uniqid();

        $file_berita_acara_name = null;
        $file_berita_acara      = $request->file('file_berita_acara');
        if ($file_berita_acara) {
            $file_berita_acara_name = rand() . $file_berita_acara->getClientOriginalName();
            $file_berita_acara->storeAs('public/btb/media/', $file_berita_acara_name);
        }

        $file_quot_name = null;
        $file_quot      = $request->file('file_quot');
        if ($file_quot) {
            $file_quot_name = rand() . $file_quot->getClientOriginalName();
            $file_quot->storeAs('public/btb/media/', $file_quot_name);
        }

        $file_po_name = null;
        $file_po      = $request->file('file_po');
        if ($file_po) {
            $file_po_name = rand() . $file_po->getClientOriginalName();
            $file_po->storeAs('public/btb/media/', $file_po_name);
        }

        $file_invoice_name = null;
        $file_invoice      = $request->file('file_invoice');
        if ($file_invoice) {
            $file_invoice_name = rand() . $file_invoice->getClientOriginalName();
            $file_invoice->storeAs('public/btb/media/', $file_invoice_name);
        }

        DB::beginTransaction();
        try {
            $outletDetail = BusinessToBusinessOutletDetailTransaction::where('id', $request->is_parent_b2b_outlet_id)->first();
            $detail = BusinessToBusinessOutletDetailTransaction::create([
                'business_to_business_outlet_transaction_id' => $request->business_to_business_outlet_transaction_id,
                'unit_ke' => $outletDetail->unit_ke,
                'merek' => $outletDetail->merek,
                'remark' => !empty($request->remark) ? $request->remark : '-',
                'no_quotation' => !empty($request->no_quotation) ? $request->no_quotation : '-',
                'status_quotation' => !empty($request->status_quotation) ? $request->status_quotation : '-',
                'no_po' => !empty($request->no_po) ? $request->no_po : '-' ,
                'no_invoice' => !empty($request->no_invoice) ? $request->no_invoice : '-',
                'nominal_quot' => !empty($request->nominal_quot) ? $request->nominal_quot : '0',
                'tanggal_pembayaran' => !empty($request->tanggal_pembayaran) ? $request->tanggal_pembayaran : null,
                'tanggal_quot' => !empty($request->tanggal_quot) ? $request->tanggal_quot : null,
                'tanggal_po' => !empty($request->tanggal_po) ? $request->tanggal_po : null,
                'tanggal_Invoice' => !empty($request->tanggal_Invoice) ? $request->tanggal_Invoice : null,
                'tanggal_pengerjaan' => !empty($request->tanggal_pengerjaan) ? $request->tanggal_pengerjaan : null,
                // 'file_berita_acara' => $file_berita_acara_name,
                // 'file_quot' => $file_quot_name,
                // 'file_po' => $file_po_name,
                // 'file_invoice' => $file_invoice_name,
                'uniq_key' => $uniq_key,
                'user_created' => Auth::id(),
                'is_parent_b2b_outlet_id' => $request->is_parent_b2b_outlet_id
            ]);

            
            $imageBeritaAcara = ImageBeritaAcara::create([
                'image_berita_acara' => $file_berita_acara_name,
                'type' => 'file_berita_acara',
                'ms_b2b_detail' => $detail->id,
            ]);
        

        
            $imageInvoice = ImageInvoice::create([
                'image_invoice' => $file_invoice_name,
                'type' => 'image_invoice',
                'ms_b2b_detail' => $detail->id,
            ]);
        
        

        
            $imagePurchaseOrder = ImagePurchaseOrder::create([
                'image_po' => $file_po_name,
                'type' => 'image_po',
                'ms_b2b_detail' => $detail->id,
            ]);
        
        

        
            $imageQuotation = ImageQuotation::create([
                'image_quot' => $file_quot_name,
                'type' => 'image_quot',
                'ms_b2b_detail' => $detail->id,
            ]);
            

            $newData = BusinessToBusinessOutletDetailTransaction::with('user_create', 'user_update', 'business_to_business_outlet_transaction.business_to_business_transaction.company')
                ->where('id', $detail->id)
                ->firstOrFail();

            BusinessToBusinessTransactionLog::create([
                'no_transaksi' => $newData->business_to_business_outlet_transaction->business_to_business_transaction->no_transaksi,
                'playload_after' => json_encode($newData),
                'user_id' => Auth::id(),
                'business_to_business_transaction_id' => $newData->business_to_business_outlet_transaction->business_to_business_transaction_id,
                'business_to_business_outlet_transaction_id' => $newData->business_to_business_outlet_transaction->id,
                'business_to_business_outlet_detail_transaction_id' => $detail->id,
                'log_type' => 'create',
                'log_modul' => 'outlet_detail'
            ]);

            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            throw new SendErrorMsgException($e->getMessage());
        }

        return $this->successResponse(
            $newData,
            $this->successStoreMsg(),
            201
        );
    }

    public function update(Request $request, BusinessToBusinessTransaction $btb)
    {
        return $this->successResponse(
            $btb->update([
                'company_id' => $request->company_id,
                'user_updated' => Auth::id()
            ]),
            $this->successStoreMsg(),
            200
        );
    }

    public function updateOutlet(Request $request, BusinessToBusinessOutletTransaction $headerOurlet)
    {
        DB::beginTransaction();
        try {
            $beforeData = BusinessToBusinessOutletTransaction::with('user_create', 'user_update', 'business_to_business_transaction.company')
                ->where('id', $headerOurlet->id)
                ->firstOrFail();

            $headerOurlet->update([
                'business_to_business_transaction_id' => $request->business_to_business_transaction_id,
                'name' => $request->name,
                'alamat' => $request->alamat,
                'jumlah_unit' => $request->jumlah_unit,
                'searchMap' => $request->searchMap,
                'nomor_pic' => $request->nomor_pic,
                'nama_pic' => $request->nama_pic,
                'lat' => $request->lat,
                'long' => $request->long,
                'type' => $request->type,
                'user_updated' => Auth::id()
            ]);

            $newData = BusinessToBusinessOutletTransaction::with('user_create', 'user_update', 'business_to_business_transaction.company')
                ->where('id', $headerOurlet->id)
                ->firstOrFail();

            BusinessToBusinessTransactionLog::create([
                'no_transaksi' => $newData->business_to_business_transaction->no_transaksi,
                'playload_before' => json_encode($beforeData),
                'playload_after' => json_encode($newData),
                'user_id' => Auth::id(),
                'business_to_business_transaction_id' => $request->business_to_business_transaction_id,
                'business_to_business_outlet_transaction_id' => $headerOurlet->id,
                'log_type' => 'edit',
                'log_modul' => 'outlet'
            ]);

            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            throw new SendErrorMsgException($e->getMessage());
        }

        return $this->successResponse(
            $newData,
            $this->successStoreMsg(),
            201
        );
    }

    public function updateOutletDetail(Request $request, BusinessToBusinessOutletDetailTransaction $detailOutlet)
    {
        // return $request->tanggal_pengerjaan;
        $uniq_key = $detailOutlet->uniq_key;

        $file_berita_acara_name =  $detailOutlet->file_berita_acara;
        $file_berita_acara      = $request->file('file_berita_acara');
        if ($file_berita_acara) {
            $file_berita_acara_name = rand() . $file_berita_acara->getClientOriginalName();
            $file_berita_acara->storeAs('public/btb/media/', $file_berita_acara_name);

            Storage::delete('public/btb/media/' . $detailOutlet->file_berita_acara);
        }

        $file_quot_name = $detailOutlet->file_quot;
        $file_quot      = $request->file('file_quot');
        if ($file_quot) {
            $file_quot_name = rand() . $file_quot->getClientOriginalName();
            $file_quot->storeAs('public/btb/media/', $file_quot_name);

            Storage::delete('public/btb/media/' . $detailOutlet->file_quot);
        }

        $file_po_name = $detailOutlet->file_po;
        $file_po      = $request->file('file_po');
        if ($file_po) {
            $file_po_name = rand() . $file_po->getClientOriginalName();
            $file_po->storeAs('public/btb/media/', $file_po_name);

            Storage::delete('public/btb/media/' . $detailOutlet->file_po);
        }

        $file_invoice_name = $detailOutlet->file_invoice;
        $file_invoice      = $request->file('file_invoice');
        if ($file_invoice) {
            $file_invoice_name = rand() . $file_invoice->getClientOriginalName();
            $file_invoice->storeAs('public/btb/media/', $file_invoice_name);

            Storage::delete('public/btb/media/' . $detailOutlet->file_invoice);
        }

        // return $file_quot_name;
        // if($request->status_quotation == 'Processing'){
            
        // }

        DB::beginTransaction();
        try {
            $beforeData = BusinessToBusinessOutletDetailTransaction::with('user_create', 'user_update', 'business_to_business_outlet_transaction.business_to_business_transaction.company')
                ->where('id', $detailOutlet->id)
                ->firstOrFail();

            $detailOutlet->update([
                'merek' => !empty($request->merek) ? $request->merek : $beforeData->merek,
                'unit_ke' => !empty($request->unit_ke) ? $request->unit_ke : $beforeData->unit_ke,
                'type' => !empty($request->type) ? $request->type : $beforeData->type,
                'remark' => !empty($request->remark) ? $request->remark : $beforeData->remark,
                'no_quotation' => !empty($request->no_quotation) ? $request->no_quotation : $beforeData->no_quotation,
                // 'status_quotation' => !empty($request->status_quotation) ? $request->status_quotation : null,
                'no_po' => !empty($request->no_po) ? $request->no_po : $beforeData->no_po,
                'no_invoice' => !empty($request->no_invoice) ? $request->no_invoice : $beforeData->no_invoice,
                'nominal_quot' => !empty($request->nominal_quot) ? $request->nominal_quot : $beforeData->nominal_quot,
                'tanggal_pembayaran' => !empty($request->tanggal_pembayaran) ? $request->tanggal_pembayaran : null,
                'tanggal_quot' => !empty($request->tanggal_quot) ? $request->tanggal_quot : null,
                'tanggal_po' => !empty($request->tanggal_po) ? $request->tanggal_po : null,
                'tanggal_Invoice' => !empty($request->tanggal_Invoice) ? $request->tanggal_Invoice : null,
                'tanggal_pengerjaan' => !empty($request->tanggal_pengerjaan) ? $request->tanggal_pengerjaan : null,
                'imei' => !empty($request->imei) ? $request->imei : null,
                'serial_number' => !empty($request->serial_number) ? $request->serial_number : null,
                // 'file_berita)acara' => $file_berita_acara_name,
                // 'file_quot' => $file_quot_name,
                // 'file_po' => $file_po_name,
                // 'file_invoice' => $file_invoice_name,
                'uniq_key' => $uniq_key,
                'user_updated' => Auth::id()
            ]);

            if(!empty($request->tanggal_pengerjaan)){
                $detailOutlet->update([
                    'status_quotation' => 'Waiting Payment'
                ]);
            }

            // $updateStatusRequestOrder = RequestOrders::where('ms_transaction_b2b_detail', $detailOutlet->is_parent_b2b_outlet_id)->update([
            //     'status_quotation' => $request->status_quotation,
            // ]);

            if (!empty($request->file('file_berita_acara'))){
                $imageBeritaAcara = ImageBeritaAcara::create([
                    'image_berita_acara' => $file_berita_acara_name,
                    'type' => 'file_berita_acara',
                    'ms_b2b_detail' => $detailOutlet->id,
                ]);
            }

            if (!empty($request->file('file_invoice'))) {
                $imageInvoice = ImageInvoice::create([
                    'image_invoice' => $file_invoice_name,
                    'type' => 'image_invoice',
                    'ms_b2b_detail' => $detailOutlet->id,
                ]);
            }
            

            if (!empty($request->file('file_po'))) {
                $imagePurchaseOrder = ImagePurchaseOrder::create([
                    'image_po' => $file_po_name,
                    'type' => 'image_po',
                    'ms_b2b_detail' => $detailOutlet->id,
                ]);
            }
           

            if (!empty($request->file('file_quot'))) {
                $imageQuotation = ImageQuotation::create([
                    'image_quot' => $file_quot_name,
                    'type' => 'image_quot',
                    'ms_b2b_detail' => $detailOutlet->id,
                ]);
            }
           

            $newData = BusinessToBusinessOutletDetailTransaction::with('user_create', 'user_update', 'business_to_business_outlet_transaction.business_to_business_transaction.company')
                ->where('id', $detailOutlet->id)
                ->firstOrFail();

            BusinessToBusinessTransactionLog::create([
                'no_transaksi' => $newData->business_to_business_outlet_transaction->business_to_business_transaction->no_transaksi,
                'playload_before' => json_encode($beforeData),
                'playload_after' => json_encode($newData),
                'user_id' => Auth::id(),
                'business_to_business_transaction_id' => $newData->business_to_business_outlet_transaction->business_to_business_transaction_id,
                'business_to_business_outlet_transaction_id' => $newData->business_to_business_outlet_transaction->id,
                'business_to_business_outlet_detail_transaction_id' => $newData->id,
                'log_type' => 'edit',
                'log_modul' => 'outlet_detail'
            ]);

            // if($request->status_quotation == 'Process'){
            //     $cekFileQuot = ImageQuotation::where('ms_b2b_detail', $detailOutlet->id)->count();
            //     if(!empty($request->tanggal_quot)){
            //         if($cekFileQuot > 0){
            //             DB::commit();
            //         }else{
            //             DB::rollback();
            //             throw new SendErrorMsgException('File Quotation Is Required');
            //         }
            //     }else{
            //         DB::rollback();
            //         throw new SendErrorMsgException('Tanggal Quotation Is Required');
            //     }
            // }

            // if($request->status_quotation == 'Scheduling'){
            //     $cekpo = ImagePurchaseOrder::where('ms_b2b_detail', $detailOutlet->id)->count();
            //     if(
            //         !empty($request->tanggal_po) && 
            //         !empty($request->tanggal_pengerjaan  && 
            //         $cekpo > 0
            //     )){
            //         DB::commit();
            //     }else{
            //         DB::rollback();
            //         throw new SendErrorMsgException('Tanggal Po, Upload File PO & Tanggal Pengerjaan Is Required');
            //     }
            // }

            // if($request->status_quotation == 'Finished'){
            //     $cekBeritaAcara = ImageBeritaAcara::where('ms_b2b_detail', $detailOutlet->id)->count();
                
            //     if($cekBeritaAcara > 0){
            //         DB::commit();
            //     }else{
            //         DB::rollback();
            //         throw new SendErrorMsgException('File Berita Acara Is Required');
            //     }
            // }

            // if($request->status_quotation == 'Waiting Payment'){
            //     $cekInv = ImageInvoice::where('ms_b2b_detail', $detailOutlet->id)->count();
            //     if($cekInv > 0){
            //         DB::commit();
            //     }else{
            //         DB::rollback();
            //         throw new SendErrorMsgException('File Invoice & Tanggal Invoice Is Required');
            //     }
            // }

            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            throw new SendErrorMsgException($e->getMessage());
        }

        return $this->successResponse(
            $newData,
            $this->successStoreMsg(),
            201
        );
    }

    public function changeStatus(Request $request, BusinessToBusinessOutletDetailTransaction $detailOutlet)
    {
        $ubahStatus = $detailOutlet->update([
            'status_quotation' => !empty($request->status_quotation) ? $request->status_quotation : null,
        ]);

        return $this->successResponse(
            $ubahStatus,
            $this->successStoreMsg(),
            201
        );
    }

    public function parentOutletUpdate(Request $request, BusinessToBusinessOutletDetailTransaction $detailOutlet)
    {
        // dd('asd');
        $uniq_key = $detailOutlet->uniq_key;

        $file_berita_acara_name =  $detailOutlet->file_berita_acara;
        $file_berita_acara      = $request->file('file_berita_acara');
        if ($file_berita_acara) {
            $file_berita_acara_name = rand() . $file_berita_acara->getClientOriginalName();
            $file_berita_acara->storeAs('public/btb/media/', $file_berita_acara_name);

            Storage::delete('public/btb/media/' . $detailOutlet->file_berita_acara);
        }

        $file_quot_name = $detailOutlet->file_quot;
        $file_quot      = $request->file('file_quot');
        if ($file_quot) {
            $file_quot_name = rand() . $file_quot->getClientOriginalName();
            $file_quot->storeAs('public/btb/media/', $file_quot_name);

            Storage::delete('public/btb/media/' . $detailOutlet->file_quot);
        }

        $file_po_name = $detailOutlet->file_po;
        $file_po      = $request->file('file_po');
        if ($file_po) {
            $file_po_name = rand() . $file_po->getClientOriginalName();
            $file_po->storeAs('public/btb/media/', $file_po_name);

            Storage::delete('public/btb/media/' . $detailOutlet->file_po);
        }

        $file_invoice_name = $detailOutlet->file_invoice;
        $file_invoice      = $request->file('file_invoice');
        if ($file_invoice) {
            $file_invoice_name = rand() . $file_invoice->getClientOriginalName();
            $file_invoice->storeAs('public/btb/media/', $file_invoice_name);

            Storage::delete('public/btb/media/' . $detailOutlet->file_invoice);
        }

        // return $file_quot_name;
        // if($request->status_quotation == 'Processing'){
            
        // }

        DB::beginTransaction();
        try {
            $beforeData = BusinessToBusinessOutletDetailTransaction::with('user_create', 'user_update', 'business_to_business_outlet_transaction.business_to_business_transaction.company')
                ->where('id', $detailOutlet->id)
                ->firstOrFail();

            $detailOutlet->update([
                'merek' => !empty($request->merek) ? $request->merek : $beforeData->merek,
                'unit_ke' => !empty($request->unit_ke) ? $request->unit_ke : $beforeData->unit_ke,
                'remark' => !empty($request->remark) ? $request->remark : $beforeData->remark,
                'no_quotation' => !empty($request->no_quotation) ? $request->no_quotation : $beforeData->no_quotation,
                'status_quotation' => !empty($request->status_quotation) ? $request->status_quotation : null,
                'no_po' => !empty($request->no_po) ? $request->no_po : $beforeData->no_po,
                'no_invoice' => !empty($request->no_invoice) ? $request->no_invoice : $beforeData->no_invoice,
                'nominal_quot' => !empty($request->nominal_quot) ? $request->nominal_quot : $beforeData->nominal_quot,
                'tanggal_pembayaran' => !empty($request->tanggal_pembayaran) ? $request->tanggal_pembayaran : null,
                'tanggal_quot' => !empty($request->tanggal_quot) ? $request->tanggal_quot : null,
                'tanggal_po' => !empty($request->tanggal_po) ? $request->tanggal_po : null,
                'tanggal_Invoice' => !empty($request->tanggal_Invoice) ? $request->tanggal_Invoice : null,
                'tanggal_pengerjaan' => !empty($request->tanggal_pengerjaan) ? $request->tanggal_pengerjaan : null,
                // 'file_berita)acara' => $file_berita_acara_name,
                // 'file_quot' => $file_quot_name,
                // 'file_po' => $file_po_name,
                // 'file_invoice' => $file_invoice_name,
                'uniq_key' => $uniq_key,
                'user_updated' => Auth::id()
            ]);

            // $updateStatusRequestOrder = RequestOrders::where('ms_transaction_b2b_detail', $detailOutlet->is_parent_b2b_outlet_id)->update([
            //     'status_quotation' => $request->status_quotation,
            // ]);

            if (!empty($request->file('file_berita_acara'))){
                $imageBeritaAcara = ImageBeritaAcara::create([
                    'image_berita_acara' => $file_berita_acara_name,
                    'type' => 'file_berita_acara',
                    'ms_b2b_detail' => $detailOutlet->id,
                ]);
            }

            if (!empty($request->file('file_invoice'))) {
                $imageInvoice = ImageInvoice::create([
                    'image_invoice' => $file_invoice_name,
                    'type' => 'image_invoice',
                    'ms_b2b_detail' => $detailOutlet->id,
                ]);
            }
            

            if (!empty($request->file('file_po'))) {
                $imagePurchaseOrder = ImagePurchaseOrder::create([
                    'image_po' => $file_po_name,
                    'type' => 'image_po',
                    'ms_b2b_detail' => $detailOutlet->id,
                ]);
            }
           

            if (!empty($request->file('file_quot'))) {
                $imageQuotation = ImageQuotation::create([
                    'image_quot' => $file_quot_name,
                    'type' => 'image_quot',
                    'ms_b2b_detail' => $detailOutlet->id,
                ]);
            }
           

            $newData = BusinessToBusinessOutletDetailTransaction::with('user_create', 'user_update', 'business_to_business_outlet_transaction.business_to_business_transaction.company')
                ->where('id', $detailOutlet->id)
                ->firstOrFail();

            BusinessToBusinessTransactionLog::create([
                'no_transaksi' => $newData->business_to_business_outlet_transaction->business_to_business_transaction->no_transaksi,
                'playload_before' => json_encode($beforeData),
                'playload_after' => json_encode($newData),
                'user_id' => Auth::id(),
                'business_to_business_transaction_id' => $newData->business_to_business_outlet_transaction->business_to_business_transaction_id,
                'business_to_business_outlet_transaction_id' => $newData->business_to_business_outlet_transaction->id,
                'business_to_business_outlet_detail_transaction_id' => $newData->id,
                'log_type' => 'edit',
                'log_modul' => 'outlet_detail'
            ]);

            
            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            throw new SendErrorMsgException($e->getMessage());
        }

        return $this->successResponse(
            $newData,
            $this->successStoreMsg(),
            201
        );
    }

    public function getUnitAvailable($id){
        $header = BusinessToBusinessOutletTransaction::with('business_to_business_outlet_detail_transactions')
            ->where("id", $id)
            ->firstOrFail();

        $hitung = $header->business_to_business_outlet_detail_transactions->count();
            
        $jumlah_unit = $header->jumlah_unit;
        if($hitung > 0 ){
            foreach($header->business_to_business_outlet_detail_transactions as $detail){
                $tidak_ada[] = $detail->unit_ke;
            }
            $range = range(1, $jumlah_unit);
            $available = [];
            foreach($range as $no){
                if(!in_array($no, $tidak_ada)){
                    $available[] = $no;
                }            
            }
        }else{
            $i;
            $available= [];
            for ($i = 1; $i <= $header->jumlah_unit; $i++) {
                $available[] = $i;
            }
        }
        return $this->successResponse(
            $available,
            $this->successStoreMsg(),
            201
        );
    }

    public function destroy(Request $request, BusinessToBusinessTransaction $btb)
    {
        DB::beginTransaction();
        try {
            $beforeData = BusinessToBusinessTransaction::with('user_create', 'user_update', 'company')->where('id', $btb->id)->firstOrFail();

            $btb->update([
                'is_deleted' => 1
            ]);

            BusinessToBusinessTransactionLog::create([
                'no_transaksi' => $beforeData->no_transaksi,
                'playload_before' => json_encode($beforeData),
                'user_id' => Auth::id(),
                'business_to_business_transaction_id' => $beforeData->id,
                'log_type' => 'delete',
                'log_modul' => 'header'
            ]);

            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            throw new SendErrorMsgException($e->getMessage());
        }

        return $this->successResponse(
            $beforeData,
            $this->successStoreMsg(),
            204
        );
    }

    public function destroyOutlet(Request $request, BusinessToBusinessOutletTransaction $headerOurlet)
    {
        DB::beginTransaction();
        try {
            $beforeData = BusinessToBusinessOutletTransaction::with('user_create', 'user_update', 'business_to_business_transaction.company')->where('id', $headerOurlet->id)->firstOrFail();

            $headerOurlet->delete();

            BusinessToBusinessTransactionLog::create([
                'no_transaksi' => $beforeData->business_to_business_transaction->no_transaksi,
                'playload_before' => json_encode($beforeData),
                'user_id' => Auth::id(),
                'business_to_business_transaction_id' => $beforeData->business_to_business_transaction_id,
                'business_to_business_outlet_transaction_id' => $beforeData->id,
                'log_type' => 'delete',
                'log_modul' => 'outlet'
            ]);

            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            throw new SendErrorMsgException($e->getMessage());
        }

        return $this->successResponse(
            $beforeData,
            $this->successStoreMsg(),
            204
        );
    }

    public function destroyOutletDetail(Request $request, BusinessToBusinessOutletDetailTransaction $detailOutlet)
    {
        DB::beginTransaction();
        try {
            $beforeData = BusinessToBusinessOutletDetailTransaction::with('user_create', 'user_update', 'business_to_business_outlet_transaction.business_to_business_transaction.company')
                ->where('id', $detailOutlet->id)
                ->firstOrFail();

            $detailOutlet->delete();

            BusinessToBusinessTransactionLog::create([
                'no_transaksi' => $beforeData->business_to_business_outlet_transaction->business_to_business_transaction->no_transaksi,
                'playload_before' => json_encode($beforeData),
                'user_id' => Auth::id(),
                'business_to_business_transaction_id' => $beforeData->business_to_business_outlet_transaction->business_to_business_transaction_id,
                'business_to_business_outlet_transaction_id' => $beforeData->business_to_business_outlet_transaction->id,
                'business_to_business_outlet_detail_transaction_id' => $beforeData->id,
                'log_type' => 'delete',
                'log_modul' => 'outlet_detail'
            ]);

            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            throw new SendErrorMsgException($e->getMessage());
        }

        $this->deleteMediaDetail($beforeData);

        return $this->successResponse(
            $beforeData,
            $this->successStoreMsg(),
            204
        );
    }

    public function createNomorTransaksi()
    {
        $btb = BusinessToBusinessTransaction::orderBy('id', 'desc')->first();

        // jika tidak ada data
        if ($btb == null) {
            $id = 1;
        } else {
            // jika ada data
            $no_transaksi = explode('-', $btb->no_transaksi);
            $id = abs($no_transaksi[1]) + 1;
        }

        // dapatkan tahun
        $year = date("y");

        $no_trx = $year . '-';

        $length = strlen($id);
        switch ($length) {
            case 1:
                $no_trx .= '00' . $id;
                break;

            case 2:
                $no_trx .= '0' . $id;
                break;

            default:
                $no_trx .= $id;
                break;
        }

        return $no_trx;
    }

    public function deleteMediaDetail($row)
    {
        if ($row->file_berita_acara != null) {
            Storage::delete('public/btb/media/' . $row->uniq_key . '/' . $row->file_berita_acara);
        }

        if ($row->file_quot != null) {
            Storage::delete('public/btb/media/' . $row->uniq_key . '/' . $row->file_quot);
        }

        if ($row->file_po != null) {
            Storage::delete('public/btb/media/' . $row->uniq_key . '/' . $row->file_po);
        }

        if ($row->file_invoice != null) {
            Storage::delete('public/btb/media/' . $row->uniq_key . '/' . $row->file_invoice);
        }
    }
}
