<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Http\Requests\UserLoginRequest;
use App\Http\Requests\UserRegisterRequest;
use App\Services\UserService;
use App\User;
use App\UserB2b;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class AuthController extends ApiController
{
    /**
     * registrasi user.
     */
    public function register(UserService $userService, UserRegisterRequest $request)
    {
        // create user
        $user = $userService->create($request->all());

        // generate token
        $token = $user->createToken($userService->getTokenName())->accessToken;

        return $this->successResponse(
            ['token' => $token, 'user' => $user],
            'registrasi sukses'
        );
    }

    /**
     * login user.
     */
    public function login(UserLoginRequest $request, UserService $userService)
    {
        return 'sad';
        if ($userService->login($request->all())) {
            // get user login
            $user = $userService->getUser();

            // check user verify
            if ($userService->isVerify() == false) {
                // send response
                return $this->errorResponse($user, 'your account has not been validated');
            }

            // get access token
            $token = $user->createToken($userService->getTokenName())->accessToken;

            // send response
            return $this->successResponse([
                'token' => $token,
                'user' => $user,
            ], 'login sukses');
        }

        // send response
        return $this->errorResponse(
            [],
            'login gagal',
            400
        );
    }


    public function sendPasswordResetEmail(Request $request)
    {
        $service = new UserService();

        $user = User::where('email', '=', $request->email)->first();

        if ($user === null) {
            // send response
            return $this->errorResponse(
                [],
                'email not exists',
                400
            );
        }

        $service->sentPasswordReset($user, $request);

        // send response
        return $this->successResponse([], 'email success send');
    }

    public function bypassLoginSocialMedia(Request $request, UserService $userService)
    {
        $authUser = User::where('email', $request->email)->first();
        if ($authUser == null) {
            $customerRole = Role::where('name', 'Customer')->first();
            $username = explode('@', $request->email);
            $user = User::create([
                'name'     => $username[0],
                'email_verified_at' => date('Y-m-d H:i:s'),
                'email'    => $request->email,
                'provider' => $request->provider,
                'provider_id' => $request->provider_id,
                'is_social_login' => 1
            ]);

            $user->assignRole($customerRole->id);
        } else {
            $user = User::where('email', $request->email)
                ->where('provider_id', $request->provider_id)
                ->where('provider', $request->provider)
                ->first();

            if ($user == null) {
                return $this->errorResponse(
                    [
                        'token' => '',
                        'user' => null
                    ],
                    'Email has already been use, please login'
                );
            }
        }

        // generate token
        $token = $user->createToken($userService->getTokenName())->accessToken;

        return $this->successResponse(
            ['token' => $token, 'user' => $user],
            'ok',
            200
        );
    }
}
