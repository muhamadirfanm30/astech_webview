<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Services\OtpService;
use App\Services\UserService;
use App\User;
use Illuminate\Auth\Events\Verified;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Http\Request;
use Auth;

class VerificationController extends Controller
{
    /**
     * Where to redirect users after verification.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
        $this->middleware('signed')->only('verify');
        $this->middleware('throttle:60,1')
            ->only('verify', 'resend', 'otpResend');
    }

    /**
     * Mark the authenticated user's email address as verified.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function verify(Request $request)
    {
        $user = User::where('api_token', $request->token)->first();
        if ($user == null) {
            return redirect('/');
        }

        $valid = false;
        if ($user->hasVerifiedEmail()) {
            $valid = true;
        }

        if ($user != null && $user->markEmailAsVerified()) {
            $valid = true;
        }

        if($valid){
            if(Auth::check()){
                if ($user->hasRole('Customer')) {
                    if ($user->is_login_as_teknisi == 1) {
                        return redirect(route('user.info'));
                    }
                    if ($user->is_login_as_teknisi == null) {
                        return redirect(route('users.address.create'));
                    }
                    return redirect(route('user.home'))->with('success', 'Your Verification Has Been Success');
                }
                if ($user->hasRole('Technician')) {
                    return redirect(route('teknisi.home'))->with('success', 'Your Verification Has Been Success');
                }
            }else{
                return redirect('/')->with('success', 'Your Verification Has Been Success');
            }
        }
    }

    public function old_verify(Request $request)
    {
        $user = $request->user();

        if ($request->user()->hasVerifiedEmail()) {

            if (Auth::user()->hasRole('Customer')) {
                if (Auth::user()->is_login_as_teknisi == 1) {
                    return redirect(route('user.info'));
                }
                if (Auth::user()->is_login_as_teknisi == null) {
                    return redirect(route('users.address.create'));
                }
                return redirect(route('user.home'))->with('success', 'akun sudah diverifikasi');
            }
            if (Auth::user()->hasRole('Technician')) {
                return redirect(route('teknisi.home'))->with('success', 'akun sudah diverifikasi');
            }
        }

        if ($request->route('id') == $user->getKey() && $user->markEmailAsVerified()) {
            // return redirect('/')->with('success', 'Your Verification Has Been Success');
            if (Auth::user()->hasRole('Customer')) {
                if (Auth::user()->is_login_as_teknisi == 1) {
                    return redirect(route('user.info'));
                }
                if (Auth::user()->is_login_as_teknisi == null) {
                    return redirect(route('users.address.create'));
                }
                return redirect(route('user.home'))->with('success', 'Your Verification Has Been Success');
            }
            if (Auth::user()->hasRole('Technician')) {
                return redirect(route('teknisi.home'))->with('success', 'Your Verification Has Been Success');
            }
        }
    }

    /**
     * Resend the email verification notification.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function resend(Request $request)
    {
        if ($request->user()->hasVerifiedEmail()) {
            // User already have verified email!
            return redirect('/home');
        }

        $service = new UserService();

        // send mail
        $service->sendEmailVerification($request->user());

        // The notification has been resubmitted
        return redirect()->back()->with('success', 'email berhasil dikirim kembali');
    }

    /**
     * verify otp token
     */
    public function otpVerify(Request $request)
    {
        $user = auth()->user();

        $otp = new OtpService();

        $token = str_replace(' ', '', $request->otp);

        if ($otp->verify($token, $user->otp_url)) {
            $user->update([
                'otp_verified_at' => now()
            ]);

            return redirect()->back()->with('success', 'Success Verifications');
        }
        return redirect()->back()->with('error', 'Verification Error');
    }

    public function otpVerifyAjax(Request $request)
    {
        $user = auth()->user();

        $otp = new OtpService();

        $token = str_replace(' ', '', $request->otp);

        if ($otp->verify($token, $user->otp_url)) {
            $user->update([
                'otp_verified_at' => now()
            ]);

            return response()->json([
                'msg' => 'success',
            ], 200);
        }
        return response()->json([
            'msg' => 'error',
        ], 400);
    }



    /**
     * resend otp token
     */
    public function otpResend(Request $request)
    {
        $user = auth()->user();

        $otp = (new OtpService())->create();

        $otp->setLabel($user->id);

        $token = $otp->now();

        $url = $otp->getProvisioningUri();

        // $nexmo = (new OtpService())->sendSMS([
        //     'to' => $user->phone,
        //     'text' => $token,
        // ]);

        $user->update([
            'otp_code' => $token,
            'otp_url' => $url,
        ]);

        return redirect()->back()->with('success', 'kode berhasil dikirim kembali');
    }
}
