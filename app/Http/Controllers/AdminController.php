<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Menu;
use App\Events\OrderEvent;
use App\Models\Master\Chat;
use App\Models\Master\Order;
use App\Models\Notification;
use Illuminate\Http\Request;
use App\Models\Master\EmailOther;
use App\Models\Master\WebSetting;
use App\Models\Master\ChatMessage;
use App\Models\Master\HistoryOrder;
use App\Models\Master\OrdersStatus;
use App\Models\Master\ProductGroup;
use App\Http\Controllers\Controller;
use App\Models\Master\GeneralSetting;
use App\Models\Master\MsTicketDetail;
use App\Models\Technician\Technician;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Route;
use App\Models\Master\EmailStatusOrder;
use App\Http\Controllers\ApiController;
use App\Models\Master\EwalletHistory;
use App\Models\Master\MsOrderComplaint;
use App\Services\Master\GaransiChatService;
use App\Models\Master\ProductDetailInformation;
use App\User;
use App\Models\RequestOrders;

class AdminController extends ApiController
{
    public $breadcrumbs = [];

    public function viewAdmin($file, $data = [])
    {
        $this->setData($data);
        return view($file, $this->getData());
    }

    public function getDefaulData(): array
    {
        $user = Auth::user();
        return [
            'title' => 'List Menu Management',
            'role_prefix' => \Request::segment(1),
            'menus' => $user->hasRole(['Customer', 'Technician']) ? '' : $this->getMenu(),
            'breadcrumbs' => $this->getBreadcrumb(),
            'isActive' => Route::getFacadeRoot()->current()->uri(),
            'setting' => $this->getSetting(),
            'notifications' => Notification::readAndUnread(),
            'new_notification' => Notification::issetNew(),
            'unread_count' => Notification::unreadCount(Auth::id()),
            'chat_count_notif' => $user->hasRole('admin') ? 0 : $this->getUnreadChatCount(),
            'count_new_order' => $this->getCountOrder($user),
            'count_new_ticket' => $this->getCountNewTicket($user),
            'count_garansi_customer' => $this->getGaransiCustomerAvailable($user),
            'count_garansi_teknisi' => $this->getGaransiTeknisiAvailable($user),
            'garansi_teknisi' => $this->getGaransiTeknisi($user),
            'count_waiting_review' => $this->getCountWaitingReview($user),
            'count_complaint_order' => $this->getCountComplaint($user),
            'google_maps_key_setting' => GeneralSetting::where('id', 25)->value('value'),
            'title_header_global' => WebSetting::value('title_setting'),
            'metta_header_global' => WebSetting::value('metta_setting'),
            'get_logo_astech' => WebSetting::value('logo'),
        ];
    }

    public function getCountComplaint($user)
    {
        if ($user->hasRole('Customer')) {
            return MsOrderComplaint::where('user_id', '!=', Auth::id())
                ->whereNull('read_at')
                ->groupBy('ms_orders_id')
                ->whereHas('order', function ($query) {
                    $query->where('users_id', Auth::id())->where('orders_statuses_id', 11);
                })
                ->count();
        }

        if ($user->hasRole('Technician')) {
            $teknisi = Technician::where('user_id', $user->id)->firstOrFail();
            return MsOrderComplaint::where('user_id', '!=', Auth::id())
                ->whereNull('read_at')
                ->groupBy('ms_orders_id')
                ->whereHas('order.detail', function ($query) use ($teknisi) {
                    $query->where('technicians_id', $teknisi->id)->where('orders_statuses_id', 11);
                })
                ->count();
        }
    }

    public function getCountWaitingReview($user)
    {
        if ($user->hasRole('Customer')) {
            return Order::whereNull('rating_and_review_id')
                ->where('orders_statuses_id', 10)
                ->where('users_id', $user->id)
                ->count();
        }

        return 0;
    }

    public function getCountNewTicket($user)
    {
        if ($user->hasRole('Customer')) {
            return MsTicketDetail::where('user_id', '!=', Auth::id())
                ->whereNull('read_at')
                ->groupBy('ms_ticket_id')
                ->count();
        }

        return 0;
    }

    public function getGaransiCustomerAvailable($user)
    {
        if ($user->hasRole('Customer')) {
            return (new GaransiChatService)->totalAvailable(function ($o) use ($user) {
                $o->where('users_id', $user->id);
            });
        }

        return 0;
    }

    public function getGaransiTeknisiAvailable($user)
    {
        if ($user->hasRole('Technician')) {
            $teknisi = Technician::where('user_id', $user->id)->firstOrFail();
            return Order::whereNull('teknisi_read_at')
                ->whereHas('garansi', function ($query) {
                    $query->where('status', '!=', 'done')->where('customer_can_reply', 1);
                })
                ->whereHas('service_detail.technician', function ($query) use ($teknisi) {
                    $query->where('technicians_id', $teknisi->id);
                })->count();
        }

        return 0;
    }

    public function getGaransiTeknisi($user)
    {
        if ($user->hasRole('Technician')) {
            $teknisi = Technician::where('user_id', $user->id)->firstOrFail();
            return Order::whereHas('garansi', function ($query) {
                $query->where('status', '!=', 'done')->where('customer_can_reply', 1);
            })
                ->whereHas('service_detail.technician', function ($query) use ($teknisi) {
                    $query->where('technicians_id', $teknisi->id);
                })->first();
        }

        return null;
    }

    public function getCountOrder($user)
    {
        $jumNOtif = 0;
        if ($user->hasRole('Customer')) {
            $jumNOtif = Order::where('users_id', $user->id)
                ->where("notifikasi_type", 1)
                ->whereNull('customer_read_at')
                ->count();
        }

        if ($user->hasRole('Technician')) {
            $teknisi = Technician::where('user_id', $user->id)->firstOrFail();
            $jumNOtif = Order::where("notifikasi_type", 1)->whereNull('teknisi_read_at')
                ->whereHas('service_detail.technician', function ($query) use ($teknisi) {
                    $query->where('technicians_id', $teknisi->id);
                })->count();
        }

        return $jumNOtif;
    }

    public function getUnreadChatCount()
    {
        return Chat::whereHas('members_group', function ($query) {
            $query->where('user_id', Auth::id());
        })->whereHas('messages', function ($query) {
            $query->whereNull('read_at')->where('user_id', '!=', Auth::id());
        })->count();
    }

    public function getSetting()
    {
        $showData = WebSetting::first();
        return $showData;
    }

    public function getMenu()
    {
        $topUpsheetCount = EwalletHistory::where('payment_status', 'processing')->count();
        
        $countNotif = '';
        $newBadge = '';
        $jumNOtif = Order::whereNull('admin_read_at')->count();
        if ($jumNOtif > 0) {
            $countNotif = "<span class='badge badge-pill badge-warning' style='border-radius: 0px;padding: 5px;'>" . $jumNOtif . "</span>";
        }

        

        $countNotiGaransi = '';
        $jumNOtifGaransi = Order::whereNull('admin_read_at')->whereHas('garansi')->count();
        if ($jumNOtifGaransi > 0) {
            $countNotiGaransi = "<span class='badge badge-pill badge-warning' style='border-radius: 0px;padding: 5px;'>" . $jumNOtifGaransi . "</span>";
        }

        $ticketCount = '';
        $jumTicket = MsTicketDetail::where('user_id', '!=', Auth::id())->whereNull('read_at')->distinct('ms_ticket_id')->count();
        if ($jumTicket > 0) {
            $ticketCount = "<span class='badge badge-pill badge-warning' style='border-radius: 0px;padding: 5px;'>New</span> <span class='badge badge-pill badge-success' style='border-radius: 0px;padding: 5px;'>" . $jumTicket . "</span>";
        }

        $complainCount = '';
        $jumComplain = Order::whereHas('complaints', function ($query) {
            $query->whereNull('read_at');
        })
            ->where('orders_statuses_id', 11)
            ->where('users_id', '!=', Auth::id())
            ->count();
        if ($jumComplain > 0) {
            $complainCount = "<span class='badge badge-pill badge-warning' style='border-radius: 0px;padding: 5px;'>" . $jumComplain . "</span>";
        }

        if (($jumNOtif + $jumComplain) > 0) {
            $newBadge = "<span class='badge badge-pill badge-warning' style='border-radius: 0px;padding: 5px;'>New</span>";
        }

        $pendingTeknisi = User::with('address')
            ->where('is_login_as_teknisi', 1)
            ->where('status', 7)
            ->whereNotNull('email_verified_at')
            ->whereNotNull('phone')
            ->count();

        $pendingTeknisiCount = '';
        if ($pendingTeknisi > 0) {
            $pendingTeknisiCount = "<span class='badge badge-pill badge-warning' style='border-radius: 0px;padding: 5px;'>" . $pendingTeknisi . "</span>";
            // $pendingTeknisiCount = "<span class='badge badge-pill badge-warning' style='border-radius: 0px;padding: 5px;'></span>";
        }

        $b2bCountExpand = '';
        $jumCountB2b = RequestOrders::whereNull('read_at')->count();
        if ($jumCountB2b > 0) {
            $b2bCountExpand = "<span class='badge badge-pill badge-warning' style='border-radius: 0px;padding: 5px;'>" . $jumCountB2b . "</span>";
        }
        
        $b2bParentNew = '';
        $countNewB2b = RequestOrders::whereNull('read_at')->count();
        if ($countNewB2b > 0) {
            $b2bParentNew = "<span class='badge badge-pill badge-warning' style='border-radius: 0px;padding: 5px;'> New</span>";
        }

        $that = $this;
        $seconds = 604800; // 1 week
        // $menus = Cache::remember('menus', $seconds, function () use ($that) {
        //     $showData = Menu::orderBy('display_order')->get();
        //     return $that->getHTML($showData);
        // });

        $showData = Menu::orderBy('display_order')->get();
        $menus = $this->getHTML($showData);
        
        // $b2bCountExpand, $b2bParentNew,
        // '{{b2b-order-count-notif}}',  '{{parent-b2b-countf}}',
        
        return str_replace(
            [ '{{b2b-order-count-notif}}','{{parent-b2b-countf}}','{{topUpsheetCount-notif}}', '{{count-notif}}', '{{new-notif}}', '{{ticket-count-notif}}', '{{count-notif-garansi}}', '{{complain-count-notif}}', '{{teknisi-count-notif}}'],
            [ $b2bCountExpand, $b2bParentNew, $topUpsheetCount, $countNotif, $newBadge, $ticketCount, $countNotiGaransi, $complainCount, $pendingTeknisiCount],
            $menus
        );
    }

    public function setBreadcrumb($name, $link)
    {
        $this->breadcrumbs[] = [
            'name' => $name,
            'link' => $link,
        ];

        return $this;
    }

    public function getBreadcrumb()
    {
        return array_merge([
            [
                'name' => 'home',
                'link' => '/admin/home',
            ]
        ], $this->breadcrumbs);
    }

    public function buildMenu($menu, $parentid = null)
    {
        $getUser = User::where('id', Auth::id())->first();
        $result = null;
        $uri = \Request::segment(2);
        $tampil_menu = false;
        foreach ($menu as $key => $item) {
            $active = false;
            if ($item->module === $uri) {
                $active = true;
            }

            if ($item->parent_id == $parentid) {

                if ($item->permission_item != null) {
                    $listPermission = array_map('intval', explode(',', $item->permission_item ));
                    if ( $getUser->hasAnyPermission($listPermission) ) {
                        $tampil_menu = true;
                    }
                }else{
                    if ($getUser->hasPermissionTo($item->permission_id)) {
                        $tampil_menu = true;
                    }
                }
                

                if ($item->parent_id == null) {
                    $result .="<li class='app-sidebar__heading'></li>";
                }
                // ======================
                if ($tampil_menu) {
                    $topUpsheetCount = '';
                    if ($item->url == '/admin/topup/show-list') {
                        $topUpsheetCount = '{{new-notif}}';
                    }

                    $newBadge = '';
                    if ($item->name == 'List Transactions') {
                        $newBadge = '{{new-notif}}';
                    }

                    $countNotif = '';
                    if ($item->url == '/admin/transactions/show') {
                        $countNotif = '{{count-notif}}';
                    }

                    $ticketCount = '';
                    if ($item->url == 'admin/tickets') {
                        $ticketCount = '{{ticket-count-notif}}';
                    }

                    $countNotifGaransi = '';
                    if ($item->url == '/admin/klaim-garansi/show') {
                        $countNotifGaransi = '{{count-notif-garansi}}';
                    }

                    $countNotifTeknisi = '';
                    if ($item->url == '/admin/all-technician/show' || $item->id == 7) {
                        $countNotifTeknisi = '{{teknisi-count-notif}}';
                    }

                    $countNotifComplain = '';
                    if ($item->url == '/admin/transaction-complaint/show') {
                        $countNotifComplain = '{{complain-count-notif}}';
                    }

                    $countNotifB2bOrder = '';
                    if ($item->url == '/admin/business-to-business-user-request-order/show') {
                        $countNotifB2bOrder = '{{b2b-order-count-notif}}';
                    }

                    $showNewOrderB2b = '';
                    if ($item->id == 106) {
                        $showNewOrderB2b = '{{parent-b2b-countf}}';
                    }
                    // . $countNotifB2bOrder . $showNewOrderB2b

                    $combinasi = $item->name .  $countNotifB2bOrder . ' ' . $showNewOrderB2b . ' ' . $newBadge . ' ' . $countNotif . $ticketCount  . $countNotifGaransi . $countNotifComplain . $countNotifTeknisi. $topUpsheetCount;
                    $tampil_menu = false;
                    $result .=
                        "<li class='" . ($active ? "mm-active" : "") . "'>
                            <a href='" . url($item->url) . "' style='height:2.5rem;' class='" . ($active ? "mm-active" : "") . "'>
                                <i class = '" . ($item->parent_id == null ? "metismenu-icon" : "") . " " . $item->icon . "'></i>&nbsp;&nbsp;" . $combinasi;
                    if ($this->checkIsParent($item->id)) {
                        $result .=
                            "<i class='metismenu-state-icon fa fa-chevron-up fa fa-chevron-down'></i>";
                    }
                    $result .=
                        "</a>
                " . $this->buildMenu($menu, $item->id);
                }

                // ======================
            }
        }
        return $result ?  ($parentid == null ? $result . "</li>" :  "<ul>" . $result . "</ul></li>") : null;
    }

    public function checkIsParent($id)
    {
        $find = Menu::where('parent_id', $id)->count();
        if ($find > 0) {
            return true;
        }
        return false;
    }

    // Getter for the HTML menu builder
    public function getHTML($items)
    {
        return $this->buildMenu($items);
    }

    public function saveHistoryOrder($order_id, $status_id)
    {
        $query = HistoryOrder::where('orders_id', $order_id)->where('orders_statuses_id', $status_id);
        if ($query->count() > 0) {
            return $query = $query->update(['created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);
        } else {
            return HistoryOrder::create([
                'orders_id'          => $order_id,
                'orders_statuses_id' => $status_id
            ]);
        }
    }

    public function getTechnicianId()
    {
        return Technician::where('user_id', Auth::user()->id)->select('id')->value('id');
    }

    public function getTechnicianUser($id)
    {
        $val = Technician::with('user')->where('id', $id)->first();
        return $val;
    }

    public function getTechnicianUserbyEmail($email)
    {
        $val = Technician::with('user')
            ->whereHas('user', function ($q) use ($email) {
                $q->where('email', '=', $email);
            })->first();
        if ($val) {
            return $val;
        }
        return 0;
    }

    public function getIconStatusOrder()
    {
        return $fa_icon = ['hourglass-start', 'credit-card', 'spinner', 'calendar-check-o', 'thumbs-o-up', 'times-circle'];
    }

    public function autoProductName(Request $request)
    {
        $name = '';
        if ($request->type == 'product_group') {
            $product_group_name = ProductGroup::where('id', $request->val)->value('name');
            $data = ProductDetailInformation::where($request->el, 'like', '%' . $request->phrase . '%')
                ->where('product_group_name', strtolower($product_group_name));
        } else {
            $data = ProductDetailInformation::where($request->type, strtolower($request->val))->where($request->el,  'like', '%' . $request->phrase . '%');
        }

        return $data = $data->where('user_id', Auth::user()->id)->get();
    }

    public function sendEmailOrder($modul, $name_module = null, $type = null)
    {
        $datas = null;
        if ($name_module != null) {
            $datas = EmailOther::where('name_module', $name_module);
            if ($type != null) {
                $datas = $datas->where('type', $type);
            }
            $datas = $datas->get();
        } else {
            $datas = EmailStatusOrder::where('orders_statuses_id', $modul->orders_statuses_id)->get();
        }
        foreach ($datas as $data) {
            event(new OrderEvent($modul, $data));
        }
    }

    public function comaToArray($str)
    {
        return explode(',', $str);
    }
}
