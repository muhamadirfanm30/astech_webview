<?php

namespace App\Http\Controllers;

use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use Auth;
use App\Models\Master\WebSetting;
use App\User;
use App\Models\PasswordReset;
use Illuminate\Support\Facades\Auth as FacadesAuth;
use App\Services\UserService;
use App\Mail\SendPasswordReset;
use App\Models\Master\TeknisiInfo;
use App\Models\Master\MsTicket;
use App\Models\Master\Order;
use App\Models\Master\TechnicianSaldo;
use App\Models\Master\Services;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\Master\TeknisiInfoUpdateRequest;
use App\Helpers\ImageUpload;

class HomeController extends AdminController
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function backLogin()
    {
        return view('users.template.login');
    }

    public function showdashboar()
    {
        // if (Auth::user()->hasRole('Customer')) {
        //     $showService = Services::where('is_visible', 1)->skip(0)->take(4)->get();
        //     return $this->viewAdmin('users.dashboard.userDashboard', [
        //         'showService' => $showService,
        //     ]);
        // }

        // if (Auth::user()->hasRole('Technician')) {
        //     return $this->viewAdmin('users.dashboard.teknisiDashboard');
        // }

        $no = 1;
        $showTeknisi = User::with('address')
            ->where('is_login_as_teknisi', 1)
            ->where('status', 7)
            ->whereNotNull('email_verified_at')
            ->whereNotNull('phone')
            // ->whereNotNull('otp_verified_at')
            // ->whereNotNull('otp_code')
            // ->whereNotNull('otp_url')
            ->orderBy('id', 'DESC')
            ->get();

        $tickets = MsTicket::with('user')->limit(5)->orderBy('id', 'DESC')->get();

        $netSalesThisMounth = Order::whereMonth('created_at', '=', date('m'))->get();
        $amount_net_sales_this_mount = 0;
        foreach($netSalesThisMounth as $getAmount){
            $amount_net_sales_this_mount += $getAmount->grand_total;
        }
        // return $totalThisMont;

        $netSalesLastMont = Order::whereMonth('created_at', '=', date("m",strtotime("-1 month")))->get();
        $amount_net_sales_last_mount = 0;
        foreach($netSalesLastMont as $getAmountLastMonth){
            $amount_net_sales_last_mount += $getAmountLastMonth->grand_total;
        }

        $totalTransactionComplete = Order::where('orders_statuses_id', 10)->get();
        $amount_order_complete = 0;
        foreach($totalTransactionComplete as $getAmountorderComplete){
            $amount_order_complete += $getAmountorderComplete->grand_total;
        }

        $totalPendingTransfer = TechnicianSaldo::where('transfer_status', 0)->get();
        $amount_pending_transfer = 0;
        foreach($totalPendingTransfer as $getAmountPendingTransfer){
            $amount_pending_transfer += $getAmountPendingTransfer->total;
        }

        $totalCustomer = User::role('customer')->count();
        $totalteknisi = User::role('technician')->count();


        // return $totalLastMont;
        return $this->viewAdmin('admin.dashboard', [
            'tickets' => $tickets,
            'showTeknisi' => $showTeknisi,
            'no' => $no,
            'title' => 'Dashboard',
            'amount_net_sales_this_mount' => $amount_net_sales_this_mount,
            'amount_net_sales_last_mount' => $amount_net_sales_last_mount,
            'amount_order_complete' => $amount_order_complete,
            'amount_pending_transfer' => $amount_pending_transfer,
            'totalteknisi' => $totalteknisi,
            'totalCustomer' => $totalCustomer,
        ]);
    }

    public function showDashboarUsers()
    {
        $userLogin = Auth::user()->id;
        $updateUsers = User::with('address')->where('id', $userLogin)->first();
        if ($updateUsers->address == null) {
            return $this->viewAdmin('admin.setting.listAddress.addresCreate');
        } else {
            return $this->viewAdmin('users.dashboard.userDashboard');
        }
    }

    public function detail($id)
    {
        $updateTeknisi = User::with(['address', 'teknisidetail', 'teknisidetail.devplant', 'info.marital', 'info.religion'])
            ->where('id', $id)
            ->first();
        // return $updateTeknisi;
        return $this->viewAdmin('admin.detailTeknisi', compact('updateTeknisi'));
    }

    public function update($id, Request $request)
    {
        $updateTeknisi = User::with(['address', 'teknisidetail', 'teknisidetail.devplant'])
            ->where('id', $id)
            ->first();
        return $this->viewAdmin('admin.updateTeknisi', compact('updateTeknisi'));
    }

    public function updateOfferTechnician(TeknisiInfoUpdateRequest $request, $id)
    {
        $user = User::where('id', Auth::user()->id)->first();
        $storage = TeknisiInfo::getImagePathUpload();
        $file = null;

        if ($request->hasFile('attachment')) {
            $image = (new ImageUpload)->upload($request->file('attachment'), $storage);
            $file = $image->getFilename();
        }

        if ($user->status == 8) {
            $user->update([
                'status' => 7,
                'note_reject' => null
            ]);

            TeknisiInfo::where('user_id', Auth::user()->id)->update([
                'attachment' => $file,
                'no_identity' => $request->no_identity,
                'ms_curriculum_id' => $request->ms_curriculum_id,
                'user_id' => $user->id,
            ]);
        }
        return $this->successResponse('success Rejected');
    }

    public function showregisterForm()

    {
        return view('admin.template.register');
    }

    public function showForgotPasswordForm()
    {
        $title_header_global = WebSetting::value('title_setting');
        $metta_header_global = WebSetting::value('metta_setting');
        $get_logo_astech = WebSetting::value('logo');
        return view('admin.template.forgotpassword', compact('title_header_global',
        'metta_header_global',
        'get_logo_astech'));
    }

    public function passwordReset(Request $request)
    {
        $service = new UserService();
        $user = User::where('email', '=', $request->email)->first();

        if ($user === null) {
            return redirect()->back()->with(['error' => 'email not exists']);
        }

        $service->sentPasswordReset($user, $request);

        return redirect()->back()->with(['success' => 'Success']);
    }

    public function showCreatePasswordForm(Request $request)
    {
        $updatePass = PasswordReset::where('token', $request->token)->first();

        if ($updatePass == null) {
            return redirect()->back()->with(['error' => 'error']);
        }
        $user = User::where('email', $updatePass->email)->first();

        $getToken = $request->token;
        $getEmail = $request->email;
        return view('admin.template.passwordCreate', [
            'getToken' => $getToken,
            'user' => $user,
        ]);
    }

    public function updatePassword(Request $request)
    {
        $passwordReset =  PasswordReset::where('token', $request->key)->first();

        if ($passwordReset == null) {
            return redirect()->back()->with(['error' => 'terjadi kesalahan, coba lagi']);
        }
        $user = User::where('email', $passwordReset->email)->first();
        if ($user != null) {
            $user->update([
                'password' => Hash::make($request->password),
            ]);
            $passwordReset->delete();

            return redirect('/')->with(['success' => 'Password Has Been Updated']);
        }
    }

    public function showOtpVerification()
    {
        $getUserLogin = \Auth::user()->id;
        $getDataUser = User::where('id', $getUserLogin)->first();
        return view('admin.template.otpverify', compact('getDataUser'));
    }

    public function logout(Request $request)
    {
        Auth::logout();
        return redirect('/');
    }

    public function verify(Request $request)
    {
        $getUserLogin = Auth::user()->id;
        $getDataUser = User::where('id', $getUserLogin)->first();

        return view('admin.template.verifyEmail', ['link' => $request->url, 'getDataUser' => $getDataUser]);
    }

    public function login()
    {
        return view('admin.template.logiin');
    }

    // public function showTabLocation()
    // {
    //     return $this->viewAdmin('admin.master._tabLocation');
    // }

}
