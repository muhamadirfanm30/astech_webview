<?php

namespace App\Http\Controllers\Customer\Web;

use App\Http\Controllers\AdminController;
use App\Http\Controllers\Controller;
use App\Model\Master\MsAdditionalInfo;
use App\Model\Master\MsAddress;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;
use Kreait\Firebase\Factory;

class ProfileController extends AdminController
{
    public function index()
    {
        $user = User::with(['info.religion', 'info.marital'])
            ->where('id', Auth::id())
            ->first();

        $addresses = MsAddress::with('city')->where('user_id', Auth::id())->get();

        return $this->viewAdmin('admin.customer.profile.profile', [
            'title' => 'Profile',
            'user'  => $user,
            'addresses'  => $addresses,
        ]);
    }

    public function firebaseTest()
    {
        $firebase = (new Factory)
            ->withServiceAccount(__DIR__ . '/FirebaseKey.json');

        $database = $firebase->createDatabase();
        $newPost = $database
            ->getReference('blog/posts')
            ->push(['title' => 'Post title', 'body' => 'This should probably be longer.']);
    }

    public function editInfo()
    {
        return $this->viewAdmin('admin.customer.profile.info', [
            'title' => 'EDIT INFO',
            'user'  => Auth::user(),
            'info'  => MsAdditionalInfo::where('user_id', Auth::id())->first(),
        ]);
    }

    public function createAddress()
    {
        return $this->viewAdmin('admin.customer.profile.create_address', [
            'title' => 'CREATE ADDRESS',
            'user'  => Auth::user(),
            'info'  => MsAdditionalInfo::where('user_id', Auth::id())->first(),
        ]);
    }

    public function editAddress($id)
    {
        return $this->viewAdmin('admin.customer.profile.edit_address', [
            'title'  => 'EDIT ADDRESS',
            'user'   => Auth::user(),
            'addres' => MsAddress::where('user_id', Auth::id())->where('id', $id)->first(),
        ]);
    }
}
