<?php

namespace App\Http\Controllers\Customer\Web;

use App\Http\Controllers\AdminController;
use App\Model\GeneralSetting;
use App\Model\Master\GaransiChat;
use App\Model\Master\Order;
use App\Services\Master\GaransiChatService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class GaransiController extends AdminController
{
    /**
     * list garansi
     */
    public function index(GaransiChatService $garansiChatService)
    {
        $orders = $garansiChatService->available();

        $claims = GaransiChat::with('order.service_detail.technician.user.info')->where('user_id', Auth::id())->whereIn('status', ['pending', 'revisit'])->paginate(10);
        $claimsDone = GaransiChat::with('order')->where('user_id', Auth::id())->where('status', 'done')->paginate(10);
        $claimsReject = GaransiChat::with('order')->where('user_id', Auth::id())->where('status', 'reject')->paginate(10);

        return $this
            ->viewAdmin('admin.customer.garansi.index', [
                'title' => 'Claim Garansi',
                'orders' => $orders,
                'claims' => $claims,
                'claimsDone' => $claimsDone,
                'claimsReject' => $claimsReject,
            ]);
    }

    public function show($id)
    {
        $garansi = GaransiChat::with(['order.detail.technician.user.info', 'user.info', 'garansi_details.user.info'])->where('id', $id)->firstOrFail();
        // return $garansi;
        $garansi->order->markAsRead();

        return $this
            ->viewAdmin('admin.customer.garansi.detail', [
                'title' => 'Claim Garansi detail',
                'auth'  => Auth::user(),
                'garansi' => $garansi,
            ]);
    }
}
