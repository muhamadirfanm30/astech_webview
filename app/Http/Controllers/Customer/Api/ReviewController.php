<?php

namespace App\Http\Controllers\Customer\Api;

use App\Exceptions\SendErrorMsgException;
use App\Http\Controllers\ApiController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\Controller;
use App\Http\Requests\RatingStoreRequest;
use App\Model\Master\Order;
use App\Model\Master\Rating;
use App\Model\Master\RatingAndReview;
use App\Model\Master\Review;
use App\Services\OrderService;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ReviewController extends ApiController
{
    /**
     * statistic total review
     */
    public function statistic(OrderService $orderService)
    {
        $done = Order::whereNotNull('rating_and_review_id')
            ->where('users_id', Auth::id())
            ->count();

        $waiting = Order::whereNull('rating_and_review_id')
            ->where('orders_statuses_id', 10)
            ->where('users_id', Auth::id())
            ->count();

        $response = [
            'waiting' => $waiting,
            'done' => $done,
            'all' => $waiting + $done
        ];
        return $this->successResponse($response);
    }

    /**
     * save review
     */
    public function store(RatingStoreRequest $request)
    {
        $note          = $request->note;
        $star          = $request->star;

        $id_tech_team  = null; //$request->id_tech_team;
        $order_id      = $request->order_id;
        $user          = Auth::user();

        $order = Order::where('id', $order_id)
            ->with('detail')
            ->where('users_id', $user->id)
            ->where('orders_statuses_id', 10)
            ->firstOrFail();

        // if ($order->detail->technicians_id != $technician_id) {
        //     throw new SendErrorMsgException('terjadi kesalahan');
        // }

        $technician_id = $order->detail->technicians_id;
        $rating = Rating::where('user_id', $user->id)->where('technician_id', $technician_id)->where('order_id', $order_id)->first();
        $review = Review::where('user_id', $user->id)->where('technician_id', $technician_id)->where('order_id', $order_id)->first();

        $data_rating = [
            'user_id'       => $user->id,
            'technician_id' => $technician_id,
            'id_tech_team'  => $id_tech_team,
            'order_id'      => $order_id,
            'value'         => $star,
        ];

        $data_review = [
            'technician_id' => $technician_id,
            'id_tech_team'  => $id_tech_team,
            'order_id'      => $order_id,
            'user_id'       => $user->id,
            'description'   => $note,
        ];
        $instance = new AdminController();

        $data_email = [
            'customer_name'    => $user->name,
            'technician_email' => $instance->getTechnicianUser($technician_id)->email,
            'technician_name'  => $instance->getTechnicianUser($technician_id)->name,
            'description'      => $note,
            'order_id'         => $order_id,
            'order_code'       => $order->code,
            'rating'           => $star,
        ];

        DB::beginTransaction();
        try {
            // rating
            if ($rating == null) {
                $rating = Rating::create($data_rating);
            } else {
                $rating->update($data_rating);
            }

            // revierw
            if ($review == null) {
                $review = Review::create($data_review);
            } else {
                $review->update($data_review);
            }

            if ($order->rating_and_review_id == null) {
                $rr = RatingAndReview::create([
                    'rating_id' => $rating->id,
                    'review_id' => $review->id,
                ]);
                $order->update([
                    'rating_and_review_id' => $rr->id,
                ]);
            }
            // commit db

            // $instance->sendEmailOrder($data_email, 'review', 2);
            DB::commit();
        } catch (QueryException $e) {
            // rollback db
            DB::rollback();

            throw new SendErrorMsgException($e->getMessage());
        }

        return $this->successResponse($order, $this->successStoreMsg());
    }

    /**
     * list waiting review
     */
    public function waiting(OrderService $orderService)
    {
        $order = Order::with([
            'user',
            'symptom',
            'service',
            'order_status',
            'service_detail.technician.user.info',
            'service_detail.technician.sum_total_rating',
            'service_detail.technician.count_total_review',
            'service_detail.symptom',
            'service_detail.services_type',
        ])
            ->whereNull('rating_and_review_id')
            ->where('orders_statuses_id', 10)
            ->where('users_id', Auth::id())
            ->paginate(10);

        return $this->successResponse($order);
    }

    /**
     * list done review
     */
    public function done(OrderService $orderService)
    {
        $order = $orderService->hasReview()->where('users_id', Auth::id())->paginate(10);

        return $this->successResponse($order);
    }
}
