<?php

namespace App\Http\Controllers;

use App\Models\Notification;
use App\Services\NotifikasiService;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;

class NotifikasiController extends AdminController
{
    public function firebasePushView()
    {
        return $this
            ->viewAdmin('admin.notification.firebase_push', [
                'title' => 'Notification Push',
                'users' => User::whereNotNull('device_token')->get()
            ]);
    }


    public function read($id)
    {
        $noty = Notification::where('id', $id)->firstOrFail();
        $data = json_decode($noty->data);

        Notification::markAsRead($id);

        return redirect($data->url);
    }

    public function readAll()
    {
        Notification::markAsReadUser(Auth::id());

        return $this
            ->viewAdmin('admin.notification.index', [
                'title' => 'Notification',
                'notifications' => Notification::getList()
            ]);
    }

    public function deleteAll()
    {
        return Auth::user()->notifications()->delete();
    }

    public function APIIndex()
    {
        $resp = Notification::getList();
        return $this->successResponse($resp, 'ok');
    }

    public function APIListRead()
    {
        $resp = DB::table('notifications')
            ->where('notifiable_id', Auth::id())
            ->whereNotNull('read_at')
            ->orderBy('created_at', 'desc')
            ->paginate(50);

        return $this->successResponse($resp, 'ok');
    }

    public function APIListUnread()
    {
        $resp = DB::table('notifications')
            ->where('notifiable_id', Auth::id())
            ->whereNull('read_at')
            ->orderBy('created_at', 'desc')
            ->paginate(50);

        return $this->successResponse($resp, 'ok');
    }

    public function APIMarkAsRead(Request $request)
    {
        if (empty($request->id)) {
            $resp = Notification::markAsReadUser(Auth::id());
        } else {
            $resp = Notification::markAsRead($request->id);
        }
        return $this->successResponse($resp, 'ok');
    }

    public function firebaseSendMessage(Request $request)
    {
        $user = User::where('device_token', $request->device_token)->firstOrFail();
        $dataPush = [
            'title' => $request->title,
            'body' => $request->body,
        ];

        (new NotifikasiService)->firebasePushNotifikasi($user, $dataPush);

        return $this->successResponse($dataPush, 'ok', 200);
    }
}
