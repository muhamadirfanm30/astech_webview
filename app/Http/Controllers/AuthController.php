<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Socialite;
use App\Http\Requests\UserLoginRequest;
use App\Http\Requests\UserRegisterRequest;
use App\Http\Requests\UserRegisterWebRequest;
use App\Services\UserService;
use App\User;
use App\Models\Master\MsAdditionalInfo;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use Spatie\Permission\Models\Role;

class AuthController extends Controller
{
    /**
     * registrasi user.
     */
    public function register(UserService $userService, UserRegisterRequest $request)
    {
        // dd($request->agree);
        if ($request->agree == null) {
            // return 'harus di klik';
            return redirect()->back()->with('error', 'setujui dulu');
        } elseif ($request->agree == 1) {
            $request->validated();
            $phone = $request->phone;
            $getPhone = User::where('phone', '=', $phone)->count();
            
            $email = $request->email;

            if($getPhone > 0) {
                // dd('asdas');
                return redirect()->back()->with(['error' => 'This number already exists']);
            }else{
                $userService->create($request->all());
            }

            // create user

            // login kan user
            if ($userService->login($request->all())) {
                if ($userService->isVerify() == false) {
                    // redirect
                    return redirect('/')->with('error', 'akun belum diverifikasi');
                }

                // redirect
                if (Auth::user()->hasRole('Customer')) {
                    if ($request->role_id  == 2) {
                        return redirect(route('user.info'));
                    }
                    return redirect(route('user.home'));
                }
                return redirect(route('home'));
            }
        }
        // validasi

    }

    public function newRegisteredUsers(UserService $userService,  UserRegisterWebRequest $request)
    {
        // dd($request->agree);
        if ($request->agree == null) {
            // return 'harus di klik';
            return redirect()->back()->with('error', 'You must agree to our terms and conditions')->withInput();
        } elseif ($request->agree == 1) {
            $request->validated();
            $phone = $request->phone;
            $getPhone = User::where('phone', '=', $phone)->count();
            
            $email = $request->email;

            if($getPhone > 0) {
                // dd('asdas');
                return redirect()->back()->with(['error' => 'This number already exists']);
            }else{
                $userService->create($request->all());
                return redirect('/')->with('success', 'Your Accound Has Been Created.');
            }

            // create user

            // login kan user
            if ($userService->login($request->all())) {
                if ($userService->isVerify() == false) {
                    // redirect
                    return redirect('/')->with('error', 'akun belum diverifikasi');
                }

                // redirect
                if (Auth::user()->hasRole('Customer')) {
                    if ($request->role_id  == 2) {
                        return redirect(route('user.info'));
                    }
                    return redirect(route('user.home'));
                }
                return redirect(route('home'));
            }
        }
        // validasi

    }

    /**
     * login user.
     */
    public function loginB2b(UserLoginRequest $request, UserService $userService)
    {
        
        // validasi
        $request->validated();

        if ($userService->login($request->all())) {
            return redirect(route('home'));
        }
        // redirect
        return redirect('/')->with('error', 'wrong email or password');
    }

    public function logoutB2b(Request $request) 
    {
        Auth::logout();
        return redirect('/');
    }

    public function loginUser(UserLoginRequest $request, UserService $userService)
    {
        // validasi
        $request->validated();

        if ($userService->login($request->all())) {
            // check user verify
            if ($userService->isVerify() == false) {
                return redirect('/verify')->with('error', 'your account has not been validated');
            }

            // redirect

            if (Auth::user()->hasRole('Technician') && Auth::user()->hasRole('Customer')) {
                return redirect(route('teknisi.home'));
            }

            if (Auth::user()->hasRole('Customer')) {
                return redirect(route('user.home'));
            }

            if (Auth::user()->hasRole('Technician')) {
                return redirect(route('teknisi.home'));
            }


            return redirect(route('home'));
        }
        // redirect
        return redirect('/')->with('error', 'wrong email or password');
    }

    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->user();
        $a = User::where('email', '=', $user->email)->where('is_social_login', 0)->count();
        if ($a > 0) {
            return redirect('/')->with('error', 'This email has been used by another user');
        }
        
        $b = User::where('email', '=', $user->email)->first();
        // dd($b->id);
        $getId = User::where('id', $user->id)->first();
        
        if(!empty($user->email)){
                $authUser = $this->findOrCreateUser($user, $provider);
                $createAvatar = $this->createAvatar($user, $b);
                Auth::login($authUser, true);
                return redirect('/customer/dashboard');
        }else{
            return redirect('/')->with('error', 'You have to share your email address on your Facebook account for further processing');
        }
    }

    public function findOrCreateUser($user, $provider)
    {
       $authUser = User::where('email', $user->email)->first();
        if ($authUser) {
            return $authUser;
        }
        else{
            $customer = Role::where('name', 'Customer')->first();
            $data = User::create([
                'name'     => $user->name,
                'email_verified_at' => date('Y-m-d H:i:s'),
                'email'    => !empty($user->email)? $user->email : '' ,
                'provider' => $provider,
                'provider_id' => $user->id,
                'is_social_login' => 1
            ])->assignRole($customer->id);
            return $data;
        }
    }

    public function createAvatar($user, $b)
    {
        $b = User::where('email', '=', $user->email)->first();
        // dd($getId);
        $addAvatar = MsAdditionalInfo::create([
            'user_id' => $b->id,
            'first_name' => $b->name,
            'picture' => !empty($user->avatar)? $user->avatar : '' ,
        ]);

    }

    public function errorMessage()
    {
        return view('admin/errorMessageRegister');
    }

    /**
     * logout user.
     */
    public function logout()
    {
        Auth::logout();

        return redirect('/')->with('success', 'logout sukses');
    }
}
