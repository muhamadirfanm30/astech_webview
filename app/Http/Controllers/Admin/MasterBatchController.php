<?php

namespace App\Http\Controllers\Admin;

use App\Model\Master\Batch;
use App\Model\Master\BatchItem;
use App\Http\Controllers\AdminController;
use App\Model\Master\BatchShipmentHistory;

class MasterBatchController extends AdminController
{
    public function index()
    {
        return $this
            ->setBreadcrumb('List Batch', '/admin/batch/show')
            ->viewAdmin('admin.master.batch.index', [
                'title' => 'MASTER SHIPPING BATCH'
            ]);
    }

    public function create()
    {
        return $this
            ->setBreadcrumb('List Batch', '/admin/batch/show')
            ->setBreadcrumb('Create Batch', '/admin/batch/create')
            ->viewAdmin('admin.master.batch.create', [
                'title' => 'CREATE SHIPPING BATCH'
            ]);
    }

    public function edit($id)
    {
        $batch = Batch::where('id', $id)
            ->with(['items.product', 'items.productVendor.vendor', 'items.varian.productVarianAttributes', 'items.status'])
            ->firstOrFail();

        return $this
            ->setBreadcrumb('List Batch', '/admin/batch/show')
            ->setBreadcrumb('Update Batch', '#')
            ->viewAdmin('admin.master.batch.edit', [
                'title' => 'EDIT SHIPPING BATCH',
                'batch' => $batch
            ]);
    }

    public function show($id)
    {
        $batch = Batch::where('id', $id)
            ->with(['items.product', 'items.productVendor.vendor', 'items.varian.productVarianAttributes', 'items.status'])
            ->firstOrFail();

        return $this
            ->setBreadcrumb('List Batch', '/admin/batch/show')
            ->setBreadcrumb('Update Batch', '#')
            ->viewAdmin('admin.master.batch.show', [
                'title' => 'DETAIL SHIPPING BATCH',
                'batch' => $batch
            ]);
    }

    public function statusIndex()
    {
        return $this
            ->setBreadcrumb('Status Batch', '/admin/batch/status')
            ->viewAdmin('admin.master.batch_status.index', [
                'title' => 'MASTER SHIPPING STATUS'
            ]);
    }

    public function statusShipmentIndex()
    {
        return $this
            ->setBreadcrumb('List Batch Status Shipment', '/admin/batch-status-shipment/show')
            ->viewAdmin('admin.master.batch_status_shipment.index', [
                'title' => 'MASTER STATUS SHIPMENTS'
            ]);
    }

    public function batchShipment($id) {
        $last_status = BatchShipmentHistory::with('batch', 'batch_status_shipment')->where('batch_id', $id)->orderBy('id','desc')->first();
        $batch = Batch::where('id',$id)->first();
        $data_confirmed = BatchItem::where('batch_id', $id)->pluck('is_confirmed')->toArray();
        $is_confirmed_all = 0;
        if(array_sum($data_confirmed) == count($data_confirmed)) {
            $is_confirmed_all = 1;
        }
        return $this
        ->setBreadcrumb('Update Batch', '/admin/batch/shipment')
        ->setBreadcrumb('Status Batch', '#')
        ->viewAdmin('admin.master.batch.shipment', [
            'title' => 'SHIPMENTS STATUS',
            'last_status' => $last_status,
            'batch' => $batch,
            'is_confirmed_all' => $is_confirmed_all
        ]);
    }
}
