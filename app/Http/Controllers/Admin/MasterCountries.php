<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use App\Model\Master\MsCountry;

class MasterCountries extends AdminController
{
    public function showCountries()
    {
        return $this
            ->setBreadcrumb('List Country', '/admin/country/show')
            ->viewAdmin('admin.master.location.countries.index', [
                'title' => 'MASTER COUNTRY'
            ]);
    }

    public function createCountries()
    {
        return $this
            ->setBreadcrumb('List Country', '/admin/country/show')
            ->setBreadcrumb('Create Country', '/admin/country/create')
            ->viewAdmin('admin.master.location.countries.create', [
                'title' => 'CREATE COUNTRY'
            ]);
    }

    public function updateCountries($id)
    {
        $updateCountries = MsCountry::findOrFail($id);
        return $this
            ->setBreadcrumb('List Country', '/admin/country/show')
            ->setBreadcrumb('Update Country', '/admin/country/update')
            ->viewAdmin('admin.master.location.countries.update', compact('updateCountries'));
    }
}
