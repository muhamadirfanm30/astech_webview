<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use App\Model\Master\Educations;
use Auth;

class CurriculumInfoController extends AdminController
{
    public function index()
    {
       return $this
            ->setBreadcrumb('Education', '/admin/curriculum/show')
            ->viewAdmin('admin.setting.educations.curriculum.index',[
                'title' => 'List Curriculum',
            ]);
    }

    public function create()
    {
        return $this
            ->setBreadcrumb('Education', '/admin/curriculum/show')
            ->setBreadcrumb('Education Create', '#')
            ->viewAdmin('admin.setting.educations.curriculum.create',[
                'title' => 'Create Curriculum'
            ]);
    }

    // public function update($id)
    // {
    //     $viewEducation = Educations::where('id', $id)->first();
        
    //     return $this
    //         ->setBreadcrumb('Education', '/admin/curriculum/show')
    //         ->setBreadcrumb('Education Update', '#')
    //         ->viewAdmin('admin.setting.educations.curriculum.update',[
    //             'title' => 'Update Curriculum',
    //             'viewEducation' => $viewEducation
    //         ]);
    // }
}
