<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\AdminController;

class ImportExcelController extends AdminController
{
    public function showImportExcelMaster()
    {
        return $this
        ->setBreadcrumb('Import', '/admin/import-batch-inventory')
        ->viewAdmin('admin.master.import-excel.index');
    }
    public function showImportExcelBatchInventory()
    {
        return $this
        ->setBreadcrumb('Import', '/admin/import-batch-inventory')
        ->viewAdmin('admin.master.inventory.import-excel.index');
    }

    public function showImportOrdersDataExcel()
    {
        return $this
        ->setBreadcrumb('Import', '/admin/import-orders-data-excel')
        ->viewAdmin('admin.master.import-orders-data-excel.index');
    }

}
