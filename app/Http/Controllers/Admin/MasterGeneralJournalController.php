<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\Master\GeneralJournal;
use App\Model\Master\GeneralSetting;
use Rap2hpoutre\FastExcel\FastExcel;
use App\Http\Controllers\AdminController;
use function App\Helpers\thousanSparator;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use App\Model\Master\GeneralJournalDetail;
use App\Services\Master\GeneralJournalService;

class MasterGeneralJournalController extends AdminController
{
    public function index()
    {
        return $this->setData([
            'title' => 'Master General Journal '
        ])
            ->setBreadcrumb('List General Journal', '/admin/general-journal/show')
            ->viewAdmin('admin.master.general-journal.index');
    }


    public function create(GeneralJournalService $service)
    {
        return $this->setData([
            'title' => 'Create Master General Journal',
            'code' => $service->generateCode()
        ])
            ->setBreadcrumb('List services', '/admin/general-journal/show')
            ->viewAdmin('admin.master.general-journal.create');
    }

    public function update($id)
    {
        $getData = GeneralJournal::where('id', $id)->first();
        $detailsData = GeneralJournalDetail::with('asc')->where('general_journal_id', $id)->orderBy('date','ASC')->get();
        // return $detailsData;
        return $this->setData([
            'title' => 'Update Master General Journal',
            'getData' => $getData,
            'detailsData' => $detailsData
        ])
            ->setBreadcrumb('List General Journal', '/admin/general-journal/show')
            ->viewAdmin('admin.master.general-journal.update');
    }

    public function detail($id)
    {
        $getData = GeneralJournal::where('id', $id)->first();
        $detailsData = GeneralJournalDetail::where('general_journal_id', $id)->orderBy('date','ASC')->get();

        return $this->setData([
            'title' => 'Detail Master General Journal',
            'getData' => $getData,
            'detailsData' => $detailsData
        ])
            ->setBreadcrumb('Detail General Journal', '/admin/general-journal/detail')
            ->viewAdmin('admin.master.general-journal.detail');
    }

    public function logs()
    {
        return $this->setData([
            'title' => 'General Journal Logs (Deleted Only)'
        ])
            ->setBreadcrumb('List General Journal', '/admin/general-journal/logs')
            ->viewAdmin('admin.master.general-journal.logs');
    }

    public function getValuePart($details_data, $field, $i) {
        if($details_data != null && $details_data[$i-1]->{$field} !== "") {
            $val = $field == 'date' ? Carbon::parse($details_data[$i-1]->{$field})->format('d-m-Y') : $details_data[$i-1]->{$field};
        } else {
            $val = null;
        }
        return $val;
    }

    public function showImportDetailExcel($id)
    {
        $getData = GeneralJournal::where('id', $id)->first();

        return $this->setData([
            'title' => 'Import Master General Journal',
            'getData' => $getData,
        ])
            ->setBreadcrumb('List General Journal', '/admin/general-journal/show')
            ->viewAdmin('admin.master.general-journal.import-details');
    }

    public function exportData(Request $request) {

        $query = GeneralJournal::with('general_journal_detail');
        // return $q;
        if(!empty($request->date_from) && !empty($request->date_to)) {
            $query->whereHas('general_journal_detail', function ($q) use ($request) {
                $date_from = Carbon::parse($request->date_from)->format('Y-m-d');
                $date_to = Carbon::parse($request->date_to)->format('Y-m-d');
                $q->where('date', '>=', $date_from)
                ->where('date', '<=', $date_to);
            });
        }
        $query = $query->get();
        $data = [];

        foreach($query as $row) {
            $details_data = [];
            $count_detail = count($row->general_journal_detail);
            foreach(range(1,$count_detail) as $val) {
                $details_data['date_'.$val.''] = $this->getValuePart($row->general_journal_detail, 'date', $val);
                $details_data['category_'.$val.''] = $this->getValuePart($row->general_journal_detail, 'category', $val);
                $details_data['description_'.$val.''] = $this->getValuePart($row->general_journal_detail, 'description', $val);
                $details_data['quantity_'.$val.''] = $this->getValuePart($row->general_journal_detail, 'quantity', $val);
                $details_data['debit_'.$val.''] = $this->getValuePart($row->general_journal_detail, 'debit', $val);
                $details_data['kredit_'.$val.''] = $this->getValuePart($row->general_journal_detail, 'kredit', $val);
                $details_data['balance_'.$val.''] = $this->getValuePart($row->general_journal_detail, 'balance', $val);

            }
            $gj_data = [
                "code" => $row['code'],
                "title" => $row['title'],
                "image" => $row['image'],
                "created_at" => Carbon::parse($row['created_at'])->format('d-m-Y')
            ];

            $data [] = array_merge($gj_data, $details_data);
        }

        return (new FastExcel($data))->download('Export_General_Journal_Data_'.$request->date_from.'-'.$request->date_to.'.xlsx');
    }

    public function exportDetailExcel(Request $request, $id) {
        $data_excel = GeneralJournalDetail::with('general_journal')->where('general_journal_id', $id)->get();
        // return $data_excel;

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->getSheetView()->setZoomScale(80);
        $sheet->setShowGridlines(false);
        $sheet->getStyle("C1:I1")->getFont()->setSize(22)->setBold(true)->setName('Arial');
        $sheet->mergeCells('C1:I1');
        $sheet->setCellValue('C1',$data_excel[0]->general_journal->title. ' - CODE('. $data_excel[0]->general_journal->code.')');
        $sheet->getStyle("C1")->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getColumnDimension('A')->setWidth(2);
        $sheet->getColumnDimension('B')->setWidth(2);
        $sheet->getColumnDimension('C')->setWidth(15);
        $sheet->getColumnDimension('D')->setWidth(30);
        $sheet->getColumnDimension('E')->setWidth(50);
        $sheet->getColumnDimension('F')->setWidth(10);
        $sheet->getColumnDimension('G')->setWidth(20);
        $sheet->getColumnDimension('H')->setWidth(20);
        $sheet->getColumnDimension('I')->setWidth(20);
        $sheet->setCellValue('C3', 'Date')->getStyle('C3')->getFont()->setSize(12)->setBold(true);
        $sheet->setCellValue('D3', 'Category')->getStyle('D3')->getFont()->setSize(12)->setBold(true);
        $sheet->setCellValue('E3', 'Description')->getStyle('E3')->getFont()->setSize(12)->setBold(true);
        $sheet->setCellValue('F3', 'Qty')->getStyle('F3')->getFont()->setSize(12)->setBold(true);
        $sheet->setCellValue('G3', 'Debit')->getStyle('G3')->getFont()->setSize(12)->setBold(true);
        $sheet->setCellValue('H3', 'Credit')->getStyle('H3')->getFont()->setSize(12)->setBold(true);
        $sheet->setCellValue('I3', 'Balance')->getStyle('I3')->getFont()->setSize(12)->setBold(true);
        $spreadsheet->getActiveSheet()->getStyle('C3:I3')
        ->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $sheet->getStyle('C3:I3')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $sheet->getStyle('C3:I3')->getFill()
        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        ->getStartColor()->setARGB('2F75B5');

        foreach($data_excel as $key => $val) {
            $row = $key + 4;
            $sheet->setCellValue('C'.$row, Carbon::parse($val->date)->format('d-m-Y'))->getStyle('C'.$row)->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()->setARGB('DDEBF7');
            $sheet->setCellValue('D'.$row, $val->category)->getStyle('D'.$row)->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()->setARGB('DDEBF7');
            $sheet->setCellValue('E'.$row, $val->description)->getStyle('E'.$row)->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()->setARGB('DDEBF7');
            $sheet->setCellValue('F'.$row, $val->quantity)->getStyle('F'.$row)->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()->setARGB('DDEBF7');
            $sheet->setCellValue('G'.$row, 'Rp. '.($val->debit != 0 ? thousanSparator($val->debit) : '0'))->getStyle('G'.$row)->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()->setARGB('DDEBF7');
            $sheet->setCellValue('H'.$row, 'Rp. '.($val->kredit != 0 ? thousanSparator($val->kredit) : '0'))->getStyle('H'.$row)->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()->setARGB('C6E0B4');
            $sheet->setCellValue('I'.$row, 'Rp. '.($val->balance != 0 ? thousanSparator($val->balance) : '0'))->getStyle('I'.$row)->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()->setARGB('DDEBF7');
        }
        $row_after_load = 4 + count($data_excel);

        $sheet->setCellValue('C'.$row_after_load, 'Ending');
        $sheet->setCellValue('D'.$row_after_load, '');
        $sheet->setCellValue('E'.$row_after_load, '');
        $sheet->setCellValue('F'.$row_after_load, '');
        $sheet->setCellValue('G'.$row_after_load, 'Rp. '.($data_excel[0]->general_journal->total_debit != 0 ? thousanSparator($data_excel[0]->general_journal->total_debit) : ''));
        $sheet->setCellValue('H'.$row_after_load, 'Rp. '.($data_excel[0]->general_journal->total_kredit != 0 ? thousanSparator($data_excel[0]->general_journal->total_kredit) : ''));
        $sheet->setCellValue('I'.$row_after_load, 'Rp. '.($data_excel[0]->general_journal->total_balance != 0 ? thousanSparator($data_excel[0]->general_journal->total_balance) : ''))->getStyle('I'.$row_after_load)->getFill()
        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        ->getStartColor()->setARGB('FFFF00');
        $spreadsheet->getActiveSheet()->getStyle('C4:I'.($row_after_load))
        ->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $row_info = $row_after_load+2;
        $sheet->getStyle('D'.$row_info)->getFill()
        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        ->getStartColor()->setARGB('8EA9DB');
        $sheet->getStyle('D'.($row_info+1))->getFill()
        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        ->getStartColor()->setARGB('FFFF00');
        $sheet->getStyle('D'.($row_info+2))->getFill()
        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        ->getStartColor()->setARGB('92D050');
        $sheet->getStyle('D'.($row_info+3))->getFill()
        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        ->getStartColor()->setARGB('C65911');
        $sheet->getStyle('D'.($row_info+4))->getFill()
        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        ->getStartColor()->setARGB('FF0000');
        $sheet->setCellValue('E'.$row_info, 'Nota fisik Belum dikirim');
        $sheet->setCellValue('E'.($row_info+1), 'Nota kirim Terbaru');
        $sheet->setCellValue('E'.($row_info+2), 'Nota Sudah dikirimkan');
        $sheet->setCellValue('E'.($row_info+3), 'Habis pakai');
        $sheet->setCellValue('E'.($row_info+4), 'revisi Item/Nota');

        $styleArray = [
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                ],
            ],
        ];
        $sheet->getStyle('D'.($row_after_load+2).':E'.($row_after_load+2+4))->applyFromArray($styleArray);

        $sheet->setCellValue('H'.($row_info+2), 'upload image')->getStyle('H'.($row_info+2))->getFont()->setSize(22)->setBold(true);

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="General_Journal_Details.xls"');
        $writer = new Xlsx($spreadsheet);
        $writer->save("php://output");

    }

    public function showReportHppItems()
    {
        $margin_rate_auto = intval(GeneralSetting::where('name','margin_rate_auto')->value('value'));
        return $this
            ->setBreadcrumb('Report HPP Items', '#')
            ->viewAdmin('admin.master.general-journal.report-hpp-items', [
                'title' => 'Report HPP Items',
                'margin_rate_auto' => $margin_rate_auto
            ]);
    }

    public function showReportHppItemDetails($pdes_id, $month, $year)
    {
        $data = GeneralJournalDetail::select([
            DB::raw('(CEIL(SUM(general_journal_details.kredit) / SUM(general_journal_details.quantity))) AS hpp_average_month'),
            'part_data_stock_id'])
            ->with(['hpp_item' => function ($query) use($month, $year) {
                $query->where([
                    ['type', '=', 1], //type 1 mean Month
                    ['month', '=', $month],
                    ['year', '=', $year],
                ]);
            }])
            ->join('ode_part_data_stock', 'ode_part_data_stock.id', '=', 'general_journal_details.part_data_stock_id')
            ->where('part_data_stock_id', $pdes_id)
            ->where('category', 'material')
            ->whereMonth('general_journal_details.created_at', $month)
            ->whereYear('general_journal_details.created_at', $year)
            ->groupBy('part_data_stock_id')->whereNotNull('part_data_stock_id')
            ->first();
        $details = GeneralJournalDetail::with('part_data_stock_inventory')->where('part_data_stock_inventories_id', $pdes_id)->whereNotNull('part_data_stock_inventories_id')->get();
        return $this
            ->setBreadcrumb('Report HPP Items', '#')
            ->viewAdmin('admin.master.general-journal.report-hpp-item-details', [
                'title' => 'Report HPP Items Details',
                'month' => $month,
                'year' => $year,
                'details' => $details,
                'hpp_avg_month' => $data->hpp_average_month,
                'hpp_avg_month_manual' => $data->hpp_item != null ? $data->hpp_item->hpp_average_month_manual : 0,
                'margin_rate_auto' => $data->margin_rate_auto
            ]);
    }

    public function showReportHppItemDetailsYearly($pdes_id)
    {
        $data = GeneralJournalDetail::select([
            DB::raw('(CEIL(SUM(general_journal_details.kredit) / SUM(general_journal_details.quantity))) AS hpp_average_all'),
            DB::raw('(MIN(YEAR(general_journal_details.created_at))) AS min_year'),
            DB::raw('(MAX(YEAR(general_journal_details.created_at))) AS max_year'),
            'part_data_stock_id'])
            ->with(['hpp_item' => function ($query) {
                $query->where([
                    ['type', '=', 2], //type 1 mean Year
                ]);
            }])
            ->join('ode_part_data_stock', 'ode_part_data_stock.id', '=', 'general_journal_details.part_data_stock_id')
            ->where('part_data_stock_id', $pdes_id)
            ->where('category', 'material')
            ->groupBy('part_data_stock_id')->whereNotNull('part_data_stock_id')
            ->first();

        $details = GeneralJournalDetail::with('part_data_stock')->where('part_data_stock_id', $pdes_id)->whereNotNull('part_data_stock_id')->get();
        return $this
            ->setBreadcrumb('Report HPP Items Year', '#')
            ->viewAdmin('admin.master.general-journal.report-hpp-item-details-year', [
                'title' => 'Report HPP Items Details Yearly',
                'pdes_id' => $pdes_id,
                'details' => $details,
                'hpp_avg_all' => $data->hpp_average_all,
                'hpp_avg_all_manual' => $data->hpp_item != null ? $data->hpp_item->hpp_average_all_manual : 0,
                'margin_rate_auto' => $data->margin_rate_auto,
                'min_year' => $data->min_year,
                'max_year' => $data->max_year
            ]);
    }



}
