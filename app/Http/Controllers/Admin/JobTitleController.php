<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;

class JobTitleController extends AdminController
{
    public function index()
    {
        return $this
            ->setBreadcrumb('Job Title', '/product')
            ->viewAdmin('admin.teknisi.job_title.index', [
                'title' => 'MASTER JOB TITLE',
            ]);
    }
}
