<?php

namespace App\Http\Controllers\Admin;

use Auth;
use App\User;
use PDF;
use App\Model\Master\Order;
use Illuminate\Http\Request;
use App\Model\Master\MsProduct;
use App\Model\Master\Educations;
use App\Model\Master\ItemDetail;
use App\Model\Master\ConfrimPayment;
use App\Model\Master\BankTransfer;
use App\Model\Technician\Technician;
use App\Model\Master\SparepartDetail;
use App\Http\Controllers\AdminController;
use App\Model\Master\TechnicianSparepart;

class CheckOutController extends AdminController
{
    public function index(Request $request, $order_id)
    {

        $getBankName = BankTransfer::where('bank_name', 'like', '%' . $request->q . '%')
                        ->get();
        $getDataUser = User::where('id', Auth::user()->id)
                        ->with(['info', 'address', 'address.city', 'address.district', 'address.village', 'address.zipcode',])
                        ->first();
                        // return $getDataUser;

        $order = Order::where('id', $order_id)
            ->with([
                'user',
                'user.address',
                'service',
                'symptom',
                'order_status',
                'service_detail.symptom',
                'service_detail.services_type',
                'service_detail.price_service',
            ])
            ->firstOrFail();
            // return $order;

            $sparepart_details = SparepartDetail::where('orders_id', $order_id)
            ->with([
                'order',
                'technician_sparepart'
            ])->get();

            $item_details = ItemDetail::where('orders_id', $order_id)
                ->with(['order'])->get();
                // return [$sparepart_details, $item_details];
       return $this
            ->setBreadcrumb('CheckOut', '')
            ->viewAdmin('admin.customer.checkout.checkout',[
                'title' => 'Checkout Form',
                'getBankName' => $getBankName,
                'getDataUser' => $getDataUser,
                'order' => $order,
                'sparepart_details' => $sparepart_details,
                'item_details' => $item_details
            ]);
    }

    public function showOrderDetail($order_id)
    {
        $getDataUser = User::where('id', Auth::user()->id)
                        ->with(['info', 'address', 'address.city', 'address.district', 'address.village', 'address.zipcode',])
                        ->first();
                        // return $getDataUser;

        $order = Order::where('id', $order_id)
            ->with([
                'user',
                'bank',
                'user.address',
                'service',
                'symptom',
                'order_status',
                'service_detail.symptom',
                'service_detail.services_type',
                'service_detail.price_service',
            ])
            ->firstOrFail();
            // return $order;
        return $this->viewAdmin('admin.customer.orderDetail.orderDetail', [
            'getDataUser' => $getDataUser,
            'order' => $order,
        ]);
    }

    public function paymentConfirmation($order_id)
    {
        return 'asd';
        $detailOrder = Order::where('id', $order_id)
            ->with([
                'user',
                'bank',
                'user.address',
                'service',
                'symptom',
                'order_status',
                'service_detail.symptom',
                'service_detail.services_type',
                'service_detail.price_service',
            ])
            ->firstOrFail();
            // return $detailOrder;

        $user = ConfrimPayment::where('ms_order_id', '=', $order_id)->first();
        // return $user;
        if ($user !== null) {
            return $this
            ->viewAdmin('admin.customer.checkout.paymentDetail',[
                'title' => 'Confirmation Payment',
                'detailOrder' => $detailOrder,
                'user' => $user,
            ]);
        }

        return $this
            ->viewAdmin('admin.customer.checkout.confirmationPayment',[
                'title' => 'Confirmation Payment',
                'detailOrder' => $detailOrder
            ]);
    }

    public function invoice($order_id) {
        $order = Order::where('id', $order_id)
            // ->with([
            //     'user',
            //     'service',
            //     'symptom',
            //     'order_status',
            //     'service_detail.symptom',
            //     'service_detail.services_type',
            //     'service_detail.technician.user',
            //     'service_detail.price_service.service_type.symptom.services',
            //     'payment_method'
            // ])
            ->with([
                'orderpayment',
                'user.ewallet',
                'service',
                'symptom',
                'order_status',
                'sparepart_detail',
                'service_detail.symptom',
                'service_detail.services_type',
                'service_detail.technician.user',
                'service_detail.price_service.service_type.symptom.services',
                'history_order.order_status',
            ])
            ->firstOrFail();
            // return $order;
        $technicians_id    = Technician::where('user_id', Auth::user()->id)->value('id');
        $astech_spareparts = MsProduct::where('ms_services_id', $order->service->id)->get();
        $tech_spareparts   = TechnicianSparepart::where('technicians_id', $technicians_id)->get();
        $sparepart_details = SparepartDetail::where('orders_id', $order_id)
                                ->with(['order','technician_sparepart',])->get();
        $item_details = ItemDetail::where('orders_id', $order_id)->with(['order'])->get();
        // return $item_details;

        // $user = UserDetail::find($user->id);
        // $data["info"] = $user;
        // $pdf = PDF::loadView('whateveryourviewname', $data);
        // return $pdf->stream('whateveryourviewname.pdf');
        // return $order;
        // $pdf = PDF::loadview('admin.customer.checkout.invoice',[
        //         'title'             => 'DETAIL INVOICE',
        //         'order'             => $order,
        //         'order_id'          => $order_id,
        //         'astech_spareparts' => $astech_spareparts,
        //         'tech_spareparts'   => $tech_spareparts,
        //         'sparepart_details' => $sparepart_details,
        //         'item_details'      => $item_details,
        //     ]);
        // return $pdf->stream();
    	// return $pdf->download('invoice-pdf');
        return $this
            ->viewAdmin('admin.customer.checkout.invoice', [
                'title'             => 'DETAIL INVOICE',
                'order'             => $order,
                'order_id'          => $order_id,
                'astech_spareparts' => $astech_spareparts,
                'tech_spareparts'   => $tech_spareparts,
                'sparepart_details' => $sparepart_details,
                'item_details'      => $item_details,
            ]);

    }
}
