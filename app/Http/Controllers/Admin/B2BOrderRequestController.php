<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use App\Models\RequestOrders;
use App\Models\RequestOrderImage;
use App\Models\RequestOrderDetail;
use App\Models\ImageBeritaAcara;
use App\Models\ImageInvoice;
use App\Models\B2BAssignTeknisi;
use App\Models\ImagePurchaseOrder;
use App\Models\B2BDataCostEstimations;
use App\Models\ImageQuotation;
use App\Models\B2BMasterType;
use App\Models\B2BMasterBrand;
use App\Models\B2BLabor;
use App\Models\GeneralSettingB2b;
use App\Models\B2BDataTransactions;
use App\User;
use PDF;
use App\Models\BusinessToBusinessOutletDetailTransaction;

class B2BOrderRequestController extends AdminController
{
    public function index()
    {
        return $this
            ->viewAdmin('admin.business_to_business_order_request.index', [
                'title' => 'BUSSINES TO BUSSINES REQUEST ORDER',
            ]);
    }

    public function create()
    {
        return $this
            ->viewAdmin('admin.business_to_business_order_request.createRequestOrder', [
                'title' => 'BUSSINES TO BUSSINES REQUEST ORDER',
            ]);
    }

    public function detail($request_code)
    {
        $data = RequestOrderDetail::with('get_b2b_order_detail')->where('request_code', $request_code)->get();
        $getId = RequestOrderDetail::where('request_code', $request_code)->orderBy('id', 'Desc')->first();

        RequestOrders::where('request_code', $request_code)->update([
            'read_at' => date('Y-m-d H:i:s')
        ]);

        return $this
            ->viewAdmin('admin.business_to_business_order_request.detail', [
                'title' => 'Request Order Detail ',
                'data' => $data,
                'getId' => $getId,
                // 'cekQuot' => $cekQuot,
                // 'cekInvoice' => $cekInvoice,
                // 'cekPo' => $cekPo,
                // 'cekBeritaAcara' => $cekBeritaAcara,
            ]);
    }

    public function createQuotation($id, Request $request)
    {
        $data = RequestOrderDetail::where('id', $id)
        ->with([
            'get_outlet.business_to_business_transaction.company',
            'get_b2b_detail',
            'get_b2b_order_detail.get_type',
            'get_user'
        ])
        ->first();

        $type = B2BMasterType::orderBy('id', 'desc')->get();
        $brand = B2BMasterBrand::orderBy('id', 'desc')->get();

        $status_waranty = GeneralSettingB2b::where('name', 'status_waranty')->first();
        $kerusakan = GeneralSettingB2b::where('name', 'kerusakan')->first();
        $validity = GeneralSettingB2b::where('name', 'validity')->first();
        $payment = GeneralSettingB2b::where('name', 'Payment')->first();
        $minimum_order = GeneralSettingB2b::where('name', 'minimum_order')->first();
        $time_of_delivery = GeneralSettingB2b::where('name', 'time_of_delivery')->first();
        $status_proce = GeneralSettingB2b::where('name', 'status_price')->first();

        return $this
        ->viewAdmin('admin.business_to_business_order_request.createQuotation', [
            'data' => $data,
            'status_waranty' => $status_waranty,
            'kerusakan' => $kerusakan,
            'validity' => $validity,
            'payment' => $payment,
            'minimum_order' => $minimum_order,
            'time_of_delivery' => $time_of_delivery,
            'status_proce' => $status_proce,
            // 'getSchedule' => $getSchedule,
            'brand' => $brand,
            'type' => $type,
            // 'getDataQuot' => $getDataQuot
        ]);
    }

    public function viewQuotation($id)
    {
        $getSparepart = B2BDataTransactions::where('id', $id)->with(['get_part.get_stok'])->first();
        // $data = B2BDataTransactions::with(['get_asc', 'get_labor', 'get_trans'])->where('id', $id)->first();
        $data = B2BDataTransactions::with(['get_asc', 'get_labor', 'get_trans'])->where('id', $id)->first();

        $type = B2BMasterType::orderBy('id', 'desc')->get();
        $brand = B2BMasterBrand::orderBy('id', 'desc')->get();

        $status_waranty = GeneralSettingB2b::where('name', 'status_waranty')->first();
        $kerusakan = GeneralSettingB2b::where('name', 'kerusakan')->first();
        $validity = GeneralSettingB2b::where('name', 'validity')->first();
        $payment = GeneralSettingB2b::where('name', 'Payment')->first();
        $minimum_order = GeneralSettingB2b::where('name', 'minimum_order')->first();
        $time_of_delivery = GeneralSettingB2b::where('name', 'time_of_delivery')->first();
        $status_proce = GeneralSettingB2b::where('name', 'status_price')->first();

        return $this
        ->viewAdmin('admin.business_to_business_order_request.viewQuotation', [
            'data' => $data,
            'status_waranty' => $status_waranty,
            'kerusakan' => $kerusakan,
            'validity' => $validity,
            'payment' => $payment,
            'minimum_order' => $minimum_order,
            'time_of_delivery' => $time_of_delivery,
            'status_proce' => $status_proce,
            'getSparepart' => $getSparepart,
            'brand' => $brand,
            'type' => $type,
            // 'getDataQuot' => $getDataQuot
        ]);
    }

    public function previewQuotation($id)
    {
        $showQuotation = B2BDataTransactions::with(['get_part', 'get_type',  'get_labor'])->find($id);
        return $this
            ->viewAdmin('admin.business_to_business_order_request.previewQuotation', [
                'showQuotation' => $showQuotation,
            ]);
    }

    public function previewInvoice($id)
    {
        $account_no = GeneralSettingB2b::where('name', 'account_no')->first();
        $bank_name = GeneralSettingB2b::where('name', 'bank_name')->first();
        $bank_address = GeneralSettingB2b::where('name', 'bank_address')->first();
        $beneficary = GeneralSettingB2b::where('name', 'beneficary')->first();
        $showInv = B2BDataTransactions::with(['get_part', 'get_type', 'get_labor'])->where('id_transaction', $id)->orderBy('id', 'Desc')->first();
        return $this
            ->viewAdmin('admin.business_to_business_order_request.previewInvoice', [
                'showInv' => $showInv,
                'bank_name' => $bank_name,
                'bank_address' => $bank_address,
                'beneficary' => $beneficary,
                'account_no' => $account_no,
            ]);
    }

    public function file_quotation($id)
    {
        $showQuotation = B2BDataTransactions::with(['get_part', 'get_type',  'get_labor'])->where('id_transaction', $id)->orderBy('id', 'Desc')->first();
        return $this
            ->viewAdmin('admin.business_to_business_order_request.previewQuotation', [
                'showQuotation' => $showQuotation
            ]);
    }

    public function detailOrderReq($id)
    {
        $hitungQuot = B2BDataTransactions::where('id_transaction', $id)->count();
        $ambil_tanggal_quot = B2BDataTransactions::where('id_transaction', $id)->orderBy('id', 'desc')->first();
        $status_waranty = GeneralSettingB2b::where('name', 'status_waranty')->first();
        $kerusakan = GeneralSettingB2b::where('name', 'kerusakan')->first();
        $validity = GeneralSettingB2b::where('name', 'validity')->first();
        $payment = GeneralSettingB2b::where('name', 'Waiting Payment')->first();
        $minimum_order = GeneralSettingB2b::where('name', 'minimum_order')->first();
        $time_of_delivery = GeneralSettingB2b::where('name', 'time_of_delivery')->first();
        $status_proce = GeneralSettingB2b::where('name', 'status_price')->first();
        $cekStatus = B2BAssignTeknisi::where('order_id', $id)->orderBy('id', 'desc')->first();


        $type = B2BMasterType::orderBy('id', 'desc')->get();
        $brand = B2BMasterBrand::orderBy('id', 'desc')->get();
        $data = RequestOrderDetail::where('id', $id)
        ->with([
            'get_outlet.business_to_business_transaction.company',
            'get_b2b_detail',
            'get_b2b_order_detail.get_type',
            'get_user'
        ])
        ->first();
        $teknisiSchedule = B2BAssignTeknisi::with('get_teknisi')->where('order_id', $id)->count();
        $getSchedule = B2BAssignTeknisi::where('status', 'Selesai')->where('order_id', $id)->count();
        $getDataQuot = B2BDataTransactions::where('id_transaction', $id)->count();
        $cekQuot = 0;
        $cekInvoice = 0;
        $cekPo = 0;
        $cekBeritaAcara = 0;
        $cekOrderImage = RequestOrderDetail::find($id);

        // $data->update([
        //     'read_at' => now()
        // ]);

        return $this
            ->viewAdmin('admin.business_to_business_order_request.detailByRequest', [
                'title' => 'Request Order Detail ',
                'brand' => $brand,
                'type' => $type,
                'data' => $data,
                'cekQuot' => $cekQuot,
                'cekInvoice' => $cekInvoice,
                'cekPo' => $cekPo,
                'cekStatus' => $cekStatus,
                'cekOrderImage' => $cekOrderImage,
                'cekBeritaAcara' => $cekBeritaAcara,
                'teknisiSchedule' => $teknisiSchedule,
                'status_waranty' => $status_waranty,
                'kerusakan' => $kerusakan,
                'validity' => $validity,
                'payment' => $payment,
                'minimum_order' => $minimum_order,
                'time_of_delivery' => $time_of_delivery,
                'status_proce' => $status_proce,
                'getSchedule' => $getSchedule,
                'getDataQuot' => $getDataQuot,
                'hitungQuot' => $hitungQuot,
                'ambil_tanggal_quot' => $ambil_tanggal_quot
            ]);
    }

    public function viewPageTeknisi($id)
    {
        $data = RequestOrderDetail::where('id', $id)
        ->with([
            'get_outlet.business_to_business_transaction.company',
            'get_b2b_order_detail',
            'get_user'
        ])
        ->first();
        $getTeknisi = User::where('is_b2b_teknisi', 1)->get();
        return $this
            ->viewAdmin('admin.business_to_business_order_request.assignTeknisi', [
                'getTeknisi' => $getTeknisi,
                'data' => $data,
                'id' => $id,
            ]);
    }

    public function viewPageUpdateScheduleTeknisi($id)
    {
        $getOldSchedule = B2BAssignTeknisi::where('id', $id)->where('is_deleted', 0)->with('get_teknisi')->get();
        $get_id = B2BAssignTeknisi::where('id', $id)->first();
        // return $getOldSchedule;
        $getTeknisi = User::where('is_b2b_teknisi', 1)->get();
        return $this
            ->viewAdmin('admin.business_to_business_order_request.updateSchedule', [
                'getTeknisi' => $getTeknisi,
                'getOldSchedule' => $getOldSchedule,
                'id' => $id,
                'get_id' => $get_id
            ]);
    }

    public function print($id)
    {
        $showQuotation = B2BDataTransactions::with(['get_part', 'get_type'])->find($id);
        $pdf = PDF::loadview('admin.business_to_business_order_request.previewQuotation', compact('showQuotation'));
        return $pdf->stream('quotation.pdf');
    }

    public function workmanshipReportPdf($order_id)
    {
        // // Fetch all customers from database
        // $data = Customer::get();
        // Send data to the view using loadView function of PDF facade
        // $order = Order::where('id', $order_id)->first();
        $showQuotation = B2BDataTransactions::with(['get_part', 'get_type'])->find($id);
        $pdf = PDF::loadView('admin.teknisi.printpdf', compact('showQuotation'));
        // If you want to store the generated pdf to the server then you can use the store function
        // $pdf->save(storage_path().'_filename.pdf');
        // Finally, you can download the file using download function
        return $pdf->stream('workmanship_report.pdf');
    }
}
