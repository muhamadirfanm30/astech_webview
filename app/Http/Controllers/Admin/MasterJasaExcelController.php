<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;

class MasterJasaExcelController extends AdminController
{
    public function index()
    {
        return $this->setData([
            'title' => 'Jasa Excel'
        ])
        ->setBreadcrumb('List Jasa Excel', '/admin/jasa-excel/show')
        ->viewAdmin('admin.master.jasa-excel.index');
    }
}
