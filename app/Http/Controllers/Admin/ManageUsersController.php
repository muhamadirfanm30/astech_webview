<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\User;
use Spatie\Permission\Models\Role;
use App\Model\Master\MsAdditionalInfo;
use DB;
use App\Model\Master\MsAddress;

class ManageUsersController extends AdminController
{
    public function showManageUsers()
    {
        // dd('asd');
        return $this
            ->setBreadcrumb('Users', '/admin/user/show')
            ->viewAdmin('admin.setting.permission.manageusers.index', [
                'title' => 'DATA USER',
            ]);
    }

    public function createManageUsers()
    {
        // $getid  = DB::table('users')->orderBy('id', 'desc')->first();
        // $lastID     = isset($getLastId->id) ? $getLastId->id + 1 : 1;

        $getLastId  = DB::table('users')->where('id', 'desc')->first();


        $id = User::orderBy('id', 'DESC')->first();


        $showRole = Role::get();
        return $this
            ->setBreadcrumb('Users', '/admin/user/show')
            ->setBreadcrumb('Users Create', '#')
            ->viewAdmin('admin.setting.permission.manageusers.create', [
                'showRole' => $showRole,
                // 'lastID' => $lastID,
                'title'    => 'CREATE USER',
            ]);
    }

    public function updateManageUsers($id)
    {
        $updateUsers = User::with(['address', 'address.city', 'address.district', 'address.village', 'address.zipcode', 'address.types'])->find($id);

        $showRole = Role::get();

        return $this
            ->setBreadcrumb('Users', '/admin/user/show')
            ->setBreadcrumb('Users Update', '#')
            ->viewAdmin('admin.setting.permission.manageusers.update', [
                'updateUsers' => $updateUsers,
                'showRole'    => $showRole,
                'title'       => 'EDIT USER',
            ]);
    }

    public function detail($id)
    {
        $user = User::with([
            'info',
            'technician',
            'badge'
        ])->where('id', $id)->firstOrFail();

        return $this
            ->setBreadcrumb('Users', '/admin/user/show')
            ->setBreadcrumb('Users Update', '#')
            ->viewAdmin('admin.setting.permission.manageusers.detail', [
                'user'  => $user,
                'title' => 'DETAIL USER',
            ]);
    }

    public function info($id)
    {
        $user = User::with([
            'info',
            'technician'
        ])->where('id', $id)->firstOrFail();

        return $this
            ->setBreadcrumb('Users', '/admin/user/show')
            ->setBreadcrumb('Users Update', '#')
            ->viewAdmin('admin.setting.permission.manageusers.edit_info', [
                'info'  => $user->info,
                'user'  => $user,
                'title' => 'EDIT INFO USER',
            ]);
    }

    public function addressCreate($id)
    {
        $user = User::with([
            'info'
        ])->where('id', $id)->firstOrFail();

        return $this
            ->setBreadcrumb('Users', '/admin/user/show')
            ->setBreadcrumb('Users Update', '#')
            ->viewAdmin('admin.setting.permission.manageusers.alamat.create', [
                'user'  => $user,
                'title' => 'CREATE ADDRESS USER',
            ]);
    }

    public function addressEdit($id)
    {
        $address = MsAddress::where('id', $id)->firstOrFail();

        $user = $address->user;

        return $this
            ->setBreadcrumb('Users', '/admin/user/show')
            ->setBreadcrumb('Users Update', '#')
            ->viewAdmin('admin.setting.permission.manageusers.alamat.edit', [
                'addres'  => $address,
                'user'  => $user,
                'title' => 'EDIT ADDRESS USER',
            ]);
    }

}
