<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;

class JobTitleCategoryController extends AdminController
{
    public function index()
    {
        return $this
            ->setBreadcrumb('Job Title Category', '/product')
            ->viewAdmin('admin.teknisi.job_title_category.index', [
                'title' => 'MASTER JOB TITLE CATEGORY',
            ]);
    }
}
