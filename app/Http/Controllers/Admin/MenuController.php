<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Traits\MenuWebService;

class MenuController extends Controller
{
    use MenuWebService;
}
