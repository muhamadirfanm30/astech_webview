<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use App\Model\Master\MsServicesType;

class MasterServiceTypeController extends AdminController
{
    public function showListServiceType()
    {
        return $this
            ->setBreadcrumb('list Service Types', '/admin/service-type/show')
            ->viewAdmin('admin.master.service-type.index', [
                'title' => 'List Service Type',
            ]);
    }
}
