<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use App\User;
use App\Helpers\ImageUpload;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Intervention\Image\ImageManagerStatic as Image;
use App\Mail\B2bSuccessRegisterTeknisi;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;
use DB;

class B2BMasterTeknisiController extends AdminController
{
    public function index()
    {
        return $this
            ->viewAdmin('admin.business_to_business_master_teknisi.index', [
                'title' => 'BUSSINES TO BUSSINES LIST TEKNISI',
            ]);
    }
    
    public function delete($id)
    {
        $user   = User::where('id', $id)->firstOrFail();

        DB::beginTransaction();
        try {
            $user->delete();
            // commit db
            DB::commit();
        } catch (\Exception $e) {
            // rollback db
            DB::rollback();
            throw new SendErrorMsgException($e->getMessage());
        }
    }

    public function create()
    {
        return $this
            ->viewAdmin('admin.business_to_business_master_teknisi.create', [
                'title' => 'BUSSINES TO BUSSINES CREATE TEKNISI',
            ]);
    }

    public function datatables()
    {
        $teknisi = User::whereNotNull('is_b2b_teknisi')->orderBy('id', 'desc');

        return DataTables::of($teknisi)
            ->addIndexColumn()
            ->toJson();
    }

    public function update($id)
    {
        $dataTeknisi = User::where('id', $id)->first();
        return $this
            ->viewAdmin('admin.business_to_business_master_teknisi.update', [
                'title' => 'BUSSINES TO BUSSINES UPDATE TEKNISI',
                'dataTeknisi' => $dataTeknisi
            ]);
    }

    public function store(Request $request)
    {
        $path = User::getImagePathUpload();
        $file_name = null;

        if ($request->image_teknisi != null) {
            $image_teknisi = (new ImageUpload)->upload($request->image_teknisi, $path);
            $file_name = $image_teknisi->getFilename();
        }else{
            $file_name = '';
        }

        $password = Hash::make($request->password);

        $storeTeknisi = User::create([
            'name' => $request->name,
            'image_teknisi' => $file_name,
            'email_verified_at' => date('Y-m-d H:i:s'),
            'email' => $request->email,
            'phone' => $request->phone,
            'password' => $password,
            'address' => $request->address,
            'api_token' => Str::random(80),
            'is_b2b_users' => 1,
            'is_b2b_teknisi' => 1,
        ]);

        if(!empty($request->email)){
            Mail::to($request->email)->send(new B2bSuccessRegisterTeknisi($storeTeknisi->id,$request->password));
            $response = [
                'status'=>true,
                'status_code'=>200,
                'message'=>'success',
                'email'=>'Sent Email register Email'
            ];
        }else{
            $response = [
                'status'=>true,
                'status_code'=>400,
                'message'=>'error',
                'email'=>'Failed Sent Email : register Email'
            ];
        }

        return redirect()->route('teknisi.show')->with('success', 'Berhasil.!');
    }

    public function edit(Request $request, $id)
    {
        $cek = User::where('id', $id)->first();

        if(!empty($request->image_teknisi)){
            if ($request->hasFile('image_teknisi')) {
                $image      = $request->file('image_teknisi');
                $filename   = time().'1'. '.' . $image->getClientOriginalExtension();
                $img = Image::make($image);
                $img->stream(); // <-- Key point
                Storage::disk('local')->put('public/user/profile/avatar/' . $filename, $img);
    
                $exists1 = Storage::disk('local')->has('public/user/profile/avatar/' . $cek->image_teknisi);
                if ($exists1) {
                    Storage::delete('public/user/profile/avatar/' . $cek->image_teknisi);
                }
            }
        }else{
            $filename = $cek->image_teknisi;
        }

        $password = Hash::make($request->password);

        $storeTeknisi = User::find($id)->update([
            'name' => $request->name,
            'image_teknisi' => $filename,
            'email' => $request->email,
            'phone' => $request->phone,
            'address' => $request->address,
            'api_token' => Str::random(80),
            'is_b2b_users' => 1,
            'is_b2b_teknisi' => 1,
        ]);

        return redirect()->route('teknisi.show')->with('success', 'Berhasil.!');
    }

    public function destroy($id)
    {
        $cek = User::where('id', $id)->first();

        if(!empty($request->image_teknisi)){
            if ($request->hasFile('image_teknisi')) {
                $image      = $request->file('image_teknisi');
                $filename   = time().'1'. '.' . $image->getClientOriginalExtension();
                $img = Image::make($image);
                $img->stream(); // <-- Key point
                Storage::disk('local')->put('public/user/profile/avatar/' . $filename, $img);
    
                $exists1 = Storage::disk('local')->has('public/user/profile/avatar/' . $cek->image_teknisi);
                if ($exists1) {
                    Storage::delete('public/user/profile/avatar/' . $cek->image_teknisi);
                }
            }
        }else{
            $filename = $cek->image_teknisi;
        }
        $storeTeknisi = User::find($id)->delete();

        return redirect()->route('teknisi.show')->with('success', 'Berhasil.!');
    }
}
