<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use App\Model\Master\Vendor;

class MasterVendorController extends AdminController
{
    public function showListVendor()
    {
        return $this
        ->setBreadcrumb('List Vendor', '/admin/vendor/show')
        ->viewAdmin('admin.master.product.vendors.index', [
            'title' => 'Master Vendor'
        ]);
    }

    public function createVendor()
    {
        return $this
        ->setBreadcrumb('List Vendor', '/admin/vendor/show')
        ->setBreadcrumb('Create Vendor', '#')
        ->viewAdmin('admin.master.product.vendors.create', [
            'title' => 'Create Vendor'
        ]);
    }

    public function updateVendor($id)
    {
        $editVendor = Vendor::with('user')->where('id', $id)->first();
        
        return $this
        ->setBreadcrumb('List Vendor', '/admin/vendor/show')
        ->setBreadcrumb('Update Vendor', '#')
        ->viewAdmin('admin.master.product.vendors.update', [
            'title' => 'Edit Vendor',
            'editVendor' => $editVendor,
        ]);
    }
}
