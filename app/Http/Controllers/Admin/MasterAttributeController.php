<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use App\Model\Product\ProductAttribute;

class MasterAttributeController extends AdminController
{
    public function showListAttribute()
    {
        return $this
            ->setBreadcrumb('List Attribute', '/admin/product-attribute/show')
            ->viewAdmin('admin.master.product.attribute.index', [
                'title' => 'Master Attribute'
            ]);
    }

    public function createMasterAttribute()
    {
        return $this
            ->setBreadcrumb('List Additionals', '/admin/product-attribute/show')
            ->setBreadcrumb('Create Additionals', '#')
            ->viewAdmin('admin.master.product.attribute.create', [
                'title' => 'Create Attribute'
            ]);
    }

    public function childMasterAttribute($id)
    {
        $viewAttribut = ProductAttribute::where('id', $id)->first();
        return $this->viewAdmin('admin.master.product.attribute.childAttributeCreate', compact('viewAttribut'));
    }

    public function updateMasterAttribute($id)
    {
        $attribute = ProductAttribute::with('childs')
            ->where('id', $id)
            ->firstOrFail();

        return $this
            ->setBreadcrumb('List Additionals', '/admin/product-attribute/show')
            ->setBreadcrumb('Update Additionals', '#')
            ->viewAdmin('admin.master.product.attribute.update', [
                'attribute' => $attribute,
                'title' => 'Edit Attribute'
            ]);
    }
}
