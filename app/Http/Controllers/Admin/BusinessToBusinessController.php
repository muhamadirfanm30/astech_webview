<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Model\BusinessToBusinessOutletDetailTransaction;
use App\Model\BusinessToBusinessOutletTransaction;
use App\Model\B2BMasterType;
use App\Model\B2BMasterBrand;
use App\Model\ImageBeritaAcara;
use App\Model\ImageInvoice;
use App\Model\ImagePurchaseOrder;
use App\Model\ImageQuotation;
use App\Model\BusinessToBusinessTransaction;
use Illuminate\Http\Request;

class BusinessToBusinessController extends AdminController
{
    public function index()
    {
        return $this
            ->viewAdmin('admin.business_to_business.index', [
                'title' => 'BUSSINES TO BUSSINES',
            ]);
    }

    public function log()
    {
        return $this
            ->viewAdmin('admin.business_to_business.log', [
                'title' => 'LOG BUSSINES TO BUSSINES',
            ]);
    }

    public function show($id)
    {
        $btb = BusinessToBusinessTransaction::with(['company'])->where('id', $id)->firstOrFail();
        $type = B2BMasterType::get();
        $brand = B2BMasterBrand::get();
        $statuses = BusinessToBusinessOutletDetailTransaction::whereNotNull('status_quotation')->groupBy('status_quotation')->get();
        return $this
            ->viewAdmin('admin.business_to_business.show', [
                'title' => 'DETAIL BUSSINES TO BUSSINESsssss',
                'btb' => $btb,
                'statuses' => $statuses,
                'type' => $type,
                'brand' => $brand,
            ]);
    }

    public function detail($id)
    {
        $outlet = BusinessToBusinessOutletTransaction::with(['business_to_business_transaction.company'])->where('id', $id)->firstOrFail();
        $type = B2BMasterType::get();
        $brand = B2BMasterBrand::get();
        return $this
            ->viewAdmin('admin.business_to_business.detail', [
                'title' => 'DETAIL BUSSINES TO BUSSINES',
                'outlet' => $outlet,
                'type' => $type,
                'brand' => $brand,
            ]);
    }
    
    public function detailOutlet($id)
    {
        $outletDetail = BusinessToBusinessOutletDetailTransaction::find($id);
        $cekQuot = ImageQuotation::count();
        $cekInvoice = ImageInvoice::count();
        $cekPo = ImagePurchaseOrder::count();
        $cekBeritaAcara = ImageBeritaAcara::count();
        $outlet = BusinessToBusinessOutletTransaction::with(['business_to_business_transaction.company'])->where('id', $outletDetail->business_to_business_outlet_transaction_id)->firstOrFail();
        // return $outletDetail->merek;
        return $this
            ->viewAdmin('admin.business_to_business.detailOutlet', [
                'title' => 'DETAIL BUSSINES TO BUSSINES',
                'outletDetail' => $outletDetail,
                'outlet' => $outlet,
                'cekQuot' => $cekQuot,
                'cekInvoice' => $cekInvoice,
                'cekPo' => $cekPo,
                'cekBeritaAcara' => $cekBeritaAcara,
            ]);
    }

    public function mediaDownload($uniq_key, Request $request)
    {
        $detailOutlet = BusinessToBusinessOutletDetailTransaction::where('uniq_key', $uniq_key)->firstOrFail();
        $type = $request->type;
        $file = null;
        $fileType = null;

        if ($type === 'file_berita_acara') {
            $file = storage_path('app/public/btb/media/' . $detailOutlet->uniq_key . '/' . $detailOutlet->file_berita_acara);
        }

        if ($type === 'file_quot') {
            $file = storage_path('app/public/btb/media/' . $detailOutlet->uniq_key . '/' . $detailOutlet->file_quot);
        }

        if ($type === 'file_po') {
            $file = storage_path('app/public/btb/media/' . $detailOutlet->uniq_key . '/' . $detailOutlet->file_po);
        }

        if ($type === 'file_invoice') {
            $file = storage_path('app/public/btb/media/' . $detailOutlet->uniq_key . '/' . $detailOutlet->file_invoice);
        }

        if ($file === null) {
            return '';
        }

        if ($file !== null) {
            $ext = pathinfo($file);
            $fileType = $ext['extension'];
            $canPreview = ['pdf', 'png', 'jpg', 'jpeg'];

            if (in_array($fileType, $canPreview)) {
                return response()->file($file);
            }

            return '';
        }

        return response()->download($file);
    }
}
