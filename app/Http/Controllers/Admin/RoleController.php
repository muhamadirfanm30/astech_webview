<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use App\Traits\RoleWebService;

class RoleController extends AdminController
{
    use RoleWebService;

    public function create()
    {
        $permissions = Permission::orderBy('name', 'ASC')->get()->groupBy('groups');
        return $this
            ->setBreadcrumb('Roles', '/admin/role/show')
            ->setBreadcrumb('Roles Create', '#')
            ->viewAdmin('admin.setting.role.create', [
                'title' => 'Create Roles',
                'permissions' => $permissions,
                'no' => 1,
            ]);
    }

    public function edit($id)
    {
        $permissions = Permission::all()->groupBy('groups');

        $role = Role::with('permissions')->findOrFail($id);
        $perm = $role->permissions;

        return $this
            ->setBreadcrumb('Roles', '/admin/role/show')
            ->setBreadcrumb('Roles Update', '#')
            ->viewAdmin('admin.setting.role.edit', [
                'title' => 'Edit Roles',
                'permissions' => $permissions,
                'no' => 1,
                'role' => $role,
                'perm' => $perm
            ]);
    }
}
