<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use App\Model\Master\MsPriceItems;

class MasterPriceItemsController extends AdminController
{
    public function showListPriceItem()
    {
        return $this
        ->setBreadcrumb('list Price Items', '/admin/price-item/show')
        ->viewAdmin('admin.master.priceItems.index', [
            'title' => 'List Price Items',
        ]);
    }

    public function updatePriceItems(Request $request, $id)
    {
        $viewPriceItems = MsPriceItems::where('id',$id)->with(['unit', 'city', 'district', 'village', 'product'])->find($id);
        // return $viewPriceItems;
        return $this
        ->setBreadcrumb('List Price Items', '/admin/price-item/show')
        ->setBreadcrumb('Update Price Items', '#')
        ->viewAdmin('admin.master.priceItems.update', compact('viewPriceItems'));
    }
}
