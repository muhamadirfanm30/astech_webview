<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;

class MasterASCExcelController extends AdminController
{
    public function index()
    {
        return $this->setData([
            'title' => 'Master ASC Excel'
        ])
        ->setBreadcrumb('List Master ASC Excel', '/admin/asc-excel/show')
        ->viewAdmin('admin.master.asc-excel.index');
    }
}
