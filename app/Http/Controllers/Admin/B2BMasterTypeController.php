<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use App\Model\B2BMasterType;
use Yajra\DataTables\DataTables;

class B2BMasterTypeController extends AdminController
{
    public function index()
    {
        return $this
            ->viewAdmin('admin.business_to_business_master_type.index', [
                'title' => 'BUSSINES TO BUSSINES LIST TYPE',
            ]);
    }

    public function datatables()
    {
        $brand = B2BMasterType::orderBy('id', 'desc');

        return DataTables::of($brand)
            ->addIndexColumn()
            ->toJson();
    }

    public function store(Request $request)
    {
        $store = B2BMasterType::create([
            'name' => $request->name,
            'desc' => $request->desc,
        ]);

        return $this->successResponse($store,'success',201);
    }

    public function update(Request $request, $id)
    {
        $update = B2BMasterType::where('id', $id)->update([
            'name' => $request->name,
            'desc' => $request->desc,
        ]);

        return $this->successResponse($update,'success',201);
    }

    public function destroy($id)
    {
       $delete = B2BMasterType::where('id', $id)->delete();
       return $this->successResponse($delete,'success',201);
    }
}
