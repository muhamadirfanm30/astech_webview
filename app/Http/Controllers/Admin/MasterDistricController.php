<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use App\Model\Master\MsDistrict;

class MasterDistricController extends AdminController
{
    public function showDistric()
    {
        return $this
        ->setBreadcrumb('List District', '/admin/district/show')
        ->viewAdmin('admin.master.location.distric.index', [
            'title' => 'MASTER DISTRICT'
        ]);
    }

    public function createDistric()
    {
        return $this
        ->setBreadcrumb('List District', '/admin/district/show')
        ->setBreadcrumb('create District', '/admin/district/create')
        ->viewAdmin('admin.master.location.distric.create');
    }

    public function updateDistric($id)
    {
        $editDistric = MsDistrict::with('city.province.country','city.province','city')->where('id', $id)->first();
        // return $editDistric;
        // return $editDistric;
        return $this
        ->setBreadcrumb('List District', '/admin/district/show')
        ->setBreadcrumb('Update District', '/admin/district/update')
        ->viewAdmin('admin.master.location.distric.update', compact('editDistric'));
    }
}
