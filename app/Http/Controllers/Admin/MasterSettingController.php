<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use App\Model\Master\MsServicesType;
use App\Model\Master\GeneralSetting;

class MasterSettingController extends AdminController
{
    public function showFormCommissionSetting()
    {
        $getDataSetting = GeneralSetting::whereIn('name', [
            'commission_type_rate',
            'pinalty_before_approve',
            'pinalty_after_approve_under_24_hours',
            'penalty_above_24_hours',
            'timer_order_setting',
            'commission_penalty_setting',
            'auto_cancel_and_jobs_completed',
            'garansi_due_date'
        ])->get();
        return $this
            ->setBreadcrumb('General Settings', '#')
            ->viewAdmin('admin.master.general-setting.orderSetting.general_setting', [
                'title' => 'GENERAL SETTING',
                'getDataSetting' => $getDataSetting
            ]);
    }

    public function socialMediaSetting()
    {
        $getDataSetting = GeneralSetting::whereIn('name', [
            'facebook',
            'youtube',
            'twiter',
            'instagram',
            'email',
            'contact_us',
            'addreses',
        ])->get();
        return $this
            ->setBreadcrumb('General Settings', '#')
            ->viewAdmin('admin.master.general-setting.socialMediaSetting.create', [
                'title' => 'Social Media Setting',
                'getDataSetting' => $getDataSetting
            ]);
    }

    public function firebaseSettings()
    {
        $settings = GeneralSetting::whereIn('name', [
            'firebase_apiKey',
            'firebase_authDomain',
            'firebase_databaseURL',
            'firebase_projectId',
            'firebase_storageBucket',
            'fireabse_messagingSenderId',
            'fireabse_appId',
            'firebase_serverKey'
        ])->get();

        return $this
            ->setBreadcrumb('General Settings', '#')
            ->viewAdmin('admin.master.general-setting.firebase.create', [
                'title' => 'Firebase Setting',
                'settings' => $settings
            ]);
    }

    public function socialLogin()
    {
        $getDataSetting = GeneralSetting::whereIn('name', [
            'client_key_facebook',
            'secret_key_facebook',
            'callback_url_facebook',
            'client_key_google',
            'secret_key_google',
            'callback_url_google',
        ])->get();
        return $this
            ->setBreadcrumb('General Settings', '#')
            ->viewAdmin('admin.master.general-setting.socialLoginSetting.create', [
                'title' => 'Social Login Setting',
                'getDataSetting' => $getDataSetting
            ]);
    }

    public function googleMapsKeySettings()
    {

        $val = GeneralSetting::where('id', 25)->first();
        return $this
            ->viewAdmin('admin.master.general-setting.googleMapsKeySettings.index', [
                'title' => 'Google Maps Key Setting',
                'data' => $val
            ]);
    }

    public function socialMediaLogin()
    {
        $settings = GeneralSetting::whereIn('name', [
            'social_media_login_fb',
            'social_media_login_google',
        ])->get();

        return $this
            ->setBreadcrumb('General Settings', '#')
            ->viewAdmin('admin.master.general-setting.social_login.create', [
                'title' => 'Social Login Setting',
                'settings' => $settings
            ]);
    }
}
