<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Model\Master\BankTransfer;
use App\Model\Master\EmailStatusOrder;
use App\Http\Controllers\AdminController;

class MasterEmailStatusOrder extends AdminController
{
    public function index()
    {
        return $this->setData([
            'title' => 'Master Email Status Order'
        ])
        ->setBreadcrumb('List Template', '/admin/email-status-order/show')
        ->viewAdmin('admin.master.email_status_order.index');
    }


    public function create()
    {
        return $this->setData([
            'title' => 'Create Template Email Status Order'
        ])
        ->setBreadcrumb('List Template', '/admin/email-status-order/create')
        ->viewAdmin('admin.master.email_status_order.create');
    }

    public function update($id)
    {
        $getData = EmailStatusOrder::where('orders_statuses_id', $id)->get();
        $id_email = $getData->pluck('id');
        return $this->setData([
            'title' => 'Update Email Status Order',
            'getData' => $getData,
            'id_email' => $id_email,
        ])
        ->setBreadcrumb('List Template', '/admin/email-status-order/edit')
        ->viewAdmin('admin.master.email_status_order.update');
    }
}
