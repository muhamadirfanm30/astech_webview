<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;

class MasterTypeJobExcelController extends AdminController
{
    public function index()
    {
        return $this->setData([
            'title' => 'Master Type Job Excel'
        ])
        ->setBreadcrumb('List Master Type Job Excel', '/admin/type-job-excel/show')
        ->viewAdmin('admin.master.type-job-excel.index');
    }
}
