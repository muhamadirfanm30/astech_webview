<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;

class ProductGroupController extends AdminController
{
    public function index()
    {
        return $this
            ->setBreadcrumb('list Product Group', '/admin/grades')
            ->viewAdmin('admin.master.product_group.index', [
                'title' => 'List Product Group',
            ]);
    }
}
