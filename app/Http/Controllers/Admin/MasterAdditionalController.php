<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use App\Model\Product\ProductAdditional;

class MasterAdditionalController extends AdminController
{
    public function showAddtitonal()
    {
        return $this
            ->setBreadcrumb('List Additionals', '/admin/product-additional/show')
            ->viewAdmin('admin.master.product.additional.index', [
                'title' => 'Master Product Additional'
            ]);
    }

    public function createAddtitonal()
    {
        return $this
            ->setBreadcrumb('List Additionals', '/admin/product-additional/show')
            ->setBreadcrumb('Create Additionals', '#')
            ->viewAdmin('admin.master.product.additional.create', [
                'title' => 'Create Product Additional'
            ]);
    }

    public function updateAddtitonal($id)
    {
        $updateAdditional = ProductAdditional::where('id', $id)->first();
        return $this
            ->setBreadcrumb('List Additionals', '/admin/product-additional/show')
            ->setBreadcrumb('Update Additionals', '#')
            ->viewAdmin('admin.master.product.additional.update', compact('updateAdditional'));
    }
}
