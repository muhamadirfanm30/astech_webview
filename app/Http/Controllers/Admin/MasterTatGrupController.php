<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;

class MasterTatGrupController extends AdminController
{
    public function index()
    {
        return $this
            ->setBreadcrumb('list TAT Grup', '/admin/tat_grup')
            ->viewAdmin('admin.master.tat_grup.index', [
                'title' => 'List TAT Grup',
            ]);
    }
}
