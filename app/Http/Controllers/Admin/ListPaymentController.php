<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;

class ListPaymentController extends AdminController
{
    public function index()
    {
        return $this
            // ->setBreadcrumb('Job Experience', '/product')
            ->viewAdmin('admin.master.listPayment.index', [
                'title' => 'Payment List',
            ]);
    }
}
