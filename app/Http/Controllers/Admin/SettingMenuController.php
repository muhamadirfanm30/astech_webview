<?php

namespace App\Http\Controllers\Admin;

use App\Model\Menu;
use Illuminate\Http\Request;
use App\Services\MenuService;
use Spatie\Permission\Models\Permission;
use App\Http\Controllers\AdminController;

class SettingMenuController extends AdminController
{
    // public function showListMenu()
    // {
    //     return $this->viewAdmin('admin.setting.menusetting.index');
    // }

    // public function createMenus()
    // {
    //     $showPermission = Permission::get();
    //     $showMenu = Menu::whereNull('parent_id')->get();
    //     return $this->viewAdmin('admin.setting.menusetting.create', compact('showPermission', 'showMenu'));
    // }

    // public function updateMenus($id)
    // {
    //     $showPermission = Permission::where('id', $id);
    //     $showMenu = Menu::whereNull('parent_id')->get();
    //     $updateMenus = Menu::findOrFail($id);
    //     // return $updateMenus;
    //     return $this->viewAdmin('admin.setting.menusetting.update', compact('showPermission', 'showMenu', 'updateMenus'));
    // }

    public function showListMenu(Request $request)
    {
        $showData = Menu::orderBy('display_order')->get();
		$menu = new MenuService;
        $menu = $menu->getHTML($showData);

        // return $showData;
        return $this
        ->setBreadcrumb('Menus', '/admin/menu/show')
        ->viewAdmin('admin.setting.menu-management.index2', compact('showData','menu'));
    }
}
