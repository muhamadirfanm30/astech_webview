<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use App\Model\Master\MsSymptom;

class MasterSymptomCodeController extends AdminController
{
    public function showListSymptom()
    {
        return $this
        // ->setBreadcrumb('list Symptoms', '/admin/symptom/show')
        ->viewAdmin('admin.master.symptom_code.index', [
            'title' => 'List Symptoms Code',
        ]);
    }

}
