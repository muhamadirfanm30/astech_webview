<?php

namespace App\Http\Requests\Product;

use App\Traits\HandleFailedValidationApi;
use Illuminate\Foundation\Http\FormRequest;

class ProductPartCategoryRequest extends FormRequest
{
    use HandleFailedValidationApi;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (isset($this->id)) {
            return $this->updateRule();
        }

        return $this->insertRule();
    }

    public function insertRule()
    {
        return [
            'part_category' => ['required', 'string', 'max:50', 'unique:product_part_categories,part_category'],
            'part_code' => ['required', 'string', 'max:50'],
        ];
    }

    public function updateRule()
    {
        return [
            'part_category' => ['required', 'string', 'max:50', 'unique:product_part_categories,part_category,' . $this->id],
            'part_code' => ['required', 'string', 'max:50'],
        ];
    }
}
