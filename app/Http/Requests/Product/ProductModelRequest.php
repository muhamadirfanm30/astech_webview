<?php

namespace App\Http\Requests\Product;

use App\Model\Master\MsProductModel;
use App\Traits\HandleFailedValidationApi;
use Illuminate\Foundation\Http\FormRequest;

class ProductModelRequest extends FormRequest
{
    use HandleFailedValidationApi;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (isset($this->id)) {
            return $this->updateRule();
        }

        return $this->insertRule();
    }

    public function insertRule()
    {
        return [
            'name' => ['required', 'string', 'max:150'],
            'model_code' => ['required', 'string', 'max:50'],
            'ms_product_category_id' => ['required', 'numeric'],
            'product_brands_id' => ['required', 'numeric'],
        ];
    }

    public function updateRule()
    {
        return [
            'name' => ['required', 'string', 'max:150', 'unique:' . (new MsProductModel)->getTable() . ',name,' . $this->id],
            'model_code' => ['required', 'string', 'max:50'],
            'ms_product_category_id' => ['required', 'numeric'],
            'product_brands_id' => ['required', 'numeric'],
        ];
    }
}
