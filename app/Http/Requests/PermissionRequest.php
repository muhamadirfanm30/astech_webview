<?php

namespace App\Http\Requests;

use App\Traits\HandleFailedValidationApi;
use Illuminate\Foundation\Http\FormRequest;

class PermissionRequest extends FormRequest
{
    use HandleFailedValidationApi;
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // update
        if (isset($this->id)) {
            return [
                'name' => ['required', 'string', 'max:50', 'unique:permissions,name,' . $this->id],
            ];
        }
        // insert
        return [
            'name' => ['required', 'string', 'max:50', 'unique:permissions'],
        ];
    }
}
