<?php

namespace App\Http\Requests;

use App\Traits\HandleFailedValidationApi;
use Illuminate\Foundation\Http\FormRequest;

class RevisitGaransiRequest extends FormRequest
{
    use HandleFailedValidationApi;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'technician_id' => ['required'],
            'schedule' => ['required'],
            'garansi_id' => ['required'],
            'hours' => ['required'],
            'estimation_hours' => ['required']
        ];
    }
}
