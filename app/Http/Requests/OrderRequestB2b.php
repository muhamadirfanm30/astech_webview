<?php

namespace App\Http\Requests;

use App\Traits\HandleFailedValidationApi;
use Illuminate\Foundation\Http\FormRequest;

class OrderRequestB2b extends FormRequest
{
    use HandleFailedValidationApi;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'outlet_id' => ['required'],
            'service_type' => ['required'],
            'images' => ['required'],
            'unit_id' => ['required'],
        ];
    }

    // public function messages()
    // {
    //     return [
    //         'ms_outlet_id.required' => 'Outlet is required',
    //         'service_type.required' => 'Service Type is required',
    //         'image.required' => 'Image/Vidio is required',
    //         'ms_transaction_b2b_detail.required' => 'Unit is required',
    //     ];
    // }
}
