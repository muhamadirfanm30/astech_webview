<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Traits\HandleFailedValidationApi;

class UserAddressRequest extends FormRequest
{
    use HandleFailedValidationApi;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone1' => ['required', 'string', 'max:20'],
            'phone2' => ['max:20'],
            'ms_address_type_id' => ['required'],
            // 'ms_village_id' => ['numeric'],
            // 'ms_district_id' => ['numeric'],
            // 'ms_city_id' => ['numeric'],
            // 'ms_zipcode_id' => ['numeric'],
            'lat' => ['required', 'string', 'max:191'],
            'long' => ['required', 'string', 'max:191'],
            'cities' => ['max:191'],
            'province' => ['max:191'],
            'address' => ['required'],
        ];
    }
}
