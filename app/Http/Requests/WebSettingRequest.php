<?php

namespace App\Http\Requests;

use App\Traits\HandleFailedValidationApi;
use Illuminate\Foundation\Http\FormRequest;

class WebSettingRequest extends FormRequest
{
    use HandleFailedValidationApi;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // update
        if (isset($this->id)) {
            return $this->updateRule();
        }
        // insert
        return [
            'app_name' => ['required', 'string', 'max:150'],
            'image_logo' => ['file', 'max:1024', 'image', 'mimetypes:image/jpeg,image/png, image/gif'],
            'image_icon' => ['file', 'max:1024', 'image', 'mimetypes:image/jpeg,image/png, image/gif'],

        ];
    }

    public function updateRule()
    {
        return [
            'app_name' => ['required', 'string', 'max:150'],
            'image_logo' => ['file', 'max:1024', 'image', 'mimetypes:image/jpeg,image/png, image/gif'],
            'image_icon' => ['file', 'max:1024', 'image', 'mimetypes:image/jpeg,image/png, image/gif']
        ];
    }
}
