<?php

namespace App\Http\Requests\Technician;

use App\Traits\HandleFailedValidationApi;
use Illuminate\Foundation\Http\FormRequest;

class JobExperienceRequest extends FormRequest
{
    use HandleFailedValidationApi;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->insertRule();
    }

    public function insertRule()
    {
        return [
            'company_name' => ['required'],
            'position' => ['required'],
            'period_start' => ['required'],
            'period_end' => ['required'],
            'type' => ['required'],
            'user_id' => ['required'],
            'job_title_id' => ['required'],
        ];
    }

    public function updateRule()
    {
        return [];
    }
}
