<?php

namespace App\Http\Requests\Master;

use App\Traits\HandleFailedValidationApi;
use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    use HandleFailedValidationApi;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        if (isset($this->id)) {

            return $this->updateRule();
        }

        return $this->insertRule();
    }

    public function insertRule()
    {
        return [
            'name' => ['required', 'string', 'max:250'],
            'product_description' => ['required'],
            'product_model_id' => ['required'],
            'product_additionals_id' => ['required'],
            'product_category_id' => ['required'],
            'vendors' => ['array']
        ];
    }

    public function updateRule()
    {
        return [
            'name' => ['required', 'string', 'max:250'],
            'product_description' => ['required'],
            'product_model_id' => ['required'],
            'product_additionals_id' => ['required'],
            'product_category_id' => ['required'],
            'vendors' => ['array']
        ];
    }
}
