<?php

namespace App\Http\Requests\Master;

use App\Traits\HandleFailedValidationApi;
use Illuminate\Foundation\Http\FormRequest;

class TechnicianSparepartRequest extends FormRequest
{
    use HandleFailedValidationApi;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->insertRule();
    }

    public function insertRule()
    {
        return [
            'ms_services_id' => ['required', 'string', 'max:30'],
            'name' => ['required', 'string', 'max:255'],
            'price' => ['required', 'numeric'],
        ];
    }

    public function updateRule()
    {
        return [
            'ms_services_id' => ['required', 'string', 'max:30', 'unique:ms_services,' . $this->id],
            'name' => ['required', 'string', 'max:255'],
            'price' => ['required', 'numeric'],
        ];
    }
}
