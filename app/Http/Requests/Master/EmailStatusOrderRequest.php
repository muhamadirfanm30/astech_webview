<?php

namespace App\Http\Requests\Master;

use App\Traits\HandleFailedValidationApi;
use Illuminate\Foundation\Http\FormRequest;

class EmailStatusOrderRequest extends FormRequest
{
    use HandleFailedValidationApi;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'subject0' => ['required', 'string'],
            'content0' => ['required', 'string'],
            'orders_statuses_id' => ['required', 'string'],
            'subject1' => ['required', 'string'],
            'content1' => ['required', 'string'],
        ];
    }
}
