<?php

namespace App\Http\Requests\Master;

use App\Traits\HandleFailedValidationApi;
use Illuminate\Foundation\Http\FormRequest;

class VoucherGeneralRequest extends FormRequest
{
    use HandleFailedValidationApi;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'code'              => ['required', 'string'],
            'type'              => ['required', 'integer'],
            'value'             => ['required'],
            // 'max_nominal'       => ['required'],
            'min_payment'       => ['required'],
            'valid_at'          => ['required', 'date'],
            'valid_until'       => ['required', 'date'],
            // 'is_new_register'   => ['required', 'integer'],
        ];
    }
}
