<?php

namespace App\Http\Requests\Master;

use App\Model\Master\Vendor;
use App\Traits\HandleFailedValidationApi;
use Illuminate\Foundation\Http\FormRequest;

class VendorReuqest extends FormRequest
{
    use HandleFailedValidationApi;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (isset($this->id)) {
            return $this->updateRule();
        }

        return $this->insertRule();
    }

    public function insertRule()
    {
        return [
            'name' => ['required', 'string', 'max:100', 'unique:' . (new Vendor())->getTable() . ',name'],
            'code' => ['required', 'string', 'max:50'],
            'office_email' => ['required', 'email'],
            'is_business_type' => ['required'],
        ];
    }

    public function updateRule()
    {
        return [
            'name' => ['required', 'string', 'max:100', 'unique:' . (new Vendor())->getTable() . ',name,' . $this->id],
            'code' => ['required', 'string', 'max:50'],
            'office_email' => ['required', 'email'],
            'is_business_type' => ['required'],
        ];
    }
}
