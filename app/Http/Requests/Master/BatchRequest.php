<?php

namespace App\Http\Requests\Master;

use App\Traits\HandleFailedValidationApi;
use Illuminate\Foundation\Http\FormRequest;

class BatchRequest extends FormRequest
{
    use HandleFailedValidationApi;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // insert
        return [
            // 'shipping_cost' => ['required', 'numeric'],
            'batch_no' => ['required'],
            'batch_date' => ['required'],
            'vendor_id' => ['required'],
        ];
    }
}
