<?php

namespace App\Http\Requests\Master;

use App\Traits\HandleFailedValidationApi;
use Illuminate\Foundation\Http\FormRequest;

class CurriculumSequenceRequest extends FormRequest
{
    use HandleFailedValidationApi;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->insertRule();
    }

    public function insertRule()
    {
        // update
        if (isset($this->id)) {
            return [
                'sequence_name' => ['required', 'string', 'max:50' . $this->id],
                'dev_program_id' => ['required', 'string', 'max:50' . $this->id],
                'curriculum_id' => ['required', 'string', 'max:50' . $this->id],
            ];
        }
        // insert
        return [
            'sequence_name' => ['required', 'string', 'max:50'],
            'dev_program_id' => ['required', 'string', 'max:50'],
            'curriculum_id' => ['required', 'string', 'max:50'],
        ];
    }
}
