<?php

namespace App\Http\Requests\Master;

use App\Model\Badge;
use App\Traits\HandleFailedValidationApi;
use Illuminate\Foundation\Http\FormRequest;

class BadgeRequest extends FormRequest
{
    use HandleFailedValidationApi;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (isset($this->id)) {
            return $this->updateRule();
        }

        return $this->insertRule();
    }

    public function insertRule()
    {
        // insert
        return [
            'name' => ['required', 'string', 'max:30'],
        ];
    }

    public function updateRule()
    {
        return [
            'name' => ['required', 'string', 'max:30' . $this->id],
        ];
    }
}
