<?php

namespace App\Http\Requests\Master;

use App\Model\Master\MsZipcode;
use App\Traits\HandleFailedValidationApi;
use Illuminate\Foundation\Http\FormRequest;

class ZipcodeRequest extends FormRequest
{
    use HandleFailedValidationApi;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (isset($this->id)) {
            return $this->updateRule();
        }

        return $this->insertRule();
    }

    public function insertRule()
    {
        return [
            'zip_no' => ['required', 'string', 'max:100', 'unique:' . (new MsZipcode())->getTable() . ',zip_no'],
            'ms_district_id' => ['required']
        ];
    }

    public function updateRule()
    {
        return [
            'zip_no' => ['required', 'string', 'max:100', 'unique:' . (new MsZipcode())->getTable() . ',zip_no,' . $this->id],
            'ms_district_id' => ['required']
        ];
    }
}
