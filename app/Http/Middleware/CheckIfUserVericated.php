<?php

namespace App\Http\Middleware;

use App\Services\UserService;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckIfUserVericated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $isVerify = (new UserService())->setUser(Auth::user())->isVerify();
        if ($isVerify == false) {
            return $this->handleResponse($request);
        }
        return $next($request);
    }

    public function handleResponse($request)
    {
        if ($request->ajax()) {
            return response()->json([
                'msg' => 'user belum verifikasi',
                'data' => null,
                'status_code' => 401,
                'success' => false
            ], 401);
        } else {
            return redirect()->back()->with('success', 'asdasdasda');
        }
    }
}
