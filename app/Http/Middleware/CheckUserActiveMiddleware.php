<?php

namespace App\Http\Middleware;

use App\Services\UserService;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckUserActiveMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $auth = Auth::user();
        if ($auth->status == 0) {
            return $this->handleResponse($request);
        }

        $isVerify = (new UserService())->setUser(Auth::user())->isVerify();
        if ($isVerify == false) {
            return $this->emailVerifyhandleResponse($request);
        }

        return $next($request);
    }

    public function handleResponse($request)
    {
        if ($request->ajax()) {
            return response()->json([
                'msg' => 'We are sorry, This account has not been activated yet. Please contact administrator for more info',
                'data' => null,
                'status_code' => 401,
                'success' => false
            ], 401);
        } else {
            return response()->view('admin.template._user_not_active');
        }
    }

    public function emailVerifyhandleResponse($request)
    {
        if ($request->ajax()) {
            return response()->json([
                'msg' => 'your account has not been verified',
                'data' => null,
                'status_code' => 401,
                'success' => false
            ], 401);
        }
    }
}
