<?php

namespace App\Listeners;

use App\Events\TeknisiApprovalEvent;
use App\Notifications\ApprovalSuccessNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;


class SendSuccessApproval
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TeknisiApprovalEvent  $event
     * @return void
     */
    public function handle(TeknisiApprovalEvent $event)
    {
        $user = $event->user;

        $user->notify(new ApprovalSuccessNotification($user));
    }
}
