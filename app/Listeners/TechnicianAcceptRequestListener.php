<?php

namespace App\Listeners;

use App\Events\TechnicianAcceptRequest;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Notifications\TechnicianAcceptRequestNotification;

class TechnicianAcceptRequestListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TechnicianAcceptRequest  $event
     * @return void
     */
    public function handle(TechnicianAcceptRequest $event)
    {
        $order = $event->order;
        $order->user->notify(new TechnicianAcceptRequestNotification($order));
    }
}
