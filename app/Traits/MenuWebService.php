<?php

namespace App\Traits;

use App\Http\Requests\MenuRequest;
use App\Model\Menu;
use App\Services\MenuService;

trait MenuWebService
{
    public function store(MenuRequest $request, MenuService $menuService)
    {
        // validasi
        $request->validated();

        // store
        $menu = $menuService->create($request->all());

        // redirect
        return $this->redirectTo($menu);
    }

    public function update(MenuRequest $request, Menu $menu, MenuService $menuService)
    {
        // validasi
        $request->validated();

        // redirect
        return $this->redirectTo(
            $menuService->setMenu($menu)->update($request->all())
        );
    }

    public function destroy(Menu $menu, MenuService $menuService)
    {
        // redirect
        return $this->redirectTo(
            $menuService->setMenu($menu)->delete()
        );
    }

    private function redirectTo($condition)
    {
        return $this->getRedirection($condition, [
            'route' => route('index.menu')
        ]);
    }
}
