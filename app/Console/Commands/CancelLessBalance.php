<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use App\Models\Master\Order;
use Illuminate\Http\Request;
use App\Models\Master\Ewallet;
use App\Services\OrderService;
use Illuminate\Console\Command;
use App\Models\Master\HistoryOrder;
use Illuminate\Support\Facades\Log;
use App\Models\Master\EwalletHistory;
use App\Models\Master\GeneralSetting;
use App\Models\Master\TechnicianSaldo;
use App\Models\Master\TechnicianSchedule;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\CustomerController;

class CancelLessBalance extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cancelLessBalance:do';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Aotumatic Cancel Less Balance if they no topUp Balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $settings = GeneralSetting::all();
        if($settings[9]->value !== "0") {

            $orders = Order::with([
                'order_status',
                'history_order'
            ])->where( function ($q) {
                $q->where('orders_statuses_id', 2)
                ->orWhere('orders_statuses_id', 3);
            })->whereHas('history_order', function ($q2) use ($settings) {
                $q2->where( function($q3) {
                    $q3->where('orders_statuses_id', 2)
                    ->orWhere('orders_statuses_id', 3);
                })->where('created_at', '<', Carbon::now()->subHours(intval($settings[9]->value)));
            })->get();

            foreach($orders as $key => $order) {
                try {
                    $orderService = new OrderService();
                    $order = $orderService->setOrder($order->id)
                    ->setChangeBySystem()
                    ->notSendNotif()
                    ->changeToCancel();
                    Log::info('autocancel');
                } catch (QueryException $e) {
                    Log::error($e->getMessage());
                }
            }

        }

    }
}
