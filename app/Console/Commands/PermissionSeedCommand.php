<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;

class PermissionSeedCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'permission:seed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'jalankan permission seeder';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->dropTable();

        Artisan::call('migrate');
        Artisan::call('db:seed --class=PermissionSeed');
        Artisan::call('passport:install --force');
        Artisan::call('db:seed --class=CommonSeeder');
        Artisan::call('db:seed --class=AddressSeeder');
    }

    public function dropTable()
    {
        $tables = DB::select('SHOW TABLES');

        $colname = 'Tables_in_' . env("DB_DATABASE", 'laravel_jasa');

        $droplist = [];
        foreach ($tables as $table) {
            $droplist[] = $table->$colname;
        }
        if (count($droplist) == 0) {
            return;
        }
        $droplist = implode(',', $droplist);

        DB::beginTransaction();
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        DB::statement("DROP TABLE $droplist");
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
        DB::commit();
    }
}
