<?php

namespace App\Console\Commands;

use App\Models\Master\GeneralSetting;
use App\Models\Master\EwalletHistory;
use Illuminate\Support\Facades\Log;
use Illuminate\Console\Command;
use Carbon\Carbon;

class autoCancelTopupWallet extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'autoCancelTopupWallet:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // Log::info('autocanceltopup');
        $settings = GeneralSetting::all();
        if($settings[9]->value !== "0") {
            $orders = EwalletHistory::where('status', '=', 'unpaid')
                ->where('created_at', '<', Carbon::now()->subHours(intval($settings[9]->value)))
                ->get();

            foreach($orders as $key => $order) {
                Log::info('autocanceltopup');
                $order->update([
                    'status' => 'cancel',
                    'payment_status' => 'cancel',
                ]);

            }
        }
    }
}
